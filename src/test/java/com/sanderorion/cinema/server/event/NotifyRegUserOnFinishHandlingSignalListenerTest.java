package com.sanderorion.cinema.server.event;

import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.event.listener.NotifyRegUserOnFinishHandlingSignalListener;
import com.sanderorion.cinema.server.service.RegUserNotificationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyRegUserOnFinishHandlingSignalListenerTest {

    @MockBean
    private RegUserNotificationService regUserNotificationService;

    @SpyBean
    private NotifyRegUserOnFinishHandlingSignalListener notifyRegUserOnFinishHandlingSignalListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEvent() {
        SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
        event.setSignalToContentManagerId(UUID.randomUUID());
        event.setNewStatus(SignalStatus.FIXED);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyRegUserOnFinishHandlingSignalListener, Mockito.timeout(4500)).onEvent(event);
        Mockito.verify(regUserNotificationService, Mockito.timeout(4500))
                .notifyOnSignalStatusChangedToFinished(event.getSignalToContentManagerId());
    }

    @Test
    public void testOnEventNotFinished() {
        for (SignalStatus signalStatus : SignalStatus.values()) {
            if (signalStatus == SignalStatus.FIXED) {
                continue;
            }

            SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
            event.setSignalToContentManagerId(UUID.randomUUID());
            event.setNewStatus(signalStatus);
            applicationEventPublisher.publishEvent(event);

            Mockito.verify(notifyRegUserOnFinishHandlingSignalListener, Mockito.never()).onEvent(any());
            Mockito.verify(regUserNotificationService, Mockito.never()).notifyOnSignalStatusChangedToFinished(any());
        }
    }

    @Test
    public void testOnEventAsync() throws InterruptedException {
        SignalToContentManagerStatusChangedEvent event = new SignalToContentManagerStatusChangedEvent();
        event.setSignalToContentManagerId(UUID.randomUUID());
        event.setNewStatus(SignalStatus.FIXED);

        List<Integer> check = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            check.add(2);
            latch.countDown();
            return null;
        }).when(regUserNotificationService).notifyOnSignalStatusChangedToFinished(event.getSignalToContentManagerId());

        applicationEventPublisher.publishEvent(event);
        check.add(1);

        latch.await();
        Mockito.verify(notifyRegUserOnFinishHandlingSignalListener).onEvent(event);
        Mockito.verify(regUserNotificationService)
                .notifyOnSignalStatusChangedToFinished(event.getSignalToContentManagerId());
        Assert.assertEquals(Arrays.asList(1, 2), check);
    }
}