package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.LikeArticle;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleCreateDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePatchDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePutDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.LikeArticleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LikeArticleController.class)
public class LikeArticleControllerTest extends BaseControllerTest {

    @MockBean
    private LikeArticleService likeArticleService;

    @Test
    public void testCreateLikeArticleValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();
        LikeArticleCreateDTO likeArticle = new LikeArticleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/like-articles/{articleId}",
                regUser.getId(), article.getId())
                .content(objectMapper.writeValueAsString(likeArticle))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(likeArticleService, Mockito.never())
                .createLikeArticle(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetLikeArticle() throws Exception {
        RegUserReadDTO readRegUser = createRegUserRead();
        ArticleReadDTO readArticle = createArticleRead();
        LikeArticleReadDTO read = createLikeArticleRead(readRegUser, readArticle);

        Mockito.when(likeArticleService.getLikeArticle(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{id}/like-articles/{articleId}", readRegUser.getId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeArticleReadDTO actualLikeArticle = objectMapper.readValue(resultJson, LikeArticleReadDTO.class);
        Assertions.assertThat(actualLikeArticle).isEqualToComparingFieldByField(read);

        Mockito.verify(likeArticleService).getLikeArticle(read.getId());
    }

    @Test
    public void testGetRegUserLikeArticle() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        RegUserReadDTO readRegUser = createRegUserRead();
        ArticleReadDTO readArticle = createArticleRead();
        LikeArticleReadDTO read = createLikeArticleRead(readRegUser, readArticle);

        List<LikeArticleReadDTO> expectedLike = List.of(read);

        Mockito.when(likeArticleService.getRegUserLikeArticles(regUserId)).thenReturn(expectedLike);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-articles", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<LikeArticleReadDTO> actualLike = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedLike, actualLike);
        Mockito.verify(likeArticleService).getRegUserLikeArticles(regUserId);
    }

    @Test
    public void testGetLikeArticleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID regUserId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(LikeArticle.class, wrongId);
        Mockito.when(likeArticleService.getLikeArticle(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-articles/{id}", regUserId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetLikeArticleWrongFormatId() throws Exception {
        String error = "articleLikeId should be of type java.util.UUID";
        UUID regUserId = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-articles/123", regUserId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateLikeArticle() throws Exception {
        RegUserReadDTO readRegUser = createRegUserRead();
        UUID regUserId = readRegUser.getId();
        ArticleReadDTO readArticle = createArticleRead();
        UUID articleId = readArticle.getId();

        LikeArticleCreateDTO create = new LikeArticleCreateDTO();
        create.setUserLike(true);

        LikeArticleReadDTO read = createLikeArticleRead(readRegUser, readArticle);

        Mockito.when(likeArticleService.createLikeArticle(regUserId, articleId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/like-articles/{aId}", regUserId, articleId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeArticleReadDTO actualLikeArticle = objectMapper.readValue(resultJson, LikeArticleReadDTO.class);
        Assertions.assertThat(actualLikeArticle).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchLikeArticle() throws Exception {
        LikeArticlePatchDTO patchDTO = generateObject(LikeArticlePatchDTO.class);

        RegUserReadDTO readRegUser = createRegUserRead();
        ArticleReadDTO readArticle = createArticleRead();
        LikeArticleReadDTO read = createLikeArticleRead(readRegUser, readArticle);

        Mockito.when(likeArticleService.patchLikeArticle(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/reg-users/{id}/like-articles/{articleId}", readRegUser.getId(), read.getId()
                        .toString())
                        .content(objectMapper.writeValueAsString(patchDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeArticleReadDTO actualLikeArticle = objectMapper.readValue(resultJson, LikeArticleReadDTO.class);
        Assert.assertEquals(read, actualLikeArticle);
    }

    @Test
    public void testUpdateLikeArticle() throws Exception {
        LikeArticlePutDTO putDTO = generateObject(LikeArticlePutDTO.class);

        RegUserReadDTO readRegUser = createRegUserRead();
        ArticleReadDTO readArticle = createArticleRead();
        LikeArticleReadDTO read = createLikeArticleRead(readRegUser, readArticle);

        Mockito.when(likeArticleService.updateLikeArticle(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/reg-users/{id}/like-articles/{articleId}", readRegUser.getId(),
                read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeArticleReadDTO actualLikeArticle = objectMapper.readValue(resultJson, LikeArticleReadDTO.class);
        Assert.assertEquals(read, actualLikeArticle);
    }

    @Test
    public void testDeleteLikeArticle() throws Exception {
        UUID id = UUID.randomUUID();
        UUID articleId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/{id}/like-articles/{articleId}", id, articleId))
                .andExpect(status().isOk());

        Mockito.verify(likeArticleService).deleteLikeArticle(articleId);
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private ArticleReadDTO createArticleRead() {
        return generateObject(ArticleReadDTO.class);
    }

    private LikeArticleReadDTO createLikeArticleRead(RegUserReadDTO user, ArticleReadDTO article) {
        LikeArticleReadDTO read = generateObject(LikeArticleReadDTO.class);
        read.setRegUserId(user.getId());
        read.setArticleId(article.getId());
        return read;
    }
}