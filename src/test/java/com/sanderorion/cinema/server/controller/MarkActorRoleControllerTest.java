package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.MarkActorRole;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.MarkActorRoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(MarkActorRoleController.class)
public class MarkActorRoleControllerTest extends BaseControllerTest {

    @MockBean
    private MarkActorRoleService markActorRoleService;

    @Test
    public void testCreateMarkActorRoleValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        MarkActorRoleCreateDTO markActorRole = new MarkActorRoleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/mark-actor-roles/{actorRoleId}",
                regUser.getId(), actorRole.getId())
                .content(objectMapper.writeValueAsString(markActorRole))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(markActorRoleService, Mockito.never())
                .createMarkActorRole(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetMarkActorRole() throws Exception {
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        MarkActorRoleReadDTO read = createMarkActorRoleRead(regUser, actorRole);

        Mockito.when(markActorRoleService.getMarkActorRole(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/mark-actor-roles/{id}", regUser.getId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MarkActorRoleReadDTO actualMarkActorRole = objectMapper
                .readValue(resultJson, MarkActorRoleReadDTO.class);
        Assertions.assertThat(actualMarkActorRole).isEqualToComparingFieldByField(read);

        Mockito.verify(markActorRoleService).getMarkActorRole(read.getId());
    }

    @Test
    public void testGetUserMarkActorRoles() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        MarkActorRoleReadDTO read = createMarkActorRoleRead(regUser, actorRole);

        List<MarkActorRoleReadDTO> expectedMark = List.of(read);

        Mockito.when(markActorRoleService.getUserMarkActorRoles(regUserId)).thenReturn(expectedMark);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/mark-actor-roles", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MarkActorRoleReadDTO> actualMark = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedMark, actualMark);
        Mockito.verify(markActorRoleService).getUserMarkActorRoles(regUserId);
    }

    @Test
    public void testGetMarkActorRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MarkActorRole.class, wrongId);
        Mockito.when(markActorRoleService.getMarkActorRole(wrongId)).thenThrow(exception);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/mark-actor-roles/{id}", id, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMarkActorRoleWrongFormatId() throws Exception {
        String error = "actorRoleId should be of type java.util.UUID";
        UUID id = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/mark-actor-roles/123", id))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateMarkActorRole() throws Exception {
        MarkActorRoleCreateDTO create = new MarkActorRoleCreateDTO();
        create.setMark(5);

        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        MarkActorRoleReadDTO read = createMarkActorRoleRead(regUser, actorRole);

        Mockito.when(markActorRoleService
                .createMarkActorRole(regUser.getId(), actorRole.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/mark-actor-roles/{actorRoleId}",
                regUser.getId(), actorRole.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MarkActorRoleReadDTO actualMarkActorRole = objectMapper
                .readValue(resultJson, MarkActorRoleReadDTO.class);
        Assertions.assertThat(actualMarkActorRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteMarkActorRole() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/mark-actor-roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(markActorRoleService).deleteMarkActorRole(id);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private ActorRoleReadDTO createActorRoleRead(MovieReadDTO movie) {
        ActorRoleReadDTO read = generateObject(ActorRoleReadDTO.class);
        read.setMovieId(movie.getId());
        return read;
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private MarkActorRoleReadDTO createMarkActorRoleRead(RegUserReadDTO regUser, ActorRoleReadDTO actorRole) {
        MarkActorRoleReadDTO read = generateObject(MarkActorRoleReadDTO.class);
        read.setRegUserId(regUser.getId());
        read.setActorRoleId(actorRole.getId());
        return read;
    }
}
