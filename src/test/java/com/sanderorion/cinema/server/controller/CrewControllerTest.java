package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.Crew;
import com.sanderorion.cinema.server.domain.enums.Profession;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPatchDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.CrewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(CrewController.class)
public class CrewControllerTest extends BaseControllerTest {

    @MockBean
    private CrewService crewService;

    @Test
    public void testCreateCrewValidationFailed() throws Exception {
        MovieReadDTO movie = createMovieRead();
        CrewCreateDTO actorRole = new CrewCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/crews", movie.getId())
                .content(objectMapper.writeValueAsString(actorRole))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(crewService, Mockito.never()).createCrew(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetCrew() throws Exception {
        UUID movieId = createMovieRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        CrewReadDTO crew = createCrewRead(readMovie);

        Mockito.when(crewService.getCrew(crew.getId())).thenReturn(crew);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/{id}", movieId, crew.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assertions.assertThat(actualCrew).isEqualToComparingFieldByField(crew);

        Mockito.verify(crewService).getCrew(crew.getId());
    }

    @Test
    public void testGetMovieCrews() throws Exception {
        UUID movieId = createMovieRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        CrewReadDTO crew = createCrewRead(readMovie);

        List<CrewReadDTO> expectedCrews = List.of(crew);

        Mockito.when(crewService.getMovieCrews(movieId)).thenReturn(expectedCrews);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewReadDTO> actualCrew = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedCrews, actualCrew);
        Mockito.verify(crewService).getMovieCrews(movieId);
    }

    @Test
    public void testGetCrewWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID movieId = createMovieRead().getId();

        EntityNotFoundException exception = new EntityNotFoundException(Crew.class, wrongId);
        Mockito.when(crewService.getCrew(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/{id}", movieId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetCrewWrongFormatId() throws Exception {
        UUID movieId = UUID.randomUUID();

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/crews/123", movieId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateCrew() throws Exception {
        CrewCreateDTO create = generateObject(CrewCreateDTO.class);

        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        CrewReadDTO read = createCrewRead(readMovie);

        Mockito.when(crewService.createCrew(movieId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/crews", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assertions.assertThat(actualCrew).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchCrew() throws Exception {
        CrewPatchDTO patchDTO = generateObject(CrewPatchDTO.class);

        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        CrewReadDTO read = createCrewRead(readMovie);

        Mockito.when(crewService.patchCrew(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{movieId}/crews/{id}", movieId, read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assert.assertEquals(read, actualCrew);
    }

    @Test
    public void testUpdateCrew() throws Exception {
        CrewPutDTO putDTO = generateObject(CrewPutDTO.class);

        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        CrewReadDTO read = createCrewRead(readMovie);

        Mockito.when(crewService.updateCrew(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{movieId}/crews/{id}", movieId, read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewReadDTO actualCrew = objectMapper.readValue(resultJson, CrewReadDTO.class);
        Assert.assertEquals(read, actualCrew);
    }

    @Test
    public void testDeleteCrew() throws Exception {
        UUID id = UUID.randomUUID();
        UUID movieId = createMovieRead().getId();

        mvc.perform(delete("/api/v1/movies/{movieId}/crews/{id}", movieId, id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(crewService).deleteCrew(id);
    }

    private CrewReadDTO createCrewRead(MovieReadDTO movie) {
        CrewReadDTO read = new CrewReadDTO();
        read.setId(UUID.randomUUID());
        read.setProfession(Profession.CAST);
        read.setMovieId(movie.getId());
        return read;
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }
}