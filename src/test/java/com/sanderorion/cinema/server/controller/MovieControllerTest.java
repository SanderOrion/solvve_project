package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieCreateDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePatchDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePutDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.MovieService;
import com.sanderorion.cinema.server.service.importer.MovieImporterService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(MovieController.class)
public class MovieControllerTest extends BaseControllerTest {

    @MockBean
    private MovieService movieService;

    @MockBean
    private MovieImporterService movieImporterService;

    @Test
    public void testCreateMovieValidationFailed() throws Exception {
        MovieCreateDTO movie = new MovieCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/movies")
                .content(objectMapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(movieService, Mockito.never()).createMovie(ArgumentMatchers.any());
    }

    @Test
    public void testImportMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        int id = 680;
        Mockito.when(movieImporterService.importMovie(id)).thenReturn(movieId);

        String resultJson = mvc.perform(get("/api/v1/content-manager-users/import-movie/{id}", id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UUID actualMovie = objectMapper.readValue(resultJson, UUID.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movieId);

        Mockito.verify(movieImporterService).importMovie(id);
    }

    @Test
    public void testGetMovie() throws Exception {
        MovieReadDTO movie = createMovieRead();

        Mockito.when(movieService.getMovie(movie.getId())).thenReturn(movie);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(movie);

        Mockito.verify(movieService).getMovie(movie.getId());
    }

    @Test
    public void testGetMoviesLeaderBoard() throws Exception {
        MovieInLeaderBoardReadDTO movieLeader = createMovieLeaderRead();

        List<MovieInLeaderBoardReadDTO> expectedMovies = List.of(movieLeader);

        Mockito.when(movieService.getMoviesLeaderBoard()).thenReturn(expectedMovies);

        String resultJson = mvc.perform(get("/api/v1/movies/leader-board"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieInLeaderBoardReadDTO> actualMovies = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedMovies, actualMovies);
        Mockito.verify(movieService).getMoviesLeaderBoard();
    }

    @Test
    public void testGetMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Movie.class, wrongId);
        Mockito.when(movieService.getMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMovieWrongFormatId() throws Exception {
        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/movies/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateMovie() throws Exception {
        MovieCreateDTO create = generateObject(MovieCreateDTO.class);

        MovieReadDTO read = createMovieRead();

        Mockito.when(movieService.createMovie(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assertions.assertThat(actualMovie).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchMovie() throws Exception {
        MoviePatchDTO patchDTO = generateObject(MoviePatchDTO.class);

        MovieReadDTO read = createMovieRead();

        Mockito.when(movieService.patchMovie(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assert.assertEquals(read, actualMovie);
    }

    @Test
    public void testUpdateMovie() throws Exception {
        MoviePutDTO putDTO = generateObject(MoviePutDTO.class);

        MovieReadDTO read = createMovieRead();

        Mockito.when(movieService.updateMovie(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MovieReadDTO actualMovie = objectMapper.readValue(resultJson, MovieReadDTO.class);
        Assert.assertEquals(read, actualMovie);
    }

    @Test
    public void testDeleteMovie() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(movieService).deleteMovie(id);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private MovieInLeaderBoardReadDTO createMovieLeaderRead() {
        return generateObject(MovieInLeaderBoardReadDTO.class);
    }
}