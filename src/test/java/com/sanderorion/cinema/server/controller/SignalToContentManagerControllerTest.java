package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.SignalToContentManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(SignalToContentManagerController.class)
public class SignalToContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private SignalToContentManagerService signalToContentManagerService;

    @Test
    public void testCreateSignalToCMValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();
        SignalToContentManagerCreateDTO signal = new SignalToContentManagerCreateDTO();

        String resultJson = mvc.perform(post("/api/v1/articles/{articleId}/reg-users/{id}",
                article.getId(), regUser.getId())
                .content(objectMapper.writeValueAsString(signal))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(signalToContentManagerService, Mockito.never())
                .createSignalToCM(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetSignalToCM() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();
        SignalToContentManagerReadDTO read = createSignalToCMRead(regUser, article);

        Mockito.when(signalToContentManagerService.getSignalToCM(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{id}/signals/{signalId}", regUser.getId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignal = objectMapper
                .readValue(resultJson, SignalToContentManagerReadDTO.class);
        Assertions.assertThat(actualSignal).isEqualToComparingFieldByField(read);

        Mockito.verify(signalToContentManagerService).getSignalToCM(read.getId());
    }

    @Test
    public void testGetRegUserSignalsToCM() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();
        SignalToContentManagerReadDTO read = createSignalToCMRead(regUser, article);

        List<SignalToContentManagerReadDTO> expectedSignal = List.of(read);

        Mockito.when(signalToContentManagerService.getRegUserSignalsToCM(regUser.getId())).thenReturn(expectedSignal);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/signals", regUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<SignalToContentManagerReadDTO> actualSignal = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedSignal, actualSignal);
        Mockito.verify(signalToContentManagerService).getRegUserSignalsToCM(regUser.getId());
    }

    @Test
    public void testGetSignalsNeedToFix() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();
        SignalToContentManagerReadDTO read = createSignalToCMRead(regUser, article);

        PageResult<SignalToContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(1L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(signalToContentManagerService.getSignalsForFix(pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/content-manager-users/signals-for-fix")
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<SignalToContentManagerReadDTO> actualPage = objectMapper
                .readValue(resultJson, new TypeReference<>() {
                });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetSignalToCMWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(SignalToContentManager.class, wrongId);
        Mockito.when(signalToContentManagerService.getSignalToCM(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals/{signalId}", userId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetSignalToCMWrongFormatId() throws Exception {
        String error = "signalId should be of type java.util.UUID";
        UUID userId = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals/123", userId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateSignalToCM() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        ArticleReadDTO article = createArticleRead();

        SignalToContentManagerCreateDTO create = generateObject(SignalToContentManagerCreateDTO.class);
        SignalToContentManagerReadDTO read = createSignalToCMRead(regUser, article);

        Mockito.when(signalToContentManagerService.createSignalToCM(article.getId(), regUser.getId(), create))
                .thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/articles/{articleId}/reg-users/{id}",
                article.getId(), regUser.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actualSignalToCM = objectMapper
                .readValue(resultJson, SignalToContentManagerReadDTO.class);
        Assertions.assertThat(actualSignalToCM).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testUpdateSignalToCM() throws Exception {
        RegUserReadDTO readRegUser = createRegUserRead();
        ArticleReadDTO readArticle = createArticleRead();
        SignalToContentManagerReadDTO read = createSignalToCMRead(readRegUser, readArticle);
        UUID cmId = UUID.randomUUID();

        SignalToContentManagerPutDTO putDTO = new SignalToContentManagerPutDTO();
        putDTO.setCorrectText(null);

        Mockito.when(signalToContentManagerService.updateSignalToCM(read.getId(), cmId, putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/content-manager-users/{cmId}/signals-for-fix/{id}",
                cmId, read.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToContentManagerReadDTO actual = objectMapper.readValue(resultJson, SignalToContentManagerReadDTO.class);
        Assert.assertEquals(read, actual);
    }

    @Test
    public void testDeleteSignalToCM() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/content-manager-users/signals/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(signalToContentManagerService).deleteSignalToCM(id);
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private ArticleReadDTO createArticleRead() {
        ArticleReadDTO read = generateObject(ArticleReadDTO.class);
        read.setBody("there is a mistakes");
        return read;
    }

    private SignalToContentManagerReadDTO createSignalToCMRead(RegUserReadDTO regUser, ArticleReadDTO article) {
        SignalToContentManagerReadDTO read = new SignalToContentManagerReadDTO();
        read.setId(UUID.randomUUID());
        read.setSignalStatus(SignalStatus.NEED_TO_FIX);
        read.setTypo("mistakes");
        read.setUserSuggestion("mistake(user version)");
        read.setRegUserId(regUser.getId());
        read.setArticleId(article.getId());
        return read;
    }
}