package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ReviewMovieService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ReviewMovieController.class)
public class ReviewMovieControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewMovieService reviewMovieService;

    @Test
    public void testCreateReviewMovieValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieCreateDTO review = new ReviewMovieCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/review-movies/{movieId}",
                regUser.getId(), movie.getId())
                .content(objectMapper.writeValueAsString(review))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewMovieService, Mockito.never())
                .createReviewMovie(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetReviewMovie() throws Exception {
        MovieReadDTO readMovie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(readRegUser, readMovie);

        Mockito.when(reviewMovieService.getReviewMovie(reviewMovie.getId())).thenReturn(reviewMovie);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/review-movies/{id}", reviewMovie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewMovieReadDTO actualReviewMovie = objectMapper.readValue(resultJson, ReviewMovieReadDTO.class);
        Assertions.assertThat(actualReviewMovie).isEqualToComparingFieldByField(reviewMovie);

        Mockito.verify(reviewMovieService).getReviewMovie(reviewMovie.getId());
    }

    @Test
    public void testGetUserReviewMovie() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewMovieReadDTO read = createReviewMovieRead(readRegUser, readMovie);

        List<ReviewMovieReadDTO> expectedReview = List.of(read);

        Mockito.when(reviewMovieService.getRegUserReviewMovies(regUserId)).thenReturn(expectedReview);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/review-movies", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ReviewMovieReadDTO> actualReview = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedReview, actualReview);
        Mockito.verify(reviewMovieService).getRegUserReviewMovies(regUserId);
    }

    @Test
    public void testGetAllCheckedReviewsMovie() throws Exception {
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewMovieReadDTO read = createReviewMovieRead(readRegUser, movie, ReviewConfirmStatus.CONFIRMED);

        PageResult<ReviewMovieReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(1L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(reviewMovieService.getAllConfirmedReviewsMovie(movie.getId(), pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}/review-movies", movie.getId())
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ReviewMovieReadDTO> actualPage = objectMapper
                .readValue(resultJson, new TypeReference<>() {
                });

        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewsMovieToConfirm() throws Exception {
        MovieReadDTO readMovie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewMovieReadDTO read = createReviewMovieRead(readRegUser, readMovie, ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewMovieReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(1L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(reviewMovieService.getReviewsMovieToConfirm(pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies/review-movies-to-confirm")
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewMovieReadDTO> actualPage = objectMapper
                .readValue(resultJson, new TypeReference<>() {
                });

        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewMovie.class, wrongId);
        Mockito.when(reviewMovieService.getReviewMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/reg-users/review-movies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetReviewMovieWrongFormatId() throws Exception {
        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/review-movies/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateReviewMovie() throws Exception {
        ReviewMovieCreateDTO create = generateObject(ReviewMovieCreateDTO.class);

        MovieReadDTO movie = createMovieRead();
        UUID movieId = movie.getId();
        RegUserReadDTO regUser = createRegUserRead();
        UUID regUserId = regUser.getId();
        ReviewMovieReadDTO read = createReviewMovieRead(regUser, movie);

        Mockito.when(reviewMovieService.createReviewMovie(regUserId, movieId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/review-movies/{movieId}",
                regUserId, movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewMovieReadDTO actualReviewMovie = objectMapper.readValue(resultJson, ReviewMovieReadDTO.class);
        Assertions.assertThat(actualReviewMovie).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchReviewMovie() throws Exception {
        ReviewMoviePatchDTO patchDTO = generateObject(ReviewMoviePatchDTO.class);

        MovieReadDTO readMovie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewMovieReadDTO read = createReviewMovieRead(readRegUser, readMovie);

        Mockito.when(reviewMovieService.patchReviewMovie(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/reg-users/review-movies/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewMovieReadDTO actualReviewMovie = objectMapper.readValue(resultJson, ReviewMovieReadDTO.class);
        Assert.assertEquals(read, actualReviewMovie);
    }

    @Test
    public void testUpdateReviewMovie() throws Exception {
        ReviewMoviePutDTO putDTO = generateObject(ReviewMoviePutDTO.class);

        MovieReadDTO readMovie = createMovieRead();
        RegUserReadDTO readRegUser = createRegUserRead();
        RegUserReadDTO moderator = createRegUserRead();
        ReviewMovieReadDTO read = createReviewMovieRead(readRegUser, readMovie);

        Mockito.when(reviewMovieService.updateReviewMovie(read.getId(), moderator.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderator-users/{mId}/review-movies-to-confirm/{id}",
                moderator.getId(), read.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewMovieReadDTO actualReviewMovie = objectMapper.readValue(resultJson, ReviewMovieReadDTO.class);
        Assert.assertEquals(read, actualReviewMovie);
    }

    @Test
    public void testDeleteReviewMovie() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/review-movies/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewMovieService).deleteReviewMovie(id);
    }

    private ReviewMovieReadDTO createReviewMovieRead(RegUserReadDTO regUser, MovieReadDTO movie) {
        ReviewMovieReadDTO read = generateObject(ReviewMovieReadDTO.class);
        read.setMovieId(movie.getId());
        read.setRegUserId(regUser.getId());
        return read;
    }

    private ReviewMovieReadDTO createReviewMovieRead(RegUserReadDTO regUser, MovieReadDTO movie,
                                                     ReviewConfirmStatus checkStatus) {
        ReviewMovieReadDTO read = generateObject(ReviewMovieReadDTO.class);
        read.setMovieId(movie.getId());
        read.setRegUserId(regUser.getId());
        read.setConfirmStatus(checkStatus);
        return read;
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private MovieReadDTO createMovieRead() {
        MovieReadDTO read = generateObject(MovieReadDTO.class);
        return read;
    }
}