package com.sanderorion.cinema.server.controller.integration;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.enums.*;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.article.ArticleCreateDTO;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleCreateDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieCreateDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieCreateDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonCreateDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserCreateDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutTrustDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorCreateDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.dto.userrole.UserRoleReadDTO;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import org.apache.xmlbeans.impl.util.Base64;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class WorkingScenarioIntegrationTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Test
    public void workingScenarioIntegrationTest() throws InterruptedException {
        final String A1_PASSWORD = "123a1";
        final String M1_PASSWORD = "123m1";
        final String C1_PASSWORD = "123c";
        final String U1_PASSWORD = "123u1";
        final String U2_PASSWORD = "123u1";
        final String U3_PASSWORD = "123u1";

        final String URL_REG_USERS = "http://localhost:8080/api/v1/reg-users";
        final String URL_MOVIES = "http://localhost:8080/api/v1/movies/";
        final String URL_ARTICLES = "http://localhost:8080/api/v1/articles/";
        final String URL_MOVIE_PERSONS = "http://localhost:8080/api/v1/movie-persons";
        final String URL_CONTENT_MANAGER = "http://localhost:8080/api/v1/content-manager-users/";
        final String URL_MODERATOR = "http://localhost:8080/api/v1/moderator-users/";
        final String URL_SIGNAL = "http://localhost:8080/api/v1/reviews/";

        RegUser a1 = new RegUser();
        a1.setName("Alex");
        a1.setLogin("FirstAdmin");
        a1.setEmail("admin@admin.com");
        a1.setGender(Gender.MALE);
        a1.setEncodedPassword(passwordEncoder.encode(A1_PASSWORD));
        a1.getUserRoles().add(userRoleRepository.findUserRoleByUserRoleType(UserRoleType.ADMIN));
        regUserRepository.save(a1);

        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

//        FINAL_1
        RegUserCreateDTO m1 = new RegUserCreateDTO();
        m1.setName("Alice");
        m1.setLogin("AliceModerator");
        m1.setEmail("moderator@moderator.com");
        m1.setPassword(M1_PASSWORD);
        m1.setGender(Gender.FEMALE);

        ResponseEntity<RegUserReadDTO> response1 = restTemplate.postForEntity(
                URL_REG_USERS, m1, RegUserReadDTO.class);
        checkStatusCode(response1);
        UUID M1Id = response1.getBody().getId();

//        FINAL_2
        HttpHeaders headersAdmin = new HttpHeaders();
        headersAdmin.add("Authorization", getBasicAuthorizationHeaderValue(a1.getEmail(), A1_PASSWORD));
        HttpEntity<?> httpEntityAdmin = new HttpEntity<>(headersAdmin);

//        UserRole index:
//        0 - ADMIN
//        1 - CONTENT_MANAGER
//        2 - MODERATOR
//        3 - REG_USER
//        4 - BLOCKED
        ResponseEntity<List<UserRoleReadDTO>> responseUserRoles = restTemplate.exchange(
                "http://localhost:8080/api/v1/user-roles", HttpMethod.GET, httpEntityAdmin,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseUserRoles);

        ResponseEntity<List<UserRoleReadDTO>> response2 = restTemplate.exchange(
                URL_REG_USERS + "/" + M1Id + "/user-roles/" + responseUserRoles.getBody().get(2).getId(),
                HttpMethod.POST, httpEntityAdmin, new ParameterizedTypeReference<>() {
                });
        checkStatusCode(response2);

//        FINAL_3
        RegUserCreateDTO c1 = new RegUserCreateDTO();
        c1.setName("Mary");
        c1.setLogin("MaryContentManager");
        c1.setEmail("contentManager@contentManager.com");
        c1.setPassword(C1_PASSWORD);
        c1.setGender(Gender.FEMALE);

        ResponseEntity<RegUserReadDTO> response3 = restTemplate.postForEntity(URL_REG_USERS, c1, RegUserReadDTO.class);
        checkStatusCode(response3);
        UUID C1Id = response3.getBody().getId();

//        FINAL_4
        ResponseEntity<List<UserRoleReadDTO>> response4 = restTemplate.exchange(
                URL_REG_USERS + "/" + C1Id + "/user-roles/" + responseUserRoles.getBody().get(1).getId(),
                HttpMethod.POST, httpEntityAdmin, new ParameterizedTypeReference<>() {
                });
        checkStatusCode(response4);

//        FINAL_5
        RegUserCreateDTO u1 = new RegUserCreateDTO();
        u1.setName("user1");
        u1.setLogin("RegUser1");
        u1.setEmail("user1@user.com");
        u1.setPassword(U1_PASSWORD);
        u1.setGender(Gender.MALE);

        ResponseEntity<RegUserReadDTO> responseCreatePerson1 = restTemplate.postForEntity(
                URL_REG_USERS, u1, RegUserReadDTO.class);
        checkStatusCode(responseCreatePerson1);
        UUID u1Id = responseCreatePerson1.getBody().getId();

        RegUserCreateDTO u2 = new RegUserCreateDTO();
        u2.setName("user2");
        u2.setLogin("RegUser2");
        u2.setEmail("user2@user.com");
        u2.setPassword(U2_PASSWORD);
        u2.setGender(Gender.MALE);

        ResponseEntity<RegUserReadDTO> responseCreatePerson2 = restTemplate.postForEntity(
                URL_REG_USERS, u2, RegUserReadDTO.class);
        checkStatusCode(responseCreatePerson2);
        UUID u2Id = responseCreatePerson2.getBody().getId();

        RegUserCreateDTO u3 = new RegUserCreateDTO();
        u3.setName("user3");
        u3.setLogin("RegUser3");
        u3.setEmail("user3@user.com");
        u3.setPassword(U3_PASSWORD);
        u3.setGender(Gender.FEMALE);

        ResponseEntity<RegUserReadDTO> responseCreatePerson3 = restTemplate.postForEntity(
                URL_REG_USERS, u3, RegUserReadDTO.class);
        checkStatusCode(responseCreatePerson3);
        UUID u3Id = responseCreatePerson3.getBody().getId();

//        FINAL_6
        RegUserPatchDTO patch = new RegUserPatchDTO();
        patch.setName("User2");

        HttpHeaders headersUser2 = new HttpHeaders();
        headersUser2.add("Authorization", getBasicAuthorizationHeaderValue(u2.getEmail(), U2_PASSWORD));
        HttpEntity<?> httpEntityUser2 = new HttpEntity<>(patch, headersUser2);

        ResponseEntity<RegUserReadDTO> response6 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id, HttpMethod.PATCH, httpEntityUser2, RegUserReadDTO.class);
        checkStatusCode(response6);
        Assert.assertEquals("User2", response6.getBody().getName());

//        FINAL_7
        MovieCreateDTO movie = new MovieCreateDTO();
        movie.setOriginalTitle("The Avengers");
        movie.setTitle("The Avengers");
        movie.setImdbId("tt0848228");
        movie.setTagline("Avengers Assemble!");
        movie.setOverview("When an unexpected enemy emerges and threatens global safety and security, Nick Fury, "
                + "director of the international peacekeeping agency known as S.H.I.E.L.D., "
                + "finds himself in need of a team to pull the world back from the brink of disaster. "
                + "Spanning the globe, a daring recruitment effort begins!");
        movie.setReleaseDate(LocalDate.of(2012, 4, 11));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(143);
        movie.setLanguage("English");
        movie.setBudget(220000000L);
        movie.setRevenue(1518812988L);

        HttpHeaders headersCM = new HttpHeaders();
        headersCM.add("Authorization", getBasicAuthorizationHeaderValue(c1.getEmail(), C1_PASSWORD));
        HttpEntity<?> httpEntityCreateMovie = new HttpEntity<>(movie, headersCM);

        ResponseEntity<MovieReadDTO> responseMovie = restTemplate.exchange(
                URL_MOVIES, HttpMethod.POST, httpEntityCreateMovie, MovieReadDTO.class);
        checkStatusCode(responseMovie);
        UUID movieId = responseMovie.getBody().getId();

        MoviePersonCreateDTO moviePerson1 = new MoviePersonCreateDTO();
        moviePerson1.setName("Samuel L. Jackson");
        moviePerson1.setBirthday(LocalDate.of(1948, 12, 21));
        moviePerson1.setProfession("Actor, Producer, Soundtrack");
        moviePerson1.setInternalId(2231);
        moviePerson1.setBiography("Samuel L. Jackson is an American producer and highly prolific actor, "
                + "having appeared in over 100 films");
        moviePerson1.setGender(Gender.MALE);

        HttpEntity<?> httpEntityCreateMP1 = new HttpEntity<>(moviePerson1, headersCM);

        ResponseEntity<MoviePersonReadDTO> responseMP1 = restTemplate.exchange(
                URL_MOVIE_PERSONS, HttpMethod.POST, httpEntityCreateMP1,
                MoviePersonReadDTO.class);
        checkStatusCode(responseMP1);
        UUID mp1Id = responseMP1.getBody().getId();

        MoviePersonCreateDTO moviePerson2 = new MoviePersonCreateDTO();
        moviePerson2.setName("Scarlett Johansson");
        moviePerson2.setBirthday(LocalDate.of(1984, 11, 22));
        moviePerson2.setProfession("Actress, Soundtrack, Producer");
        moviePerson2.setInternalId(1);
        moviePerson2.setBiography("Scarlett Johansson was born in New York City.");
        moviePerson2.setGender(Gender.FEMALE);

        HttpEntity<?> httpEntityCreateMP2 = new HttpEntity<>(moviePerson2, headersCM);

        ResponseEntity<MoviePersonReadDTO> responseMP2 = restTemplate.exchange(
                URL_MOVIE_PERSONS, HttpMethod.POST, httpEntityCreateMP2,
                MoviePersonReadDTO.class);
        checkStatusCode(responseMP2);
        UUID mp2Id = responseMP2.getBody().getId();

        MoviePersonCreateDTO moviePerson3 = new MoviePersonCreateDTO();
        moviePerson3.setName("Stan Lee");
        moviePerson3.setBirthday(LocalDate.of(1922, 12, 28));
        moviePerson3.setDeathDay(LocalDate.of(2018, 11, 12));
        moviePerson3.setProfession("Producer, Writer, Actor");
        moviePerson3.setInternalId(3);
        moviePerson3.setBiography("Stan Lee was an American comic-book writer, editor, and publisher, "
                + "who was executive vice president and publisher of Marvel Comics");
        moviePerson3.setGender(Gender.MALE);

        HttpEntity<?> httpEntityCreateMP3 = new HttpEntity<>(moviePerson3, headersCM);

        ResponseEntity<MoviePersonReadDTO> responseMP3 = restTemplate.exchange(
                URL_MOVIE_PERSONS, HttpMethod.POST, httpEntityCreateMP3,
                MoviePersonReadDTO.class);
        checkStatusCode(responseMP3);
        UUID mp3Id = responseMP3.getBody().getId();

        MoviePersonCreateDTO moviePerson4 = new MoviePersonCreateDTO();
        moviePerson4.setName("Kevin Feige");
        moviePerson4.setBirthday(LocalDate.of(1973, 6, 2));
        moviePerson4.setProfession("Producer, Miscellaneous Crew, Production Manager");
        moviePerson4.setInternalId(4);
        moviePerson4.setBiography("Kevin Feige was born on June 2, 1973 in Boston, Massachusetts, USA");
        moviePerson4.setGender(Gender.MALE);

        HttpEntity<?> httpEntityCreateMP4 = new HttpEntity<>(moviePerson4, headersCM);

        ResponseEntity<MoviePersonReadDTO> responseMP4 = restTemplate.exchange(
                URL_MOVIE_PERSONS, HttpMethod.POST, httpEntityCreateMP4,
                MoviePersonReadDTO.class);
        checkStatusCode(responseMP4);
        UUID mp4Id = responseMP4.getBody().getId();

        MoviePersonCreateDTO moviePerson5 = new MoviePersonCreateDTO();
        moviePerson5.setName("Joss Whedon");
        moviePerson5.setBirthday(LocalDate.of(1964, 6, 23));
        moviePerson5.setProfession("Writer, Producer, Director");
        moviePerson5.setInternalId(5);
        moviePerson5.setBiography("Joss Whedon is the middle of five brothers - his younger brothers "
                + "are Jed Whedon and Zack Whedon.");
        moviePerson5.setGender(Gender.MALE);

        HttpEntity<?> httpEntityCreateMP5 = new HttpEntity<>(moviePerson5, headersCM);

        ResponseEntity<MoviePersonReadDTO> responseMP5 = restTemplate.exchange(
                URL_MOVIE_PERSONS, HttpMethod.POST, httpEntityCreateMP5, MoviePersonReadDTO.class);
        checkStatusCode(responseMP5);
        UUID mp5Id = responseMP5.getBody().getId();

        ActorRoleCreateDTO actorRole1 = new ActorRoleCreateDTO();
        actorRole1.setInternalId(2231);
        actorRole1.setGender(Gender.MALE);
        actorRole1.setRoleName("Samuel L. Jackson");
        actorRole1.setRoleInfo("Nick Fury");
        actorRole1.setMoviePersonId(mp1Id);

        HttpEntity<?> httpEntityCreateAR1 = new HttpEntity<>(actorRole1, headersCM);

        ResponseEntity<ActorRoleReadDTO> responseAR1 = restTemplate.exchange(
                URL_MOVIES + movieId + "/actor-roles", HttpMethod.POST, httpEntityCreateAR1, ActorRoleReadDTO.class);
        checkStatusCode(responseAR1);

        ActorRoleCreateDTO actorRole2 = new ActorRoleCreateDTO();
        actorRole2.setInternalId(211111);
        actorRole2.setGender(Gender.FEMALE);
        actorRole2.setRoleName("Scarlett Johansson");
        actorRole2.setRoleInfo("Natasha Romanoff / Black Widow");
        actorRole2.setMoviePersonId(mp2Id);

        HttpEntity<?> httpEntityCreateAR2 = new HttpEntity<>(actorRole2, headersCM);

        ResponseEntity<ActorRoleReadDTO> responseAR2 = restTemplate.exchange(
                URL_MOVIES + movieId + "/actor-roles", HttpMethod.POST, httpEntityCreateAR2, ActorRoleReadDTO.class);
        checkStatusCode(responseAR2);

        ActorRoleCreateDTO actorRole3 = new ActorRoleCreateDTO();
        actorRole3.setInternalId(3111111);
        actorRole3.setGender(Gender.MALE);
        actorRole3.setRoleName("Stan Lee");
        actorRole3.setRoleInfo("Himself");
        actorRole3.setMoviePersonId(mp3Id);

        HttpEntity<?> httpEntityCreateAR3 = new HttpEntity<>(actorRole3, headersCM);

        ResponseEntity<ActorRoleReadDTO> responseAR3 = restTemplate.exchange(
                URL_MOVIES + movieId + "/actor-roles", HttpMethod.POST, httpEntityCreateAR3, ActorRoleReadDTO.class);
        checkStatusCode(responseAR3);

        CrewCreateDTO crew1 = new CrewCreateDTO();
        crew1.setInternalId(1);
        crew1.setCrewName("Joss Whedon");
        crew1.setGender(Gender.MALE);
        crew1.setProfession(Profession.DIRECTING);
        crew1.setDescription("Director");
        crew1.setMoviePersonId(mp4Id);

        HttpEntity<?> httpEntityCreateCrew1 = new HttpEntity<>(crew1, headersCM);

        ResponseEntity<CrewReadDTO> responseCrew1 = restTemplate.exchange(
                URL_MOVIES + movieId + "/crews", HttpMethod.POST, httpEntityCreateCrew1, CrewReadDTO.class);
        checkStatusCode(responseCrew1);

        CrewCreateDTO crew2 = new CrewCreateDTO();
        crew2.setInternalId(2);
        crew2.setCrewName("Kevin Feige");
        crew2.setGender(Gender.MALE);
        crew2.setProfession(Profession.PRODUCTION);
        crew2.setDescription("Producer");
        crew2.setMoviePersonId(mp5Id);

        HttpEntity<?> httpEntityCreateCrew2 = new HttpEntity<>(crew2, headersCM);

        ResponseEntity<CrewReadDTO> responseCrew2 = restTemplate.exchange(
                URL_MOVIES + movieId + "/crews", HttpMethod.POST, httpEntityCreateCrew2, CrewReadDTO.class);
        checkStatusCode(responseCrew2);

        CrewCreateDTO crew3 = new CrewCreateDTO();
        crew3.setInternalId(3);
        crew3.setCrewName("Stan Lee");
        crew3.setGender(Gender.MALE);
        crew3.setProfession(Profession.PRODUCTION);
        crew3.setDescription("Executive Producer");
        crew3.setMoviePersonId(mp3Id);

        HttpEntity<?> httpEntityCreateCrew3 = new HttpEntity<>(crew3, headersCM);

        ResponseEntity<CrewReadDTO> responseCrew3 = restTemplate.exchange(
                URL_MOVIES + movieId + "/crews", HttpMethod.POST,
                httpEntityCreateCrew3, CrewReadDTO.class);
        checkStatusCode(responseCrew3);

        CrewCreateDTO crew4 = new CrewCreateDTO();
        crew4.setInternalId(2231);
        crew4.setCrewName("Samuel L. Jackson");
        crew4.setGender(Gender.MALE);
        crew4.setProfession(Profession.CAST);
        crew4.setDescription("Nick Fury");
        crew4.setMoviePersonId(mp1Id);

        HttpEntity<?> httpEntityCreateCrew4 = new HttpEntity<>(crew4, headersCM);

        ResponseEntity<CrewReadDTO> responseCrew4 = restTemplate.exchange(
                URL_MOVIES + movieId + "/crews", HttpMethod.POST,
                httpEntityCreateCrew4, CrewReadDTO.class);
        checkStatusCode(responseCrew4);

        CrewCreateDTO crew5 = new CrewCreateDTO();
        crew5.setInternalId(32);
        crew5.setCrewName("Scarlett Johansson");
        crew5.setGender(Gender.FEMALE);
        crew5.setProfession(Profession.PRODUCTION);
        crew5.setDescription("Natasha Romanoff / Black Widow");
        crew5.setMoviePersonId(mp5Id);

        HttpEntity<?> httpEntityCreateCrew5 = new HttpEntity<>(crew5, headersCM);

        ResponseEntity<CrewReadDTO> responseCrew5 = restTemplate.exchange(
                URL_MOVIES + movieId + "/crews", HttpMethod.POST,
                httpEntityCreateCrew5, CrewReadDTO.class);
        checkStatusCode(responseCrew5);

//        FINAL_8
        ArticleCreateDTO article = new ArticleCreateDTO();
        article.setDateArticle(LocalDateTime.of(2012, 6, 1, 15, 0, 0)
                .toInstant(ZoneOffset.UTC));
        article.setTitle("The Avengers - такого никто не ожидал");
        article.setBody("The Avengers соврали в прокате более 1 млрд долларов");

        HttpEntity<?> httpEntityCreateArticle = new HttpEntity<>(article, headersCM);

        ResponseEntity<ArticleReadDTO> responseArticle = restTemplate.exchange(
                URL_ARTICLES, HttpMethod.POST, httpEntityCreateArticle, ArticleReadDTO.class);
        checkStatusCode(responseArticle);
        UUID articleId = responseArticle.getBody().getId();

        HttpEntity<?> httpEntityCM = new HttpEntity<>(headersCM);

        ResponseEntity<List<MovieReadDTO>> responseAddMovieToArticle = restTemplate.exchange(
                URL_ARTICLES + articleId + "/movies/" + movieId,
                HttpMethod.POST, httpEntityCM, new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseAddMovieToArticle);

//        FINAL_9
        HttpHeaders headersU1 = new HttpHeaders();
        headersU1.add("Authorization", getBasicAuthorizationHeaderValue(u1.getEmail(), U1_PASSWORD));
        HttpEntity<?> httpEntityU1 = new HttpEntity<>(headersU1);

        ResponseEntity<ArticleReadDTO> responseViewNews = restTemplate.exchange(
                URL_ARTICLES + articleId, HttpMethod.GET, httpEntityU1,
                ArticleReadDTO.class);
        checkStatusCode(responseViewNews);
        Assertions.assertThat(article).isEqualToComparingFieldByField(responseViewNews.getBody());

//        FINAL_10
        LikeArticleCreateDTO likeU1 = new LikeArticleCreateDTO();
        likeU1.setUserLike(true);

        HttpEntity<?> httpEntityLikeArticleU1 = new HttpEntity<>(likeU1, headersU1);

        ResponseEntity<LikeArticleReadDTO> responseLikeArticleU1 = restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/like-articles/" + articleId, HttpMethod.POST,
                httpEntityLikeArticleU1, LikeArticleReadDTO.class);
        checkStatusCode(responseLikeArticleU1);
        Assert.assertEquals(likeU1.getUserLike(), responseLikeArticleU1.getBody().getUserLike());

        LikeArticleCreateDTO likeU2 = new LikeArticleCreateDTO();
        likeU2.setUserLike(true);

        HttpHeaders headersU2 = new HttpHeaders();
        headersU2.add("Authorization", getBasicAuthorizationHeaderValue(u2.getEmail(), U2_PASSWORD));
        HttpEntity<?> httpEntityLikeArticleU2 = new HttpEntity<>(likeU2, headersU2);

        ResponseEntity<LikeArticleReadDTO> responseLikeArticleU2 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id + "/like-articles/" + articleId, HttpMethod.POST,
                httpEntityLikeArticleU2, LikeArticleReadDTO.class);
        checkStatusCode(responseLikeArticleU2);
        Assert.assertEquals(likeU2.getUserLike(), responseLikeArticleU2.getBody().getUserLike());

//        FINAL_11
        LikeArticleCreateDTO likeU3 = new LikeArticleCreateDTO();
        likeU3.setUserLike(true);

        HttpHeaders headersU3 = new HttpHeaders();
        headersU3.add("Authorization", getBasicAuthorizationHeaderValue(u3.getEmail(), U3_PASSWORD));
        HttpEntity<?> httpEntityLikeArticleU3 = new HttpEntity<>(likeU3, headersU3);

        ResponseEntity<LikeArticleReadDTO> responseLikeArticleU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/like-articles/" + articleId, HttpMethod.POST,
                httpEntityLikeArticleU3, LikeArticleReadDTO.class);
        checkStatusCode(responseLikeArticleU3);
        Assert.assertEquals(likeU3.getUserLike(), responseLikeArticleU3.getBody().getUserLike());
        UUID likeU3Id = responseLikeArticleU3.getBody().getId();

        HttpEntity<?> httpEntityU3 = new HttpEntity<>(headersU3);

        ResponseEntity<LikeArticleReadDTO> responseLikeArticleU3Remove = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/like-articles/" + likeU3Id, HttpMethod.DELETE,
                httpEntityU3, LikeArticleReadDTO.class);
        checkStatusCode(responseLikeArticleU3Remove);

//        FINAL_12
        LikeArticleCreateDTO likeU3Dislike = new LikeArticleCreateDTO();
        likeU3Dislike.setUserLike(false);

        HttpEntity<?> httpEntityLikeArticleU3Dislike = new HttpEntity<>(likeU3Dislike, headersU3);

        ResponseEntity<LikeArticleReadDTO> responseLikeArticleU3Dislike = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/like-articles/" + articleId, HttpMethod.POST,
                httpEntityLikeArticleU3Dislike, LikeArticleReadDTO.class);
        checkStatusCode(responseLikeArticleU3Dislike);
        Assert.assertEquals(likeU3Dislike.getUserLike(), responseLikeArticleU3Dislike.getBody().getUserLike());

//        FINAL_13
        SignalToContentManagerCreateDTO signal = new SignalToContentManagerCreateDTO();
        signal.setTypo("соврали");
        signal.setUserSuggestion(null);

        HttpEntity<?> httpEntitySignal = new HttpEntity<>(signal, headersU3);

        ResponseEntity<SignalToContentManagerReadDTO> responseSignal = restTemplate.exchange(
                URL_ARTICLES + articleId + "/reg-users/" + u3Id, HttpMethod.POST,
                httpEntitySignal, SignalToContentManagerReadDTO.class);
        checkStatusCode(responseSignal);

//        FINAL_14
        ResponseEntity<PageResult<SignalToContentManagerReadDTO>> responseViewSignals = restTemplate.exchange(
                URL_CONTENT_MANAGER + "signals-for-fix", HttpMethod.GET, httpEntityCM,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewSignals);
        Long totalSignals = responseViewSignals.getBody().getTotalElements();
        Long expected = 1L;
        Assert.assertEquals(expected, totalSignals);
        UUID signalId = responseViewSignals.getBody().getData().get(0).getId();

//        FINAL_15
        SignalToContentManagerPutDTO fixSignal = new SignalToContentManagerPutDTO();
        fixSignal.setCorrectText("собрали");

        HttpEntity<?> httpEntityFixSignal = new HttpEntity<>(fixSignal, headersCM);

        ResponseEntity<SignalToContentManagerReadDTO> responseFixSignal = restTemplate.exchange(
                URL_CONTENT_MANAGER + C1Id + "/signals-for-fix/" + signalId, HttpMethod.PUT, httpEntityFixSignal,
                SignalToContentManagerReadDTO.class);
        checkStatusCode(responseFixSignal);

//        FINAL_16
        ResponseEntity<PageResult<SignalToContentManagerReadDTO>> responseViewSignalsAfterFix = restTemplate.exchange(
                URL_CONTENT_MANAGER + "/signals-for-fix", HttpMethod.GET, httpEntityCM,
                new ParameterizedTypeReference<>() {
                });
        Long totalSignalsAfterFix = responseViewSignalsAfterFix.getBody().getTotalElements();
        Long expectedAfterFix = 0L;
        Assert.assertEquals(expectedAfterFix, totalSignalsAfterFix);
        checkStatusCode(responseViewSignalsAfterFix);

//        FINAL_17
        ResponseEntity<ArticleReadDTO> responseViewNewsFromUnregUser = restTemplate.getForEntity(
                URL_ARTICLES + articleId, ArticleReadDTO.class);
        Integer amountLike = 2;
        Integer amountDislike = 1;

        checkStatusCode(responseViewNewsFromUnregUser);
        Assert.assertEquals(responseArticle.getBody().getTitle(), responseViewNewsFromUnregUser.getBody().getTitle());
        Assert.assertEquals("The Avengers собрали в прокате более 1 млрд долларов",
                responseViewNewsFromUnregUser.getBody().getBody());
        Assert.assertEquals(amountLike, responseViewNewsFromUnregUser.getBody().getAmountLike());
        Assert.assertEquals(amountDislike, responseViewNewsFromUnregUser.getBody().getAmountDislike());

//        FINAL_18
        ResponseEntity<MovieReadDTO> responseViewMovieU1 = restTemplate.exchange(
                URL_MOVIES + movieId, HttpMethod.GET, httpEntityU1, MovieReadDTO.class);
        checkStatusCode(responseViewMovieU1);
        Assertions.assertThat(movie).isEqualToComparingFieldByField(responseViewMovieU1.getBody());

//        FINAL_19
        ReviewMovieCreateDTO reviewMovie = new ReviewMovieCreateDTO();
        reviewMovie.setBody("Фильм мне очень понравился. Впервые были собраны все Мстители. "
                + "<details>На летающем корабле Локи вывел из себя Беннера и тот стал Халком.</details>)");

        HttpEntity<?> httpEntityReviewMovieU1 = new HttpEntity<>(reviewMovie, headersU1);

        ResponseEntity<ReviewMovieReadDTO> responseReviewMovie = restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/review-movies/" + movieId, HttpMethod.POST,
                httpEntityReviewMovieU1, ReviewMovieReadDTO.class);
        checkStatusCode(responseReviewMovie);
        Assert.assertEquals(reviewMovie.getBody(), responseReviewMovie.getBody().getBody());

        MarkMovieCreateDTO markMovieU1 = new MarkMovieCreateDTO();
        markMovieU1.setMark(5);

        HttpEntity<?> httpEntityMarkMovieU1 = new HttpEntity<>(markMovieU1, headersU1);

        ResponseEntity<MarkMovieReadDTO> responseMarkMovieU1 = restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/mark-movies/" + movieId, HttpMethod.POST,
                httpEntityMarkMovieU1, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkMovieU1);
        Assert.assertEquals(markMovieU1.getMark(), responseMarkMovieU1.getBody().getMark());

//        FINAL_20
        HttpEntity<?> httpEntityU2 = new HttpEntity<>(headersU2);

        ResponseEntity<MovieReadDTO> responseViewMovieU2 = restTemplate.exchange(
                URL_MOVIES + movieId, HttpMethod.GET, httpEntityU2, MovieReadDTO.class);
        checkStatusCode(responseViewMovieU2);
        Assertions.assertThat(movie).isEqualToComparingFieldByField(responseViewMovieU2.getBody());

        ResponseEntity<PageResult<ReviewMovieReadDTO>> responseViewReviewsMovieU2 = restTemplate.exchange(
                URL_MOVIES + movieId + "/review-movies", HttpMethod.GET, httpEntityU2,
                new ParameterizedTypeReference<>() {
                });

        Long totalReviews = responseViewReviewsMovieU2.getBody().getTotalElements();
        Long expectedReviews = 0L;
        Assert.assertEquals(expectedReviews, totalReviews);
        checkStatusCode(responseViewReviewsMovieU2);

//        FINAL_21
        MarkMovieCreateDTO markMovieU2 = new MarkMovieCreateDTO();
        markMovieU2.setMark(4);

        HttpEntity<?> httpEntityMarkMovieU2 = new HttpEntity<>(markMovieU2, headersU2);

        ResponseEntity<MarkMovieReadDTO> responseMarkMovieU2 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id + "/mark-movies/" + movieId, HttpMethod.POST,
                httpEntityMarkMovieU2, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkMovieU2);
        Assert.assertEquals(markMovieU2.getMark(), responseMarkMovieU2.getBody().getMark());

//        FINAL_22
        HttpHeaders headersModerator = new HttpHeaders();
        headersModerator.add("Authorization", getBasicAuthorizationHeaderValue(m1.getEmail(), M1_PASSWORD));
        HttpEntity<?> httpEntityM1 = new HttpEntity<>(headersModerator);

        ResponseEntity<PageResult<ReviewMovieReadDTO>> responseViewReviewsMovieToConfirm = restTemplate.exchange(
                URL_MOVIES + "/review-movies-to-confirm", HttpMethod.GET, httpEntityM1,
                new ParameterizedTypeReference<>() {
                });

        UUID reviewToConfirmId = responseViewReviewsMovieToConfirm.getBody().getData().get(0).getId();
        Long totalReviewsToConfirm = responseViewReviewsMovieToConfirm.getBody().getTotalElements();
        Long expectedReviewsToConfirm = 1L;
        Assert.assertEquals(expectedReviewsToConfirm, totalReviewsToConfirm);
        checkStatusCode(responseViewReviewsMovieToConfirm);

//        FINAL_23
        ReviewMoviePutDTO reviewMoviePutDTO = new ReviewMoviePutDTO();
        reviewMoviePutDTO.setConfirmStatus(ReviewConfirmStatus.CONFIRMED);

        HttpEntity<?> httpEntityConfirmReviewMovie = new HttpEntity<>(reviewMoviePutDTO, headersModerator);

        ResponseEntity<ReviewMovieReadDTO> responseConfirmReviewMovie = restTemplate.exchange(
                URL_MODERATOR + M1Id + "/review-movies-to-confirm/" + reviewToConfirmId, HttpMethod.PUT,
                httpEntityConfirmReviewMovie, ReviewMovieReadDTO.class);
        checkStatusCode(responseConfirmReviewMovie);
        Assert.assertEquals(ReviewConfirmStatus.CONFIRMED, responseConfirmReviewMovie.getBody().getConfirmStatus());

        RegUserPutTrustDTO updateUserTrustStatus = new RegUserPutTrustDTO();
        updateUserTrustStatus.setTrustForReview(TrustForReview.CREDIBLE);

        HttpEntity<?> httpEntityUpdateUserTrustStatus = new HttpEntity<>(updateUserTrustStatus, headersModerator);

        ResponseEntity<RegUserReadDTO> responseUpdateUserTrustStatus = restTemplate.exchange(
                URL_MODERATOR + "update-users-for-review/reg-users/" + u1Id, HttpMethod.PUT,
                httpEntityUpdateUserTrustStatus, RegUserReadDTO.class);
        checkStatusCode(responseUpdateUserTrustStatus);

//      FINAL_24
        ResponseEntity<MovieReadDTO> responseViewMovieU3 = restTemplate.exchange(
                URL_MOVIES + movieId, HttpMethod.GET, httpEntityU3, MovieReadDTO.class);
        checkStatusCode(responseViewMovieU3);
        Assertions.assertThat(movie).isEqualToComparingFieldByField(responseViewMovieU3.getBody());

        MarkMovieCreateDTO markMovieU3 = new MarkMovieCreateDTO();
        markMovieU3.setMark(5);

        HttpEntity<?> httpEntityMarkMovieU3 = new HttpEntity<>(markMovieU3, headersU3);

        ResponseEntity<MarkMovieReadDTO> responseMarkMovieU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/mark-movies/" + movieId, HttpMethod.POST,
                httpEntityMarkMovieU3, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkMovieU3);
        Assert.assertEquals(markMovieU3.getMark(), responseMarkMovieU3.getBody().getMark());

        Thread.sleep(4000);

        ResponseEntity<PageResult<ReviewMovieReadDTO>> responseViewReviewsMovieU3 = restTemplate.exchange(
                URL_MOVIES + movieId + "/review-movies", HttpMethod.GET, httpEntityU3,
                new ParameterizedTypeReference<>() {
                });
        UUID confirmedReviewMovieId = responseViewReviewsMovieU3.getBody().getData().get(0).getId();

        Long totalReviewsU3 = responseViewReviewsMovieU3.getBody().getTotalElements();
        Long expectedReviewsU3 = 1L;
        Assert.assertEquals(expectedReviewsU3, totalReviewsU3);
        checkStatusCode(responseViewReviewsMovieU3);

//      FINAL_25
        LikeReviewMovieCreateDTO likeReviewMovieU3 = new LikeReviewMovieCreateDTO();
        likeReviewMovieU3.setUserLike(true);

        HttpEntity<?> httpEntityLikeReviewMovieU3 = new HttpEntity<>(likeReviewMovieU3, headersU3);

        ResponseEntity<LikeReviewMovieReadDTO> responseLikeReviewMovieU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/like-review-movies/" + confirmedReviewMovieId, HttpMethod.POST,
                httpEntityLikeReviewMovieU3, LikeReviewMovieReadDTO.class);
        checkStatusCode(responseLikeReviewMovieU3);
        Assert.assertEquals(likeReviewMovieU3.getUserLike(), responseLikeReviewMovieU3.getBody().getUserLike());

//      FINAL_26
        ResponseEntity<MovieReadDTO> responseViewMovieUnregUser = restTemplate.getForEntity(
                URL_MOVIES + movieId, MovieReadDTO.class);
        checkStatusCode(responseViewMovieUnregUser);
        Assertions.assertThat(movie).isEqualToComparingFieldByField(responseViewMovieUnregUser.getBody());

        Double expectedMark = 4.67;
        Assert.assertEquals(expectedMark, responseViewMovieUnregUser.getBody().getAvgRating());

//      FINAL_27
        HttpEntity<?> httpEntityC1 = new HttpEntity<>(headersCM);
        int internalMovieId = 680;

        ResponseEntity<UUID> responseImportMovie = restTemplate.exchange(
                URL_CONTENT_MANAGER + "/import-movie/" + internalMovieId, HttpMethod.GET, httpEntityC1, UUID.class);
        checkStatusCode(responseImportMovie);
        UUID importedMovieId = UUID.fromString(responseImportMovie.getBody().toString());

//      FINAL_28
        MarkMovieCreateDTO markImportMovieU1 = new MarkMovieCreateDTO();
        markImportMovieU1.setMark(1);

        HttpEntity<?> httpEntityMarkImportMovieU1 = new HttpEntity<>(markImportMovieU1, headersU1);

        ResponseEntity<MarkMovieReadDTO> responseMarkImportMovieU1 = restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/mark-movies/" + importedMovieId, HttpMethod.POST,
                httpEntityMarkImportMovieU1, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkImportMovieU1);
        Assert.assertEquals(markImportMovieU1.getMark(), responseMarkImportMovieU1.getBody().getMark());

        ReviewMovieCreateDTO reviewImportMovie = new ReviewMovieCreateDTO();
        reviewImportMovie.setBody("Фильм ужасный. Много насилия. Дурак этот Тарантино");

        HttpEntity<?> httpEntityReviewImportMovieU1 = new HttpEntity<>(reviewImportMovie, headersU1);

        ResponseEntity<ReviewMovieReadDTO> responseReviewImportMovie = restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/review-movies/" + importedMovieId, HttpMethod.POST,
                httpEntityReviewImportMovieU1, ReviewMovieReadDTO.class);
        checkStatusCode(responseReviewImportMovie);
        Assert.assertEquals(reviewImportMovie.getBody(), responseReviewImportMovie.getBody().getBody());

//      FINAL_29
        MarkMovieCreateDTO markImportMovieU2 = new MarkMovieCreateDTO();
        markImportMovieU2.setMark(5);

        HttpEntity<?> httpEntityMarkImportMovieU2 = new HttpEntity<>(markImportMovieU2, headersU2);

        ResponseEntity<MarkMovieReadDTO> responseMarkImportMovieU2 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id + "/mark-movies/" + importedMovieId, HttpMethod.POST,
                httpEntityMarkImportMovieU2, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkImportMovieU2);
        Assert.assertEquals(markImportMovieU2.getMark(), responseMarkImportMovieU2.getBody().getMark());

        MarkMovieCreateDTO markImportMovieU3 = new MarkMovieCreateDTO();
        markImportMovieU3.setMark(4);

        HttpEntity<?> httpEntityMarkImportMovieU3 = new HttpEntity<>(markImportMovieU3, headersU3);

        ResponseEntity<MarkMovieReadDTO> responseMarkImportMovieU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/mark-movies/" + importedMovieId, HttpMethod.POST,
                httpEntityMarkImportMovieU3, MarkMovieReadDTO.class);
        checkStatusCode(responseMarkImportMovieU3);
        Assert.assertEquals(markImportMovieU3.getMark(), responseMarkImportMovieU3.getBody().getMark());

        ResponseEntity<PageResult<ReviewMovieReadDTO>> responseViewReviewsImportMovieU2 = restTemplate.exchange(
                URL_MOVIES + importedMovieId + "/review-movies", HttpMethod.GET, httpEntityU2,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewReviewsImportMovieU2);

        UUID confirmedReviewImportMovieIdU2 = responseViewReviewsImportMovieU2.getBody().getData().get(0).getId();
        Long totalReviewsImportMovieU2 = responseViewReviewsImportMovieU2.getBody().getTotalElements();
        Long expectedReviewsImportMovie = 1L;
        Assert.assertEquals(expectedReviewsImportMovie, totalReviewsImportMovieU2);

        SignalToModeratorCreateDTO signalU2 = new SignalToModeratorCreateDTO();
        signalU2.setTypo("Дурак");
        signalU2.setSignalType(SignalType.INSULT);

        HttpEntity<?> httpEntitySignalU2 = new HttpEntity<>(signalU2, headersU2);

        ResponseEntity<SignalToModeratorReadDTO> responseSignalU2 = restTemplate.exchange(
                URL_SIGNAL + confirmedReviewImportMovieIdU2 + "/reg-users/" + u2Id, HttpMethod.POST,
                httpEntitySignalU2, SignalToModeratorReadDTO.class);
        checkStatusCode(responseSignalU2);
        Assert.assertEquals(signalU2.getTypo(), responseSignalU2.getBody().getTypo());
        Assert.assertEquals(signalU2.getSignalType(), responseSignalU2.getBody().getSignalType());

        ResponseEntity<PageResult<ReviewMovieReadDTO>> responseViewReviewsImportMovieU3 = restTemplate.exchange(
                URL_MOVIES + importedMovieId + "/review-movies", HttpMethod.GET, httpEntityU3,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewReviewsImportMovieU3);

        UUID confirmedReviewImportMovieIdU3 = responseViewReviewsImportMovieU3.getBody().getData().get(0).getId();
        Long totalReviewsImportMovieU3 = responseViewReviewsImportMovieU3.getBody().getTotalElements();
        Assert.assertEquals(expectedReviewsImportMovie, totalReviewsImportMovieU3);

        SignalToModeratorCreateDTO signalU3 = new SignalToModeratorCreateDTO();
        signalU3.setTypo("Дурак");
        signalU3.setSignalType(SignalType.INSULT);

        HttpEntity<?> httpEntitySignalU3 = new HttpEntity<>(signalU3, headersU3);

        ResponseEntity<SignalToModeratorReadDTO> responseSignalU3 = restTemplate.exchange(
                URL_SIGNAL + confirmedReviewImportMovieIdU3 + "/reg-users/" + u3Id, HttpMethod.POST,
                httpEntitySignalU3, SignalToModeratorReadDTO.class);
        checkStatusCode(responseSignalU3);
        Assert.assertEquals(signalU3.getTypo(), responseSignalU3.getBody().getTypo());
        Assert.assertEquals(signalU3.getSignalType(), responseSignalU3.getBody().getSignalType());

//      FINAL_30
        ResponseEntity<PageResult<SignalToModeratorReadDTO>> responseViewSignalToModerator = restTemplate.exchange(
                URL_MODERATOR + "signals-for-check", HttpMethod.GET, httpEntityM1,
                new ParameterizedTypeReference<>() {
                });

        Long totalSignalToModerator = responseViewSignalToModerator.getBody().getTotalElements();
        Long expectedSignalToModerator = 2L;
        Assert.assertEquals(expectedSignalToModerator, totalSignalToModerator);
        checkStatusCode(responseViewSignalToModerator);
        UUID signalToCheckId = responseViewSignalToModerator.getBody().getData().get(0).getId();
        UUID reviewMovieCheckId = responseViewSignalToModerator.getBody().getData().get(0).getReviewMovieId();

//      FINAL_31
        ResponseEntity<ReviewMovieReadDTO> responseViewReviewMovieToCheck = restTemplate.exchange(
                URL_REG_USERS + "/review-movies/" + reviewMovieCheckId, HttpMethod.GET, httpEntityM1,
                ReviewMovieReadDTO.class);
        checkStatusCode(responseViewReviewMovieToCheck);
        Assert.assertEquals(responseReviewImportMovie.getBody().getBody(),
                responseViewReviewMovieToCheck.getBody().getBody());
        UUID reviewRegUserId = responseViewReviewMovieToCheck.getBody().getRegUserId();

        SignalToModeratorPutDTO signalToModeratorPut = new SignalToModeratorPutDTO();
        signalToModeratorPut.setCorrectText(null);

        HttpEntity<?> httpEntitySignalToModerator = new HttpEntity<>(signalToModeratorPut, headersModerator);

        ResponseEntity<SignalToModeratorReadDTO> responseSignalToModerator = restTemplate.exchange(
                URL_MODERATOR + M1Id + "/signals-for-check/" + signalToCheckId, HttpMethod.PUT,
                httpEntitySignalToModerator, SignalToModeratorReadDTO.class);
        checkStatusCode(responseSignalToModerator);
        Assert.assertEquals(SignalStatus.FIXED, responseSignalToModerator.getBody().getSignalStatus());

        ResponseEntity<ReviewMovieReadDTO> responseRemoveReviewMovie = restTemplate.exchange(
                URL_REG_USERS + "/review-movies/" + reviewMovieCheckId, HttpMethod.DELETE, httpEntityM1,
                ReviewMovieReadDTO.class);
        checkStatusCode(responseRemoveReviewMovie);

        ResponseEntity<List<UserRoleReadDTO>> responseViewUserRole = restTemplate.exchange(
                URL_REG_USERS + "/" + reviewRegUserId + "/user-roles", HttpMethod.GET, httpEntityM1,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewUserRole);
        UUID userRoleId = responseViewUserRole.getBody().get(0).getId();

        ResponseEntity<List<UserRoleReadDTO>> responseRemoveUserRole = restTemplate.exchange(
                URL_REG_USERS + "/" + reviewRegUserId + "/user-roles/" + userRoleId, HttpMethod.DELETE, httpEntityM1,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseRemoveUserRole);

        ResponseEntity<List<UserRoleReadDTO>> responseAddUserRoleBlocked = restTemplate.exchange(
                URL_REG_USERS + "/" + reviewRegUserId + "/user-roles/" + responseUserRoles.getBody().get(4).getId(),
                HttpMethod.POST, httpEntityM1, new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseAddUserRoleBlocked);

//      FINAL_32
        ResponseEntity<PageResult<SignalToModeratorReadDTO>> responseViewSignalToModeratorAfter = restTemplate
                .exchange(URL_MODERATOR + "signals-for-check", HttpMethod.GET, httpEntityM1,
                        new ParameterizedTypeReference<>() {
                        });

        Long totalSignalToModeratorAfter = responseViewSignalToModeratorAfter.getBody().getTotalElements();
        Long expectedSignalToModeratorAfter = 0L;
        Assert.assertEquals(expectedSignalToModeratorAfter, totalSignalToModeratorAfter);
        checkStatusCode(responseViewSignalToModeratorAfter);

//      FINAL_33
//      просмотр U2 списка ролей первого фильма
        ResponseEntity<List<ActorRoleReadDTO>> responseViewMovieActorRoleU2 = restTemplate.exchange(
                URL_MOVIES + movieId + "/actor-roles", HttpMethod.GET, httpEntityU2,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewMovieActorRoleU2);
        UUID movieActorRoleIdU2 = responseViewMovieActorRoleU2.getBody().get(0).getId();

//      оценка U2 первого фильма
        MarkActorRoleCreateDTO markU2 = new MarkActorRoleCreateDTO();
        markU2.setMark(5);

        HttpEntity<?> httpEntityMarkActorRoleU2 = new HttpEntity<>(markU2, headersU2);

        ResponseEntity<MarkActorRoleReadDTO> responseMarkActorRoleU2 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id + "/mark-actor-roles/" + movieActorRoleIdU2, HttpMethod.POST,
                httpEntityMarkActorRoleU2, MarkActorRoleReadDTO.class);
        checkStatusCode(responseMarkActorRoleU2);
        Assert.assertEquals(markU2.getMark(), responseMarkActorRoleU2.getBody().getMark());

//      просмотр U2 списка ролей второго фильма
        ResponseEntity<List<ActorRoleReadDTO>> responseViewImportMovieActorRoleU2 = restTemplate.exchange(
                URL_MOVIES + importedMovieId + "/actor-roles", HttpMethod.GET, httpEntityU2,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewImportMovieActorRoleU2);
        UUID importMovieActorRoleIdU2 = responseViewImportMovieActorRoleU2.getBody().get(1).getId();

//      оценка U2 второго фильма
        MarkActorRoleCreateDTO importMovieMarkU2 = new MarkActorRoleCreateDTO();
        importMovieMarkU2.setMark(5);

        HttpEntity<?> httpEntityMarkActorRoleImportMovieU2 = new HttpEntity<>(importMovieMarkU2, headersU2);

        ResponseEntity<MarkActorRoleReadDTO> responseMarkActorRoleImportMovieU2 = restTemplate.exchange(
                URL_REG_USERS + "/" + u2Id + "/mark-actor-roles/" + importMovieActorRoleIdU2, HttpMethod.POST,
                httpEntityMarkActorRoleImportMovieU2, MarkActorRoleReadDTO.class);
        checkStatusCode(responseMarkActorRoleImportMovieU2);
        Assert.assertEquals(importMovieMarkU2.getMark(), responseMarkActorRoleImportMovieU2.getBody().getMark());

//      просмотр U3 списка ролей первого фильма
        ResponseEntity<List<ActorRoleReadDTO>> responseViewMovieActorRoleU3 = restTemplate.exchange(
                URL_MOVIES + movieId + "/actor-roles", HttpMethod.GET, httpEntityU3,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewMovieActorRoleU3);
        UUID movieActorRoleIdU3 = responseViewMovieActorRoleU3.getBody().get(0).getId();

//      оценка U3 первого фильма
        MarkActorRoleCreateDTO markU3 = new MarkActorRoleCreateDTO();
        markU3.setMark(4);

        HttpEntity<?> httpEntityMarkActorRoleU3 = new HttpEntity<>(markU3, headersU3);

        ResponseEntity<MarkActorRoleReadDTO> responseMarkActorRoleU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/mark-actor-roles/" + movieActorRoleIdU3, HttpMethod.POST,
                httpEntityMarkActorRoleU3, MarkActorRoleReadDTO.class);
        checkStatusCode(responseMarkActorRoleU3);
        Assert.assertEquals(markU3.getMark(), responseMarkActorRoleU3.getBody().getMark());

//      просмотр U3 списка ролей второго фильма
        ResponseEntity<List<ActorRoleReadDTO>> responseViewImportMovieActorRoleU3 = restTemplate.exchange(
                URL_MOVIES + importedMovieId + "/actor-roles", HttpMethod.GET, httpEntityU3,
                new ParameterizedTypeReference<>() {
                });
        checkStatusCode(responseViewImportMovieActorRoleU3);
        UUID importMovieActorRoleIdU3 = responseViewImportMovieActorRoleU3.getBody().get(1).getId();

//      оценка U3 второго фильма
        MarkActorRoleCreateDTO importMovieMarkU3 = new MarkActorRoleCreateDTO();
        importMovieMarkU3.setMark(3);

        HttpEntity<?> httpEntityMarkActorRoleImportMovieU3 = new HttpEntity<>(importMovieMarkU3, headersU3);

        ResponseEntity<MarkActorRoleReadDTO> responseMarkActorRoleImportMovieU3 = restTemplate.exchange(
                URL_REG_USERS + "/" + u3Id + "/mark-actor-roles/" + importMovieActorRoleIdU3, HttpMethod.POST,
                httpEntityMarkActorRoleImportMovieU3, MarkActorRoleReadDTO.class);
        checkStatusCode(responseMarkActorRoleImportMovieU3);
        Assert.assertEquals(importMovieMarkU3.getMark(), responseMarkActorRoleImportMovieU3.getBody().getMark());

        Thread.sleep(20000);

//      FINAL_34
//      U1 оценивает роль
        MarkActorRoleCreateDTO markU1 = new MarkActorRoleCreateDTO();
        markU1.setMark(1);

        HttpEntity<?> httpEntityMarkActorRoleU1 = new HttpEntity<>(markU1, headersU1);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                URL_REG_USERS + "/" + u1Id + "/mark-actor-roles/" + movieActorRoleIdU3, HttpMethod.POST,
                httpEntityMarkActorRoleU1, MarkActorRoleReadDTO.class)).isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);

//      FINAL_35
        ResponseEntity<MoviePersonReadDTO> responseViewMoviePersonUnregUser = restTemplate.getForEntity(
                URL_MOVIE_PERSONS + "/" + mp1Id, MoviePersonReadDTO.class);
        checkStatusCode(responseViewMoviePersonUnregUser);
        Assertions.assertThat(moviePerson1).isEqualToComparingFieldByField(responseViewMoviePersonUnregUser.getBody());

        Double expectedAvgRoleRating = 4.25;
        Double expectedAvgMovieRating = 4.00;
        Assert.assertEquals(expectedAvgRoleRating, responseViewMoviePersonUnregUser.getBody().getAvgRoleRating());
        Assert.assertEquals(expectedAvgMovieRating, responseViewMoviePersonUnregUser.getBody().getAvgMovieRating());
    }

    private String getBasicAuthorizationHeaderValue(String name, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", name, password).getBytes()));
    }

    private void checkStatusCode(ResponseEntity responseEntity) {
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}