package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieCreateDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.MarkMovieService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(MarkMovieController.class)
public class MarkMovieControllerTest extends BaseControllerTest {

    @MockBean
    private MarkMovieService markMovieService;

    @Test
    public void testCreateMarkMovieValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        MarkMovieCreateDTO markActorRole = new MarkMovieCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/mark-movies/{movieId}",
                regUser.getId(), movie.getId())
                .content(objectMapper.writeValueAsString(markActorRole))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(markMovieService, Mockito.never())
                .createMarkMovie(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetMarkMovie() throws Exception {
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        MarkMovieReadDTO read = createMarkMovieRead(regUser, movie);

        Mockito.when(markMovieService.getMarkMovie(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/mark-movies/{id}", regUser.getId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MarkMovieReadDTO actualMarkMovie = objectMapper
                .readValue(resultJson, MarkMovieReadDTO.class);
        Assertions.assertThat(actualMarkMovie).isEqualToComparingFieldByField(read);

        Mockito.verify(markMovieService).getMarkMovie(read.getId());
    }

    @Test
    public void testGetUserMarkActorRoles() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        MarkMovieReadDTO read = createMarkMovieRead(regUser, movie);

        List<MarkMovieReadDTO> expectedMark = List.of(read);

        Mockito.when(markMovieService.getUserMarksMovies(regUserId)).thenReturn(expectedMark);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/mark-movies", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MarkMovieReadDTO> actualMark = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedMark, actualMark);
        Mockito.verify(markMovieService).getUserMarksMovies(regUserId);
    }

    @Test
    public void testGetMarkMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MarkMovie.class, wrongId);
        Mockito.when(markMovieService.getMarkMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/mark-movies/{id}", id, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMarkMovieWrongFormatId() throws Exception {
        String error = "movieId should be of type java.util.UUID";
        UUID id = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/mark-movies/123", id))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateMarkMovie() throws Exception {
        MarkMovieCreateDTO create = new MarkMovieCreateDTO();
        create.setMark(5);

        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        MarkMovieReadDTO read = createMarkMovieRead(regUser, movie);

        Mockito.when(markMovieService.createMarkMovie(regUser.getId(), movie.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/mark-movies/{movieId}",
                regUser.getId(), movie.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MarkMovieReadDTO actualMarkMovie = objectMapper
                .readValue(resultJson, MarkMovieReadDTO.class);
        Assertions.assertThat(actualMarkMovie).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteMarkMovie() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/mark-movies/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(markMovieService).deleteMarkMovie(id);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private MarkMovieReadDTO createMarkMovieRead(RegUserReadDTO regUser, MovieReadDTO movie) {
        MarkMovieReadDTO read = generateObject(MarkMovieReadDTO.class);
        read.setRegUserId(regUser.getId());
        read.setMovieId(movie.getId());
        return read;
    }
}
