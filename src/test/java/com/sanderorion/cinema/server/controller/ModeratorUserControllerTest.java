package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.domain.ModeratorUser;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserCreateDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPatchDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPutDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ModeratorUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ModeratorUserController.class)
public class ModeratorUserControllerTest extends BaseControllerTest {

    @MockBean
    private ModeratorUserService moderatorUserService;

    @Test
    public void testGetModeratorUser() throws Exception {
        ModeratorUserReadDTO moderatorUser = createModeratorUserPersonRead();

        Mockito.when(moderatorUserService.getModeratorUser(moderatorUser.getId())).thenReturn(moderatorUser);

        String resultJson = mvc.perform(get("/api/v1/moderator-users/{id}", moderatorUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorUserReadDTO actualModeratorUser = objectMapper.readValue(resultJson, ModeratorUserReadDTO.class);
        Assertions.assertThat(actualModeratorUser).isEqualToComparingFieldByField(moderatorUser);

        Mockito.verify(moderatorUserService).getModeratorUser(moderatorUser.getId());
    }

    @Test
    public void testGetModeratorUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ModeratorUser.class, wrongId);
        Mockito.when(moderatorUserService.getModeratorUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/moderator-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetModeratorUserWrongFormatId() throws Exception {

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/moderator-users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateModeratorUser() throws Exception {
        ModeratorUserCreateDTO create = generateObject(ModeratorUserCreateDTO.class);

        ModeratorUserReadDTO read = createModeratorUserPersonRead();

        Mockito.when(moderatorUserService.createModeratorUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/moderator-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorUserReadDTO actualModeratorUser = objectMapper.readValue(resultJson, ModeratorUserReadDTO.class);
        Assertions.assertThat(actualModeratorUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchModeratorUser() throws Exception {
        ModeratorUserPatchDTO patchDTO = generateObject(ModeratorUserPatchDTO.class);

        ModeratorUserReadDTO read = createModeratorUserPersonRead();

        Mockito.when(moderatorUserService.patchModeratorUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/moderator-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorUserReadDTO actualModeratorUser = objectMapper.readValue(resultJson, ModeratorUserReadDTO.class);
        Assert.assertEquals(read, actualModeratorUser);
    }

    @Test
    public void testUpdateModeratorUser() throws Exception {
        ModeratorUserPutDTO putDTO = generateObject(ModeratorUserPutDTO.class);

        ModeratorUserReadDTO read = createModeratorUserPersonRead();

        Mockito.when(moderatorUserService.updateModeratorUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderator-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorUserReadDTO actualModeratorUser = objectMapper.readValue(resultJson, ModeratorUserReadDTO.class);
        Assert.assertEquals(read, actualModeratorUser);
    }

    @Test
    public void testDeleteModeratorUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/moderator-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(moderatorUserService).deleteModeratorUser(id);
    }

    private ModeratorUserReadDTO createModeratorUserPersonRead() {
        return generateObject(ModeratorUserReadDTO.class);
    }
}