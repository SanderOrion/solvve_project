package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.LikeReviewActorRole;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.LikeReviewActorRoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LikeReviewActorRoleController.class)
public class LikeReviewActorRoleControllerTest extends BaseControllerTest {

    @MockBean
    private LikeReviewActorRoleService likeReviewActorRoleService;

    @Test
    public void testCreateLikeReviewActorRoleValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleCreateDTO likeReviewActorRole = new LikeReviewActorRoleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/like-review-actor-roles/{reviewActorRoleId}",
                regUser.getId(), reviewActorRole.getId())
                .content(objectMapper.writeValueAsString(likeReviewActorRole))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(likeReviewActorRoleService, Mockito.never())
                .createLikeReviewActorRole(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetLikeReviewActorRole() throws Exception {
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleReadDTO read = createLikeReviewActorRoleRead(regUser, reviewActorRole);

        Mockito.when(likeReviewActorRoleService.getLikeReviewActorRole(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/like-review-actor-roles/{id}", regUser.getId(),
                        read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewActorRoleReadDTO actualLikeReviewActorRole = objectMapper
                .readValue(resultJson, LikeReviewActorRoleReadDTO.class);
        Assertions.assertThat(actualLikeReviewActorRole).isEqualToComparingFieldByField(read);

        Mockito.verify(likeReviewActorRoleService).getLikeReviewActorRole(read.getId());
    }

    @Test
    public void testGetRegUserLikeActorRole() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleReadDTO read = createLikeReviewActorRoleRead(regUser, reviewActorRole);

        List<LikeReviewActorRoleReadDTO> expectedLike = List.of(read);

        Mockito.when(likeReviewActorRoleService.getRegLikeReviewActorRoles(regUserId)).thenReturn(expectedLike);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-review-actor-roles", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<LikeReviewActorRoleReadDTO> actualLike = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedLike, actualLike);
        Mockito.verify(likeReviewActorRoleService).getRegLikeReviewActorRoles(regUserId);
    }

    @Test
    public void testGetLikeReviewActorRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID regUserId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(LikeReviewActorRole.class, wrongId);
        Mockito.when(likeReviewActorRoleService.getLikeReviewActorRole(wrongId)).thenThrow(exception);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{regUserId}/like-review-actor-roles/{id}", regUserId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetLikeReviewActorRoleWrongFormatId() throws Exception {
        String error = "likeReviewActorRoleId should be of type java.util.UUID";
        UUID id = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-review-actor-roles/123", id))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateLikeReviewActorRole() throws Exception {
        LikeReviewActorRoleCreateDTO create = new LikeReviewActorRoleCreateDTO();
        create.setUserLike(true);

        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleReadDTO read = createLikeReviewActorRoleRead(regUser, reviewActorRole);

        Mockito.when(likeReviewActorRoleService.createLikeReviewActorRole(regUser.getId(), actorRole.getId(), create))
                .thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/like-review-actor-roles/{id}",
                regUser.getId(), actorRole.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewActorRoleReadDTO actualLikeReviewActorRole = objectMapper
                .readValue(resultJson, LikeReviewActorRoleReadDTO.class);
        Assertions.assertThat(actualLikeReviewActorRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchLikeReviewActorRole() throws Exception {
        LikeReviewActorRolePatchDTO patchDTO = generateObject(LikeReviewActorRolePatchDTO.class);

        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleReadDTO read = createLikeReviewActorRoleRead(regUser, reviewActorRole);

        Mockito.when(likeReviewActorRoleService.patchLikeReviewActorRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/reg-users/{id}/like-review-actor-roles/{id}", regUser.getId(), read.getId()
                        .toString())
                        .content(objectMapper.writeValueAsString(patchDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewActorRoleReadDTO actualLikeReviewActorRole = objectMapper
                .readValue(resultJson, LikeReviewActorRoleReadDTO.class);
        Assert.assertEquals(read, actualLikeReviewActorRole);
    }

    @Test
    public void testUpdateLikeReviewActorRole() throws Exception {
        LikeReviewActorRolePutDTO putDTO = generateObject(LikeReviewActorRolePutDTO.class);

        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);
        LikeReviewActorRoleReadDTO read = createLikeReviewActorRoleRead(regUser, reviewActorRole);

        Mockito.when(likeReviewActorRoleService.updateLikeReviewActorRole(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc
                .perform(put("/api/v1/reg-users/{id}/like-review-actor-roles/{id}", regUser.getId(), read.getId()
                        .toString())
                        .content(objectMapper.writeValueAsString(putDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewActorRoleReadDTO actualLikeReviewActorRole = objectMapper
                .readValue(resultJson, LikeReviewActorRoleReadDTO.class);
        Assert.assertEquals(read, actualLikeReviewActorRole);
    }

    @Test
    public void testDeleteLikeReviewActorRole() throws Exception {
        UUID id = UUID.randomUUID();
        UUID reviewId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/{id}/like-review-actor-roles/{reviewId}", id, reviewId))
                .andExpect(status().isOk());

        Mockito.verify(likeReviewActorRoleService).deleteLikeReviewActorRole(reviewId);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private ActorRoleReadDTO createActorRoleRead(MovieReadDTO movie) {
        ActorRoleReadDTO read = generateObject(ActorRoleReadDTO.class);
        read.setMovieId(movie.getId());
        return read;
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private ReviewActorRoleReadDTO createReviewActorRoleRead(RegUserReadDTO regUser, ActorRoleReadDTO actorRole) {
        ReviewActorRoleReadDTO read = generateObject(ReviewActorRoleReadDTO.class);
        read.setActorRoleId(actorRole.getId());
        read.setRegUserId(regUser.getId());
        return read;
    }

    private LikeReviewActorRoleReadDTO createLikeReviewActorRoleRead(RegUserReadDTO user, ReviewActorRoleReadDTO r) {
        LikeReviewActorRoleReadDTO read = generateObject(LikeReviewActorRoleReadDTO.class);
        read.setRegUserId(user.getId());
        read.setReviewActorRoleId(r.getId());
        return read;
    }
}