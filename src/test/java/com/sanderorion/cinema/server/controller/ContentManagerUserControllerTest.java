package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.domain.ContentManagerUser;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserCreateDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPatchDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPutDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ContentManagerUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ContentManagerUserController.class)
public class ContentManagerUserControllerTest extends BaseControllerTest {

    @MockBean
    private ContentManagerUserService contentManagerUserService;

    @Test
    public void testGetContentManagerUser() throws Exception {
        ContentManagerUserReadDTO regUser = createContentManagerUserPersonRead();

        Mockito.when(contentManagerUserService.getContentManagerUser(regUser.getId())).thenReturn(regUser);

        String resultJson = mvc.perform(get("/api/v1/content-manager-users/{id}", regUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerUserReadDTO actualContentManagerUser = objectMapper
                .readValue(resultJson, ContentManagerUserReadDTO.class);
        Assertions.assertThat(actualContentManagerUser).isEqualToComparingFieldByField(regUser);

        Mockito.verify(contentManagerUserService).getContentManagerUser(regUser.getId());
    }

    @Test
    public void testGetContentManagerUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ContentManagerUser.class, wrongId);
        Mockito.when(contentManagerUserService.getContentManagerUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/content-manager-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetContentManagerUserWrongFormatId() throws Exception {

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/content-manager-users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateContentManagerUser() throws Exception {
        ContentManagerUserCreateDTO create = generateObject(ContentManagerUserCreateDTO.class);

        ContentManagerUserReadDTO read = createContentManagerUserPersonRead();

        Mockito.when(contentManagerUserService.createContentManagerUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-manager-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerUserReadDTO actualContentManagerUser = objectMapper
                .readValue(resultJson, ContentManagerUserReadDTO.class);
        Assertions.assertThat(actualContentManagerUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchContentManagerUser() throws Exception {
        ContentManagerUserPatchDTO patchDTO = generateObject(ContentManagerUserPatchDTO.class);

        ContentManagerUserReadDTO read = createContentManagerUserPersonRead();

        Mockito.when(contentManagerUserService.patchContentManagerUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/content-manager-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerUserReadDTO actualContentManagerUser = objectMapper
                .readValue(resultJson, ContentManagerUserReadDTO.class);
        Assert.assertEquals(read, actualContentManagerUser);
    }

    @Test
    public void testUpdateContentManagerUser() throws Exception {
        ContentManagerUserPutDTO putDTO = generateObject(ContentManagerUserPutDTO.class);

        ContentManagerUserReadDTO read = createContentManagerUserPersonRead();

        Mockito.when(contentManagerUserService.updateContentManagerUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/content-manager-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerUserReadDTO actualContentManagerUser = objectMapper
                .readValue(resultJson, ContentManagerUserReadDTO.class);
        Assert.assertEquals(read, actualContentManagerUser);
    }

    @Test
    public void testDeleteContentManagerUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/content-manager-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(contentManagerUserService).deleteContentManagerUser(id);
    }

    private ContentManagerUserReadDTO createContentManagerUserPersonRead() {
        return generateObject(ContentManagerUserReadDTO.class);
    }
}