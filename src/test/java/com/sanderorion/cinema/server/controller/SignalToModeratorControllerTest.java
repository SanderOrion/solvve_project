package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.SignalToModerator;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.domain.enums.SignalType;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutTrustDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorCreateDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.SignalToModeratorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(SignalToModeratorController.class)
public class SignalToModeratorControllerTest extends BaseControllerTest {

    @MockBean
    private SignalToModeratorService signalToModeratorService;

    @Test
    public void testCreateSignalToModeratorValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieReadDTO review = createReviewMovieRead(regUser, movie);

        SignalToModeratorCreateDTO signal = new SignalToModeratorCreateDTO();

        String resultJson = mvc.perform(post("/api/v1/reviews/{reviewId}/reg-users/{id}", review.getId(),
                regUser.getId())
                .content(objectMapper.writeValueAsString(signal))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(signalToModeratorService, Mockito.never())
                .createSignalToModerator(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetSignalToModeratorMovie() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieReadDTO review = createReviewMovieRead(regUser, movie);

        SignalToModeratorReadDTO signal = createSignalMovie(regUser, review);

        Mockito.when(signalToModeratorService.getSignalToModerator(signal.getId())).thenReturn(signal);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals-to-moderator/{signalId}",
                regUser.getId(), signal.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToModeratorReadDTO actualSignal = objectMapper
                .readValue(resultJson, SignalToModeratorReadDTO.class);
        Assertions.assertThat(actualSignal).isEqualToComparingFieldByField(signal);

        Mockito.verify(signalToModeratorService).getSignalToModerator(signal.getId());
    }

    @Test
    public void testGetSignalToModeratorActorRole() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        ReviewActorRoleReadDTO review = createReviewActorRoleRead(regUser, actorRole);

        SignalToModeratorReadDTO signal = createSignalActorRole(regUser, review);

        Mockito.when(signalToModeratorService.getSignalToModerator(signal.getId())).thenReturn(signal);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals-to-moderator/{signalId}",
                regUser.getId(), signal.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToModeratorReadDTO actualSignal = objectMapper
                .readValue(resultJson, SignalToModeratorReadDTO.class);
        Assertions.assertThat(actualSignal).isEqualToComparingFieldByField(signal);

        Mockito.verify(signalToModeratorService).getSignalToModerator(signal.getId());
    }

    @Test
    public void testGetSignalToModeratorWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(SignalToModerator.class, wrongId);
        Mockito.when(signalToModeratorService.getSignalToModerator(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals-to-moderator/{signalId}", userId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetSignalToModeratorWrongFormatId() throws Exception {
        String error = "signalId should be of type java.util.UUID";
        UUID userId = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/signals-to-moderator/123", userId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testGetRegUserSignalsToModerator() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);

        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);

        SignalToModeratorReadDTO signalMovie = createSignalMovie(regUser, reviewMovie);
        SignalToModeratorReadDTO signalActorRole = createSignalActorRole(regUser, reviewActorRole);

        List<SignalToModeratorReadDTO> expected = List.of(signalMovie, signalActorRole);

        Mockito.when(signalToModeratorService.getRegUserSignalsToModerator(regUser.getId())).thenReturn(expected);

        String resultJson = mvc.perform(get("/api/v1//reg-users/{id}/signals-to-moderator", regUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<SignalToModeratorReadDTO> actualSignal = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expected, actualSignal);
        Mockito.verify(signalToModeratorService).getRegUserSignalsToModerator(regUser.getId());
    }

    @Test
    public void testGetSignalsForCheck() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);

        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);

        SignalToModeratorReadDTO signalMovie = createSignalMovie(regUser, reviewMovie);
        SignalToModeratorReadDTO signalActorRole = createSignalActorRole(regUser, reviewActorRole);

        PageResult<SignalToModeratorReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(2L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(signalMovie, signalActorRole));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(signalToModeratorService.getSignalsForCheck(pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/moderator-users/signals-for-check")
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<SignalToModeratorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testCreateSignalToModeratorMovie() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);

        SignalToModeratorCreateDTO create = generateObject(SignalToModeratorCreateDTO.class);
        SignalToModeratorReadDTO read = createSignalMovie(regUser, reviewMovie);

        Mockito.when(signalToModeratorService.createSignalToModerator(regUser.getId(), reviewMovie.getId(), create))
                .thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reviews/{reviewId}/reg-users/{id}",
                reviewMovie.getId(), regUser.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToModeratorReadDTO actualSignal = objectMapper.readValue(resultJson, SignalToModeratorReadDTO.class);
        Assertions.assertThat(actualSignal).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreateSignalToModeratorActorRole() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        ReviewActorRoleReadDTO reviewActorRole = createReviewActorRoleRead(regUser, actorRole);

        SignalToModeratorCreateDTO create = generateObject(SignalToModeratorCreateDTO.class);
        SignalToModeratorReadDTO read = createSignalActorRole(regUser, reviewActorRole);

        Mockito.when(signalToModeratorService
                .createSignalToModerator(regUser.getId(), reviewActorRole.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reviews/{reviewId}/reg-users/{id}",
                reviewActorRole.getId(), regUser.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToModeratorReadDTO actualSignal = objectMapper.readValue(resultJson, SignalToModeratorReadDTO.class);
        Assertions.assertThat(actualSignal).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testUpdateSignalToModerator() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);

        SignalToModeratorReadDTO read = createSignalMovie(regUser, reviewMovie);
        UUID moderatorId = UUID.randomUUID();

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText(null);

        Mockito.when(signalToModeratorService.updateSignalToModerator(read.getId(), moderatorId, put))
                .thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderator-users/{moderatorId}/signals-for-check/{id}",
                moderatorId, read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToModeratorReadDTO actual = objectMapper.readValue(resultJson, SignalToModeratorReadDTO.class);
        Assert.assertEquals(read, actual);
    }

    @Test
    public void testUpdateUserTrustForReview() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();

        RegUserPutTrustDTO putDTO = new RegUserPutTrustDTO();
        putDTO.setTrustForReview(TrustForReview.CREDIBLE);

        RegUserReadDTO read = generateObject(RegUserReadDTO.class);
        read.setTrustForReview(null);

        Mockito.when(signalToModeratorService.updateUserTrustForReview(regUser.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderator-users/update-users-for-review/reg-users/{id}",
                regUser.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegUserReadDTO actualActorRole = objectMapper.readValue(resultJson, RegUserReadDTO.class);
        Assert.assertEquals(read, actualActorRole);
    }

    @Test
    public void testDeleteSignalToModerator() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1//moderator-users/signals/{id}", id)).andExpect(status().isOk());

        Mockito.verify(signalToModeratorService).deleteSignalToModerator(id);
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private ActorRoleReadDTO createActorRoleRead(MovieReadDTO movie) {
        ActorRoleReadDTO read = generateObject(ActorRoleReadDTO.class);
        read.setMovieId(movie.getId());
        return read;
    }

    private ReviewMovieReadDTO createReviewMovieRead(RegUserReadDTO regUser, MovieReadDTO movie) {
        ReviewMovieReadDTO read = generateObject(ReviewMovieReadDTO.class);
        read.setMovieId(movie.getId());
        read.setRegUserId(regUser.getId());
        read.setBody("Тут есть спойлер");
        return read;
    }

    private ReviewActorRoleReadDTO createReviewActorRoleRead(RegUserReadDTO regUser, ActorRoleReadDTO actorRole) {
        ReviewActorRoleReadDTO read = generateObject(ReviewActorRoleReadDTO.class);
        read.setActorRoleId(actorRole.getId());
        read.setRegUserId(regUser.getId());
        read.setBody("There is spoiler");
        return read;
    }

    private SignalToModeratorReadDTO createSignalMovie(RegUserReadDTO regUser, ReviewMovieReadDTO review) {
        SignalToModeratorReadDTO read = new SignalToModeratorReadDTO();
        read.setId(UUID.randomUUID());
        read.setTypo("spoiler");
        read.setSignalType(SignalType.SPOILER_ALERT);
        read.setSignalStatus(SignalStatus.NEED_TO_FIX);
        read.setRegUserId(regUser.getId());
        read.setReviewMovieId(review.getId());
        return read;
    }

    private SignalToModeratorReadDTO createSignalActorRole(RegUserReadDTO regUser, ReviewActorRoleReadDTO review) {
        SignalToModeratorReadDTO read = new SignalToModeratorReadDTO();
        read.setId(UUID.randomUUID());
        read.setTypo("spoiler");
        read.setSignalType(SignalType.SPOILER_ALERT);
        read.setSignalStatus(SignalStatus.NEED_TO_FIX);
        read.setRegUserId(regUser.getId());
        read.setReviewActorRoleId(review.getId());
        return read;
    }
}