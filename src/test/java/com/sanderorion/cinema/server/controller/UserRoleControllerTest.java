package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.userrole.UserRoleReadDTO;
import com.sanderorion.cinema.server.service.UserRoleService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(UserRoleController.class)
public class UserRoleControllerTest extends BaseControllerTest {

    @MockBean
    private UserRoleService userRoleService;

    @Test
    public void testGetAllUserRoles() throws Exception {
        List<UserRoleReadDTO> expectedUserRoles = new ArrayList<>();
        Mockito.when(userRoleService.getAllUserRoles()).thenReturn(expectedUserRoles);

        String resultJson = mvc.perform(get("/api/v1/user-roles"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUserRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedUserRoles, actualUserRoles);
        Mockito.verify(userRoleService).getAllUserRoles();
    }

    @Test
    public void testGetUserRoles() throws Exception {
        RegUserReadDTO regUser = generateObject(RegUserReadDTO.class);

        UserRoleReadDTO read = generateObject(UserRoleReadDTO.class);
        List<UserRoleReadDTO> expectedUserRoles = List.of(read);

        Mockito.when(userRoleService.getUserRoles(regUser.getId())).thenReturn(expectedUserRoles);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/user-roles", regUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUserRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedUserRoles, actualUserRoles);
        Mockito.verify(userRoleService).getUserRoles(regUser.getId());
    }

    @Test
    public void testAddUserRoleToRegUser() throws Exception {
        UUID regUserId = UUID.randomUUID();

        UserRoleReadDTO read = generateObject(UserRoleReadDTO.class);
        List<UserRoleReadDTO> expectedUserRoles = List.of(read);
        Mockito.when(userRoleService.addUserRoleToRegUser(regUserId, read.getId())).thenReturn(expectedUserRoles);

        String resultJson = mvc
                .perform(post("/api/v1/reg-users/{regUserId}/user-roles/{userRoleId}", regUserId, read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUserRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedUserRoles, actualUserRoles);
    }

    @Test
    public void testRemoveUserRoleFromRegUser() throws Exception {
        UUID regUserId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/{regUserId}/user-roles/{userRoleId}", regUserId, userRoleId.toString()))
                .andExpect(status().isOk());

        Mockito.verify(userRoleService).removeUserRoleToRegUser(regUserId, userRoleId);
    }
}