package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.LikeReviewMovie;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.LikeReviewMovieService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LikeReviewMovieController.class)
public class LikeReviewMovieControllerTest extends BaseControllerTest {

    @MockBean
    private LikeReviewMovieService likeReviewMovieService;

    @Test
    public void testCreateLikeReviewMovieValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        LikeReviewMovieCreateDTO likeReviewMovie = new LikeReviewMovieCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/like-review-movies/{reviewMovieId}",
                regUser.getId(), reviewMovie.getId())
                .content(objectMapper.writeValueAsString(likeReviewMovie))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(likeReviewMovieService, Mockito.never())
                .createLikeReviewMovie(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetLikeReviewMovie() throws Exception {
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        LikeReviewMovieReadDTO read = createLikeReviewMovieRead(regUser, reviewMovie);

        Mockito.when(likeReviewMovieService.getLikeReviewMovie(read.getId())).thenReturn(read);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{id}/like-review-movies/{id}", regUser.getId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewMovieReadDTO actualLikeReviewMovie = objectMapper
                .readValue(resultJson, LikeReviewMovieReadDTO.class);
        Assertions.assertThat(actualLikeReviewMovie).isEqualToComparingFieldByField(read);

        Mockito.verify(likeReviewMovieService).getLikeReviewMovie(read.getId());
    }

    @Test
    public void testGetUserLikeReviewMovie() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        LikeReviewMovieReadDTO read = createLikeReviewMovieRead(regUser, reviewMovie);

        List<LikeReviewMovieReadDTO> expectedLike = List.of(read);

        Mockito.when(likeReviewMovieService.getRegLikeReviewMovies(regUserId)).thenReturn(expectedLike);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/like-review-movies", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<LikeReviewMovieReadDTO> actualLike = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedLike, actualLike);
        Mockito.verify(likeReviewMovieService).getRegLikeReviewMovies(regUserId);
    }

    @Test
    public void testGetLikeReviewMovieWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID regUserId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(LikeReviewMovie.class, wrongId);
        Mockito.when(likeReviewMovieService.getLikeReviewMovie(wrongId)).thenThrow(exception);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/{id}/like-review-movies/{id}", regUserId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetLikeReviewMovieWrongFormatId() throws Exception {
        String error = "likeReviewMovieId should be of type java.util.UUID";
        UUID regUserId = UUID.randomUUID();

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}/like-review-movies/123", regUserId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateLikeReviewMovie() throws Exception {
        LikeReviewMovieCreateDTO create = new LikeReviewMovieCreateDTO();
        create.setUserLike(true);

        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ReviewMovieReadDTO reviewMovie = createReviewMovieRead(regUser, movie);
        LikeReviewMovieReadDTO read = createLikeReviewMovieRead(regUser, reviewMovie);

        Mockito.when(likeReviewMovieService.createLikeReviewMovie(regUser.getId(), reviewMovie.getId(), create))
                .thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/like-review-movies/{reviewMovieId}",
                regUser.getId(), reviewMovie.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewMovieReadDTO actualLikeReviewMovie = objectMapper
                .readValue(resultJson, LikeReviewMovieReadDTO.class);
        Assertions.assertThat(actualLikeReviewMovie).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchLikeReviewMovie() throws Exception {
        LikeReviewMoviePatchDTO patchDTO = generateObject(LikeReviewMoviePatchDTO.class);

        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ReviewMovieReadDTO reviewActorRole = createReviewMovieRead(regUser, movie);
        LikeReviewMovieReadDTO read = createLikeReviewMovieRead(regUser, reviewActorRole);

        Mockito.when(likeReviewMovieService.patchLikeReviewMovie(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc
                .perform(patch("/api/v1/reg-users/{id}/like-review-movies/{id}", regUser.getId(), read.getId()
                        .toString())
                        .content(objectMapper.writeValueAsString(patchDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewMovieReadDTO actualLikeReviewMovie = objectMapper
                .readValue(resultJson, LikeReviewMovieReadDTO.class);
        Assert.assertEquals(read, actualLikeReviewMovie);
    }

    @Test
    public void testUpdateLikeReviewMovie() throws Exception {
        LikeReviewMoviePutDTO putDTO = generateObject(LikeReviewMoviePutDTO.class);

        MovieReadDTO movie = createMovieRead();
        RegUserReadDTO regUser = createRegUserRead();
        ReviewMovieReadDTO reviewActorRole = createReviewMovieRead(regUser, movie);
        LikeReviewMovieReadDTO read = createLikeReviewMovieRead(regUser, reviewActorRole);

        Mockito.when(likeReviewMovieService.updateLikeReviewMovie(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc
                .perform(put("/api/v1/reg-users/{id}/like-review-movies/{id}", regUser.getId(), read.getId()
                        .toString())
                        .content(objectMapper.writeValueAsString(putDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReviewMovieReadDTO actualLikeReviewMovie = objectMapper
                .readValue(resultJson, LikeReviewMovieReadDTO.class);
        Assert.assertEquals(read, actualLikeReviewMovie);
    }

    @Test
    public void testDeleteLikeReviewMovie() throws Exception {
        UUID id = UUID.randomUUID();
        UUID regUserId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/{id}/like-review-movies/{id}", regUserId, id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(likeReviewMovieService).deleteLikeReviewMovie(id);
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }

    private RegUserReadDTO createRegUserRead() {
        RegUserReadDTO read = generateObject(RegUserReadDTO.class);
        return read;
    }

    private ReviewMovieReadDTO createReviewMovieRead(RegUserReadDTO regUser, MovieReadDTO movie) {
        ReviewMovieReadDTO read = generateObject(ReviewMovieReadDTO.class);
        read.setMovieId(movie.getId());
        read.setRegUserId(regUser.getId());
        return read;
    }

    private LikeReviewMovieReadDTO createLikeReviewMovieRead(RegUserReadDTO regUser, ReviewMovieReadDTO reviewMovie) {
        LikeReviewMovieReadDTO read = generateObject(LikeReviewMovieReadDTO.class);
        read.setRegUserId(regUser.getId());
        read.setReviewMovieId(reviewMovie.getId());
        return read;
    }
}