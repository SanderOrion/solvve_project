package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.ReviewActorRole;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ReviewActorRoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ReviewActorRoleController.class)
public class ReviewActorRoleControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewActorRoleService reviewActorRoleService;

    @Test
    public void testCreateReviewActorRoleValidationFailed() throws Exception {
        RegUserReadDTO regUser = createRegUserRead();
        MovieReadDTO movie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(movie);
        ReviewActorRoleCreateDTO review = new ReviewActorRoleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users/{id}/review-actor-roles/{actorRoleId}",
                regUser.getId(), actorRole.getId())
                .content(objectMapper.writeValueAsString(review))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewActorRoleService, Mockito.never())
                .createReviewActorRole(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetReviewActorRole() throws Exception {
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO readActorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewActorRoleReadDTO rActorRole = createReviewActorRoleRead(readRegUser, readActorRole);

        Mockito.when(reviewActorRoleService.getReviewActorRole(rActorRole.getId())).thenReturn(rActorRole);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/review-actor-roles/{id}", rActorRole.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewActorRoleReadDTO actualReviewActorRole = objectMapper
                .readValue(resultJson, ReviewActorRoleReadDTO.class);
        Assertions.assertThat(actualReviewActorRole).isEqualToComparingFieldByField(rActorRole);

        Mockito.verify(reviewActorRoleService).getReviewActorRole(rActorRole.getId());
    }

    @Test
    public void testGetUserReviewActorRoles() throws Exception {
        UUID regUserId = createRegUserRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO readActorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(readRegUser, readActorRole);

        List<ReviewActorRoleReadDTO> expectedReview = List.of(read);

        Mockito.when(reviewActorRoleService.getRegUserReviewActorRoles(regUserId)).thenReturn(expectedReview);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{regUserId}/review-actor-roles", regUserId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ReviewActorRoleReadDTO> actualReview = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedReview, actualReview);
        Mockito.verify(reviewActorRoleService).getRegUserReviewActorRoles(regUserId);
    }

    @Test
    public void testGetAllConfirmedReviewsActorRole() throws Exception {
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(readRegUser, actorRole, ReviewConfirmStatus.CONFIRMED);

        PageResult<ReviewActorRoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(1L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(reviewActorRoleService.getAllConformedReviewsActorRole(actorRole.getId(), pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies/{id}/actor-roles/review-actor-roles", actorRole.getId())
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewActorRoleReadDTO> actualPage = objectMapper
                .readValue(resultJson, new TypeReference<>() {
                });

        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewsActorRoleToConfirm() throws Exception {
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO readActorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(readRegUser, readActorRole,
                ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewActorRoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(1);
        resultPage.setPageSize(25);
        resultPage.setTotalElements(1L);
        resultPage.setTotalPages(1);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(1, 25);
        Mockito.when(reviewActorRoleService.getReviewsActorRoleToConfirm(pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/movies/actor-roles/review-actor-roles-to-confirm")
                .param("page", Integer.toString(1))
                .param("size", Integer.toString(25)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewActorRoleReadDTO> actualPage = objectMapper
                .readValue(resultJson, new TypeReference<>() {
                });

        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewActorRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewActorRole.class, wrongId);
        Mockito.when(reviewActorRoleService.getReviewActorRole(wrongId)).thenThrow(exception);

        String resultJson = mvc
                .perform(get("/api/v1/reg-users/review-actor-roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetReviewActorRoleWrongFormatId() throws Exception {
        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/review-actor-roles/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateReviewActorRole() throws Exception {
        ReviewActorRoleCreateDTO create = generateObject(ReviewActorRoleCreateDTO.class);

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(readMovie);
        RegUserReadDTO regUser = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(regUser, actorRole);

        Mockito.when(reviewActorRoleService.createReviewActorRole(regUser.getId(), actorRole.getId(), create))
                .thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users/{regUserId}/review-actor-roles/{actorRoleId}",
                regUser.getId(), actorRole.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewActorRoleReadDTO actualReviewActorRole = objectMapper
                .readValue(resultJson, ReviewActorRoleReadDTO.class);
        Assertions.assertThat(actualReviewActorRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchReviewActorRole() throws Exception {
        ReviewActorRolePatchDTO patchDTO = generateObject(ReviewActorRolePatchDTO.class);

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO readActorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(readRegUser, readActorRole);

        Mockito.when(reviewActorRoleService
                .patchReviewActorRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/reg-users/review-actor-roles/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewActorRoleReadDTO actualReviewActorRole = objectMapper
                .readValue(resultJson, ReviewActorRoleReadDTO.class);
        Assert.assertEquals(read, actualReviewActorRole);
    }

    @Test
    public void testUpdateReviewActorRole() throws Exception {
        ReviewActorRolePutDTO putDTO = generateObject(ReviewActorRolePutDTO.class);

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO readActorRole = createActorRoleRead(readMovie);
        RegUserReadDTO readRegUser = createRegUserRead();
        RegUserReadDTO moderator = createRegUserRead();
        ReviewActorRoleReadDTO read = createReviewActorRoleRead(readRegUser, readActorRole);

        Mockito.when(reviewActorRoleService.updateReviewActorRole(read.getId(), moderator.getId(), putDTO))
                .thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1//moderator-users/{mId}/review-actor-roles-to-confirm/{id}",
                moderator.getId(), read.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewActorRoleReadDTO actualReviewActorRole = objectMapper
                .readValue(resultJson, ReviewActorRoleReadDTO.class);
        Assert.assertEquals(read, actualReviewActorRole);
    }

    @Test
    public void testDeleteReviewActorRole() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/review-actor-roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewActorRoleService).deleteReviewActorRole(id);
    }

    private ReviewActorRoleReadDTO createReviewActorRoleRead(RegUserReadDTO regUser, ActorRoleReadDTO actorRole) {
        ReviewActorRoleReadDTO read = generateObject(ReviewActorRoleReadDTO.class);
        read.setActorRoleId(actorRole.getId());
        read.setRegUserId(regUser.getId());
        return read;
    }

    private ReviewActorRoleReadDTO createReviewActorRoleRead(RegUserReadDTO regUser, ActorRoleReadDTO actorRole,
                                                             ReviewConfirmStatus checkStatus) {
        ReviewActorRoleReadDTO read = generateObject(ReviewActorRoleReadDTO.class);
        read.setActorRoleId(actorRole.getId());
        read.setRegUserId(regUser.getId());
        read.setConfirmStatus(checkStatus);
        return read;
    }

    private RegUserReadDTO createRegUserRead() {
        return generateObject(RegUserReadDTO.class);
    }

    private ActorRoleReadDTO createActorRoleRead(MovieReadDTO movie) {
        ActorRoleReadDTO read = generateObject(ActorRoleReadDTO.class);
        read.setMovieId(movie.getId());
        return read;
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }
}