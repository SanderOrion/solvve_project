package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.domain.AdminUser;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserCreateDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPatchDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPutDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.AdminUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(AdminUserController.class)
public class AdminUserControllerTest extends BaseControllerTest {

    @MockBean
    private AdminUserService adminUserService;

    @Test
    public void testGetAdminUser() throws Exception {
        AdminUserReadDTO adminUser = createAdminUserPersonRead();

        Mockito.when(adminUserService.getAdminUser(adminUser.getId())).thenReturn(adminUser);

        String resultJson = mvc.perform(get("/api/v1/admin-users/{id}", adminUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminUserReadDTO actualAdminUser = objectMapper.readValue(resultJson, AdminUserReadDTO.class);
        Assertions.assertThat(actualAdminUser).isEqualToComparingFieldByField(adminUser);

        Mockito.verify(adminUserService).getAdminUser(adminUser.getId());
    }

    @Test
    public void testGetAdminUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(AdminUser.class, wrongId);
        Mockito.when(adminUserService.getAdminUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/admin-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetAdminUserWrongFormatId() throws Exception {

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/admin-users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateAdminUser() throws Exception {
        AdminUserCreateDTO create = generateObject(AdminUserCreateDTO.class);

        AdminUserReadDTO read = createAdminUserPersonRead();

        Mockito.when(adminUserService.createAdminUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/admin-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminUserReadDTO actualAdminUser = objectMapper.readValue(resultJson, AdminUserReadDTO.class);
        Assertions.assertThat(actualAdminUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchAdminUser() throws Exception {
        AdminUserPatchDTO patchDTO = generateObject(AdminUserPatchDTO.class);

        AdminUserReadDTO read = createAdminUserPersonRead();

        Mockito.when(adminUserService.patchAdminUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/admin-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminUserReadDTO actualAdminUser = objectMapper.readValue(resultJson, AdminUserReadDTO.class);
        Assert.assertEquals(read, actualAdminUser);
    }

    @Test
    public void testUpdateAdminUser() throws Exception {
        AdminUserPutDTO putDTO = generateObject(AdminUserPutDTO.class);

        AdminUserReadDTO read = createAdminUserPersonRead();

        Mockito.when(adminUserService.updateAdminUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/admin-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminUserReadDTO actualAdminUser = objectMapper.readValue(resultJson, AdminUserReadDTO.class);
        Assert.assertEquals(read, actualAdminUser);
    }

    @Test
    public void testDeleteAdminUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/admin-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(adminUserService).deleteAdminUser(id);
    }

    private AdminUserReadDTO createAdminUserPersonRead() {
        return generateObject(AdminUserReadDTO.class);
    }
}