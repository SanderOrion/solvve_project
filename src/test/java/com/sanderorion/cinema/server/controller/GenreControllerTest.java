package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.dto.genre.GenreReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.service.GenreService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(GenreController.class)
public class GenreControllerTest extends BaseControllerTest {

    @MockBean
    private GenreService genreService;

    @Test
    public void testGetAllMovieGenres() throws Exception {
        List<GenreReadDTO> expectedGenres = new ArrayList<>();
        Mockito.when(genreService.getAllGenres()).thenReturn(expectedGenres);

        String resultJson = mvc.perform(get("/api/v1/genres"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedGenres, actualGenres);
        Mockito.verify(genreService).getAllGenres();
    }

    @Test
    public void testGetGenres() throws Exception {
        MovieReadDTO movie = generateObject(MovieReadDTO.class);

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        List<GenreReadDTO> expectedGenres = List.of(read);

        Mockito.when(genreService.getGenres(movie.getId())).thenReturn(expectedGenres);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/genres", movie.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedGenres, actualGenres);
        Mockito.verify(genreService).getGenres(movie.getId());
    }

    @Test
    public void testAddGenreToMovie() throws Exception {
        UUID movieId = UUID.randomUUID();

        GenreReadDTO read = generateObject(GenreReadDTO.class);
        List<GenreReadDTO> expectedGenres = List.of(read);
        Mockito.when(genreService.addGenreToMovie(movieId, read.getId())).thenReturn(expectedGenres);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/genres{genreId}", movieId, read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedGenres, actualGenres);
    }

    @Test
    public void testRemoveGenreFromMovie() throws Exception {
        UUID movieId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movies/{movieId}/genres{genreId}", movieId, genreId.toString()))
                .andExpect(status().isOk());

        Mockito.verify(genreService).removeGenreFromMovie(movieId, genreId);
    }
}