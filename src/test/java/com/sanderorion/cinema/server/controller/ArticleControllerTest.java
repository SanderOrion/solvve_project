package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.article.*;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ArticleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ArticleController.class)
public class ArticleControllerTest extends BaseControllerTest {

    @MockBean
    private ArticleService articleService;

    @Test
    public void testCreateArticleValidationFailed() throws Exception {
        ArticleCreateDTO article = new ArticleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/articles")
                .content(objectMapper.writeValueAsString(article))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(articleService, Mockito.never()).createArticle(ArgumentMatchers.any());
    }

    @Test
    public void testGetArticle() throws Exception {
        ArticleReadDTO article = createArticleRead();

        Mockito.when(articleService.getArticle(article.getId())).thenReturn(article);

        String resultJson = mvc.perform(get("/api/v1/articles/{id}", article.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ArticleReadDTO actualArticle = objectMapper.readValue(resultJson, ArticleReadDTO.class);
        Assertions.assertThat(actualArticle).isEqualToComparingFieldByField(article);

        Mockito.verify(articleService).getArticle(article.getId());
    }

    @Test
    public void testGetArticles() throws Exception {
        ArticleFilter articleFilter = new ArticleFilter();
        ArticleReadDTO read = createArticleRead();

        PageResult<ArticleReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(read));

        Mockito.when(articleService.getArticles(articleFilter, PageRequest.of(0, defaultPageSize)))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/articles"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ArticleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetArticleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Article.class, wrongId);
        Mockito.when(articleService.getArticle(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/articles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetArticleWrongFormatId() throws Exception {
        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/articles/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateArticle() throws Exception {
        ArticleCreateDTO create = generateObject(ArticleCreateDTO.class);

        ArticleReadDTO read = createArticleRead();

        Mockito.when(articleService.createArticle(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/articles")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ArticleReadExtendedDTO actualArticle = objectMapper.readValue(resultJson, ArticleReadExtendedDTO.class);
        Assertions.assertThat(actualArticle).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchArticle() throws Exception {
        ArticlePatchDTO patchDTO = generateObject(ArticlePatchDTO.class);

        ArticleReadDTO read = createArticleRead();

        Mockito.when(articleService.patchArticle(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/articles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ArticleReadDTO actualArticle = objectMapper.readValue(resultJson, ArticleReadDTO.class);
        Assert.assertEquals(read, actualArticle);
    }

    @Test
    public void testUpdateArticle() throws Exception {
        ArticlePutDTO putDTO = generateObject(ArticlePutDTO.class);

        ArticleReadDTO read = createArticleRead();

        Mockito.when(articleService.updateArticle(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/articles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ArticleReadDTO actualArticle = objectMapper.readValue(resultJson, ArticleReadDTO.class);
        Assert.assertEquals(read, actualArticle);
    }

    @Test
    public void testCreateArticleWrongInfo() throws Exception {
        ArticleCreateDTO create = new ArticleCreateDTO();
        create.setTitle("new movie");
        create.setBody("new movie");
        create.setDateArticle(LocalDateTime.of(2020, 3, 21, 5, 0).toInstant(ZoneOffset.UTC));

        String resultJson = mvc.perform(post("/api/v1/articles")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("title"));
        Assert.assertTrue(errorInfo.getMessage().contains("body"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(articleService, Mockito.never()).createArticle(ArgumentMatchers.any());
    }

    @Test
    public void testDeleteArticle() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/articles/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(articleService).deleteArticle(id);
    }

    @Test
    public void testGetArticlesWithPagingAndSorting() throws Exception {
        ArticleFilter articleFilter = new ArticleFilter();
        ArticleReadDTO read = createArticleRead();

        int page = 1;
        int size = 25;

        PageResult<ArticleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100L);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "dateArticle"));
        Mockito.when(articleService.getArticles(articleFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/articles")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "dateArticle,desc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ArticleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetArticlesWithBigPage() throws Exception {
        ArticleFilter articleFilter = new ArticleFilter();
        ArticleReadDTO read = createArticleRead();

        int page = 0;
        int size = 99999;

        PageResult<ArticleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(100L);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize);
        Mockito.when(articleService.getArticles(articleFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/articles")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ArticleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testAddMovieToArticle() throws Exception {
        UUID articleId = UUID.randomUUID();

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        List<MovieReadDTO> expectedMovies = List.of(read);
        Mockito.when(articleService.addMovieToArticle(articleId, read.getId())).thenReturn(expectedMovies);

        String resultJson = mvc.perform(post("/api/v1/articles/{articleId}/movies/{movieId}", articleId, read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MovieReadDTO> actualMovies = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedMovies, actualMovies);
    }

    @Test
    public void testRemoveMovieFromArticle() throws Exception {
        UUID articleId = UUID.randomUUID();
        UUID movieId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/articles/{articleId}/movies/{movieId}", articleId, movieId))
                .andExpect(status().isOk());

        Mockito.verify(articleService).removeMovieToArticle(articleId, movieId);
    }

    private ArticleReadDTO createArticleRead() {
        return generateObject(ArticleReadDTO.class);
    }
}