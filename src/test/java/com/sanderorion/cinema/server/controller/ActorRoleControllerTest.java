package com.sanderorion.cinema.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePutDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.ActorRoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ActorRoleController.class)
public class ActorRoleControllerTest extends BaseControllerTest {

    @MockBean
    private ActorRoleService actorRoleService;

    @Test
    public void testCreateActorRoleValidationFailed() throws Exception {
        MovieReadDTO movie = createMovieRead();
        ActorRoleCreateDTO actorRole = new ActorRoleCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/actor-roles", movie.getId())
                .content(objectMapper.writeValueAsString(actorRole))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(actorRoleService, Mockito.never())
                .createActorRole(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetActorRole() throws Exception {
        UUID movieId = createMovieRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(readMovie);

        Mockito.when(actorRoleService.getActorRole(actorRole.getId())).thenReturn(actorRole);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/{id}", movieId, actorRole.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assertions.assertThat(actualActorRole).isEqualToComparingFieldByField(actorRole);

        Mockito.verify(actorRoleService).getActorRole(actorRole.getId());
    }

    @Test
    public void testGetMovieActorRoles() throws Exception {
        UUID movieId = createMovieRead().getId();
        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO actorRole = createActorRoleRead(readMovie);

        List<ActorRoleReadDTO> expectedActorRoles = List.of(actorRole);

        Mockito.when(actorRoleService.getMovieActorRoles(movieId)).thenReturn(expectedActorRoles);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles", movieId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ActorRoleReadDTO> actualActorRoles = objectMapper.readValue(resultJson, new TypeReference<>() {
        });

        Assert.assertEquals(expectedActorRoles, actualActorRoles);
        Mockito.verify(actorRoleService).getMovieActorRoles(movieId);
    }

    @Test
    public void testGetActorRoleWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        UUID movieId = createMovieRead().getId();

        EntityNotFoundException exception = new EntityNotFoundException(ActorRole.class, wrongId);
        Mockito.when(actorRoleService.getActorRole(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/{id}", movieId, wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetActorRoleWrongFormatId() throws Exception {
        UUID movieId = UUID.randomUUID();

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/movies/{movieId}/actor-roles/123", movieId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateActorRole() throws Exception {
        ActorRoleCreateDTO create = generateObject(ActorRoleCreateDTO.class);
        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO read = createActorRoleRead(readMovie);

        Mockito.when(actorRoleService.createActorRole(movieId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movies/{movieId}/actor-roles", movieId)
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assertions.assertThat(actualActorRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchActorRole() throws Exception {
        ActorRolePatchDTO patchDTO = generateObject(ActorRolePatchDTO.class);

        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO read = createActorRoleRead(readMovie);

        Mockito.when(actorRoleService.patchActorRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movies/{movieId}/actor-roles/{id}", movieId, read.getId()
                .toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assert.assertEquals(read, actualActorRole);
    }

    @Test
    public void testUpdateActorRole() throws Exception {
        ActorRolePutDTO putDTO = generateObject(ActorRolePutDTO.class);

        UUID movieId = createMovieRead().getId();

        MovieReadDTO readMovie = createMovieRead();
        ActorRoleReadDTO read = createActorRoleRead(readMovie);

        Mockito.when(actorRoleService.updateActorRole(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movies/{movieId}/actor-roles/{id}", movieId, read.getId()
                .toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorRoleReadDTO actualActorRole = objectMapper.readValue(resultJson, ActorRoleReadDTO.class);
        Assert.assertEquals(read, actualActorRole);
    }

    @Test
    public void testDeleteActorRole() throws Exception {
        UUID id = UUID.randomUUID();
        UUID movieId = createMovieRead().getId();

        mvc.perform(delete("/api/v1/movies/{movieId}/actor-roles/{id}", movieId, id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(actorRoleService).deleteActorRole(id);
    }

    private ActorRoleReadDTO createActorRoleRead(MovieReadDTO movie) {
        ActorRoleReadDTO read = generateObject(ActorRoleReadDTO.class);
        read.setMovieId(movie.getId());
        return read;
    }

    private MovieReadDTO createMovieRead() {
        return generateObject(MovieReadDTO.class);
    }
}