package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.reguser.RegUserCreateDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.RegUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(RegUserController.class)
public class RegUserControllerTest extends BaseControllerTest {

    @MockBean
    private RegUserService regUserService;

    @Test
    public void testCreateRegUserValidationFailed() throws Exception {
        RegUserCreateDTO regUser = new RegUserCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/reg-users")
                .content(objectMapper.writeValueAsString(regUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(regUserService, Mockito.never()).createRegUser(ArgumentMatchers.any());
    }

    @Test
    public void testGetRegUser() throws Exception {
        RegUserReadDTO regUser = createRegUserPersonRead();

        Mockito.when(regUserService.getRegUser(regUser.getId())).thenReturn(regUser);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}", regUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegUserReadDTO actualRegUser = objectMapper.readValue(resultJson, RegUserReadDTO.class);
        Assertions.assertThat(actualRegUser).isEqualToComparingFieldByField(regUser);

        Mockito.verify(regUserService).getRegUser(regUser.getId());
    }

    @Test
    public void testGetRegUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RegUser.class, wrongId);
        Mockito.when(regUserService.getRegUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/reg-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetRegUserWrongFormatId() throws Exception {
        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/reg-users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateRegUser() throws Exception {
        RegUserCreateDTO create = generateObject(RegUserCreateDTO.class);

        RegUserReadDTO read = createRegUserPersonRead();

        Mockito.when(regUserService.createRegUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/reg-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegUserReadDTO actualRegUser = objectMapper.readValue(resultJson, RegUserReadDTO.class);
        Assertions.assertThat(actualRegUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegUser() throws Exception {
        RegUserPatchDTO patchDTO = generateObject(RegUserPatchDTO.class);

        RegUserReadDTO read = createRegUserPersonRead();

        Mockito.when(regUserService.patchRegUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/reg-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegUserReadDTO actualRegUser = objectMapper.readValue(resultJson, RegUserReadDTO.class);
        Assert.assertEquals(read, actualRegUser);
    }

    @Test
    public void testUpdateRegUser() throws Exception {
        RegUserPutDTO putDTO = generateObject(RegUserPutDTO.class);

        RegUserReadDTO read = createRegUserPersonRead();

        Mockito.when(regUserService.updateRegUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/reg-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegUserReadDTO actualRegUser = objectMapper.readValue(resultJson, RegUserReadDTO.class);
        Assert.assertEquals(read, actualRegUser);
    }

    @Test
    public void testDeleteRegUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reg-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(regUserService).deleteRegUser(id);
    }

    @Test
    public void testNoSession() throws Exception {
        UUID wrongId = UUID.randomUUID();

        Mockito.when(regUserService.getRegUser(wrongId)).thenReturn(new RegUserReadDTO());

        MvcResult mvcResult = mvc.perform(get("/api/v1/reg-users/{id}", wrongId))
                .andExpect(status().isOk())
                .andReturn();
        Assert.assertNull(mvcResult.getRequest().getSession(false));
    }

    private RegUserReadDTO createRegUserPersonRead() {
        return generateObject(RegUserReadDTO.class);
    }
}