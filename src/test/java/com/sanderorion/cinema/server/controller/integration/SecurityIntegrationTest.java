package com.sanderorion.cinema.server.controller.integration;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.UserRole;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import org.apache.xmlbeans.impl.util.Base64;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Test
    public void testHealthNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:8080/health", Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetArticlesNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/articles", HttpMethod.GET, HttpEntity.EMPTY,
                new ParameterizedTypeReference<PageResult<ArticleReadDTO>>() {
                })).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetArticles() {
        String email = "test@test.com";
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER);
        createUser(email, password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<ArticleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/articles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetArticlesWrongPassword() {
        String email = "test@test.com";
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER);
        createUser(email, password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, "wrong pass"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/articles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<ArticleReadDTO>>() {
                })).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetArticlesWrongUser() {
        String email = "test@test.com";
        String password = "pass123";
        createRegUser(email, password);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue("wrong user", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/articles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<ArticleReadDTO>>() {
                })).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetArticlesNoSession() {
        String email = "test@test.com";
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER);
        createUser(email, password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<ArticleReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/articles", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNull(response.getHeaders().get("Set-Cookie"));
    }

    @Test
    public void testGetRegUserMarkMoviesUser() {
        String email = "test@test.com";
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER);
        RegUser regUser = createUser(email, password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<MarkMovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/reg-users/" + regUser.getId() + "/mark-movies", HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetRegUserMarkMoviesAdmin() {
        String email = "test@test.com";
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.ADMIN);
        RegUser regUser = createUser(email, password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<MarkMovieReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/reg-users/" + regUser.getId() + "/mark-movies", HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetRegUserMarkMoviesWrongUser() {
        String password = "pass123";
        UserRole role = userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER);
        RegUser user1 = createUser("test1@test.com", "abc", role);
        RegUser user2 = createUser("test2@test.com", password, role);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(user2.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/reg-users/" + user1.getId() + "/mark-movies", HttpMethod.GET,
                httpEntity, new ParameterizedTypeReference<List<MarkMovieReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    private RegUser createRegUser(String email, String password) {
        RegUser regUser = new RegUser();
        regUser.setEmail(email);
        regUser.setEncodedPassword(passwordEncoder.encode(password));
        regUser.setName("asdsa");
        regUser.setLogin("343ds");
        regUser.setGender(Gender.MALE);
        return regUserRepository.save(regUser);
    }

    private RegUser createUser(String email, String password, UserRole role) {
        RegUser regUser = new RegUser();
        regUser.setEmail(email);
        regUser.setEncodedPassword(passwordEncoder.encode(password));
        regUser.setName("asdsa");
        regUser.setLogin("343ds");
        regUser.getUserRoles().add(role);
        regUser.setGender(Gender.MALE);
        return regUserRepository.save(regUser);
    }

    private String getBasicAuthorizationHeaderValue(String name, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", name, password).getBytes()));
    }
}