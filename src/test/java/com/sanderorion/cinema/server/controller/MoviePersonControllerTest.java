package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonCreateDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPatchDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPutDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.hander.ErrorInfo;
import com.sanderorion.cinema.server.service.MoviePersonService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(MoviePersonController.class)
public class MoviePersonControllerTest extends BaseControllerTest {

    @MockBean
    private MoviePersonService moviePersonService;

    @Test
    public void testCreateMoviePersonValidationFailed() throws Exception {
        MoviePersonCreateDTO moviePerson = new MoviePersonCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/movie-persons")
                .content(objectMapper.writeValueAsString(moviePerson))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(moviePersonService, Mockito.never()).createMoviePerson(ArgumentMatchers.any());
    }

    @Test
    public void testGetMoviePerson() throws Exception {
        MoviePersonReadDTO moviePerson = createMoviePersonRead();

        Mockito.when(moviePersonService.getMoviePerson(moviePerson.getId())).thenReturn(moviePerson);

        String resultJson = mvc.perform(get("/api/v1/movie-persons/{id}", moviePerson.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MoviePersonReadDTO actualMoviePerson = objectMapper.readValue(resultJson, MoviePersonReadDTO.class);
        Assertions.assertThat(actualMoviePerson).isEqualToComparingFieldByField(moviePerson);

        Mockito.verify(moviePersonService).getMoviePerson(moviePerson.getId());
    }

    @Test
    public void testGetMoviePersonWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MoviePerson.class, wrongId);
        Mockito.when(moviePersonService.getMoviePerson(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/movie-persons/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMoviePersonWrongFormatId() throws Exception {

        String error = "id should be of type java.util.UUID";

        ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class, error);

        String resultJson = mvc.perform(get("/api/v1/movie-persons/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        ErrorInfo actualErrorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualErrorInfo).isEqualToComparingFieldByField(errorInfo);
    }

    @Test
    public void testCreateMoviePerson() throws Exception {
        MoviePersonCreateDTO create = generateObject(MoviePersonCreateDTO.class);

        MoviePersonReadDTO read = createMoviePersonRead();

        Mockito.when(moviePersonService.createMoviePerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/movie-persons")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MoviePersonReadDTO actualMoviePerson = objectMapper.readValue(resultJson, MoviePersonReadDTO.class);
        Assertions.assertThat(actualMoviePerson).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchMoviePerson() throws Exception {
        MoviePersonPatchDTO patchDTO = generateObject(MoviePersonPatchDTO.class);

        MoviePersonReadDTO read = createMoviePersonRead();

        Mockito.when(moviePersonService.patchMoviePerson(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/movie-persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MoviePersonReadDTO actualMoviePerson = objectMapper.readValue(resultJson, MoviePersonReadDTO.class);
        Assert.assertEquals(read, actualMoviePerson);
    }

    @Test
    public void testUpdateMoviePerson() throws Exception {
        MoviePersonPutDTO putDTO = generateObject(MoviePersonPutDTO.class);

        MoviePersonReadDTO read = createMoviePersonRead();

        Mockito.when(moviePersonService.updateMoviePerson(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/movie-persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MoviePersonReadDTO actualMoviePerson = objectMapper.readValue(resultJson, MoviePersonReadDTO.class);
        Assert.assertEquals(read, actualMoviePerson);
    }

    @Test
    public void testDeleteMoviePerson() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/movie-persons/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(moviePersonService).deleteMoviePerson(id);
    }

    private MoviePersonReadDTO createMoviePersonRead() {
        return generateObject(MoviePersonReadDTO.class);
    }
}