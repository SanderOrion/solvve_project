package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.LikeReviewMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LikeReviewMovieServiceTest extends BaseTest {

    @Autowired
    private LikeReviewMovieService likeReviewMovieService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private LikeReviewMovieRepository likeReviewMovieRepository;

    @Test
    public void testGetLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie = createLikeReviewMovie(regUser, reviewMovie);

        LikeReviewMovieReadDTO readDTO = likeReviewMovieService.getLikeReviewMovie(likeReviewMovie.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(likeReviewMovie, "regUserId", "reviewMovieId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getReviewMovieId()).isEqualTo(reviewMovie.getId());
    }

    @Test
    public void testGetRegLikeReviewMovies() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie1 = createReviewMovie(regUser, movie);
        ReviewMovie reviewMovie2 = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie1 = createLikeReviewMovie(regUser, reviewMovie1);
        LikeReviewMovie likeReviewMovie2 = createLikeReviewMovie(regUser, reviewMovie2);

        List<UUID> likeReviewMoviesIds = new ArrayList<>();
        likeReviewMoviesIds.add(likeReviewMovie1.getId());
        likeReviewMoviesIds.add(likeReviewMovie2.getId());

        List<LikeReviewMovieReadDTO> readDTO = likeReviewMovieService.getRegLikeReviewMovies(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(likeReviewMoviesIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetLikeReviewMovieWrongId() {
        likeReviewMovieService.getLikeReviewMovie(UUID.randomUUID());
    }

    @Test
    public void testCreateLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovieCreateDTO create = new LikeReviewMovieCreateDTO();
        create.setUserLike(true);

        LikeReviewMovieReadDTO read = likeReviewMovieService
                .createLikeReviewMovie(regUser.getId(), reviewMovie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        LikeReviewMovie likeReviewMovie = likeReviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(likeReviewMovie, "regUserId", "reviewMovieId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getReviewMovieId()).isEqualTo(reviewMovie.getId());
    }

    @Test
    public void testPatchLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie = createLikeReviewMovie(regUser, reviewMovie);

        LikeReviewMoviePatchDTO patch = new LikeReviewMoviePatchDTO();
        patch.setUserLike(false);

        LikeReviewMovieReadDTO read = likeReviewMovieService.patchLikeReviewMovie(likeReviewMovie.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        likeReviewMovie = likeReviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(likeReviewMovie)
                .isEqualToIgnoringGivenFields(read, "regUser", "reviewMovie", "createdAt", "updatedAt");
        Assertions.assertThat(likeReviewMovie.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(likeReviewMovie.getReviewMovie().getId()).isEqualTo(reviewMovie.getId());
    }

    @Test
    public void testUpdateLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie = createLikeReviewMovie(regUser, reviewMovie);

        LikeReviewMoviePutDTO put = new LikeReviewMoviePutDTO();
        put.setUserLike(false);

        LikeReviewMovieReadDTO read = likeReviewMovieService.updateLikeReviewMovie(likeReviewMovie.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        likeReviewMovie = likeReviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(likeReviewMovie)
                .isEqualToIgnoringGivenFields(read, "regUser", "reviewMovie", "createdAt", "updatedAt");
        Assertions.assertThat(likeReviewMovie.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(likeReviewMovie.getReviewMovie().getId()).isEqualTo(reviewMovie.getId());
    }

    @Test
    public void testPatchReviewMovieEmptyPatch() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie = createLikeReviewMovie(regUser, reviewMovie);

        LikeReviewMoviePatchDTO patch = new LikeReviewMoviePatchDTO();
        LikeReviewMovieReadDTO read = likeReviewMovieService.patchLikeReviewMovie(likeReviewMovie.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        LikeReviewMovie likeReviewMovieUpdate = likeReviewMovieRepository.findById(read.getId()).get();

        Assertions.assertThat(likeReviewMovieUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(likeReviewMovie)
                .isEqualToIgnoringGivenFields(likeReviewMovieUpdate, "regUser", "reviewMovie");
    }

    @Test
    public void testDeleteReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie likeReviewMovie = createLikeReviewMovie(regUser, reviewMovie);

        likeReviewMovieService.deleteLikeReviewMovie(likeReviewMovie.getId());
        Assert.assertFalse(likeReviewMovieRepository.existsById(likeReviewMovie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteLikeReviewMovieNotFound() {
        likeReviewMovieService.deleteLikeReviewMovie(UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie) {
        ReviewMovie reviewMovie = generateFlatEntityWithoutId(ReviewMovie.class);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(movie);
        return reviewMovieRepository.save(reviewMovie);
    }

    private LikeReviewMovie createLikeReviewMovie(RegUser regUser, ReviewMovie reviewMovie) {
        LikeReviewMovie likeReviewMovie = generateFlatEntityWithoutId(LikeReviewMovie.class);
        likeReviewMovie.setRegUser(regUser);
        likeReviewMovie.setReviewMovie(reviewMovie);
        return likeReviewMovieRepository.save(likeReviewMovie);
    }
}