package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonCreateDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPatchDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPutDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.MoviePersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.UUID;

public class MoviePersonServiceTest extends BaseTest {

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private MoviePersonService moviePersonService;

    @Test
    public void testGetMoviePerson() {
        MoviePerson moviePerson = createMoviePerson();

        MoviePersonReadDTO readDTO = moviePersonService.getMoviePerson(moviePerson.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(moviePerson);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMoviePersonWrongId() {
        moviePersonService.getMoviePerson(UUID.randomUUID());
    }

    @Test
    public void testCreateMoviePerson() {
        MoviePersonCreateDTO create = generateObject(MoviePersonCreateDTO.class);

        MoviePersonReadDTO read = moviePersonService.createMoviePerson(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MoviePerson moviePerson = moviePersonRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(moviePerson);
    }

    @Test
    public void testPatchMoviePerson() {
        MoviePerson moviePerson = createMoviePerson();

        MoviePersonPatchDTO patch = generateObject(MoviePersonPatchDTO.class);

        MoviePersonReadDTO read = moviePersonService.patchMoviePerson(moviePerson.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        moviePerson = moviePersonRepository.findById(read.getId()).get();
        Assertions.assertThat(moviePerson).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateMoviePerson() {
        MoviePerson moviePerson = createMoviePerson();

        MoviePersonPutDTO put = generateObject(MoviePersonPutDTO.class);

        MoviePersonReadDTO read = moviePersonService.updateMoviePerson(moviePerson.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        moviePerson = moviePersonRepository.findById(read.getId()).get();
        Assertions.assertThat(moviePerson).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testPatchMoviePersonEmptyPatch() {
        MoviePerson moviePerson = createMoviePerson();

        MoviePersonPatchDTO patch = new MoviePersonPatchDTO();
        MoviePersonReadDTO read = moviePersonService.patchMoviePerson(moviePerson.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        MoviePerson moviePersonUpdate = moviePersonRepository.findById(read.getId()).get();

        Assertions.assertThat(moviePersonUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(moviePerson).isEqualToComparingFieldByField(moviePersonUpdate);
    }

    @Test
    public void testDeleteMoviePerson() {
        MoviePerson moviePerson = createMoviePerson();

        moviePersonService.deleteMoviePerson(moviePerson.getId());
        Assert.assertFalse(moviePersonRepository.existsById(moviePerson.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMoviePersonNotFound() {
        moviePersonService.deleteMoviePerson(UUID.randomUUID());
    }

    private MoviePerson createMoviePerson() {
        MoviePerson moviePerson = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(moviePerson);
    }
}