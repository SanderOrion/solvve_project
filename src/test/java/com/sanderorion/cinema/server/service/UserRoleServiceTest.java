package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.UserRole;
import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import com.sanderorion.cinema.server.dto.userrole.UserRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

public class UserRoleServiceTest extends BaseTest {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetAllUserRoles() {
        List<UserRoleReadDTO> allUserRole = userRoleService.getAllUserRoles();

        int size = allUserRole.size();

        Assertions.assertThat(size).isEqualTo(userRoleRepository.count());
    }

    @Test
    public void testGetUserRoles() {
        RegUser regUser = createRegUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);
        UUID userRoleId2 = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.ADMIN);

        userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId);
        userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId2);

        List<UserRoleReadDTO> userRoles = userRoleService.getUserRoles(regUser.getId());

        Assertions.assertThat(userRoles).extracting("id").containsExactlyInAnyOrder(userRoleId, userRoleId2);
        Assertions.assertThat(userRoles).extracting("userRoleType")
                .containsExactlyInAnyOrder(UserRoleType.REG_USER, UserRoleType.ADMIN);
    }

    @Test
    public void testAddUserRoleToRegUser() {
        RegUser regUser = createRegUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);

        List<UserRoleReadDTO> userRoles = userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId);

        UserRoleReadDTO expectedRead = new UserRoleReadDTO();
        expectedRead.setId(userRoleId);
        expectedRead.setUserRoleType(UserRoleType.REG_USER);
        Assertions.assertThat(userRoles).containsExactly(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            RegUser regUserAfter = regUserRepository.findById(regUser.getId()).get();
            Assertions.assertThat(regUserAfter.getUserRoles()).extracting(UserRole::getId)
                    .containsExactlyInAnyOrder(userRoleId);
        });
    }

    @Test
    public void testDuplicateUserRole() {
        RegUser regUser = createRegUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);

        userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId);

        Assertions.assertThatThrownBy(() -> {
            userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongRegUserId() {
        UUID wrongRegUserId = UUID.randomUUID();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);
        userRoleService.addUserRoleToRegUser(wrongRegUserId, userRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongUserRoleId() {
        RegUser regUser = createRegUser();
        UUID wrongUserRoleId = UUID.randomUUID();
        userRoleService.addUserRoleToRegUser(regUser.getId(), wrongUserRoleId);
    }

    @Test
    public void testRemoveUserRoleFromRegUser() {
        RegUser regUser = createRegUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);

        userRoleService.addUserRoleToRegUser(regUser.getId(), userRoleId);

        List<UserRoleReadDTO> remainingUserRoles = userRoleService
                .removeUserRoleToRegUser(regUser.getId(), userRoleId);
        Assert.assertTrue(remainingUserRoles.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            RegUser regUserAfter = regUserRepository.findById(regUser.getId()).get();
            Assertions.assertThat(regUserAfter.getUserRoles().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedUserRole() {
        RegUser regUser = createRegUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByUserRoleType(UserRoleType.REG_USER);
        userRoleService.removeUserRoleToRegUser(regUser.getId(), userRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedUserRole() {
        RegUser regUser = createRegUser();
        userRoleService.removeUserRoleToRegUser(regUser.getId(), UUID.randomUUID());
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }
}