package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePutDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ActorRoleServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ActorRoleService actorRoleService;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Test
    public void testGetActorRole() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        ActorRoleReadDTO readDTO = actorRoleService.getActorRole(actorRole.getId());
        Assertions.assertThat(readDTO)
                .isEqualToIgnoringGivenFields(actorRole, "movieId", "moviePersonId", "ratingCount");
        Assertions.assertThat(readDTO.getMovieId()).isEqualTo(movie.getId());
        Assertions.assertThat(readDTO.getMoviePersonId()).isEqualTo(moviePerson.getId());
    }

    @Test
    public void testGetMovieActorRoles() {
        Movie movie = createMovie();
        MoviePerson moviePerson1 = createMoviePerson();
        MoviePerson moviePerson2 = createMoviePerson();
        ActorRole actorRole1 = createActorRole(movie, moviePerson1);
        ActorRole actorRole2 = createActorRole(movie, moviePerson2);

        List<UUID> actorRolesIds = new ArrayList<>();
        actorRolesIds.add(actorRole1.getId());
        actorRolesIds.add(actorRole2.getId());

        List<ActorRoleReadDTO> readDTO = actorRoleService.getMovieActorRoles(movie.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(actorRolesIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorRoleWrongId() {
        actorRoleService.getActorRole(UUID.randomUUID());
    }

    @Test
    public void testCreateActorRole() {
        Movie movie = createMovie();
        MoviePerson mp = createMoviePerson();
        ActorRoleCreateDTO create = generateObject(ActorRoleCreateDTO.class);
        create.setMoviePersonId(mp.getId());

        ActorRoleReadDTO read = actorRoleService.createActorRole(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ActorRole actorRole = actorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actorRole, "movieId", "moviePersonId");
        Assertions.assertThat(read.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchActorRole() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        MoviePerson moviePerson2 = createMoviePerson();
        ActorRolePatchDTO patch = generateObject(ActorRolePatchDTO.class);
        patch.setMoviePersonId(moviePerson2.getId());

        ActorRoleReadDTO read = actorRoleService.patchActorRole(actorRole.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        actorRole = actorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(actorRole)
                .isEqualToIgnoringGivenFields(read, "movie", "moviePerson", "createdAt", "updatedAt");
        Assertions.assertThat(actorRole.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testUpdateActorRole() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        MoviePerson moviePerson2 = createMoviePerson();
        ActorRolePutDTO put = generateObject(ActorRolePutDTO.class);
        put.setMoviePersonId(moviePerson2.getId());

        ActorRoleReadDTO read = actorRoleService.updateActorRole(actorRole.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        actorRole = actorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(actorRole)
                .isEqualToIgnoringGivenFields(read, "movie", "moviePerson", "createdAt", "updatedAt");
        Assertions.assertThat(actorRole.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchActorRoleEmptyPatch() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        ActorRolePatchDTO patch = new ActorRolePatchDTO();
        ActorRoleReadDTO read = actorRoleService.patchActorRole(actorRole.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        ActorRole actorRoleUpdate = actorRoleRepository.findById(read.getId()).get();

        Assertions.assertThat(actorRoleUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(actorRole).isEqualToIgnoringGivenFields(actorRoleUpdate, "movie", "moviePerson");
    }

    @Test
    public void testDeleteActorRole() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        actorRoleService.deleteActorRole(actorRole.getId());
        Assert.assertFalse(actorRoleRepository.existsById(actorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteActorRoleNotFound() {
        actorRoleService.deleteActorRole(UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageMarkOfActorRole() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        ActorRole a1 = createActorRole(m1);

        createMarkActorRole(r1, a1, 4);
        createMarkActorRole(r1, a1, 2);

        actorRoleService.updateAverageRatingOfActorRole(a1.getId());
        a1 = actorRoleRepository.findById(a1.getId()).get();
        Assert.assertEquals(3, a1.getAvgRating(), Double.MIN_NORMAL);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieWrongId() {
        actorRoleService.updateAverageRatingOfActorRole(UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private ActorRole createActorRole(Movie movie, MoviePerson moviePerson) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        actorRole.setMoviePerson(moviePerson);
        return actorRoleRepository.save(actorRole);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson moviePerson = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(moviePerson);
    }

    private MarkActorRole createMarkActorRole(RegUser regUser, ActorRole actorRole, Integer mark) {
        MarkActorRole markActorRole = new MarkActorRole();
        markActorRole.setRegUser(regUser);
        markActorRole.setActorRole(actorRole);
        markActorRole.setMark(mark);
        return markActorRoleRepository.save(markActorRole);
    }
}