package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import com.sanderorion.cinema.server.dto.article.ArticleCreateDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePatchDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePutDTO;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ArticleServiceTest extends BaseTest {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Test
    public void testGetArticle() {
        Article article = createArticle();

        ArticleReadDTO readDTO = articleService.getArticle(article.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(article, "amountLike", "amountDislike");
    }

    @Test
    public void testGetArticleByTime() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();

        Article a1 = createArticle(m1, 10);
        createArticle(m2, 15);
        createArticle(m2, 20);

        List<Article> res = articleRepository.findByDateArticleBetween(createInstant(9), createInstant(13));
        Assertions.assertThat(res).extracting(Article::getId).isEqualTo(Arrays.asList(a1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetArticleWrongId() {
        articleService.getArticle(UUID.randomUUID());
    }

    @Test
    public void testCreateArticle() {
        ArticleCreateDTO create = generateObject(ArticleCreateDTO.class);

        ArticleReadDTO read = articleService.createArticle(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Article article = articleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(article);
    }

    @Test
    public void testPatchArticle() {
        Article article = createArticle();
        ArticlePatchDTO patch = generateObject(ArticlePatchDTO.class);

        ArticleReadDTO read = articleService.patchArticle(article.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        article = articleRepository.findById(read.getId()).get();
        Assertions.assertThat(article)
                .isEqualToIgnoringGivenFields(read, "movies", "moviePersons", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateArticle() {
        Article article = createArticle();
        ArticlePutDTO put = generateObject(ArticlePutDTO.class);

        ArticleReadDTO read = articleService.updateArticle(article.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        article = articleRepository.findById(read.getId()).get();
        Assertions.assertThat(article)
                .isEqualToIgnoringGivenFields(read, "movies", "moviePersons", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchArticleEmptyPatch() {
        Article article = createArticle();

        ArticlePatchDTO patch = new ArticlePatchDTO();
        ArticleReadDTO read = articleService.patchArticle(article.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Article articleUpdate = articleRepository.findById(read.getId()).get();

        Assertions.assertThat(articleUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(article).isEqualToIgnoringGivenFields(articleUpdate, "movies", "moviePersons");
    }

    @Test
    public void testDeleteArticle() {
        Article article = createArticle();

        articleService.deleteArticle(article.getId());
        Assert.assertFalse(articleRepository.existsById(article.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteArticleNotFound() {
        articleService.deleteArticle(UUID.randomUUID());
    }

    @Test
    public void testGetArticlesWithEmptyFilter() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        Article a1 = createArticle(m1, mp1);
        Article a2 = createArticle(m1, mp2);
        Article a3 = createArticle(m2, mp2);

        ArticleFilter filter = new ArticleFilter();
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a1.getId(), a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticleByMovie() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        Article a1 = createArticle(m1, mp1);
        Article a2 = createArticle(m1, mp2);
        createArticle(m2, mp2);

        ArticleFilter filter = new ArticleFilter();
        filter.setMovieId(m1.getId());
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a1.getId(), a2.getId());
    }

    @Test
    public void testGetArticleByMoviePerson() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        createArticle(m1, mp1);
        Article a2 = createArticle(m1, mp2);
        Article a3 = createArticle(m2, mp2);

        ArticleFilter filter = new ArticleFilter();
        filter.setMoviePersonId(mp2.getId());
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticleByTitle() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        createArticle(m1, mp1, "new");
        Article a2 = createArticle(m1, mp2, "new Movie");
        Article a3 = createArticle(m2, mp2, "new MoviePerson");

        ArticleFilter filter = new ArticleFilter();
        filter.setTitle(Set.of("new Movie", "new MoviePerson"));
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticleByBody() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        createArticleBody(m1, mp1, "new");
        Article a2 = createArticleBody(m1, mp2, "new Movie coming soon");
        Article a3 = createArticleBody(m2, mp2, "new MoviePerson will play");

        ArticleFilter filter = new ArticleFilter();
        filter.setBody(Set.of("new Movie coming soon", "new MoviePerson will play"));
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticleByDateArticle() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        createArticleFromFilter(m1, mp1, 5);
        Article a2 = createArticleFromFilter(m1, mp2, 6);
        Article a3 = createArticleFromFilter(m2, mp2, 6);

        ArticleFilter filter = new ArticleFilter();
        filter.setDateArticle(createInstant(6));
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticlesByAllFilter() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        createArticleAll(m2, mp1, "new", "new", 5);
        Article a2 = createArticleAll(m1, mp2, "new Movie", "new Movie coming soon", 6);
        Article a3 = createArticleAll(m1, mp2, "new MoviePerson", "new MoviePerson will play", 6);

        ArticleFilter filter = new ArticleFilter();
        filter.setMovieId(m1.getId());
        filter.setMoviePersonId(mp2.getId());
        filter.setTitle(Set.of("new Movie", "new MoviePerson"));
        filter.setBody(Set.of("new Movie coming soon", "new MoviePerson will play"));
        filter.setDateArticle(createInstant(6));
        Assertions.assertThat(articleService.getArticles(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(a2.getId(), a3.getId());
    }

    @Test
    public void testGetArticlesWithEmptyFilterWithPagingAndSorting() {
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        MoviePerson mp1 = createMoviePerson();
        MoviePerson mp2 = createMoviePerson();

        Article a1 = createArticleFromFilter(m1, mp1, 5);
        Article a2 = createArticleFromFilter(m1, mp2, 6);
        createArticleFromFilter(m2, mp2, 7);
        createArticleFromFilter(m2, mp2, 8);

        ArticleFilter filter = new ArticleFilter();
        PageRequest pageRequest = PageRequest.of(1, 2, Sort.by(Sort.Direction.DESC, "dateArticle"));
        Assertions.assertThat(articleService.getArticles(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(a2.getId(), a1.getId()));
    }

    @Test
    public void testAddMovieToArticle() {
        Article article = createArticle();
        Movie movie = createMovie();

        List<MovieReadDTO> res = articleService.addMovieToArticle(article.getId(), movie.getId());

        MovieReadDTO expectedRead = new MovieReadDTO();
        expectedRead.setId(movie.getId());
        expectedRead.setOriginalTitle(movie.getOriginalTitle());
        expectedRead.setTitle(movie.getTitle());
        expectedRead.setImdbId(movie.getImdbId());
        expectedRead.setTagline(movie.getTagline());
        expectedRead.setOverview(movie.getOverview());
        expectedRead.setReleaseDate(movie.getReleaseDate());
        expectedRead.setStatus(movie.getStatus());
        expectedRead.setCountry(movie.getCountry());
        expectedRead.setDuration(movie.getDuration());
        expectedRead.setLanguage(movie.getLanguage());
        expectedRead.setAvgRating(movie.getAvgRating());
        expectedRead.setRatingCount(movie.getRatingCount());
        expectedRead.setBudget(movie.getBudget());
        expectedRead.setRevenue(movie.getRevenue());
        expectedRead.setCreatedAt(movie.getCreatedAt());
        expectedRead.setUpdatedAt(movie.getUpdatedAt());

        Assertions.assertThat(res).containsExactly(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Article articleAfter = articleRepository.findById(article.getId()).get();
            Assertions.assertThat(articleAfter.getMovies()).extracting(Movie::getId)
                    .containsExactlyInAnyOrder(movie.getId());
        });
    }

    @Test
    public void testDuplicateMovie() {
        Article article = createArticle();
        Movie movie = createMovie();

        articleService.addMovieToArticle(article.getId(), movie.getId());

        Assertions.assertThatThrownBy(() -> {
            articleService.addMovieToArticle(article.getId(), movie.getId());
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongArticleId() {
        UUID wrongArticleId = UUID.randomUUID();
        Movie movie = createMovie();
        articleService.addMovieToArticle(wrongArticleId, movie.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        Article article = createArticle();
        UUID wrongMovieId = UUID.randomUUID();
        articleService.addMovieToArticle(article.getId(), wrongMovieId);
    }

    @Test
    public void testRemoveMovieFromArticle() {
        Article article = createArticle();
        Movie movie = createMovie();

        articleService.addMovieToArticle(article.getId(), movie.getId());

        List<MovieReadDTO> remainingMovies = articleService.removeMovieToArticle(article.getId(), movie.getId());
        Assert.assertTrue(remainingMovies.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Article articleAfter = articleRepository.findById(article.getId()).get();
            Assertions.assertThat(articleAfter.getMovies().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedMovie() {
        Article article = createArticle();
        UUID movieId = UUID.randomUUID();
        articleService.removeMovieToArticle(article.getId(), movieId);
    }

    @Test
    public void testUpdateLikeAndDislikeOfArticle() {
        Article a1 = createArticleForLike();
        RegUser r1 = createRegUser();

        createLike(a1, r1, true);
        createLike(a1, r1, true);
        createLike(a1, r1, true);
        createLike(a1, r1, false);
        createLike(a1, r1, false);

        articleService.getArticle(a1.getId());
        a1 = articleRepository.findById(a1.getId()).get();

        Integer amountLike = 3;
        Integer amountDislike = 2;
        Assert.assertEquals(amountLike, a1.getAmountLike());
        Assert.assertEquals(amountDislike, a1.getAmountDislike());
    }

    private LikeArticle createLike(Article article, RegUser regUser, Boolean like) {
        LikeArticle likeArticle = new LikeArticle();
        likeArticle.setArticle(article);
        likeArticle.setRegUser(regUser);
        likeArticle.setUserLike(like);
        return likeArticleRepository.save(likeArticle);
    }

    private Article createArticleForLike() {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setAmountLike(0);
        article.setAmountDislike(0);
        return articleRepository.save(article);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private Article createArticle() {
        Article article = generateFlatEntityWithoutId(Article.class);
        return articleRepository.save(article);
    }

    private Article createArticle(Movie movie, int dateArticle) {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setMovies(List.of(movie));
        article.setDateArticle(createInstant(dateArticle));
        return articleRepository.save(article);
    }

    private Article createArticle(Movie movie, MoviePerson moviePerson) {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setMovies(List.of(movie));
        article.setMoviePersons(List.of(moviePerson));
        return articleRepository.save(article);
    }

    private Article createArticle(Movie movie, MoviePerson moviePerson, String title) {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setTitle(title);
        article.setMovies(List.of(movie));
        article.setMoviePersons(List.of(moviePerson));
        return articleRepository.save(article);
    }

    private Article createArticleBody(Movie movie, MoviePerson moviePerson, String body) {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setBody(body);
        article.setMovies(List.of(movie));
        article.setMoviePersons(List.of(moviePerson));
        return articleRepository.save(article);
    }

    private Article createArticleAll(Movie movie, MoviePerson moviePerson, String t, String b, int dateArticle) {
        Article article = new Article();
        article.setTitle(t);
        article.setBody(b);
        article.setMovies(List.of(movie));
        article.setMoviePersons(List.of(moviePerson));
        article.setDateArticle(createInstant(dateArticle));
        return articleRepository.save(article);
    }

    private Article createArticleFromFilter(Movie movie, MoviePerson moviePerson, int dateArticle) {
        Article article = generateFlatEntityWithoutId(Article.class);
        article.setMovies(List.of(movie));
        article.setMoviePersons(List.of(moviePerson));
        article.setDateArticle(createInstant(dateArticle));
        return articleRepository.save(article);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson moviePerson = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(moviePerson);
    }

    private Instant createInstant(int hour) {
        return LocalDateTime.of(2020, 3, 21, hour, 0).toInstant(ZoneOffset.UTC);
    }
}