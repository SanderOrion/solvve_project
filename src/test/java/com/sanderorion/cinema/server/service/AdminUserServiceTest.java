package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.AdminUser;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserCreateDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPatchDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPutDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.AdminUserRepository;
import com.sanderorion.cinema.server.repository.BaseTest;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class AdminUserServiceTest extends BaseTest {

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private AdminUserService adminUserService;

    @Test
    public void testGetAdminUser() {
        AdminUser adminUser = createAdminUser();

        AdminUserReadDTO readDTO = adminUserService.getAdminUser(adminUser.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(adminUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetAdminUserWrongId() {
        adminUserService.getAdminUser(UUID.randomUUID());
    }

    @Test
    public void testCreateAdminUser() {
        AdminUserCreateDTO create = generateObject(AdminUserCreateDTO.class);

        AdminUserReadDTO read = adminUserService.createAdminUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        AdminUser adminUser = adminUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(adminUser);
    }

    @Test
    public void testPatchAdminUser() {
        AdminUser adminUser = createAdminUser();

        AdminUserPatchDTO patch = generateObject(AdminUserPatchDTO.class);

        AdminUserReadDTO read = adminUserService.patchAdminUser(adminUser.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        adminUser = adminUserRepository.findById(read.getId()).get();
        Assertions.assertThat(adminUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateAdminUser() {
        AdminUser adminUser = createAdminUser();

        AdminUserPutDTO put = generateObject(AdminUserPutDTO.class);

        AdminUserReadDTO read = adminUserService.updateAdminUser(adminUser.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        adminUser = adminUserRepository.findById(read.getId()).get();
        Assertions.assertThat(adminUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testPatchAdminUserEmptyPatch() {
        AdminUser adminUser = createAdminUser();

        AdminUserPatchDTO patch = new AdminUserPatchDTO();
        AdminUserReadDTO read = adminUserService.patchAdminUser(adminUser.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        AdminUser adminUserUpdate = adminUserRepository.findById(read.getId()).get();

        Assertions.assertThat(adminUserUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(adminUser).isEqualToComparingFieldByField(adminUserUpdate);
    }

    @Test
    public void testDeleteAdminUser() {
        AdminUser adminUser = createAdminUser();

        adminUserService.deleteAdminUser(adminUser.getId());
        Assert.assertFalse(adminUserRepository.existsById(adminUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteAdminUserNotFound() {
        adminUserService.deleteAdminUser(UUID.randomUUID());
    }

    private AdminUser createAdminUser() {
        AdminUser adminUser = generateFlatEntityWithoutId(AdminUser.class);
        return adminUserRepository.save(adminUser);
    }
}