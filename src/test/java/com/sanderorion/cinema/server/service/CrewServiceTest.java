package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Crew;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.Profession;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPatchDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.CrewRepository;
import com.sanderorion.cinema.server.repository.MoviePersonRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrewServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private CrewService crewService;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Test
    public void testGetCrew() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        Crew crew = createCrew(movie, moviePerson);

        CrewReadDTO readDTO = crewService.getCrew(crew.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(crew, "movieId", "moviePersonId");
        Assertions.assertThat(readDTO.getMovieId()).isEqualTo(movie.getId());
        Assertions.assertThat(readDTO.getMoviePersonId()).isEqualTo(moviePerson.getId());
    }

    @Test
    public void testGetMovieCrews() {
        Movie movie = createMovie();
        MoviePerson moviePerson1 = createMoviePerson();
        MoviePerson moviePerson2 = createMoviePerson();
        Crew crew1 = createCrew(movie, moviePerson1);
        Crew crew2 = createCrew(movie, moviePerson2);

        List<UUID> crewsIds = new ArrayList<>();
        crewsIds.add(crew1.getId());
        crewsIds.add(crew2.getId());

        List<CrewReadDTO> readDTO = crewService.getMovieCrews(movie.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(crewsIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCrewWrongId() {
        crewService.getCrew(UUID.randomUUID());
    }

    @Test
    public void testCreateCrew() {
        Movie movie = createMovie();
        MoviePerson mp = createMoviePerson();
        CrewCreateDTO create = generateObject(CrewCreateDTO.class);
        create.setMoviePersonId(mp.getId());

        CrewReadDTO read = crewService.createCrew(movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Crew crew = crewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(crew, "movieId", "moviePersonId");
        Assertions.assertThat(read.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchCrew() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        Crew crew = createCrew(movie, moviePerson);

        MoviePerson moviePerson2 = createMoviePerson();
        CrewPatchDTO patch = generateObject(CrewPatchDTO.class);
        patch.setMoviePersonId(moviePerson2.getId());

        CrewReadDTO read = crewService.patchCrew(crew.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        crew = crewRepository.findById(read.getId()).get();
        Assertions.assertThat(crew)
                .isEqualToIgnoringGivenFields(read, "movie", "moviePerson", "createdAt", "updatedAt");
        Assertions.assertThat(crew.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testUpdateCrew() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        Crew crew = createCrew(movie, moviePerson);

        MoviePerson moviePerson2 = createMoviePerson();
        CrewPutDTO put = generateObject(CrewPutDTO.class);
        put.setMoviePersonId(moviePerson2.getId());

        CrewReadDTO read = crewService.updateCrew(crew.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        crew = crewRepository.findById(read.getId()).get();
        Assertions.assertThat(crew)
                .isEqualToIgnoringGivenFields(read, "movie", "moviePerson", "createdAt", "updatedAt");
        Assertions.assertThat(crew.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchCrewEmptyPatch() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        Crew crew = createCrew(movie, moviePerson);

        CrewPatchDTO patch = new CrewPatchDTO();
        CrewReadDTO read = crewService.patchCrew(crew.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Crew crewUpdate = crewRepository.findById(read.getId()).get();

        Assertions.assertThat(crewUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(crew).isEqualToIgnoringGivenFields(crewUpdate, "movie", "moviePerson");
    }

    @Test
    public void testDeleteCrew() {
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        Crew crew = createCrew(movie, moviePerson);

        crewService.deleteCrew(crew.getId());
        Assert.assertFalse(crewRepository.existsById(crew.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCrewNotFound() {
        crewService.deleteCrew(UUID.randomUUID());
    }

    private Crew createCrew(Movie movie, MoviePerson moviePerson) {
        Crew crew = generateFlatEntityWithoutId(Crew.class);
        crew.setMovie(movie);
        crew.setMoviePerson(moviePerson);
        return crewRepository.save(crew);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson moviePerson = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(moviePerson);
    }
}