package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieCreateDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MarkMovieServiceTest extends BaseTest {

    @Autowired
    private MarkMovieService markMovieService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Test
    public void testGetMarkMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MarkMovie markMovie = createMarkMovie(regUser, movie);

        MarkMovieReadDTO readDTO = markMovieService.getMarkMovie(markMovie.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(markMovie, "regUserId", "movieId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testGetMarkMovies() {
        RegUser regUser = createRegUser();
        Movie movie1 = createMovie();
        Movie movie2 = createMovie();
        MarkMovie markMovie1 = createMarkMovie(regUser, movie1);
        MarkMovie markMovie2 = createMarkMovie(regUser, movie2);

        List<UUID> markMoviesIds = new ArrayList<>();
        markMoviesIds.add(markMovie1.getId());
        markMoviesIds.add(markMovie2.getId());

        List<MarkMovieReadDTO> readDTO = markMovieService.getUserMarksMovies(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(markMoviesIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMarkMovieWrongId() {
        markMovieService.getMarkMovie(UUID.randomUUID());
    }

    @Test
    public void testCreateMarkMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MarkMovieCreateDTO create = new MarkMovieCreateDTO();
        create.setMark(5);

        MarkMovieReadDTO read = markMovieService.createMarkMovie(regUser.getId(), movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MarkMovie markMovie = markMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(markMovie, "regUserId", "movieId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testDeleteMarkMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MarkMovie markMovie = createMarkMovie(regUser, movie);

        markMovieService.deleteMarkMovie(markMovie.getId());
        Assert.assertFalse(markMovieRepository.existsById(markMovie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMarkMovieNotFound() {
        markMovieService.deleteMarkMovie(UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MarkMovie createMarkMovie(RegUser regUser, Movie movie) {
        MarkMovie markMovie = generateFlatEntityWithoutId(MarkMovie.class);
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        return markMovieRepository.save(markMovie);
    }
}