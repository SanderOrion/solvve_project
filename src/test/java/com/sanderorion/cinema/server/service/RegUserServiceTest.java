package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import com.sanderorion.cinema.server.dto.reguser.RegUserCreateDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

public class RegUserServiceTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private RegUserService regUserService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetRegUser() {
        RegUser regUser = createRegUser();
        RegUserReadDTO readDTO = regUserService.getRegUser(regUser.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(regUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegUserWrongId() {
        regUserService.getRegUser(UUID.randomUUID());
    }

    @Test
    public void testCreateRegUser() {
        RegUserCreateDTO create = generateObject(RegUserCreateDTO.class);

        RegUserReadDTO read = regUserService.createRegUser(create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "password");
        Assert.assertNotNull(read.getId());
        transactionTemplate.executeWithoutResult(status -> {
            RegUser regUser = regUserRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToComparingFieldByField(regUser);
            Assertions.assertThat(regUser.getUserRoles()).extracting("userRoleType")
                    .containsExactly(UserRoleType.REG_USER);
        });
    }

    @Test
    public void testPatchRegUser() {
        RegUser regUser = createRegUser();

        RegUserPatchDTO patch = generateObject(RegUserPatchDTO.class);

        RegUserReadDTO read = regUserService.patchRegUser(regUser.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        regUser = regUserRepository.findById(read.getId()).get();
        Assertions.assertThat(regUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt",
                "reviewMovies", "reviewActorRoles", "likeArticles", "likeReviewActorRoles", "likeReviewMovies",
                "markActorRoles", "markMovies", "signalToContentManagers", "userRoles", "signalToModerators");
    }

    @Test
    public void testUpdateRegUser() {
        RegUser regUser = createRegUser();

        RegUserPutDTO put = generateObject(RegUserPutDTO.class);

        RegUserReadDTO read = regUserService.updateRegUser(regUser.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        regUser = regUserRepository.findById(read.getId()).get();
        Assertions.assertThat(regUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt",
                "reviewMovies", "reviewActorRoles", "likeArticles", "likeReviewActorRoles", "likeReviewMovies",
                "markActorRoles", "markMovies", "signalToContentManagers", "userRoles", "signalToModerators");
    }

    @Test
    public void testPatchRegUserEmptyPatch() {
        RegUser regUser = createRegUser();

        RegUserPatchDTO patch = new RegUserPatchDTO();
        RegUserReadDTO read = regUserService.patchRegUser(regUser.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        RegUser regUserUpdate = regUserRepository.findById(read.getId()).get();

        Assertions.assertThat(regUserUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(regUser).isEqualToIgnoringGivenFields(regUserUpdate, "reviewMovies",
                "reviewActorRoles", "likeArticles", "likeReviewActorRoles", "likeReviewMovies",
                "markActorRoles", "markMovies", "signalToContentManagers", "userRoles", "signalToModerators");
    }

    @Test
    public void testDeleteRegUser() {
        RegUser regUser = createRegUser();

        regUserService.deleteRegUser(regUser.getId());
        Assert.assertFalse(regUserRepository.existsById(regUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRegUserNotFound() {
        regUserService.deleteRegUser(UUID.randomUUID());
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }
}