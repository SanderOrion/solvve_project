package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ContentManagerUser;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserCreateDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPatchDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPutDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.ContentManagerUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ContentManagerUserServiceTest extends BaseTest {

    @Autowired
    private ContentManagerUserRepository contentManagerUserRepository;

    @Autowired
    private ContentManagerUserService contentManagerUserService;

    @Test
    public void testGetContentManagerUser() {
        ContentManagerUser contentManagerUser = createContentManagerUser();

        ContentManagerUserReadDTO readDTO = contentManagerUserService
                .getContentManagerUser(contentManagerUser.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(contentManagerUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetContentManagerUserWrongId() {
        contentManagerUserService.getContentManagerUser(UUID.randomUUID());
    }

    @Test
    public void testCreateContentManagerUser() {
        ContentManagerUserCreateDTO create = generateObject(ContentManagerUserCreateDTO.class);

        ContentManagerUserReadDTO read = contentManagerUserService.createContentManagerUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ContentManagerUser contentManagerUser = contentManagerUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(contentManagerUser);
    }

    @Test
    public void testPatchContentManagerUser() {
        ContentManagerUser contentManagerUser = createContentManagerUser();

        ContentManagerUserPatchDTO patch = generateObject(ContentManagerUserPatchDTO.class);

        ContentManagerUserReadDTO read = contentManagerUserService
                .patchContentManagerUser(contentManagerUser.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        contentManagerUser = contentManagerUserRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManagerUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateContentManagerUser() {
        ContentManagerUser contentManagerUser = createContentManagerUser();

        ContentManagerUserPutDTO put = generateObject(ContentManagerUserPutDTO.class);

        ContentManagerUserReadDTO read = contentManagerUserService
                .updateContentManagerUser(contentManagerUser.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        contentManagerUser = contentManagerUserRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManagerUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testPatchContentManagerUserEmptyPatch() {
        ContentManagerUser contentManagerUser = createContentManagerUser();

        ContentManagerUserPatchDTO patch = new ContentManagerUserPatchDTO();
        ContentManagerUserReadDTO read = contentManagerUserService
                .patchContentManagerUser(contentManagerUser.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        ContentManagerUser contentManagerUserUpdate = contentManagerUserRepository.findById(read.getId()).get();

        Assertions.assertThat(contentManagerUserUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(contentManagerUser).isEqualToComparingFieldByField(contentManagerUserUpdate);
    }

    @Test
    public void testDeleteContentManagerUser() {
        ContentManagerUser contentManagerUser = createContentManagerUser();

        contentManagerUserService.deleteContentManagerUser(contentManagerUser.getId());
        Assert.assertFalse(contentManagerUserRepository.existsById(contentManagerUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteContentManagerUserNotFound() {
        contentManagerUserService.deleteContentManagerUser(UUID.randomUUID());
    }

    private ContentManagerUser createContentManagerUser() {
        ContentManagerUser contentManagerUser = generateFlatEntityWithoutId(ContentManagerUser.class);
        return contentManagerUserRepository.save(contentManagerUser);
    }
}