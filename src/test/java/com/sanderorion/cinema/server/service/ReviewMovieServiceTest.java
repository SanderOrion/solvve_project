package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.ReviewMovieRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ReviewMovieServiceTest extends BaseTest {

    @Autowired
    private ReviewMovieService reviewMovieService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Test
    public void testGetReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        ReviewMovieReadDTO readDTO = reviewMovieService.getReviewMovie(reviewMovie.getId());
        Assertions.assertThat(readDTO)
                .isEqualToIgnoringGivenFields(reviewMovie, "regUserId", "movieId", "moderatorId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testGetRegUserReviewMovies() {
        RegUser regUser = createRegUser();
        Movie movie1 = createMovie();
        Movie movie2 = createMovie();
        ReviewMovie reviewMovie1 = createReviewMovie(regUser, movie1);
        ReviewMovie reviewMovie2 = createReviewMovie(regUser, movie2);

        List<UUID> reviewMoviesIds = new ArrayList<>();
        reviewMoviesIds.add(reviewMovie1.getId());
        reviewMoviesIds.add(reviewMovie2.getId());

        List<ReviewMovieReadDTO> readDTO = reviewMovieService.getRegUserReviewMovies(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(reviewMoviesIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewMovieWrongId() {
        reviewMovieService.getReviewMovie(UUID.randomUUID());
    }

    @Test
    public void testGetReviewChecked() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        createReviewMovie(regUser, movie, ReviewConfirmStatus.CONFIRMED);
        createReviewMovie(regUser, movie, ReviewConfirmStatus.CONFIRMED);
        createReviewMovie(regUser, movie, ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewMovieReadDTO> result = reviewMovieService
                .getAllConfirmedReviewsMovie(movie.getId(), Pageable.unpaged());

        Assertions.assertThat(result.getTotalElements() == 2);
    }

    @Test
    public void testGetReviewsNeedToCheck() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        createReviewMovie(regUser, movie, ReviewConfirmStatus.CONFIRMED);
        createReviewMovie(regUser, movie, ReviewConfirmStatus.CONFIRMED);
        createReviewMovie(regUser, movie, ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewMovieReadDTO> result = reviewMovieService
                .getReviewsMovieToConfirm(Pageable.unpaged());

        Assertions.assertThat(result.getTotalElements() == 1);
    }

    @Test
    public void testCreateReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovieCreateDTO create = new ReviewMovieCreateDTO();
        create.setBody("1");

        ReviewMovieReadDTO read = reviewMovieService.createReviewMovie(regUser.getId(), movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewMovie reviewMovie = reviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewMovie, "regUserId", "movieId", "moderatorId");
        Assertions.assertThat(read.getConfirmStatus()).isEqualTo(ReviewConfirmStatus.NEED_TO_CONFIRM);
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testCreateReviewMovieCredible() {
        RegUser regUser = createRegUserCredible();
        Movie movie = createMovie();
        ReviewMovieCreateDTO create = new ReviewMovieCreateDTO();
        create.setBody("1");

        ReviewMovieReadDTO read = reviewMovieService.createReviewMovie(regUser.getId(), movie.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewMovie reviewMovie = reviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewMovie, "regUserId", "movieId", "moderatorId");
        Assertions.assertThat(read.getConfirmStatus()).isEqualTo(ReviewConfirmStatus.CONFIRMED);
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getMovieId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        ReviewMoviePatchDTO patch = new ReviewMoviePatchDTO();
        patch.setBody("2");

        ReviewMovieReadDTO read = reviewMovieService.patchReviewMovie(reviewMovie.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        reviewMovie = reviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewMovie)
                .isEqualToIgnoringGivenFields(read, "regUser", "movie", "createdAt", "updatedAt", "moderator");
        Assertions.assertThat(reviewMovie.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(reviewMovie.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testUpdateReviewMovie() {
        RegUser regUser = createRegUser();
        RegUser moderator = createRegUser();

        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        ReviewMoviePutDTO put = new ReviewMoviePutDTO();
        put.setConfirmStatus(ReviewConfirmStatus.CONFIRMED);

        ReviewMovieReadDTO read = reviewMovieService.updateReviewMovie(reviewMovie.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        reviewMovie = reviewMovieRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewMovie)
                .isEqualToIgnoringGivenFields(read, "regUser", "movie", "createdAt", "updatedAt", "moderator");
        Assertions.assertThat(reviewMovie.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(reviewMovie.getMovie().getId()).isEqualTo(movie.getId());
    }

    @Test
    public void testPatchReviewMovieEmptyPatch() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        ReviewMoviePatchDTO patch = new ReviewMoviePatchDTO();
        ReviewMovieReadDTO read = reviewMovieService.patchReviewMovie(reviewMovie.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("moderatorId");
        ReviewMovie reviewMovieUpdate = reviewMovieRepository.findById(read.getId()).get();

        Assertions.assertThat(reviewMovieUpdate).hasNoNullFieldsOrPropertiesExcept("moderator");
        Assertions.assertThat(reviewMovie)
                .isEqualToIgnoringGivenFields(reviewMovieUpdate, "regUser", "movie", "moderator");
    }

    @Test
    public void testDeleteReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        reviewMovieService.deleteReviewMovie(reviewMovie.getId());
        Assert.assertFalse(reviewMovieRepository.existsById(reviewMovie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewMovieNotFound() {
        reviewMovieService.deleteReviewMovie(UUID.randomUUID());
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie) {
        ReviewMovie reviewMovie = generateFlatEntityWithoutId(ReviewMovie.class);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(movie);
        return reviewMovieRepository.save(reviewMovie);
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie, ReviewConfirmStatus checkStatus) {
        ReviewMovie reviewMovie = generateFlatEntityWithoutId(ReviewMovie.class);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(movie);
        reviewMovie.setConfirmStatus(checkStatus);
        return reviewMovieRepository.save(reviewMovie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        regUser.setTrustForReview(null);
        return regUserRepository.save(regUser);
    }

    private RegUser createRegUserCredible() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        regUser.setTrustForReview(TrustForReview.CREDIBLE);
        return regUserRepository.save(regUser);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }
}