package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewActorRole;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ReviewActorRoleServiceTest extends BaseTest {

    @Autowired
    private ReviewActorRoleService reviewActorRoleService;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Test
    public void testGetReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        ReviewActorRoleReadDTO readDTO = reviewActorRoleService.getReviewActorRole(reviewActorRole.getId());
        Assertions.assertThat(readDTO)
                .isEqualToIgnoringGivenFields(reviewActorRole, "regUserId", "actorRoleId", "moderatorId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getActorRoleId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testGetRegUserReviewActorRoles() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole1 = createActorRole(movie);
        ActorRole actorRole2 = createActorRole(movie);
        ReviewActorRole reviewActorRole1 = createReviewActorRole(regUser, actorRole1);
        ReviewActorRole reviewActorRole2 = createReviewActorRole(regUser, actorRole2);

        List<UUID> reviewActorRolesIds = new ArrayList<>();
        reviewActorRolesIds.add(reviewActorRole1.getId());
        reviewActorRolesIds.add(reviewActorRole2.getId());

        List<ReviewActorRoleReadDTO> readDTO = reviewActorRoleService.getRegUserReviewActorRoles(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(reviewActorRolesIds.toArray());
    }


    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewActorRoleWrongId() {
        reviewActorRoleService.getReviewActorRole(UUID.randomUUID());
    }

    @Test
    public void testGetReviewChecked() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);

        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.NEED_TO_CONFIRM);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewActorRoleReadDTO> result = reviewActorRoleService
                .getAllConformedReviewsActorRole(actorRole.getId(), Pageable.unpaged());

        Assertions.assertThat(result.getTotalElements() == 3);
    }

    @Test
    public void testGetReviewNeedToCheck() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);

        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.CONFIRMED);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.NEED_TO_CONFIRM);
        createReviewActorRole(regUser, actorRole, ReviewConfirmStatus.NEED_TO_CONFIRM);

        PageResult<ReviewActorRoleReadDTO> result = reviewActorRoleService
                .getReviewsActorRoleToConfirm(Pageable.unpaged());

        Assertions.assertThat(result.getTotalElements() == 2);
    }

    @Test
    public void testCreateReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();

        ActorRole actorRole = createActorRole(movie);
        ReviewActorRoleCreateDTO create = new ReviewActorRoleCreateDTO();
        create.setBody("1");

        ReviewActorRoleReadDTO read = reviewActorRoleService.createReviewActorRole(regUser.getId(),
                actorRole.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewActorRole reviewActorRole = reviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read)
                .isEqualToIgnoringGivenFields(reviewActorRole, "regUserId", "actorRoleId", "moderatorId");
        Assertions.assertThat(read.getConfirmStatus()).isEqualTo(ReviewConfirmStatus.NEED_TO_CONFIRM);
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getActorRoleId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testCreateReviewActorRoleCredible() {
        RegUser regUser = createRegUserCredible();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRoleCreateDTO create = new ReviewActorRoleCreateDTO();
        create.setBody("1");

        ReviewActorRoleReadDTO read = reviewActorRoleService.createReviewActorRole(regUser.getId(),
                actorRole.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewActorRole reviewActorRole = reviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read)
                .isEqualToIgnoringGivenFields(reviewActorRole, "regUserId", "actorRoleId", "moderatorId");
        Assertions.assertThat(read.getConfirmStatus()).isEqualTo(ReviewConfirmStatus.CONFIRMED);
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getActorRoleId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testPatchReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        ReviewActorRolePatchDTO patch = new ReviewActorRolePatchDTO();
        patch.setBody("2");

        ReviewActorRoleReadDTO read = reviewActorRoleService.patchReviewActorRole(reviewActorRole.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        reviewActorRole = reviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewActorRole)
                .isEqualToIgnoringGivenFields(read, "regUser", "actorRole", "createdAt", "updatedAt", "moderator");
        Assertions.assertThat(reviewActorRole.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(reviewActorRole.getActorRole().getId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testUpdateReviewActorRole() {
        RegUser regUser = createRegUser();
        RegUser moderator = createRegUser();

        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        ReviewActorRolePutDTO put = new ReviewActorRolePutDTO();
        put.setConfirmStatus(ReviewConfirmStatus.CONFIRMED);

        ReviewActorRoleReadDTO read = reviewActorRoleService
                .updateReviewActorRole(reviewActorRole.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        reviewActorRole = reviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewActorRole)
                .isEqualToIgnoringGivenFields(read, "regUser", "actorRole", "createdAt", "updatedAt", "moderator");
        Assertions.assertThat(reviewActorRole.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(reviewActorRole.getActorRole().getId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testPatchReviewActorRoleEmptyPatch() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        ReviewActorRolePatchDTO patch = new ReviewActorRolePatchDTO();
        ReviewActorRoleReadDTO read = reviewActorRoleService
                .patchReviewActorRole(reviewActorRole.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrPropertiesExcept("moderatorId");
        ReviewActorRole reviewActorRoleUpdate = reviewActorRoleRepository.findById(read.getId()).get();

        Assertions.assertThat(reviewActorRoleUpdate).hasNoNullFieldsOrPropertiesExcept("moderator");
        Assertions.assertThat(reviewActorRole)
                .isEqualToIgnoringGivenFields(reviewActorRoleUpdate, "regUser", "actorRole", "moderator");
    }

    @Test
    public void testDeleteReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        reviewActorRoleService.deleteReviewActorRole(reviewActorRole.getId());
        Assert.assertFalse(reviewActorRoleRepository.existsById(reviewActorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewActorRoleNotFound() {
        reviewActorRoleService.deleteReviewActorRole(UUID.randomUUID());
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole) {
        ReviewActorRole reviewActorRole = generateFlatEntityWithoutId(ReviewActorRole.class);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(actorRole);
        return reviewActorRoleRepository.save(reviewActorRole);
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole,
                                                  ReviewConfirmStatus checkStatus) {
        ReviewActorRole reviewActorRole = generateFlatEntityWithoutId(ReviewActorRole.class);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(actorRole);
        reviewActorRole.setConfirmStatus(checkStatus);
        return reviewActorRoleRepository.save(reviewActorRole);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        regUser.setTrustForReview(null);
        return regUserRepository.save(regUser);
    }

    private RegUser createRegUserCredible() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        regUser.setTrustForReview(TrustForReview.CREDIBLE);
        return regUserRepository.save(regUser);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }
}