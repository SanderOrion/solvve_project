package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieCreateDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePatchDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePutDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class MovieServiceTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private MovieService movieService;

    @Test
    public void testGetMovie() {
        Movie movie = createMovie();

        MovieReadDTO readDTO = movieService.getMovie(movie.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(movie, "ratingCount");
    }

    @Test
    public void testGetMovieLeader() {
        MovieInLeaderBoardReadDTO leaderBoard = new MovieInLeaderBoardReadDTO();
        leaderBoard.setId(UUID.randomUUID());

        List<MovieInLeaderBoardReadDTO> readDTO = movieService.getMoviesLeaderBoard();
        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(readDTO.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMovieWrongId() {
        movieService.getMovie(UUID.randomUUID());
    }

    @Test
    public void testCreateMovie() {
        MovieCreateDTO create = generateObject(MovieCreateDTO.class);

        MovieReadDTO read = movieService.createMovie(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Movie movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(movie);
    }

    @Test
    public void testPatchMovie() {
        Movie movie = createMovie();
        MoviePatchDTO patch = generateObject(MoviePatchDTO.class);

        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie)
                .isEqualToIgnoringGivenFields(read, "actorRoles", "crews", "genres", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateMovie() {
        Movie movie = createMovie();
        MoviePutDTO put = generateObject(MoviePutDTO.class);

        MovieReadDTO read = movieService.updateMovie(movie.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        movie = movieRepository.findById(read.getId()).get();
        Assertions.assertThat(movie)
                .isEqualToIgnoringGivenFields(read, "actorRoles", "crews", "genres", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchMovieEmptyPatch() {
        Movie movie = createMovie();

        MoviePatchDTO patch = new MoviePatchDTO();
        MovieReadDTO read = movieService.patchMovie(movie.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        Movie movieAfterUpdate = movieRepository.findById(read.getId()).get();

        Assertions.assertThat(movieAfterUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(movie).isEqualToIgnoringGivenFields(movieAfterUpdate, "actorRoles", "crews", "genres");
    }

    @Test
    public void testDeleteMovie() {
        Movie movie = createMovie();
        movieService.deleteMovie(movie.getId());
        Assert.assertFalse(movieRepository.existsById(movie.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMovieNotFound() {
        movieService.deleteMovie(UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageMarkOfMovie() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();

        createMarkMovie(r1, m1, 4);
        createMarkMovie(r1, m1, 2);

        movieService.updateAverageRatingOfMovie(m1.getId());
        m1 = movieRepository.findById(m1.getId()).get();
        Assert.assertEquals(3, m1.getAvgRating(), Double.MIN_NORMAL);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MarkMovie createMarkMovie(RegUser regUser, Movie movie, Integer mark) {
        MarkMovie markMovie = new MarkMovie();
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        markMovie.setMark(mark);
        return markMovieRepository.save(markMovie);
    }
}