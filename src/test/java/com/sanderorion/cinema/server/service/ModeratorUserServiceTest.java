package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ModeratorUser;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserCreateDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPatchDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPutDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.ModeratorUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ModeratorUserServiceTest extends BaseTest {

    @Autowired
    private ModeratorUserRepository moderatorUserRepository;

    @Autowired
    private ModeratorUserService moderatorUserService;

    @Test
    public void testGetModeratorUser() {
        ModeratorUser moderatorUser = createModeratorUser();

        ModeratorUserReadDTO readDTO = moderatorUserService.getModeratorUser(moderatorUser.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(moderatorUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetModeratorUserWrongId() {
        moderatorUserService.getModeratorUser(UUID.randomUUID());
    }

    @Test
    public void testCreateModeratorUser() {
        ModeratorUserCreateDTO create = generateObject(ModeratorUserCreateDTO.class);

        ModeratorUserReadDTO read = moderatorUserService.createModeratorUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ModeratorUser moderatorUser = moderatorUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(moderatorUser);
    }

    @Test
    public void testPatchModeratorUser() {
        ModeratorUser moderatorUser = createModeratorUser();

        ModeratorUserPatchDTO patch = generateObject(ModeratorUserPatchDTO.class);

        ModeratorUserReadDTO read = moderatorUserService.patchModeratorUser(moderatorUser.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        moderatorUser = moderatorUserRepository.findById(read.getId()).get();
        Assertions.assertThat(moderatorUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateModeratorUser() {
        ModeratorUser moderatorUser = createModeratorUser();

        ModeratorUserPutDTO put = generateObject(ModeratorUserPutDTO.class);

        ModeratorUserReadDTO read = moderatorUserService.updateModeratorUser(moderatorUser.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        moderatorUser = moderatorUserRepository.findById(read.getId()).get();
        Assertions.assertThat(moderatorUser).isEqualToIgnoringGivenFields(read, "createdAt", "updatedAt");
    }

    @Test
    public void testPatchModeratorUserEmptyPatch() {
        ModeratorUser moderatorUser = createModeratorUser();

        ModeratorUserPatchDTO patch = new ModeratorUserPatchDTO();
        ModeratorUserReadDTO read = moderatorUserService.patchModeratorUser(moderatorUser.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        ModeratorUser moderatorUserUpdate = moderatorUserRepository.findById(read.getId()).get();

        Assertions.assertThat(moderatorUserUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(moderatorUser).isEqualToComparingFieldByField(moderatorUserUpdate);
    }

    @Test
    public void testDeleteModeratorUser() {
        ModeratorUser moderatorUser = createModeratorUser();

        moderatorUserService.deleteModeratorUser(moderatorUser.getId());
        Assert.assertFalse(moderatorUserRepository.existsById(moderatorUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteModeratorUserNotFound() {
        moderatorUserService.deleteModeratorUser(UUID.randomUUID());
    }

    private ModeratorUser createModeratorUser() {
        ModeratorUser moderatorUser = generateFlatEntityWithoutId(ModeratorUser.class);
        return moderatorUserRepository.save(moderatorUser);
    }
}