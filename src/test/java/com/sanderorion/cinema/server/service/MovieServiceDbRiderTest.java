package com.sanderorion.cinema.server.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.sanderorion.cinema.server.repository.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@DBRider
public class MovieServiceDbRiderTest extends BaseTest {

    @Autowired
    private MovieService movieService;

    @Test
    @DataSet("/datasets/testUpdateAverageMarkOfMovie.xml")
    @ExpectedDataSet("/datasets/testUpdateAverageMarkOfMovie_result.xml")
    public void testUpdateAverageMarkOfMovie() {
        UUID movieId = UUID.fromString("296274cc-f9f3-43b6-a6ed-b065982112e3");
        movieService.updateAverageRatingOfMovie(movieId);
    }
}