package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import com.sanderorion.cinema.server.security.RegUserDetailsServiceImpl;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.IterableUtil;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;

public class RegRegUserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private RegUserDetailsServiceImpl regUserDetailsService;

    @Test
    public void testLoadRegUserByUsername() {
        RegUser regUser = transactionTemplate.execute(status -> {
            RegUser r = generateFlatEntityWithoutId(RegUser.class);
            r.setUserRoles(new ArrayList<>(IterableUtil.toCollection(userRoleRepository.findAll())));
            return regUserRepository.save(r);
        });

        UserDetails regUserDetails = regUserDetailsService.loadUserByUsername(regUser.getEmail());
        Assert.assertEquals(regUser.getEmail(), regUserDetails.getUsername());
        Assert.assertEquals(regUser.getEncodedPassword(), regUserDetails.getPassword());
        Assert.assertFalse(regUserDetails.getAuthorities().isEmpty());
        Assertions.assertThat(regUserDetails.getAuthorities())
                .extracting("authority").containsExactlyInAnyOrder(
                regUser.getUserRoles().stream().map(ur -> ur.getUserRoleType().toString()).toArray());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        regUserDetailsService.loadUserByUsername("wrong name");
    }
}