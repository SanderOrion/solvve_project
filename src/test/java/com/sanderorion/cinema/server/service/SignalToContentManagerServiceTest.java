package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.SignalToContentManagerWrongStatusException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SignalToContentManagerServiceTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private SignalToContentManagerService signalToContentManagerService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetSignalToContentManager() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        SignalToContentManager signal = createSignal(regUser, article);

        SignalToContentManagerReadDTO readDTO = signalToContentManagerService.getSignalToCM(signal.getId());
        Assertions.assertThat(readDTO)
                .isEqualToIgnoringGivenFields(signal, "regUserId", "articleId", "contentManagerUserId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getArticleId()).isEqualTo(article.getId());
    }

    @Test
    public void testGetRegUserSignalsToContentManager() {
        RegUser regUser = createRegUser();
        Article article1 = createArticle();
        Article article2 = createArticle();
        SignalToContentManager signal1 = createSignal(regUser, article1);
        SignalToContentManager signal2 = createSignal(regUser, article2);

        List<UUID> signalsIds = new ArrayList<>();
        signalsIds.add(signal1.getId());
        signalsIds.add(signal2.getId());

        List<SignalToContentManagerReadDTO> readDTO = signalToContentManagerService
                .getRegUserSignalsToCM(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(signalsIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetSignalToContentManagerWrongId() {
        signalToContentManagerService.getSignalToCM(UUID.randomUUID());
    }

    @Test
    public void testCreateSignalToContentManager() {
        RegUser regUser = createRegUser();
        Article article = createArticle();

        SignalToContentManagerCreateDTO create = new SignalToContentManagerCreateDTO();
        create.setTypo("and");
        create.setUserSuggestion("end");

        SignalToContentManagerReadDTO read = signalToContentManagerService
                .createSignalToCM(article.getId(), regUser.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        SignalToContentManager signal = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(read)
                .isEqualToIgnoringGivenFields(signal, "regUserId", "articleId", "contentManagerUserId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getArticleId()).isEqualTo(article.getId());
    }

    @Test
    public void testUpdateSignalToContentManagerVersionUser() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        RegUser cm = createCM();
        SignalToContentManager signal = createSignal(regUser, article);
        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setCorrectText(null);

        SignalToContentManagerReadDTO read = signalToContentManagerService
                .updateSignalToCM(signal.getId(), cm.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "correctText");
        signal = signalToContentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(signal)
                .isEqualToIgnoringGivenFields(read, "regUser", "article", "contentManagerUser");
        Assertions.assertThat(signal.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(signal.getArticle().getId()).isEqualTo(article.getId());
        Assertions.assertThat(signal.getContentManagerUser().getId()).isEqualTo(cm.getId());
    }

    @Test
    public void testUpdateSignalToContentManagerVersionCM() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        RegUser cm = createCM();
        SignalToContentManager signal = createSignal(regUser, article);
        SignalToContentManager signal2 = createSignal(regUser, article);
        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setCorrectText("ошибка(версия КМ)");

        SignalToContentManagerReadDTO read = signalToContentManagerService
                .updateSignalToCM(signal.getId(), cm.getId(), put);
        transactionTemplate.executeWithoutResult(status -> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            SignalToContentManager signalAfter = signalToContentManagerRepository.findById(read.getId()).get();
            SignalToContentManager signalAfter2 = signalToContentManagerRepository.findById(signal2.getId()).get();
            Assertions.assertThat(signalAfter)
                    .isEqualToIgnoringGivenFields(read, "regUser", "article", "contentManagerUser");
            Assertions.assertThat(signalAfter.getRegUser().getId()).isEqualTo(regUser.getId());
            Assertions.assertThat(signalAfter.getArticle().getId()).isEqualTo(article.getId());
            Assertions.assertThat(signalAfter.getContentManagerUser().getId()).isEqualTo(cm.getId());
            Assertions.assertThat(signalAfter2.getSignalStatus()).isEqualTo(SignalStatus.CANCELED);
            Assertions.assertThat(signalAfter2.getContentManagerUser().getId()).isEqualTo(cm.getId());
        });
    }

    @Test
    public void testGetSignalsNeedToFix() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        createSignal(regUser, article);
        createSignal(regUser, article);

        Assertions.assertThat(signalToContentManagerService.getSignalsForFix(Pageable.unpaged())
                .getTotalElements() == 2);
    }

    @Test(expected = SignalToContentManagerWrongStatusException.class)
    public void testGetWrongStatus() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        RegUser cm = createCM();
        SignalToContentManager signal = createWrongSignal(regUser, article);

        SignalToContentManagerPutDTO put = new SignalToContentManagerPutDTO();
        put.setCorrectText("ошибка");
        signalToContentManagerService.updateSignalToCM(signal.getId(), cm.getId(), put);
    }

    @Test
    public void testDeleteSignalToContentManager() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        SignalToContentManager signal = createSignal(regUser, article);

        signalToContentManagerService.deleteSignalToCM(signal.getId());
        Assert.assertFalse(signalToContentManagerRepository.existsById(signal.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteSignalToContentManagerNotFound() {
        signalToContentManagerService.deleteSignalToCM(UUID.randomUUID());
    }

    private RegUser createCM() {
        RegUser cm = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(cm);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private Article createArticle() {
        Article article = new Article();
        article.setTitle("new movie");
        article.setBody("тут есть ожибка");
        article.setDateArticle(LocalDateTime.of(2020, 1, 30, 12, 0, 0)
                .toInstant(ZoneOffset.UTC));
        return articleRepository.save(article);
    }

    private SignalToContentManager createSignal(RegUser regUser, Article article) {
        SignalToContentManager signal = new SignalToContentManager();
        signal.setRegUser(regUser);
        signal.setArticle(article);
        signal.setTypo("ожибка");
        signal.setUserSuggestion("ошибка(версия юзера)");
        signal.setSignalStatus(SignalStatus.NEED_TO_FIX);
        return signalToContentManagerRepository.save(signal);
    }

    private SignalToContentManager createWrongSignal(RegUser regUser, Article article) {
        SignalToContentManager signal = new SignalToContentManager();
        signal.setRegUser(regUser);
        signal.setArticle(article);
        signal.setTypo("ожибка1");
        signal.setUserSuggestion("ошибка(версия юзера)");
        signal.setSignalStatus(SignalStatus.CANCELED);
        return signalToContentManagerRepository.save(signal);
    }
}