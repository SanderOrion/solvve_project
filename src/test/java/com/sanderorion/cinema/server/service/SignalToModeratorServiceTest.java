package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.domain.enums.SignalType;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutTrustDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorCreateDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.SignalToContentManagerWrongStatusException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SignalToModeratorServiceTest extends BaseTest {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private SignalToModeratorService signalToModeratorService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private SignalToModeratorRepository signalToModeratorRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Test
    public void testGetSignalToModerator() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        SignalToModerator signalMovie = createSignal(regUser, reviewMovie);
        SignalToModerator signalActorRole = createSignal(regUser, reviewActorRole);

        SignalToModeratorReadDTO readMovie = signalToModeratorService.getSignalToModerator(signalMovie.getId());
        SignalToModeratorReadDTO readActRole = signalToModeratorService.getSignalToModerator(signalActorRole.getId());

        Assertions.assertThat(readMovie)
                .isEqualToIgnoringGivenFields(signalMovie, "regUserId", "moderatorUserId", "reviewActorRoleId",
                        "reviewMovieId");
        Assertions.assertThat(readMovie.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readMovie.getReviewMovieId()).isEqualTo(reviewMovie.getId());

        Assertions.assertThat(readActRole)
                .isEqualToIgnoringGivenFields(signalActorRole, "regUserId", "moderatorUserId", "reviewActorRoleId",
                        "reviewMovieId");
        Assertions.assertThat(readActRole.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readActRole.getReviewActorRoleId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testGetRegUserSignalsToContentManager() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        SignalToModerator signalMovie = createSignal(regUser, reviewActorRole);
        SignalToModerator signalActorRole = createSignal(regUser, reviewMovie);

        List<UUID> signalsIds = new ArrayList<>();
        signalsIds.add(signalMovie.getId());
        signalsIds.add(signalActorRole.getId());

        List<SignalToModeratorReadDTO> readDTO = signalToModeratorService
                .getRegUserSignalsToModerator(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(signalsIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetSignalToContentManagerWrongId() {
        signalToModeratorService.getSignalToModerator(UUID.randomUUID());
    }

    @Test
    public void testGetSignalsNeedToCheck() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        createSignal(regUser, reviewActorRole);
        createSignal(regUser, reviewMovie);
        createSignal(regUser, reviewActorRole);
        createSignal(regUser, reviewMovie);

        PageResult<SignalToModeratorReadDTO> result = signalToModeratorService.getSignalsForCheck(Pageable.unpaged());

        Assertions.assertThat(result.getTotalElements() == 2);
    }

    @Test
    public void testCreateSignalToModerator() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        SignalToModeratorCreateDTO createSignalMovie = new SignalToModeratorCreateDTO();
        createSignalMovie.setSignalType(SignalType.SPAM);
        createSignalMovie.setTypo("111");

        SignalToModeratorCreateDTO createSignalActorRole = new SignalToModeratorCreateDTO();
        createSignalActorRole.setSignalType(SignalType.SPOILER_ALERT);
        createSignalActorRole.setTypo("1121");

        SignalToModeratorReadDTO readMovie = signalToModeratorService
                .createSignalToModerator(regUser.getId(), reviewMovie.getId(), createSignalMovie);
        Assertions.assertThat(createSignalMovie).isEqualToComparingFieldByField(readMovie);
        Assert.assertNotNull(readMovie.getId());

        SignalToModeratorReadDTO readActorRole = signalToModeratorService
                .createSignalToModerator(regUser.getId(), reviewActorRole.getId(), createSignalActorRole);
        Assertions.assertThat(createSignalActorRole).isEqualToComparingFieldByField(readActorRole);
        Assert.assertNotNull(readActorRole.getId());

        SignalToModerator signalMovie = signalToModeratorRepository.findById(readMovie.getId()).get();
        Assertions.assertThat(readMovie)
                .isEqualToIgnoringGivenFields(signalMovie, "regUserId", "moderatorUserId", "reviewActorRoleId",
                        "reviewMovieId");
        Assertions.assertThat(readMovie.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readMovie.getReviewMovieId()).isEqualTo(reviewMovie.getId());

        SignalToModerator signalActorRole = signalToModeratorRepository.findById(readActorRole.getId()).get();
        Assertions.assertThat(readActorRole)
                .isEqualToIgnoringGivenFields(signalActorRole, "regUserId", "moderatorUserId", "reviewActorRoleId",
                        "reviewMovieId");
        Assertions.assertThat(readActorRole.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readActorRole.getReviewActorRoleId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testUpdateSignalToModeratorMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        RegUser moderator = createModerator();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        SignalToModerator signal = createSignal(regUser, reviewMovie);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText(null);

        SignalToModeratorReadDTO read = signalToModeratorService
                .updateSignalToModerator(signal.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "correctText");
        signal = signalToModeratorRepository.findById(read.getId()).get();
        Assertions.assertThat(signal).isEqualToIgnoringGivenFields(read, "regUser", "moderatorUser",
                "reviewActorRole", "reviewMovie");
        Assertions.assertThat(signal.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(signal.getReviewMovie().getId()).isEqualTo(reviewMovie.getId());
        Assertions.assertThat(signal.getModeratorUser().getId()).isEqualTo(moderator.getId());
    }

    @Test
    public void testUpdateSignalToModeratorMovieWithCorrectText() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        RegUser moderator = createModerator();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        SignalToModerator signal = createSignal(regUser, reviewMovie);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText("уже нет");

        SignalToModeratorReadDTO read = signalToModeratorService
                .updateSignalToModerator(signal.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "correctText");
        signal = signalToModeratorRepository.findById(read.getId()).get();
        Assertions.assertThat(signal).isEqualToIgnoringGivenFields(read, "regUser", "moderatorUser",
                "reviewActorRole", "reviewMovie");
        Assertions.assertThat(signal.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(signal.getReviewMovie().getId()).isEqualTo(reviewMovie.getId());
        Assertions.assertThat(signal.getModeratorUser().getId()).isEqualTo(moderator.getId());
    }

    @Test
    public void testUpdateSignalToModeratorActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        RegUser moderator = createModerator();
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        SignalToModerator signal = createSignal(regUser, reviewActorRole);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText(null);

        SignalToModeratorReadDTO read = signalToModeratorService
                .updateSignalToModerator(signal.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "correctText");
        signal = signalToModeratorRepository.findById(read.getId()).get();
        Assertions.assertThat(signal).isEqualToIgnoringGivenFields(read, "regUser", "moderatorUser",
                "reviewActorRole", "reviewMovie");
        Assertions.assertThat(signal.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(signal.getReviewActorRole().getId()).isEqualTo(reviewActorRole.getId());
        Assertions.assertThat(signal.getModeratorUser().getId()).isEqualTo(moderator.getId());
    }

    @Test
    public void testUpdateSignalToModeratorActorRoleWithCorrectText() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        RegUser moderator = createModerator();
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        SignalToModerator signal = createSignal(regUser, reviewActorRole);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText("уже нет");

        SignalToModeratorReadDTO read = signalToModeratorService
                .updateSignalToModerator(signal.getId(), moderator.getId(), put);
        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read, "correctText");
        signal = signalToModeratorRepository.findById(read.getId()).get();
        Assertions.assertThat(signal).isEqualToIgnoringGivenFields(read, "regUser", "moderatorUser",
                "reviewActorRole", "reviewMovie");
        Assertions.assertThat(signal.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(signal.getReviewActorRole().getId()).isEqualTo(reviewActorRole.getId());
        Assertions.assertThat(signal.getModeratorUser().getId()).isEqualTo(moderator.getId());
    }

    @Test
    public void testUpdateSignalToModeratorWithSimilarSignals() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        RegUser moderator = createModerator();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        SignalToModerator signal1 = createSignal(regUser, reviewMovie);
        SignalToModerator signal2 = createSignal(regUser, reviewMovie);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText("уже нет");

        SignalToModeratorReadDTO read = signalToModeratorService
                .updateSignalToModerator(signal1.getId(), moderator.getId(), put);
        transactionTemplate.executeWithoutResult(status -> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            SignalToModerator signalAfter1 = signalToModeratorRepository.findById(read.getId()).get();
            SignalToModerator signalAfter2 = signalToModeratorRepository.findById(signal2.getId()).get();
            Assertions.assertThat(signalAfter1).isEqualToIgnoringGivenFields(read, "regUser", "moderatorUser",
                    "reviewActorRole", "reviewMovie");
            Assertions.assertThat(signalAfter1.getRegUser().getId()).isEqualTo(regUser.getId());
            Assertions.assertThat(signalAfter1.getReviewMovie().getId()).isEqualTo(reviewMovie.getId());
            Assertions.assertThat(signalAfter1.getModeratorUser().getId()).isEqualTo(moderator.getId());
            Assertions.assertThat(signalAfter2.getSignalStatus()).isEqualTo(SignalStatus.CANCELED);
            Assertions.assertThat(signalAfter2.getModeratorUser().getId()).isEqualTo(moderator.getId());
        });
    }

    @Test(expected = SignalToContentManagerWrongStatusException.class)
    public void testGetWrongStatus() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        RegUser moderator = createModerator();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        SignalToModerator signal = createSignalWrong(regUser, reviewMovie);

        SignalToModeratorPutDTO put = new SignalToModeratorPutDTO();
        put.setCorrectText(null);

        signalToModeratorService.updateSignalToModerator(signal.getId(), moderator.getId(), put);
    }

    @Test
    public void testUpdateUserTrustForReview() {
        RegUser regUser = createRegUser();
        RegUserPutTrustDTO put = new RegUserPutTrustDTO();
        put.setTrustForReview(TrustForReview.CREDIBLE);

        signalToModeratorService.updateUserTrustForReview(regUser.getId(), put);
        Assertions.assertThat(TrustForReview.CREDIBLE).isEqualTo(regUser.getTrustForReview());
    }

    @Test
    public void testDeleteSignalToModerator() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole review = createReviewActorRole(regUser, actorRole);
        SignalToModerator signalMovie = createSignal(regUser, review);
        SignalToModerator signalActorRole = createSignal(regUser, review);

        signalToModeratorService.deleteSignalToModerator(signalMovie.getId());
        signalToModeratorService.deleteSignalToModerator(signalActorRole.getId());
        Assert.assertFalse(signalToModeratorRepository.existsById(signalMovie.getId()));
        Assert.assertFalse(signalToModeratorRepository.existsById(signalActorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteSignalToModeratorNotFound() {
        signalToModeratorService.deleteSignalToModerator(UUID.randomUUID());
    }


    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole) {
        ReviewActorRole reviewActorRole = generateFlatEntityWithoutId(ReviewActorRole.class);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(actorRole);
        reviewActorRole.setBody("тут есть матюк");
        return reviewActorRoleRepository.save(reviewActorRole);
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie) {
        ReviewMovie reviewMovie = generateFlatEntityWithoutId(ReviewMovie.class);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(movie);
        reviewMovie.setBody("тут есть матюк");
        return reviewMovieRepository.save(reviewMovie);
    }

    private SignalToModerator createSignal(RegUser regUser, ReviewActorRole reviewActorRole) {
        SignalToModerator signal = new SignalToModerator();
        signal.setRegUser(regUser);
        signal.setReviewActorRole(reviewActorRole);
        signal.setTypo("матюк");
        signal.setSignalType(SignalType.INSULT);
        signal.setSignalStatus(SignalStatus.NEED_TO_FIX);
        return signalToModeratorRepository.save(signal);
    }

    private SignalToModerator createSignal(RegUser regUser, ReviewMovie reviewMovie) {
        SignalToModerator signal = new SignalToModerator();
        signal.setRegUser(regUser);
        signal.setReviewMovie(reviewMovie);
        signal.setTypo("матюк");
        signal.setSignalType(SignalType.INSULT);
        signal.setSignalStatus(SignalStatus.NEED_TO_FIX);
        return signalToModeratorRepository.save(signal);
    }

    private SignalToModerator createSignalWrong(RegUser regUser, ReviewMovie reviewMovie) {
        SignalToModerator signal = new SignalToModerator();
        signal.setRegUser(regUser);
        signal.setReviewMovie(reviewMovie);
        signal.setTypo("матюк");
        signal.setSignalType(SignalType.INSULT);
        signal.setSignalStatus(SignalStatus.FIXED);
        return signalToModeratorRepository.save(signal);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private RegUser createModerator() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }
}