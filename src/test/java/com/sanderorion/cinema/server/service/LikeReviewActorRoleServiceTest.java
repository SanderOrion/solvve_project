package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LikeReviewActorRoleServiceTest extends BaseTest {

    @Autowired
    private LikeReviewActorRoleService likeReviewActorRoleService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private LikeReviewActorRoleRepository likeReviewActorRoleRepository;

    @Test
    public void testGetLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole like = createLikeReviewActorRole(regUser, reviewActorRole);

        LikeReviewActorRoleReadDTO readDTO = likeReviewActorRoleService.getLikeReviewActorRole(like.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(like, "regUserId", "reviewActorRoleId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getReviewActorRoleId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testGetRegLikeReviewActorRoles() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole1 = createReviewActorRole(regUser, actorRole);
        ReviewActorRole reviewActorRole2 = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole like1 = createLikeReviewActorRole(regUser, reviewActorRole1);
        LikeReviewActorRole like2 = createLikeReviewActorRole(regUser, reviewActorRole2);

        List<UUID> likeReviewActorRolesIds = new ArrayList<>();
        likeReviewActorRolesIds.add(like1.getId());
        likeReviewActorRolesIds.add(like2.getId());

        List<LikeReviewActorRoleReadDTO> readDTO = likeReviewActorRoleService
                .getRegLikeReviewActorRoles(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(likeReviewActorRolesIds.toArray());
    }


    @Test(expected = EntityNotFoundException.class)
    public void testGetLikeReviewActorRoleWrongId() {
        likeReviewActorRoleService.getLikeReviewActorRole(UUID.randomUUID());
    }

    @Test
    public void testCreateLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        LikeReviewActorRoleCreateDTO create = new LikeReviewActorRoleCreateDTO();
        create.setUserLike(true);

        LikeReviewActorRoleReadDTO read = likeReviewActorRoleService
                .createLikeReviewActorRole(regUser.getId(), reviewActorRole.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        LikeReviewActorRole likeReviewActorRole = likeReviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read)
                .isEqualToIgnoringGivenFields(likeReviewActorRole, "regUserId", "reviewActorRoleId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getReviewActorRoleId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testPatchLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole like = createLikeReviewActorRole(regUser, reviewActorRole);

        LikeReviewActorRolePatchDTO patch = new LikeReviewActorRolePatchDTO();
        patch.setUserLike(false);

        LikeReviewActorRoleReadDTO read = likeReviewActorRoleService.patchLikeReviewActorRole(like.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        like = likeReviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(like)
                .isEqualToIgnoringGivenFields(read, "regUser", "reviewActorRole", "createdAt", "updatedAt");
        Assertions.assertThat(like.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(like.getReviewActorRole().getId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testUpdateLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole like = createLikeReviewActorRole(regUser, reviewActorRole);

        LikeReviewActorRolePutDTO put = new LikeReviewActorRolePutDTO();
        put.setUserLike(false);

        LikeReviewActorRoleReadDTO read = likeReviewActorRoleService.updateLikeReviewActorRole(like.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        like = likeReviewActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(like)
                .isEqualToIgnoringGivenFields(read, "regUser", "reviewActorRole", "createdAt", "updatedAt");
        Assertions.assertThat(like.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(like.getReviewActorRole().getId()).isEqualTo(reviewActorRole.getId());
    }

    @Test
    public void testPatchLikeReviewActorRoleEmptyPatch() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole like = createLikeReviewActorRole(regUser, reviewActorRole);

        LikeReviewActorRolePatchDTO patch = new LikeReviewActorRolePatchDTO();
        LikeReviewActorRoleReadDTO read = likeReviewActorRoleService.patchLikeReviewActorRole(like.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        LikeReviewActorRole likeUpdate = likeReviewActorRoleRepository.findById(read.getId()).get();

        Assertions.assertThat(likeUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(like).isEqualToIgnoringGivenFields(likeUpdate, "regUser", "reviewActorRole");

    }

    @Test
    public void testDeleteLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole likeReviewActorRole = createLikeReviewActorRole(regUser, reviewActorRole);

        likeReviewActorRoleService.deleteLikeReviewActorRole(likeReviewActorRole.getId());
        Assert.assertFalse(likeReviewActorRoleRepository.existsById(likeReviewActorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteLikeReviewActorRoleNotFound() {
        likeReviewActorRoleService.deleteLikeReviewActorRole(UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole) {
        ReviewActorRole reviewActorRole = generateFlatEntityWithoutId(ReviewActorRole.class);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(actorRole);
        return reviewActorRoleRepository.save(reviewActorRole);
    }

    private LikeReviewActorRole createLikeReviewActorRole(RegUser regUser, ReviewActorRole reviewActorRole) {
        LikeReviewActorRole likeReviewActorRole = generateFlatEntityWithoutId(LikeReviewActorRole.class);
        likeReviewActorRole.setRegUser(regUser);
        likeReviewActorRole.setReviewActorRole(reviewActorRole);
        return likeReviewActorRoleRepository.save(likeReviewActorRole);
    }
}