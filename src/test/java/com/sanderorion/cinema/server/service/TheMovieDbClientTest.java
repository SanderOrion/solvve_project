package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.client.TheMovieDbClient;
import com.sanderorion.cinema.server.client.themoviedb.dto.MovieReadDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.MovieReadShortDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.MoviesPageDTO;
import com.sanderorion.cinema.server.repository.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TheMovieDbClientTest extends BaseTest {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Test
    public void testGetMovieRu() {
        Integer movieId = 299534;
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, "ru");
        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Avengers: Endgame", movie.getOriginalTitle());
        Assert.assertEquals("Мстители: Финал", movie.getTitle());
    }

    @Test
    public void testGetMovieDefaultLanguage() {
        Integer movieId = 299534;
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, null);
        Assert.assertEquals(movieId, movie.getId());
        Assert.assertEquals("Avengers: Endgame", movie.getOriginalTitle());
        Assert.assertEquals(movie.getOriginalTitle(), movie.getTitle());
    }

    @Test
    public void testGetTopRatedMovie() {
        MoviesPageDTO moviesPage = theMovieDbClient.getTopRatedMovies();
        Assert.assertTrue(moviesPage.getTotalPages() > 0);
        Assert.assertTrue(moviesPage.getTotalResults() > 0);
        Assert.assertTrue(moviesPage.getResults().size() > 0);
        for (MovieReadShortDTO read : moviesPage.getResults()) {
            Assert.assertNotNull(read.getId());
            Assert.assertNotNull(read.getTitle());
        }
    }
}