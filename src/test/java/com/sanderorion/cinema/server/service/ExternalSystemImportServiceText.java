package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.client.themoviedb.ExternalSystemImport;
import com.sanderorion.cinema.server.client.themoviedb.ExternalSystemImportRepository;
import com.sanderorion.cinema.server.client.themoviedb.dto.ImportedEntityType;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.service.importer.ExternalSystemImportService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ExternalSystemImportServiceText extends BaseTest {

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Test
    public void testValidationNotImportedFromExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Movie.class, 123);
    }

    @Test
    public void testExceptionalWhenAlreadyImported() {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.MOVIE);
        esi = externalSystemImportRepository.save(esi);
        Integer idInExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Movie.class, idInExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testExceptionalWhenAlreadyImportedPerson() {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);
        Integer idInExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(MoviePerson.class, idInExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Movie.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityTypePerson()
            throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.MOVIE);
        esi = externalSystemImportRepository.save(esi);
        externalSystemImportService.validateNotImported(MoviePerson.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testCreateExternalSystemImport() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setId(UUID.randomUUID());
        Integer idInExternalSystem = 17523;
        UUID importId = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esi.getEntityType());
        Assert.assertEquals(movie.getId(), esi.getEntityId());
    }

    @Test
    public void testCreateExternalSystemImportPerson() {

        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setId(UUID.randomUUID());
        Integer idInExternalSystemMovie = 12743;
        UUID importIdMovie = externalSystemImportService.createExternalSystemImport(movie, idInExternalSystemMovie);
        Assert.assertNotNull(importIdMovie);
        ExternalSystemImport esiMovie = externalSystemImportRepository.findById(importIdMovie).get();
        Assert.assertEquals(idInExternalSystemMovie, esiMovie.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.MOVIE, esiMovie.getEntityType());
        Assert.assertEquals(movie.getId(), esiMovie.getEntityId());
    }
}