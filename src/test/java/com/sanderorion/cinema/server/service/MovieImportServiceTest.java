package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.client.TheMovieDbClient;
import com.sanderorion.cinema.server.client.themoviedb.dto.*;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import com.sanderorion.cinema.server.exception.ImportedEntityAlreadyExistException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.service.importer.MovieImporterService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.util.*;

public class MovieImportServiceTest extends BaseTest {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Test
    public void testMovieImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        Integer movieExternalId = 1235545;

        MovieReadDTO read = getMovieReadDTO();

        List<GenreReadDTO> genres = new ArrayList<>();
        genres.add(new GenreReadDTO("ACTION"));
        genres.add(new GenreReadDTO("ADVENTURE"));
        genres.add(new GenreReadDTO("SCIENCE FICTION"));
        read.setGenres(genres);

        CreditsReadDTO creditsRead = new CreditsReadDTO();
        creditsRead.setId(movieExternalId);
        List<CastReadDTO> cast = new ArrayList<>();

        CastReadDTO newCast = genNewCast();
        CastReadDTO newCastNull = genNewCastNull();

        cast.add(newCast);
        cast.add(newCastNull);
        creditsRead.setCast(cast);

        List<CrewReadDTO> crew = new ArrayList<>();

        CrewReadDTO newCrew = getNewCrew();
        CrewReadDTO newCrewNull = getNewCrewNull();

        crew.add(newCrew);
        crew.add(newCrewNull);
        creditsRead.setCrew(crew);

        PersonReadDTO personRead = getPersonRead();
        PersonReadDTO personReadTwo = getPersonReadTwo();
        PersonReadDTO personReadNull = getPersonReadNull();

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(creditsRead);
        Mockito.when(movieDbClient.getPerson(personRead.getId())).thenReturn(personRead);
        Mockito.when(movieDbClient.getPerson(personReadTwo.getId())).thenReturn(personReadTwo);
        Mockito.when(movieDbClient.getPerson(personReadNull.getId())).thenReturn(personReadNull);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movie = movieRepository.findById(movieId).get();

            Assert.assertEquals(read.getOriginalTitle(), movie.getOriginalTitle());
            Assert.assertEquals(read.getTitle(), movie.getTitle());
            Assert.assertEquals(read.getImdbId(), movie.getImdbId());
            Assert.assertEquals(read.getTagline(), movie.getTagline());
            Assert.assertEquals(read.getOverview(), movie.getOverview());
            Assert.assertEquals(read.getReleaseDate(), movie.getReleaseDate());
            Assert.assertEquals(MovieRelease.valueOf(read.getStatus().toUpperCase()), movie.getStatus());
            Assert.assertEquals(read.getRuntime(), movie.getDuration());
            Assert.assertEquals(read.getVoteAverage(), movie.getAvgRating());
            Assert.assertEquals(read.getVoteCount(), movie.getRatingCount());
            Assert.assertEquals(read.getRevenue(), movie.getRevenue());
            Assert.assertEquals(read.getBudget(), movie.getBudget());

            Assertions.assertThat(movie.getGenres()).extracting("movieGenre")
                    .containsExactlyInAnyOrder(MovieGenre.ACTION, MovieGenre.ADVENTURE, MovieGenre.SCIENCE_FICTION);
        });
    }

    @Test
    public void testMovieImportWithOutNull()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        Integer movieExternalId = 123554455;

        MovieReadDTO read = getMovieReadDTO();
        read.setImdbId("tt12345678");
        read.setTagline("world");
        read.setOverview("good movie");
        read.setRuntime(100);

        List<GenreReadDTO> genres = new ArrayList<>();
        genres.add(new GenreReadDTO("ACTION"));
        genres.add(new GenreReadDTO("ADVENTURE"));
        genres.add(new GenreReadDTO("SCIENCE FICTION"));
        read.setGenres(genres);

        CreditsReadDTO creditsRead = new CreditsReadDTO();
        creditsRead.setId(movieExternalId);
        List<CastReadDTO> cast = new ArrayList<>();

        CastReadDTO newCast = genNewCast();
        CastReadDTO newCastNull = genNewCastNull();

        cast.add(newCast);
        cast.add(newCastNull);
        creditsRead.setCast(cast);

        List<CrewReadDTO> crew = new ArrayList<>();

        CrewReadDTO newCrew = getNewCrew();
        CrewReadDTO newCrewNull = getNewCrewNull();

        crew.add(newCrew);
        crew.add(newCrewNull);
        creditsRead.setCrew(crew);

        PersonReadDTO personRead = getPersonRead();
        PersonReadDTO personReadTwo = getPersonReadTwo();
        PersonReadDTO personReadNull = getPersonReadNull();

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(creditsRead);
        Mockito.when(movieDbClient.getPerson(personRead.getId())).thenReturn(personRead);
        Mockito.when(movieDbClient.getPerson(personReadTwo.getId())).thenReturn(personReadTwo);
        Mockito.when(movieDbClient.getPerson(personReadNull.getId())).thenReturn(personReadNull);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movie = movieRepository.findById(movieId).get();

            Assert.assertEquals(read.getOriginalTitle(), movie.getOriginalTitle());
            Assert.assertEquals(read.getTitle(), movie.getTitle());
            Assert.assertEquals(read.getImdbId(), movie.getImdbId());
            Assert.assertEquals(read.getTagline(), movie.getTagline());
            Assert.assertEquals(read.getOverview(), movie.getOverview());
            Assert.assertEquals(read.getReleaseDate(), movie.getReleaseDate());
            Assert.assertEquals(MovieRelease.valueOf(read.getStatus().toUpperCase()), movie.getStatus());
            Assert.assertEquals(read.getRuntime(), movie.getDuration());
            Assert.assertEquals(read.getVoteAverage(), movie.getAvgRating());
            Assert.assertEquals(read.getVoteCount(), movie.getRatingCount());
            Assert.assertEquals(read.getRevenue(), movie.getRevenue());
            Assert.assertEquals(read.getBudget(), movie.getBudget());

            Assertions.assertThat(movie.getGenres()).extracting("movieGenre")
                    .containsExactlyInAnyOrder(MovieGenre.ACTION, MovieGenre.ADVENTURE, MovieGenre.SCIENCE_FICTION);
        });
    }

    @Test
    public void testMovieImportAlreadyExist() {
        Integer movieExternalId = 1278678345;

        Movie existingMovie = generateFlatEntityWithoutId(Movie.class);
        existingMovie = movieRepository.save(existingMovie);

        MovieReadDTO read = getMovieReadDTO();
        read.setTitle(existingMovie.getTitle());

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions.catchThrowableOfType(
                () -> movieImporterService.importMovie(movieExternalId), ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Movie.class, ex.getEntityClass());
        Assert.assertEquals(existingMovie.getId(), ex.getEntityId());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        Integer movieExternalId = 178623;

        MovieReadDTO read = getMovieReadDTO();

        List<GenreReadDTO> genres = new ArrayList<>();
        genres.add(new GenreReadDTO("ACTION"));
        genres.add(new GenreReadDTO("ADVENTURE"));
        genres.add(new GenreReadDTO("SCIENCE FICTION"));
        read.setGenres(genres);

        CreditsReadDTO creditsRead = new CreditsReadDTO();
        creditsRead.setId(movieExternalId);
        List<CastReadDTO> cast = new ArrayList<>();

        CastReadDTO newCast = genNewCast();
        CastReadDTO newCastNull = genNewCastNull();

        cast.add(newCast);
        cast.add(newCastNull);
        creditsRead.setCast(cast);

        List<CrewReadDTO> crew = new ArrayList<>();

        CrewReadDTO newCrew = getNewCrew();
        CrewReadDTO newCrewNull = getNewCrewNull();

        crew.add(newCrew);
        crew.add(newCrewNull);
        creditsRead.setCrew(crew);

        PersonReadDTO personRead = getPersonRead();
        PersonReadDTO personReadTwo = getPersonReadTwo();
        PersonReadDTO personReadNull = getPersonReadNull();

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getCredits(movieExternalId)).thenReturn(creditsRead);
        Mockito.when(movieDbClient.getPerson(personRead.getId())).thenReturn(personRead);
        Mockito.when(movieDbClient.getPerson(personReadTwo.getId())).thenReturn(personReadTwo);
        Mockito.when(movieDbClient.getPerson(personReadNull.getId())).thenReturn(personReadNull);

        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId, null);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService.importMovie(movieExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }

    private CastReadDTO genNewCast() {
        CastReadDTO newCast = new CastReadDTO();
        newCast.setCharacter("powerful Mary");
        newCast.setGender(1);
        newCast.setId(777);
        newCast.setName("Mary");
        return newCast;
    }

    private CastReadDTO genNewCastNull() {
        CastReadDTO newCast = new CastReadDTO();
        newCast.setCharacter("powerful Mary");
        newCast.setGender(null);
        newCast.setId(777);
        newCast.setName("Mary");
        return newCast;
    }

    private CrewReadDTO getNewCrew() {
        CrewReadDTO newCrew = new CrewReadDTO();
        newCrew.setDepartment("Camera");
        newCrew.setGender(2);
        newCrew.setId(888);
        newCrew.setJob("dude");
        newCrew.setName("Max");
        return newCrew;
    }

    private CrewReadDTO getNewCrewNull() {
        CrewReadDTO newCrew = new CrewReadDTO();
        newCrew.setDepartment("Camera");
        newCrew.setGender(null);
        newCrew.setId(888);
        newCrew.setJob("dude");
        newCrew.setName("Max");
        return newCrew;
    }

    private PersonReadDTO getPersonRead() {
        PersonReadDTO personRead = new PersonReadDTO();
        personRead.setBiography("good person");
        personRead.setBirthday(LocalDate.of(1979, 12, 12));
        personRead.setDeathday(LocalDate.of(1999, 12, 12));
        personRead.setGender(1);
        personRead.setId(777);
        personRead.setKnownForDepartment("acting");
        personRead.setName("Mary");
        personRead.setPlaceOfBirth("china");
        return personRead;
    }

    private PersonReadDTO getPersonReadTwo() {
        PersonReadDTO personReadTwo = new PersonReadDTO();
        personReadTwo.setBiography("good person");
        personReadTwo.setBirthday(LocalDate.of(1969, 12, 12));
        personReadTwo.setDeathday(LocalDate.of(1989, 12, 12));
        personReadTwo.setGender(2);
        personReadTwo.setId(888);
        personReadTwo.setKnownForDepartment("acting");
        personReadTwo.setName("Max");
        personReadTwo.setPlaceOfBirth("china");
        return personReadTwo;
    }

    private PersonReadDTO getPersonReadNull() {
        PersonReadDTO personReadTwo = new PersonReadDTO();
        personReadTwo.setBiography("gooood man");
        personReadTwo.setBirthday(null);
        personReadTwo.setDeathday(null);
        personReadTwo.setGender(0);
        personReadTwo.setId(888);
        personReadTwo.setKnownForDepartment("camerere man");
        personReadTwo.setName("Ki");
        personReadTwo.setPlaceOfBirth(null);
        return personReadTwo;
    }

    private MovieReadDTO getMovieReadDTO() {
        MovieReadDTO read = new MovieReadDTO();
        read.setId(22225);
        read.setOriginalTitle("avengers");
        read.setTitle("avengers");
        read.setImdbId(null);
        read.setTagline(null);
        read.setOverview(null);
        read.setReleaseDate(LocalDate.of(2020, 1, 1));
        read.setStatus("released");
        read.setRuntime(null);
        read.setOriginalLanguage("english");
        read.setVoteAverage(4.2);
        read.setVoteCount(100);
        read.setBudget(12345678L);
        read.setRevenue(123456789L);
        return read;
    }
}