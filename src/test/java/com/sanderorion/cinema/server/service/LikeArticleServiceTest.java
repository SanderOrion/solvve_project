package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.LikeArticle;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleCreateDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePatchDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePutDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.ArticleRepository;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.LikeArticleRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LikeArticleServiceTest extends BaseTest {

    @Autowired
    private LikeArticleService likeArticleService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Test
    public void testGetLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle likeArticle = createLikeArticle(regUser, article);

        LikeArticleReadDTO readDTO = likeArticleService.getLikeArticle(likeArticle.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(likeArticle, "regUserId", "articleId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getArticleId()).isEqualTo(article.getId());
    }

    @Test
    public void testGetRegUserLikeArticles() {
        RegUser regUser = createRegUser();
        Article article1 = createArticle();
        Article article2 = createArticle();
        LikeArticle likeArticle1 = createLikeArticle(regUser, article1);
        LikeArticle likeArticle2 = createLikeArticle(regUser, article2);

        List<UUID> likeArticlesIds = new ArrayList<>();
        likeArticlesIds.add(likeArticle1.getId());
        likeArticlesIds.add(likeArticle2.getId());

        List<LikeArticleReadDTO> readDTO = likeArticleService.getRegUserLikeArticles(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(likeArticlesIds.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetLikeArticleWrongId() {
        likeArticleService.getLikeArticle(UUID.randomUUID());
    }

    @Test
    public void testCreateLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();

        LikeArticleCreateDTO create = new LikeArticleCreateDTO();
        create.setUserLike(true);

        LikeArticleReadDTO read = likeArticleService.createLikeArticle(regUser.getId(), article.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        LikeArticle likeArticle = likeArticleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(likeArticle, "regUserId", "articleId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getArticleId()).isEqualTo(article.getId());
    }

    @Test
    public void testPatchLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle likeArticle = createLikeArticle(regUser, article);

        LikeArticlePatchDTO patch = new LikeArticlePatchDTO();
        patch.setUserLike(false);

        LikeArticleReadDTO read = likeArticleService.patchLikeArticle(likeArticle.getId(), patch);
        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        likeArticle = likeArticleRepository.findById(read.getId()).get();
        Assertions.assertThat(likeArticle)
                .isEqualToIgnoringGivenFields(read, "regUser", "article", "createdAt", "updatedAt");
        Assertions.assertThat(likeArticle.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(likeArticle.getArticle().getId()).isEqualTo(article.getId());
    }

    @Test
    public void testUpdateLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle likeArticle = createLikeArticle(regUser, article);

        LikeArticlePutDTO put = new LikeArticlePutDTO();
        put.setUserLike(false);

        LikeArticleReadDTO read = likeArticleService.updateLikeArticle(likeArticle.getId(), put);
        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        likeArticle = likeArticleRepository.findById(read.getId()).get();
        Assertions.assertThat(likeArticle)
                .isEqualToIgnoringGivenFields(read, "regUser", "article", "createdAt", "updatedAt");
        Assertions.assertThat(likeArticle.getRegUser().getId()).isEqualTo(regUser.getId());
        Assertions.assertThat(likeArticle.getArticle().getId()).isEqualTo(article.getId());
    }

    @Test
    public void testPatchLikeArticleEmptyPatch() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle likeArticle = createLikeArticle(regUser, article);

        LikeArticlePatchDTO patch = new LikeArticlePatchDTO();
        LikeArticleReadDTO read = likeArticleService.patchLikeArticle(likeArticle.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();
        LikeArticle likeArticleUpdate = likeArticleRepository.findById(read.getId()).get();

        Assertions.assertThat(likeArticleUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(likeArticle).isEqualToIgnoringGivenFields(likeArticleUpdate, "regUser", "article");
    }

    @Test
    public void testDeleteLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle likeArticle = createLikeArticle(regUser, article);

        likeArticleService.deleteLikeArticle(likeArticle.getId());
        Assert.assertFalse(likeArticleRepository.existsById(likeArticle.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteLikeArticleNotFound() {
        likeArticleService.deleteLikeArticle(UUID.randomUUID());
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private Article createArticle() {
        Article article = generateFlatEntityWithoutId(Article.class);
        return articleRepository.save(article);
    }

    private LikeArticle createLikeArticle(RegUser regUser, Article article) {
        LikeArticle likeArticle = generateFlatEntityWithoutId(LikeArticle.class);
        likeArticle.setRegUser(regUser);
        likeArticle.setArticle(article);
        return likeArticleRepository.save(likeArticle);
    }
}