package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.MarkActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MarkActorRoleServiceTest extends BaseTest {

    @Autowired
    private MarkActorRoleService markActorRoleService;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Test
    public void testGetMarkActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        MarkActorRole markActorRole = createMarkActorRole(regUser, actorRole);

        MarkActorRoleReadDTO readDTO = markActorRoleService.getMarkActorRole(markActorRole.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(markActorRole, "regUserId", "actorRoleId");
        Assertions.assertThat(readDTO.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(readDTO.getActorRoleId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testGetMarkActorRoles() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole1 = createActorRole(movie);
        ActorRole actorRole2 = createActorRole(movie);
        MarkActorRole markActorRole1 = createMarkActorRole(regUser, actorRole1);
        MarkActorRole markActorRole2 = createMarkActorRole(regUser, actorRole2);

        List<UUID> markActorRolesIds = new ArrayList<>();
        markActorRolesIds.add(markActorRole1.getId());
        markActorRolesIds.add(markActorRole2.getId());

        List<MarkActorRoleReadDTO> readDTO = markActorRoleService.getUserMarkActorRoles(regUser.getId());

        Assertions.assertThat(readDTO).extracting("id").containsExactlyInAnyOrder(markActorRolesIds.toArray());
    }


    @Test(expected = EntityNotFoundException.class)
    public void testGetMarkActorRoleWrongId() {
        markActorRoleService.getMarkActorRole(UUID.randomUUID());
    }

    @Test
    public void testCreateMarkActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        MarkActorRoleCreateDTO mark = new MarkActorRoleCreateDTO();
        mark.setMark(5);

        MarkActorRoleReadDTO read = markActorRoleService.createMarkActorRole(regUser.getId(), actorRole.getId(), mark);
        Assertions.assertThat(mark).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MarkActorRole markActorRole = markActorRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(markActorRole, "regUserId", "actorRoleId");
        Assertions.assertThat(read.getRegUserId()).isEqualTo(regUser.getId());
        Assertions.assertThat(read.getActorRoleId()).isEqualTo(actorRole.getId());
    }

    @Test
    public void testDeleteMarkActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        MarkActorRole markActorRole = createMarkActorRole(regUser, actorRole);

        markActorRoleService.deleteMarkActorRole(markActorRole.getId());
        Assert.assertFalse(markActorRoleRepository.existsById(markActorRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMarkActorRoleNotFound() {
        markActorRoleService.deleteMarkActorRole(UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MarkActorRole createMarkActorRole(RegUser regUser, ActorRole actorRole) {
        MarkActorRole markActorRole = generateFlatEntityWithoutId(MarkActorRole.class);
        markActorRole.setRegUser(regUser);
        markActorRole.setActorRole(actorRole);
        return markActorRoleRepository.save(markActorRole);
    }
}