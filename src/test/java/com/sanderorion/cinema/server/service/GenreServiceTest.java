package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Genre;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import com.sanderorion.cinema.server.dto.genre.GenreReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.repository.GenreRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class GenreServiceTest extends BaseTest {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetAllGenres() {
        List<GenreReadDTO> allGenres = genreService.getAllGenres();

        int size = allGenres.size();

        Assertions.assertThat(size).isEqualTo(genreRepository.count());
    }

    @Test
    public void testGetGenres() {
        Movie movie = createMovie();
        UUID genreId1 = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);
        UUID genreId2 = genreRepository.findGenreIdByMovieGenre(MovieGenre.ADVENTURE);

        genreService.addGenreToMovie(movie.getId(), genreId1);
        genreService.addGenreToMovie(movie.getId(), genreId2);

        List<GenreReadDTO> genres = genreService.getGenres(movie.getId());

        Assertions.assertThat(genres).extracting("id").containsExactlyInAnyOrder(genreId1, genreId2);
        Assertions.assertThat(genres).extracting("movieGenre")
                .containsExactlyInAnyOrder(MovieGenre.ACTION, MovieGenre.ADVENTURE);
    }

    @Test
    public void testAddGenreToMovie() {
        Movie movie = createMovie();
        UUID genreId = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);

        List<GenreReadDTO> res = genreService.addGenreToMovie(movie.getId(), genreId);

        GenreReadDTO expectedRead = new GenreReadDTO();
        expectedRead.setId(genreId);
        expectedRead.setMovieGenre(MovieGenre.ACTION);
        Assertions.assertThat(res).containsExactly(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfter = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfter.getGenres()).extracting(Genre::getId).containsExactlyInAnyOrder(genreId);
        });
    }

    @Test
    public void testDuplicateGenre() {
        Movie movie = createMovie();
        UUID genreId = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);

        genreService.addGenreToMovie(movie.getId(), genreId);

        Assertions.assertThatThrownBy(() -> {
            genreService.addGenreToMovie(movie.getId(), genreId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongMovieId() {
        UUID wrongMovieId = UUID.randomUUID();
        UUID genreId = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);
        genreService.addGenreToMovie(wrongMovieId, genreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongGenreId() {
        Movie movie = createMovie();
        UUID wrongGenreId = UUID.randomUUID();
        genreService.addGenreToMovie(movie.getId(), wrongGenreId);
    }

    @Test
    public void testRemoveGenreFromMovie() {
        Movie movie = createMovie();
        UUID genreId = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);

        genreService.addGenreToMovie(movie.getId(), genreId);

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromMovie(movie.getId(), genreId);
        Assert.assertTrue(remainingGenres.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Movie movieAfter = movieRepository.findById(movie.getId()).get();
            Assertions.assertThat(movieAfter.getGenres().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {
        Movie movie = createMovie();
        UUID genreId = genreRepository.findGenreIdByMovieGenre(MovieGenre.ACTION);
        genreService.removeGenreFromMovie(movie.getId(), genreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedGenre() {
        Movie movie = createMovie();
        genreService.removeGenreFromMovie(movie.getId(), UUID.randomUUID());
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }
}