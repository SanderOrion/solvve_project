package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.service.MovieService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
public class UpdateAverageRatingOfMoviesJobTest {

    @Autowired
    private UpdateAverageRatingOfMoviesJob updateAverageRatingOfMoviesJob;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @SpyBean
    private MovieService movieService;

    @Test
    public void testUpdateAverageRatingOfMovies() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();

        createMarkMovie(r1, m1, 5);
        createMarkMovie(r1, m1, 3);
        updateAverageRatingOfMoviesJob.updateAverageRatingOfMovies();

        m1 = movieRepository.findById(m1.getId()).get();
        Assert.assertEquals(4, m1.getAvgRating(), Double.MIN_NORMAL);
    }

    @Ignore
    @Test
    public void testMoviesUpdatedIndependently() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        Movie m2 = createMovie();
        createMarkMovie(r1, m1, 4);
        createMarkMovie(r1, m2, 3);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(movieService).updateAverageRatingOfMovie(Mockito.any());

        updateAverageRatingOfMoviesJob.updateAverageRatingOfMovies();

        for (Movie m : movieRepository.findAll()) {
            if (m.getId().equals(failedId[0])) {
                Assert.assertNull(m.getAvgRating());
            } else {
                Assert.assertNotNull(m.getAvgRating());
            }
        }
    }

    private Movie createMovie() {
        Movie movie = new Movie();
        movie.setTitle("MyMovie");
        movie.setReleaseDate(LocalDate.of(2020, 4, 15));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(120);
        movie.setLanguage("English");
        movie.setAvgRating(null);
        movie.setOriginalTitle("MyMovie");
        movie.setOverview("revenge");
        movie.setImdbId("12345678");
        movie.setBudget(12345L);
        movie.setRevenue(123456L);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = new RegUser();
        regUser.setName("Alice");
        regUser.setLogin("alice_1");
        regUser.setEmail("alice@user.user");
        regUser.setEncodedPassword("asd21");
        regUser.setGender(Gender.FEMALE);
        regUser.setUserRatingActivity(5.0);
        regUser.setUserRatingReviews(5.0);
        return regUserRepository.save(regUser);
    }

    private MarkMovie createMarkMovie(RegUser regUser, Movie movie, Integer mark) {
        MarkMovie markMovie = new MarkMovie();
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        markMovie.setMark(mark);
        return markMovieRepository.save(markMovie);
    }
}