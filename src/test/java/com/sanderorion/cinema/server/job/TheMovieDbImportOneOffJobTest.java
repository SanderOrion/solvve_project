package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.client.TheMovieDbClient;
import com.sanderorion.cinema.server.client.themoviedb.dto.MovieReadShortDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.MoviesPageDTO;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import com.sanderorion.cinema.server.exception.ImportedEntityAlreadyExistException;
import com.sanderorion.cinema.server.job.oneoff.TheMovieDbImportOneOffJob;
import com.sanderorion.cinema.server.repository.BaseTest;
import com.sanderorion.cinema.server.service.importer.MovieImporterService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

public class TheMovieDbImportOneOffJobTest extends BaseTest {

    @Autowired
    private TheMovieDbImportOneOffJob job;

    @MockBean
    private TheMovieDbClient client;

    @MockBean
    private MovieImporterService movieImporterService;

    @Test
    public void testDoImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    @Test
    public void testDoImportNoExceptionIfGetPageFailed() {
        Mockito.when(client.getTopRatedMovies()).thenThrow(RuntimeException.class);

        job.doImport();

        Mockito.verifyNoInteractions(movieImporterService);
    }

    @Test
    public void testDoImportFirstFailedAndSecondSuccess()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);
        Mockito.when(movieImporterService.importMovie(page.getResults().get(0).getId())).thenThrow(
                RuntimeException.class);

        job.doImport();

        for (MovieReadShortDTO m : page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    private MoviesPageDTO generatePageWith2Results() {
        MoviesPageDTO page = generateObject(MoviesPageDTO.class);
        page.getResults().add(generateObject(MovieReadShortDTO.class));
        Assert.assertEquals(2, page.getResults().size());

        return page;
    }
}