package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.domain.enums.Profession;
import com.sanderorion.cinema.server.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
public class UpdateAverageRatingMoviePersonOfMoviesTest {

    @Autowired
    private UpdateAverageRatingMoviePersonOfMoviesJob updateAverageRatingMoviePersonOfMoviesJob;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Test
    public void testUpdateAverageRatingOfMovies() {
        RegUser regUser = createRegUser();

        Movie movie1 = createMovie1();
        Movie movie2 = createMovie2();

        MoviePerson moviePerson = createMoviePerson();

        createCrew1(movie1, moviePerson);
        createCrew2(movie2, moviePerson);

        createMarkMovie(regUser, movie1, 4);
        createMarkMovie(regUser, movie2, 3);

        updateAverageRatingMoviePersonOfMoviesJob.updateAverageRatingMoviePersonOfMovies();

        moviePerson = moviePersonRepository.findById(moviePerson.getId()).get();
        Assert.assertEquals(3.5, moviePerson.getAvgMovieRating(), Double.MIN_NORMAL);
    }

    private void createCrew1(Movie movie, MoviePerson moviePerson) {
        Crew c = new Crew();
        c.setMovie(movie);
        c.setMoviePerson(moviePerson);
        c.setProfession(Profession.CAST);
        c.setDescription("Thor");
        c.setCrewName("mase");
        c.setInternalId(848);
        crewRepository.save(c);
    }

    private void createCrew2(Movie movie, MoviePerson moviePerson) {
        Crew c = new Crew();
        c.setMovie(movie);
        c.setMoviePerson(moviePerson);
        c.setProfession(Profession.CAST);
        c.setDescription("Tho2r");
        c.setCrewName("mase2");
        c.setInternalId(8438);
        crewRepository.save(c);
    }

    private Movie createMovie1() {
        Movie movie = new Movie();
        movie.setTitle("MyMovie");
        movie.setReleaseDate(LocalDate.of(2020, 4, 15));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(120);
        movie.setLanguage("English");
        movie.setAvgRating(null);
        movie.setOriginalTitle("MyMovie");
        movie.setOverview("revenge");
        movie.setImdbId("12345678");
        movie.setBudget(12345L);
        movie.setRevenue(123456L);
        return movieRepository.save(movie);
    }

    private Movie createMovie2() {
        Movie movie = new Movie();
        movie.setTitle("MyMovie1");
        movie.setReleaseDate(LocalDate.of(2020, 5, 15));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(130);
        movie.setLanguage("English");
        movie.setAvgRating(null);
        movie.setOriginalTitle("MyMoview");
        movie.setOverview("revengew");
        movie.setImdbId("12345678w");
        movie.setBudget(123435L);
        movie.setRevenue(1233456L);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = new RegUser();
        regUser.setName("Alice");
        regUser.setLogin("alice_1");
        regUser.setEmail("alice@user.user");
        regUser.setEncodedPassword("asd21");
        regUser.setGender(Gender.FEMALE);
        regUser.setUserRatingActivity(5.0);
        regUser.setUserRatingReviews(5.0);
        return regUserRepository.save(regUser);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson m = new MoviePerson();
        m.setName("alex");
        m.setInternalId(12);
        m.setProfession("actor");
        m.setBiography("cool man");
        m.setGender(Gender.MALE);
        return moviePersonRepository.save(m);
    }

    private void createMarkMovie(RegUser regUser, Movie movie, Integer mark) {
        MarkMovie markMovie = new MarkMovie();
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        markMovie.setMark(mark);
        markMovieRepository.save(markMovie);
    }
}