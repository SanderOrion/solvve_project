package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
public class UpdateAverageRatingMoviePersonOfActorRolesJobTest {

    @Autowired
    private UpdateAverageRatingMoviePersonOfActorRolesJob updateAverageRatingMoviePersonOfActorRolesJob;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Test
    public void testUpdateAverageRatingOfActorRoles() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();

        ActorRole actorRole1 = createActorRole1(movie, moviePerson);
        ActorRole actorRole2 = createActorRole2(movie, moviePerson);

        createMarkActorRole(regUser, actorRole1, 4);
        createMarkActorRole(regUser, actorRole2, 5);

        updateAverageRatingMoviePersonOfActorRolesJob.updateAverageRatingMoviePersonOfActorRoles();

        moviePerson = moviePersonRepository.findById(moviePerson.getId()).get();
        Assert.assertEquals(4.5, moviePerson.getAvgRoleRating(), Double.MIN_NORMAL);
    }

    private RegUser createRegUser() {
        RegUser regUser = new RegUser();
        regUser.setName("Alice");
        regUser.setLogin("alice_1");
        regUser.setEmail("alice@user.user");
        regUser.setEncodedPassword("asd21");
        regUser.setGender(Gender.FEMALE);
        regUser.setUserRatingActivity(5.0);
        regUser.setUserRatingReviews(5.0);
        return regUserRepository.save(regUser);
    }

    private Movie createMovie() {
        Movie movie = new Movie();
        movie.setTitle("MyMovie1");
        movie.setReleaseDate(LocalDate.of(2020, 5, 15));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(130);
        movie.setLanguage("English");
        movie.setAvgRating(null);
        movie.setOriginalTitle("MyMoview");
        movie.setOverview("revengew");
        movie.setImdbId("12345678w");
        movie.setBudget(123435L);
        movie.setRevenue(1233456L);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole1(Movie movie, MoviePerson moviePerson) {
        ActorRole a = new ActorRole();
        a.setInternalId(13);
        a.setRoleName("wes");
        a.setRoleInfo("sdw");
        a.setMovie(movie);
        a.setMoviePerson(moviePerson);
        return actorRoleRepository.save(a);
    }

    private ActorRole createActorRole2(Movie movie, MoviePerson moviePerson) {
        ActorRole a = new ActorRole();
        a.setInternalId(23);
        a.setRoleName("sds");
        a.setRoleInfo("vcv");
        a.setMovie(movie);
        a.setMoviePerson(moviePerson);
        return actorRoleRepository.save(a);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson m = new MoviePerson();
        m.setName("alex");
        m.setInternalId(12);
        m.setProfession("actor");
        m.setBiography("cool man");
        m.setGender(Gender.MALE);
        return moviePersonRepository.save(m);
    }

    private void createMarkActorRole(RegUser regUser, ActorRole actorRole, Integer mark) {
        MarkActorRole markActorRole = new MarkActorRole();
        markActorRole.setRegUser(regUser);
        markActorRole.setActorRole(actorRole);
        markActorRole.setMark(mark);
        markActorRoleRepository.save(markActorRole);
    }
}