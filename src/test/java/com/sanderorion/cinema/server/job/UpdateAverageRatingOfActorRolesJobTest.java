package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.MarkActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.repository.ActorRoleRepository;
import com.sanderorion.cinema.server.repository.MarkActorRoleRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.service.ActorRoleService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ScheduledConfigurationsTest.ScheduledTestConfig.class)
@ActiveProfiles("test")
public class UpdateAverageRatingOfActorRolesJobTest {

    @Autowired
    private UpdateAverageRatingOfActorRolesJob updateAverageRatingOfActorRolesJob;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @SpyBean
    private ActorRoleService actorRoleService;

    @Test
    public void testUpdateAverageRatingOfActorRoles() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        ActorRole a1 = createActorRole(m1);

        createMarkActorRole(r1, a1, 5);
        createMarkActorRole(r1, a1, 3);
        updateAverageRatingOfActorRolesJob.updateAverageRatingOfActorRoles();

        a1 = actorRoleRepository.findById(a1.getId()).get();
        Assert.assertEquals(4, a1.getAvgRating(), Double.MIN_NORMAL);
    }

    @Ignore
    @Test
    public void testActorRolesUpdatedIndependently() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        ActorRole a1 = createActorRole(m1);
        ActorRole a2 = createActorRole(m1);
        createMarkActorRole(r1, a1, 4);
        createMarkActorRole(r1, a2, 3);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(actorRoleService).updateAverageRatingOfActorRole(Mockito.any());

        updateAverageRatingOfActorRolesJob.updateAverageRatingOfActorRoles();

        for (ActorRole a : actorRoleRepository.findAll()) {
            if (a.getId().equals(failedId[0])) {
                Assert.assertNull(a.getAvgRating());
            } else {
                Assert.assertNotNull(a.getAvgRating());
            }
        }
    }

    private RegUser createRegUser() {
        RegUser regUser = new RegUser();
        regUser.setName("Alice");
        regUser.setLogin("alice_1");
        regUser.setEmail("alice@user.user");
        regUser.setEncodedPassword("asd21");
        regUser.setGender(Gender.FEMALE);
        regUser.setUserRatingActivity(5.0);
        regUser.setUserRatingReviews(5.0);
        return regUserRepository.save(regUser);
    }

    private Movie createMovie() {
        Movie movie = new Movie();
        movie.setTitle("MyMovie");
        movie.setReleaseDate(LocalDate.of(2020, 4, 15));
        movie.setStatus(MovieRelease.RELEASED);
        movie.setCountry("USA");
        movie.setDuration(120);
        movie.setLanguage("English");
        movie.setOriginalTitle("MyMovie");
        movie.setOverview("revenge");
        movie.setImdbId("12345678");
        movie.setBudget(12345L);
        movie.setRevenue(123456L);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = new ActorRole();
        actorRole.setRoleName("Alex");
        actorRole.setRoleInfo("play in movie");
        actorRole.setAvgRating(null);
        actorRole.setInternalId(548);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }

    private MarkActorRole createMarkActorRole(RegUser regUser, ActorRole actorRole, Integer mark) {
        MarkActorRole markActorRole = new MarkActorRole();
        markActorRole.setRegUser(regUser);
        markActorRole.setActorRole(actorRole);
        markActorRole.setMark(mark);
        return markActorRoleRepository.save(markActorRole);
    }
}