package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ActorRoleRepositoryTest extends BaseTest {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() {
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        assertNotNull(movie.getId());
        assertNotNull(actorRole.getId());
        assertTrue(actorRoleRepository.findById(actorRole.getId()).isPresent());
    }

    @Test
    public void testCreatedAtArticle() {
        Movie movie = createMovie();

        ActorRole a = createActorRole(movie);

        Instant createdAtBeforeReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        a = actorRoleRepository.findById(a.getId()).get();

        Instant createdAtAfterReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtArticle() {
        Movie movie = createMovie();

        ActorRole a = createActorRole(movie);

        Instant updatedAtBeforeReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        a = actorRoleRepository.findById(a.getId()).get();

        a.setAvgRating(4.0);
        a = actorRoleRepository.save(a);

        Instant updatedAtAfterReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Ignore
    @Test
    public void testGetIdsOfActorRoles() {
        Movie m = createMovie();
        Set<UUID> expectedIdsOfActorRoles = new HashSet<>();
        expectedIdsOfActorRoles.add(createActorRole(m).getId());
        expectedIdsOfActorRoles.add(createActorRole(m).getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfActorRoles, actorRoleRepository.getIdsOfActorRoles()
                    .collect(Collectors.toSet()));
        });
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        ActorRole actorRole = new ActorRole();
        actorRoleRepository.save(actorRole);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole actorRole = generateFlatEntityWithoutId(ActorRole.class);
        actorRole.setAvgRating(5.0);
        actorRole.setMovie(movie);
        return actorRoleRepository.save(actorRole);
    }
}