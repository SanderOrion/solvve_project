package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.AdminUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AdminUserRepositoryTest extends BaseTest {

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Test
    public void testSave() {
        AdminUser a = createAdminUser();
        assertNotNull(a.getId());
        assertTrue(adminUserRepository.findById(a.getId()).isPresent());
    }

    @Test
    public void testCreatedAtAdminUser() {
        AdminUser a = createAdminUser();

        Instant createdAtBeforeReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        a = adminUserRepository.findById(a.getId()).get();

        Instant createdAtAfterReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtAdminUser() {
        AdminUser a = createAdminUser();

        Instant updatedAtBeforeReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        a = adminUserRepository.findById(a.getId()).get();

        a.setEmail("alice1@user.user");
        a = adminUserRepository.save(a);

        Instant updatedAtAfterReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        AdminUser a = new AdminUser();
        adminUserRepository.save(a);
    }

    private AdminUser createAdminUser() {
        AdminUser a = generateFlatEntityWithoutId(AdminUser.class);
        a.setEmail("alice2@user.user");
        return adminUserRepository.save(a);
    }
}