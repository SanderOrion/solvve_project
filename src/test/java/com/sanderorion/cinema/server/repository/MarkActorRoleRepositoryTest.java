package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MarkActorRoleRepositoryTest extends BaseTest {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole a = createActorRole(m, moviePerson);
        MarkActorRole mark = createMarkActorRole(reg, a, 5);

        assertNotNull(m.getId());
        assertNotNull(reg.getId());
        assertNotNull(a.getId());
        assertNotNull(mark.getId());
        assertTrue(markActorRoleRepository.findById(mark.getId()).isPresent());
    }

    @Test
    public void testCreatedAtMarkActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);
        MarkActorRole mark = createMarkActorRole(regUser, actorRole, 5);

        Instant createdAtBeforeReload = mark.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        mark = markActorRoleRepository.findById(mark.getId()).get();

        Instant createdAtAfterReload = mark.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testCalcAverageRating() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole a1 = createActorRole(m1, moviePerson);
        ActorRole a2 = createActorRole(m1, moviePerson);

        createMarkActorRole(r1, a1, 5);
        createMarkActorRole(r1, a1, 4);
        createMarkActorRole(r1, a2, 4);

        Assert.assertEquals(4.5, markActorRoleRepository.calcAverageRatingOfActorRole(a1.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCalcAverageRatingMoviePersonForActorRole() {
        RegUser regUser1 = createRegUser();
        RegUser regUser2 = createRegUser();

        Movie movie1 = createMovie();
        Movie movie2 = createMovie();
        Movie movie3 = createMovie();

        MoviePerson moviePerson1 = createMoviePerson();
        MoviePerson moviePerson2 = createMoviePerson();
        MoviePerson moviePerson3 = createMoviePerson();

        ActorRole actorRole1 = createActorRole(movie1, moviePerson1);
        ActorRole actorRole2 = createActorRole(movie2, moviePerson1);
        ActorRole actorRole3 = createActorRole(movie2, moviePerson2);
        ActorRole actorRole4 = createActorRole(movie3, moviePerson3);

        createMarkActorRole(regUser1, actorRole1, 3);
        createMarkActorRole(regUser1, actorRole2, 4);
        createMarkActorRole(regUser2, actorRole2, 5);
        createMarkActorRole(regUser1, actorRole3, 2);
        createMarkActorRole(regUser1, actorRole4, 5);

        Assert.assertEquals(4, markActorRoleRepository
                .calcAverageRatingMoviePersonForActorRole(moviePerson1.getId()), Double.MIN_NORMAL);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        MarkActorRole m = new MarkActorRole();
        markActorRoleRepository.save(m);
    }

    @Test
    public void testGetAmountMark() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MoviePerson moviePerson = createMoviePerson();
        ActorRole actorRole = createActorRole(movie, moviePerson);

        createMarkActorRole(regUser, actorRole, 5);
        createMarkActorRole(regUser, actorRole, 4);
        createMarkActorRole(regUser, actorRole, 4);

        Integer amountLike = 3;
        Integer like = markActorRoleRepository.amountMark(actorRole.getId());

        Assert.assertEquals(like, amountLike);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson m = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(m);
    }

    private ActorRole createActorRole(Movie movie, MoviePerson moviePerson) {
        ActorRole a = generateFlatEntityWithoutId(ActorRole.class);
        a.setMovie(movie);
        a.setMoviePerson(moviePerson);
        return actorRoleRepository.save(a);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MarkActorRole createMarkActorRole(RegUser regUser, ActorRole actorRole, Integer mark) {
        MarkActorRole markActorRole = new MarkActorRole();
        markActorRole.setRegUser(regUser);
        markActorRole.setActorRole(actorRole);
        markActorRole.setMark(mark);
        return markActorRoleRepository.save(markActorRole);
    }
}