package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LikeReviewActorRoleRepositoryTest extends BaseTest {

    @Autowired
    private LikeReviewActorRoleRepository likeReviewActorRoleRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        ActorRole a = createActorRole(m);
        ReviewActorRole review = createReviewActorRole(reg, a);
        LikeReviewActorRole like = createLikeReviewActorRole(reg, review);

        assertNotNull(m.getId());
        assertNotNull(reg.getId());
        assertNotNull(a.getId());
        assertNotNull(review.getId());
        assertNotNull(like.getId());
        assertTrue(likeReviewActorRoleRepository.findById(like.getId()).isPresent());
    }

    @Test
    public void testCreatedAtLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole l = createLikeReviewActorRole(regUser, reviewActorRole);

        Instant createdAtBeforeReload = l.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        l = likeReviewActorRoleRepository.findById(l.getId()).get();

        Instant createdAtAfterReload = l.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtLikeReviewActorRole() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);
        LikeReviewActorRole l = createLikeReviewActorRole(regUser, reviewActorRole);

        Instant updatedAtBeforeReload = l.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        l = likeReviewActorRoleRepository.findById(l.getId()).get();

        l.setUserLike(false);
        l = likeReviewActorRoleRepository.save(l);

        Instant updatedAtAfterReload = l.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        LikeReviewActorRole l = new LikeReviewActorRole();
        likeReviewActorRoleRepository.save(l);
    }

    @Test
    public void testGetLike() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole reviewActorRole = createReviewActorRole(regUser, actorRole);

        createLikeReviewActorRole(regUser, reviewActorRole, true);
        createLikeReviewActorRole(regUser, reviewActorRole, true);
        createLikeReviewActorRole(regUser, reviewActorRole, false);
        createLikeReviewActorRole(regUser, reviewActorRole, false);

        Integer amountLike = 2;
        Integer amountDislike = 2;
        Integer like = likeReviewActorRoleRepository.amountLike(reviewActorRole.getId(), true);
        Integer dislike = likeReviewActorRoleRepository.amountLike(reviewActorRole.getId(), false);

        Assert.assertEquals(like, amountLike);
        Assert.assertEquals(dislike, amountDislike);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole a = generateFlatEntityWithoutId(ActorRole.class);
        a.setMovie(movie);
        return actorRoleRepository.save(a);
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole) {
        ReviewActorRole reviewActorRole = generateFlatEntityWithoutId(ReviewActorRole.class);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(actorRole);
        return reviewActorRoleRepository.save(reviewActorRole);
    }

    private LikeReviewActorRole createLikeReviewActorRole(RegUser regUser, ReviewActorRole reviewActorRole) {
        LikeReviewActorRole l = new LikeReviewActorRole();
        l.setUserLike(true);
        l.setRegUser(regUser);
        l.setReviewActorRole(reviewActorRole);
        return likeReviewActorRoleRepository.save(l);
    }

    private LikeReviewActorRole createLikeReviewActorRole(RegUser regUser, ReviewActorRole reviewActorRole,
                                                          boolean like) {
        LikeReviewActorRole l = new LikeReviewActorRole();
        l.setUserLike(like);
        l.setRegUser(regUser);
        l.setReviewActorRole(reviewActorRole);
        return likeReviewActorRoleRepository.save(l);
    }
}