package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.LikeArticle;
import com.sanderorion.cinema.server.domain.RegUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LikeArticleRepositoryTest extends BaseTest {

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave() {
        RegUser reg = createRegUser();
        Article article = createArticle();
        LikeArticle like = createLikeArticle(reg, article, true);

        assertNotNull(reg.getId());
        assertNotNull(article.getId());
        assertNotNull(like.getId());
        assertTrue(likeArticleRepository.findById(like.getId()).isPresent());
    }

    @Test
    public void testCreatedAtLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle l = createLikeArticle(regUser, article, true);

        Instant createdAtBeforeReload = l.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        l = likeArticleRepository.findById(l.getId()).get();

        Instant createdAtAfterReload = l.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtLikeArticle() {
        RegUser regUser = createRegUser();
        Article article = createArticle();
        LikeArticle l = createLikeArticle(regUser, article, true);

        Instant updatedAtBeforeReload = l.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        l = likeArticleRepository.findById(l.getId()).get();

        l.setUserLike(false);
        l = likeArticleRepository.save(l);

        Instant updatedAtAfterReload = l.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        LikeArticle l = new LikeArticle();
        likeArticleRepository.save(l);
    }

    @Test
    public void testGetLike() {
        RegUser regUser = createRegUser();
        Article article = createArticle();

        createLikeArticle(regUser, article, true);
        createLikeArticle(regUser, article, true);
        createLikeArticle(regUser, article, true);
        createLikeArticle(regUser, article, false);
        createLikeArticle(regUser, article, false);

        Integer amountLike = 3;
        Integer amountDislike = 2;
        Integer like = likeArticleRepository.amountLike(article.getId(), true);
        Integer dislike = likeArticleRepository.amountLike(article.getId(), false);

        Assert.assertEquals(like, amountLike);
        Assert.assertEquals(dislike, amountDislike);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private Article createArticle() {
        Article article = generateFlatEntityWithoutId(Article.class);
        return articleRepository.save(article);
    }

    private LikeArticle createLikeArticle(RegUser regUser, Article article, boolean like) {
        LikeArticle l = new LikeArticle();
        l.setUserLike(like);
        l.setRegUser(regUser);
        l.setArticle(article);
        return likeArticleRepository.save(l);
    }
}