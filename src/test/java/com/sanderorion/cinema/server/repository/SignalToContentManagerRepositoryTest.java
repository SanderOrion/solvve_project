package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SignalToContentManagerRepositoryTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave() {
        RegUser r = createRegUser();
        Article a = createArticle();
        SignalToContentManager s = createSignal(r, a);
        assertNotNull(r.getId());
        assertNotNull(a.getId());
        assertNotNull(s.getId());
        assertTrue(signalToContentManagerRepository.findById(s.getId()).isPresent());
    }

    @Test
    public void testCreatedAtArticle() {
        RegUser r = createRegUser();
        Article a = createArticle();
        SignalToContentManager s = createSignal(r, a);

        Instant createdAtBeforeReload = s.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        s = signalToContentManagerRepository.findById(s.getId()).get();

        Instant createdAtAfterReload = s.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtArticle() {
        RegUser r = createRegUser();
        Article a = createArticle();
        SignalToContentManager s = createSignal(r, a);

        Instant updatedAtBeforeReload = s.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        s = signalToContentManagerRepository.findById(s.getId()).get();

        s.setSignalStatus(SignalStatus.FIXED);
        s = signalToContentManagerRepository.save(s);

        Instant updatedAtAfterReload = s.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        SignalToContentManager s = new SignalToContentManager();
        signalToContentManagerRepository.save(s);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private SignalToContentManager createSignal(RegUser regUser, Article article) {
        SignalToContentManager s = generateFlatEntityWithoutId(SignalToContentManager.class);
        s.setRegUser(regUser);
        s.setArticle(article);
        return signalToContentManagerRepository.save(s);
    }

    private Article createArticle() {
        Article article = generateFlatEntityWithoutId(Article.class);
        return articleRepository.save(article);
    }
}