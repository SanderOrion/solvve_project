package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MovieRepositoryTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() {
        Movie m = createMovie();
        assertNotNull(m.getId());
        assertTrue(movieRepository.findById(m.getId()).isPresent());
    }

    @Test
    public void testCreatedAtMovie() {
        Movie m = createMovie();

        Instant createdAtBeforeReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        m = movieRepository.findById(m.getId()).get();

        Instant createdAtAfterReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtMovie() {
        Movie m = createMovie();

        Instant updatedAtBeforeReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        m = movieRepository.findById(m.getId()).get();

        m.setReleaseDate(LocalDate.of(2020, 4, 10));
        m = movieRepository.save(m);

        Instant updatedAtAfterReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test
    public void testGetIdsOfMovies() {
        Set<UUID> expectedIdsOfMovies = new HashSet<>();
        expectedIdsOfMovies.add(createMovie().getId());
        expectedIdsOfMovies.add(createMovie().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMovies, movieRepository.getIdsOfMovies().collect(Collectors.toSet()));
        });
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        Movie m = new Movie();
        movieRepository.save(m);
    }

    @Test
    public void testGetMoviesLeaderBoard() {
        RegUser r = createRegUser();

        int markCount = 100;
        Set<UUID> moviesIds = new HashSet<>();
        for (int i = 0; i < markCount; i++) {
            Movie m = createMovie();
            moviesIds.add(m.getId());

            createMarkMovie(r, m, 5);
            createMarkMovie(r, m, 4);
        }

        List<MovieInLeaderBoardReadDTO> moviesLeaderBoard = movieRepository.getMoviesLeaderBoard();
        Assertions.assertThat(moviesLeaderBoard).isSortedAccordingTo(
                Comparator.comparing(MovieInLeaderBoardReadDTO::getAvgRating).reversed());

        Assert.assertEquals(moviesIds, moviesLeaderBoard.stream().map(MovieInLeaderBoardReadDTO::getId)
                .collect(Collectors.toSet()));

        for (MovieInLeaderBoardReadDTO m : moviesLeaderBoard) {
            Assert.assertNotNull(m.getTitle());
            Assert.assertNotNull(m.getAvgRating());
            Assert.assertEquals(2, m.getMarksCount());
        }
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        movie.setReleaseDate(LocalDate.of(2020, 4, 15));
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MarkMovie createMarkMovie(RegUser regUser, Movie movie, Integer mark) {
        MarkMovie markMovie = new MarkMovie();
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        markMovie.setMark(mark);
        return markMovieRepository.save(markMovie);
    }
}