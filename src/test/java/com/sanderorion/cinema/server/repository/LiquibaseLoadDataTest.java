package com.sanderorion.cinema.server.repository;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private ContentManagerUserRepository contentManagerUserRepository;

    @Autowired
    private ModeratorUserRepository moderatorUserRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Autowired
    private LikeReviewActorRoleRepository likeReviewActorRoleRepository;

    @Autowired
    private LikeReviewMovieRepository likeReviewMovieRepository;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(genreRepository.count() > 0);
        Assert.assertTrue(articleRepository.count() > 0);
        Assert.assertTrue(movieRepository.count() > 0);
        Assert.assertTrue(crewRepository.count() > 0);
        Assert.assertTrue(actorRoleRepository.count() > 0);
        Assert.assertTrue(moviePersonRepository.count() > 0);
        Assert.assertTrue(adminUserRepository.count() > 0);
        Assert.assertTrue(contentManagerUserRepository.count() > 0);
        Assert.assertTrue(moderatorUserRepository.count() > 0);
        Assert.assertTrue(regUserRepository.count() > 0);
        Assert.assertTrue(reviewActorRoleRepository.count() > 0);
        Assert.assertTrue(reviewMovieRepository.count() > 0);
        Assert.assertTrue(likeArticleRepository.count() > 0);
        Assert.assertTrue(likeReviewActorRoleRepository.count() > 0);
        Assert.assertTrue(likeReviewMovieRepository.count() > 0);
        Assert.assertTrue(markMovieRepository.count() > 0);
        Assert.assertTrue(markActorRoleRepository.count() > 0);
        Assert.assertTrue(signalToContentManagerRepository.count() > 0);
    }
}