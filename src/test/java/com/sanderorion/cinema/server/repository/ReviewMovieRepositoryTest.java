package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewMovieRepositoryTest extends BaseTest {

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        ReviewMovie r = createReviewMovie(reg, m);

        assertNotNull(reg.getId());
        assertNotNull(m.getId());
        assertNotNull(r.getId());
        assertTrue(reviewMovieRepository.findById(r.getId()).isPresent());
    }

    @Test
    public void testCreatedAtRegUser() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie r = createReviewMovie(regUser, movie);

        Instant createdAtBeforeReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = reviewMovieRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtRegUser() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie r = createReviewMovie(regUser, movie);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = reviewMovieRepository.findById(r.getId()).get();

        r.setBody("2");
        r = reviewMovieRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        ReviewMovie r = new ReviewMovie();
        reviewMovieRepository.save(r);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie) {
        ReviewMovie r = generateFlatEntityWithoutId(ReviewMovie.class);
        r.setBody("1");
        r.setRegUser(regUser);
        r.setMovie(movie);
        return reviewMovieRepository.save(r);
    }
}