package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ContentManagerUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ContentManagerUserRepositoryTest extends BaseTest {

    @Autowired
    private ContentManagerUserRepository contentManagerUserRepository;

    @Test
    public void testSave() {
        ContentManagerUser c = createContentManagerUser();

        assertNotNull(c.getId());
        assertTrue(contentManagerUserRepository.findById(c.getId()).isPresent());
    }

    @Test
    public void testCreatedAtContentManagerUser() {
        ContentManagerUser c = createContentManagerUser();

        Instant createdAtBeforeReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        c = contentManagerUserRepository.findById(c.getId()).get();

        Instant createdAtAfterReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtContentManagerUser() {
        ContentManagerUser r = createContentManagerUser();

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = contentManagerUserRepository.findById(r.getId()).get();

        r.setEmail("alice1@user.user");
        r = contentManagerUserRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        ContentManagerUser c = new ContentManagerUser();
        contentManagerUserRepository.save(c);
    }

    private ContentManagerUser createContentManagerUser() {
        ContentManagerUser c = generateFlatEntityWithoutId(ContentManagerUser.class);
        c.setEmail("alice2@user.user");
        return contentManagerUserRepository.save(c);
    }
}