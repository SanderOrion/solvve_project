package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.LikeReviewMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LikeReviewMovieRepositoryTest extends BaseTest {

    @Autowired
    private LikeReviewMovieRepository likeReviewMovieRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        ReviewMovie r = createReviewMovie(reg, m);
        LikeReviewMovie like = createLikeReviewMovie(reg, r);

        assertNotNull(m.getId());
        assertNotNull(reg.getId());
        assertNotNull(r.getId());
        assertNotNull(like.getId());
        assertTrue(likeReviewMovieRepository.findById(like.getId()).isPresent());
    }

    @Test
    public void testCreatedAtLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie l = createLikeReviewMovie(regUser, reviewMovie);

        Instant createdAtBeforeReload = l.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        l = likeReviewMovieRepository.findById(l.getId()).get();

        Instant createdAtAfterReload = l.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtLikeReviewMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);
        LikeReviewMovie l = createLikeReviewMovie(regUser, reviewMovie);

        Instant updatedAtBeforeReload = l.getUpdatedAt();
        assertNotNull(updatedAtBeforeReload);
        l = likeReviewMovieRepository.findById(l.getId()).get();

        l.setUserLike(false);
        l = likeReviewMovieRepository.save(l);

        Instant updatedAtAfterReload = l.getUpdatedAt();
        assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        LikeReviewMovie l = new LikeReviewMovie();
        likeReviewMovieRepository.save(l);
    }

    @Test
    public void testGetLike() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ReviewMovie reviewMovie = createReviewMovie(regUser, movie);

        createLikeReviewMovie(regUser, reviewMovie, true);
        createLikeReviewMovie(regUser, reviewMovie, true);
        createLikeReviewMovie(regUser, reviewMovie, false);

        Integer amountLike = 2;
        Integer amountDislike = 1;
        Integer like = likeReviewMovieRepository.amountLike(reviewMovie.getId(), true);
        Integer dislike = likeReviewMovieRepository.amountLike(reviewMovie.getId(), false);

        Assert.assertEquals(like, amountLike);
        Assert.assertEquals(dislike, amountDislike);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private ReviewMovie createReviewMovie(RegUser regUser, Movie movie) {
        ReviewMovie reviewMovie = generateFlatEntityWithoutId(ReviewMovie.class);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(movie);
        return reviewMovieRepository.save(reviewMovie);
    }

    private LikeReviewMovie createLikeReviewMovie(RegUser regUser, ReviewMovie reviewMovie) {
        LikeReviewMovie l = new LikeReviewMovie();
        l.setUserLike(true);
        l.setRegUser(regUser);
        l.setReviewMovie(reviewMovie);
        return likeReviewMovieRepository.save(l);
    }

    private LikeReviewMovie createLikeReviewMovie(RegUser regUser, ReviewMovie reviewMovie, boolean like) {
        LikeReviewMovie l = new LikeReviewMovie();
        l.setUserLike(like);
        l.setRegUser(regUser);
        l.setReviewMovie(reviewMovie);
        return likeReviewMovieRepository.save(l);
    }
}