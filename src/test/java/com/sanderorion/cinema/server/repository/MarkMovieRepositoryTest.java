package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.Profession;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MarkMovieRepositoryTest extends BaseTest {

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        MarkMovie mark = createMarkMovie(reg, m, 5);

        assertNotNull(m.getId());
        assertNotNull(reg.getId());
        assertNotNull(mark.getId());
        assertTrue(markMovieRepository.findById(mark.getId()).isPresent());
    }

    @Test
    public void testCreatedAtMarkMovie() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        MarkMovie mark = createMarkMovie(regUser, movie, 5);

        Instant createdAtBeforeReload = mark.getCreatedAt();
        assertNotNull(createdAtBeforeReload);
        mark = markMovieRepository.findById(mark.getId()).get();

        Instant createdAtAfterReload = mark.getCreatedAt();
        assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testCalcAverageRating() {
        RegUser r1 = createRegUser();
        Movie m1 = createMovie();
        Movie m2 = createMovie();

        createMarkMovie(r1, m1, 5);
        createMarkMovie(r1, m1, 4);
        createMarkMovie(r1, m2, 4);

        Assert.assertEquals(4.5, markMovieRepository.calcAverageRatingOfMovie(m1.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCalcAverageRatingMoviePersonForMovie() {
        RegUser regUser = createRegUser();

        Movie movie1 = createMovie();
        Movie movie2 = createMovie();
        Movie movie3 = createMovie();
        Movie movie4 = createMovie();

        MoviePerson moviePerson1 = createMoviePerson();
        MoviePerson moviePerson2 = createMoviePerson();
        MoviePerson moviePerson3 = createMoviePerson();

        createCrew(movie1, moviePerson1);
        createCrew(movie2, moviePerson1);
        createCrew(movie3, moviePerson2);
        createCrew(movie4, moviePerson3);

        createMarkMovie(regUser, movie1, 5);
        createMarkMovie(regUser, movie2, 4);
        createMarkMovie(regUser, movie3, 4);
        createMarkMovie(regUser, movie4, 4);

        Assert.assertEquals(4.5, markMovieRepository
                .calcAverageRatingMoviePersonForMovie(moviePerson1.getId()), Double.MIN_NORMAL);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        MarkMovie m = new MarkMovie();
        markMovieRepository.save(m);
    }

    @Test
    public void testGetAmountMark() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();

        createMarkMovie(regUser, movie, 5);
        createMarkMovie(regUser, movie, 4);
        createMarkMovie(regUser, movie, 4);

        Integer amountLike = 3;
        Integer like = markMovieRepository.amountMark(movie.getId());

        Assert.assertEquals(like, amountLike);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson m = generateFlatEntityWithoutId(MoviePerson.class);
        return moviePersonRepository.save(m);
    }

    private Crew createCrew(Movie movie, MoviePerson moviePerson) {
        Crew c = new Crew();
        c.setMovie(movie);
        c.setMoviePerson(moviePerson);
        c.setProfession(Profession.CAST);
        c.setDescription("Thor");
        c.setCrewName("mase");
        c.setInternalId(848);
        return crewRepository.save(c);
    }

    private MarkMovie createMarkMovie(RegUser regUser, Movie movie, Integer mark) {
        MarkMovie markMovie = new MarkMovie();
        markMovie.setRegUser(regUser);
        markMovie.setMovie(movie);
        markMovie.setMark(mark);
        return markMovieRepository.save(markMovie);
    }
}