package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ArticleRepositoryTest extends BaseTest {

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave() {
        Article a = createArticle();
        assertNotNull(a.getId());
        assertTrue(articleRepository.findById(a.getId()).isPresent());
    }

    @Test
    public void testCreatedAtArticle() {
        Article a = createArticle();

        Instant createdAtBeforeReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        a = articleRepository.findById(a.getId()).get();

        Instant createdAtAfterReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtArticle() {
        Article a = createArticle();

        Instant updatedAtBeforeReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        a = articleRepository.findById(a.getId()).get();

        a.setBody("Black");
        a = articleRepository.save(a);

        Instant updatedAtAfterReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        Article article = new Article();
        articleRepository.save(article);
    }

    private Article createArticle() {
        Article a = generateFlatEntityWithoutId(Article.class);
        a.setBody("white");
        return articleRepository.save(a);
    }
}