package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.RegUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RegUserRepositoryTest extends BaseTest {

    @Autowired
    private RegUserRepository regUserRepository;

    @Test
    public void testSave() {
        RegUser r = createRegUser();
        assertNotNull(r.getId());
        assertTrue(regUserRepository.findById(r.getId()).isPresent());
    }

    @Test
    public void testCreatedAtRegUser() {
        RegUser r = createRegUser();

        Instant createdAtBeforeReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = regUserRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtRegUser() {
        RegUser r = createRegUser();

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = regUserRepository.findById(r.getId()).get();

        r.setUserRatingReviews(4.0);
        r = regUserRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        RegUser r = new RegUser();
        regUserRepository.save(r);
    }

    private RegUser createRegUser() {
        RegUser r = generateFlatEntityWithoutId(RegUser.class);
        r.setUserRatingReviews(5.0);
        return regUserRepository.save(r);
    }
}