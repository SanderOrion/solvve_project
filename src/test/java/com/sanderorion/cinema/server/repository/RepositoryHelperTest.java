package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.util.UUID;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetMovie() {
        transactionTemplate.executeWithoutResult(status -> {
            Movie m = generateFlatEntityWithoutId(Movie.class);
            movieRepository.save(m);
            UUID movieId = m.getId();
            Movie referenceIfExist = repositoryHelper.getReferenceIfExist(Movie.class, movieId);
            Assertions.assertThat(referenceIfExist.getId()).isEqualTo(m.getId());
        });
    }
}