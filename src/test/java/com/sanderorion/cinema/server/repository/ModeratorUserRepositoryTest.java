package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ModeratorUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ModeratorUserRepositoryTest extends BaseTest {

    @Autowired
    private ModeratorUserRepository moderatorUserRepository;

    @Test
    public void testSave() {
        ModeratorUser m = createModeratorUser();
        assertNotNull(m.getId());
        assertTrue(moderatorUserRepository.findById(m.getId()).isPresent());
    }

    @Test
    public void testCreatedAtModeratorUser() {
        ModeratorUser m = createModeratorUser();

        Instant createdAtBeforeReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        m = moderatorUserRepository.findById(m.getId()).get();

        Instant createdAtAfterReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtModeratorUser() {
        ModeratorUser m = createModeratorUser();

        Instant updatedAtBeforeReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        m = moderatorUserRepository.findById(m.getId()).get();

        m.setEmail("alice1@user.user");
        m = moderatorUserRepository.save(m);

        Instant updatedAtAfterReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        ModeratorUser m = new ModeratorUser();
        moderatorUserRepository.save(m);
    }

    private ModeratorUser createModeratorUser() {
        ModeratorUser m = generateFlatEntityWithoutId(ModeratorUser.class);
        m.setEmail("alice2@user.user");
        return moderatorUserRepository.save(m);
    }
}