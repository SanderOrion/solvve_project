package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.MoviePerson;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MoviePersonRepositoryTest extends BaseTest {

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() {
        MoviePerson m = createMoviePerson();
        assertNotNull(m.getId());
        assertTrue(moviePersonRepository.findById(m.getId()).isPresent());
    }

    @Test
    public void testCreatedAtMoviePerson() {
        MoviePerson m = createMoviePerson();

        Instant createdAtBeforeReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        m = moviePersonRepository.findById(m.getId()).get();

        Instant createdAtAfterReload = m.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtMoviePerson() {
        MoviePerson m = createMoviePerson();

        Instant updatedAtBeforeReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        m = moviePersonRepository.findById(m.getId()).get();

        m.setAvgRoleRating(4.0);
        m = moviePersonRepository.save(m);

        Instant updatedAtAfterReload = m.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test
    public void testGetIdsOfMoviePersons() {
        Set<UUID> expectedIdsOfMoviePersons = new HashSet<>();
        expectedIdsOfMoviePersons.add(createMoviePerson().getId());
        expectedIdsOfMoviePersons.add(createMoviePerson().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfMoviePersons, moviePersonRepository.getIdsMoviePersons()
                    .collect(Collectors.toSet()));
        });
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        MoviePerson m = new MoviePerson();
        moviePersonRepository.save(m);
    }

    private MoviePerson createMoviePerson() {
        MoviePerson m = generateFlatEntityWithoutId(MoviePerson.class);
        m.setAvgRoleRating(5.0);
        return moviePersonRepository.save(m);
    }
}