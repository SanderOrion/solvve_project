package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewActorRole;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewActorRoleRepositoryTest extends BaseTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        RegUser reg = createRegUser();
        ActorRole a = createActorRole(m);
        ReviewActorRole r = createReviewActorRole(reg, a);

        assertNotNull(m.getId());
        assertNotNull(reg.getId());
        assertNotNull(a.getId());
        assertNotNull(r.getId());
        assertTrue(reviewActorRoleRepository.findById(r.getId()).isPresent());
    }

    @Test
    public void testCreatedAtRegUser() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole r = createReviewActorRole(regUser, actorRole);

        Instant createdAtBeforeReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = reviewActorRoleRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtRegUser() {
        RegUser regUser = createRegUser();
        Movie movie = createMovie();
        ActorRole actorRole = createActorRole(movie);
        ReviewActorRole r = createReviewActorRole(regUser, actorRole);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = reviewActorRoleRepository.findById(r.getId()).get();

        r.setBody("2");
        r = reviewActorRoleRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        ReviewActorRole r = new ReviewActorRole();
        reviewActorRoleRepository.save(r);
    }

    private RegUser createRegUser() {
        RegUser regUser = generateFlatEntityWithoutId(RegUser.class);
        return regUserRepository.save(regUser);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private ActorRole createActorRole(Movie movie) {
        ActorRole a = generateFlatEntityWithoutId(ActorRole.class);
        a.setMovie(movie);
        return actorRoleRepository.save(a);
    }

    private ReviewActorRole createReviewActorRole(RegUser regUser, ActorRole actorRole) {
        ReviewActorRole r = generateFlatEntityWithoutId(ReviewActorRole.class);
        r.setBody("1");
        r.setRegUser(regUser);
        r.setActorRole(actorRole);
        return reviewActorRoleRepository.save(r);
    }
}