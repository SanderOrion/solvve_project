package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Crew;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.enums.Profession;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CrewRepositoryTest extends BaseTest {

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Test
    public void testSave() {
        Movie m = createMovie();
        Crew c = createCrew(m);

        assertNotNull(m.getId());
        assertNotNull(c.getId());
        assertTrue(crewRepository.findById(c.getId()).isPresent());
    }

    @Test
    public void testCreatedAtCrew() {
        Movie movie = createMovie();
        Crew c = createCrew(movie);

        Instant createdAtBeforeReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        c = crewRepository.findById(c.getId()).get();

        Instant createdAtAfterReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtCrew() {
        Movie movie = createMovie();
        Crew c = createCrew(movie);

        Instant updatedAtBeforeReload = c.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        c = crewRepository.findById(c.getId()).get();

        c.setProfession(Profession.PRODUCTION);
        c = crewRepository.save(c);

        Instant updatedAtAfterReload = c.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveArticleValidation() {
        Crew c = new Crew();
        crewRepository.save(c);
    }

    private Movie createMovie() {
        Movie movie = generateFlatEntityWithoutId(Movie.class);
        return movieRepository.save(movie);
    }

    private Crew createCrew(Movie movie) {
        Crew c = new Crew();
        c.setMovie(movie);
        c.setProfession(Profession.CAST);
        c.setDescription("Thor");
        c.setCrewName("mase");
        c.setInternalId(848);
        return crewRepository.save(c);
    }
}