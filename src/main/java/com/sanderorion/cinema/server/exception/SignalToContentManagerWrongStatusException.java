package com.sanderorion.cinema.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class SignalToContentManagerWrongStatusException extends RuntimeException {
    public SignalToContentManagerWrongStatusException(Class entityClass, UUID id) {
        super(String.format("Signal already processed or canceled!", entityClass.getSimpleName(), id));
    }
}