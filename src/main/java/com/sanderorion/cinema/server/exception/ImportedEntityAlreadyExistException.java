package com.sanderorion.cinema.server.exception;

import com.sanderorion.cinema.server.domain.AbstractEntity;
import lombok.Getter;

import java.util.UUID;

@Getter
public class ImportedEntityAlreadyExistException extends Exception {
    private Class<? extends AbstractEntity> entityClass;
    private UUID entityId;

    public ImportedEntityAlreadyExistException(Class<? extends AbstractEntity> entityClass, UUID entityId,
                                               String message) {
        super(message);
        this.entityClass = entityClass;
        this.entityId = entityId;
    }
}