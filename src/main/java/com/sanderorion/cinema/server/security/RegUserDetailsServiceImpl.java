package com.sanderorion.cinema.server.security;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private RegUserRepository regUserRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        RegUser regUser = regUserRepository.findByEmail(username);
        if (regUser == null) {
            throw new UsernameNotFoundException("User " + username + " is not found!");
        }
        return new UserDetailsImpl(regUser);
    }
}