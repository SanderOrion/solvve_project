package com.sanderorion.cinema.server.security;

import com.sanderorion.cinema.server.domain.RegUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserDetailsImpl extends User {

    private UUID id;

    public UserDetailsImpl(RegUser regUser) {
        super(regUser.getEmail(), regUser.getEncodedPassword(), regUser.getUserRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getUserRoleType().toString())).collect(Collectors.toList()));
        id = regUser.getId();
    }
}