package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ReviewActorRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ReviewActorRoleRepositoryCustom {
    Page<ReviewActorRole> getAllConformedReviewsActorRole(UUID id, Pageable pageable);

    Page<ReviewActorRole> getReviewsActorRoleToConfirm(Pageable pageable);
}