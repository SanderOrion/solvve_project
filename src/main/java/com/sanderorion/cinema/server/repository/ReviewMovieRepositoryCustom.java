package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ReviewMovie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ReviewMovieRepositoryCustom {
    Page<ReviewMovie> getAllConfirmedReviewsMovie(UUID id, Pageable pageable);

    Page<ReviewMovie> getReviewsMovieToConfirm(Pageable pageable);
}