package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.SignalToContentManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SignalToContentManagerRepositoryCustom {
    Page<SignalToContentManager> getAllSignalsNeedToFix(Pageable pageable);
}