package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.SignalToModerator;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;
import java.util.stream.Stream;

public interface SignalToModeratorRepository extends CrudRepository<SignalToModerator, UUID>,
        SignalToModeratorRepositoryCustom {

    @Query("select s from SignalToModerator s where s.typo =:typo"
            + " and s.signalStatus = com.sanderorion.cinema.server.domain.enums.SignalStatus.NEED_TO_FIX")
    Stream<SignalToModerator> getAllSimilarSignals(String typo);
}