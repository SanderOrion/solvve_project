package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.MarkMovie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MarkMovieRepository extends CrudRepository<MarkMovie, UUID> {

    @Query("select avg(v.mark) from MarkMovie v where v.movie.id = :movieId")
    Double calcAverageRatingOfMovie(UUID movieId);

    @Query("select count(m.mark) from MarkMovie m where m.movie.id = :movieId")
    Integer amountMark(UUID movieId);

    @Query("select avg(m.mark) from MarkMovie m join m.movie mov join mov.crews cre join cre.moviePerson "
            + "where cre.moviePerson.id = :moviePersonId")
    Double calcAverageRatingMoviePersonForMovie(UUID moviePersonId);
}