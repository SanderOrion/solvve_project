package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.SignalToModerator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SignalToModeratorRepositoryCustom {
    Page<SignalToModerator> getAllSignalsNeedToCheck(Pageable pageable);
}