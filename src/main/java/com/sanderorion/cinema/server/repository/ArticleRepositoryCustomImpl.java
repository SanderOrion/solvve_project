package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ArticleRepositoryCustomImpl implements ArticleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Article> findByFilter(ArticleFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select a from Article a");
        if (filter.getMovieId() != null) {
            qb.append("join a.movies m");
        }
        if (filter.getMoviePersonId() != null) {
            qb.append("join a.moviePersons mp");
        }
        qb.append("where 1=1");
        qb.append("and m.id = :v", filter.getMovieId());
        qb.append("and mp.id = :v", filter.getMoviePersonId());
        qb.appendIn("and a.title in :v", filter.getTitle());
        qb.appendIn("and a.body in :v", filter.getBody());
        qb.append("and a.dateArticle = :v", filter.getDateArticle());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}