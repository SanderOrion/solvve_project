package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MovieRepository extends CrudRepository<Movie, UUID> {

    @Query("select m.id from Movie m")
    Stream<UUID> getIdsOfMovies();

    @Query("select new com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO(m.id, m.title, m.avgRating,"
            + "(select count(mm) from MarkMovie mm where mm.movie.id = m.id and mm.mark is not null))"
            + "from Movie m order by m.avgRating desc")
    List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard();

    Movie findByTitle(String title);
}