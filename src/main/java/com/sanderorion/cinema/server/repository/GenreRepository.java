package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Genre;
import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GenreRepository extends CrudRepository<Genre, UUID> {

    @Query("select g from Genre g")
    List<Genre> getAllGenres();

    @Query("select g.id from Genre g where g.movieGenre = :movieGenre")
    UUID findGenreIdByMovieGenre(MovieGenre movieGenre);

    @Query("select g from Genre g where g.movieGenre = :movieGenre")
    Genre findByGenreName(MovieGenre movieGenre);
}