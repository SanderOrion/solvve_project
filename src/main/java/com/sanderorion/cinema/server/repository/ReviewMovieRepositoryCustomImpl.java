package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

public class ReviewMovieRepositoryCustomImpl implements ReviewMovieRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<ReviewMovie> getAllConfirmedReviewsMovie(UUID id, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select r from ReviewMovie r where 1=1");
        qb.append("and r.movie.id = :v", id);
        qb.append("and r.confirmStatus = :v", ReviewConfirmStatus.CONFIRMED);

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }

    @Override
    public Page<ReviewMovie> getReviewsMovieToConfirm(Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select r from ReviewMovie r where 1=1");
        qb.append("and r.confirmStatus = :v", ReviewConfirmStatus.NEED_TO_CONFIRM);

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}