package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.RegUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RegUserRepository extends CrudRepository<RegUser, UUID> {

    RegUser findByEmail(String email);

    boolean existsByIdAndEmail(UUID id, String email);
}