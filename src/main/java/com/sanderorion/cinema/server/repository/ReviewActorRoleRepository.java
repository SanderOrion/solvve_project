package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ReviewActorRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewActorRoleRepository extends CrudRepository<ReviewActorRole, UUID>,
        ReviewActorRoleRepositoryCustom {
}