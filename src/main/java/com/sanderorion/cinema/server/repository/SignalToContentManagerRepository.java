package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.SignalToContentManager;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface SignalToContentManagerRepository extends CrudRepository<SignalToContentManager, UUID>,
        SignalToContentManagerRepositoryCustom {

    @Query("select s from SignalToContentManager s where s.typo =:typo"
            + " and s.signalStatus = com.sanderorion.cinema.server.domain.enums.SignalStatus.NEED_TO_FIX")
    Stream<SignalToContentManager> getAllSimilarSignals(String typo);
}