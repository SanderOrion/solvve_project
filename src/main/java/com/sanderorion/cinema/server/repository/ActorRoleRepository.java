package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ActorRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface ActorRoleRepository extends CrudRepository<ActorRole, UUID> {

    @Query("select a.id from ActorRole a")
    Stream<UUID> getIdsOfActorRoles();
}