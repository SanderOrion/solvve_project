package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.MoviePerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MoviePersonRepository extends CrudRepository<MoviePerson, UUID> {

    MoviePerson findByInternalId(Integer internalId);

    @Query("select mp.id from MoviePerson mp")
    Stream<UUID> getIdsMoviePersons();
}