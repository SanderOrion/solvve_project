package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class SignalToContentManagerRepositoryCustomImpl implements SignalToContentManagerRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<SignalToContentManager> getAllSignalsNeedToFix(Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select s from SignalToContentManager s where 1=1");
        qb.append("and s.signalStatus = :v", SignalStatus.NEED_TO_FIX);
        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}