package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.LikeReviewActorRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LikeReviewActorRoleRepository extends CrudRepository<LikeReviewActorRole, UUID> {

    @Query("select count(l.userLike) from LikeReviewActorRole l where l.reviewActorRole.id = :reviewActorRoleId"
            + " and l.userLike = :value")
    Integer amountLike(UUID reviewActorRoleId, boolean value);
}