package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.LikeArticle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LikeArticleRepository extends CrudRepository<LikeArticle, UUID> {

    @Query("select count(l.userLike) from LikeArticle l where l.article.id = :articleId and l.userLike = :value")
    Integer amountLike(UUID articleId, boolean value);
}