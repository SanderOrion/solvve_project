package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.UUID;

@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager entityManager;

    public <E> E getReferenceIfExist(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return entityManager.getReference(entityClass, id);
    }

    public <E> E getReferenceIfExistNew(Class<E> entityClass, UUID id) {
        UUID newId = validateExistsNew(entityClass, id);
        if (newId != null) {
            return entityManager.getReference(entityClass, newId);
        }
        return null;
    }

    public <E> E getRequired(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return entityManager.find(entityClass, id);
    }

    public <E> E getByIdRequired(Class<E> entityClass, UUID id) {
        E res = entityManager.find(entityClass, id);
        if (res == null) {
            throw new EntityNotFoundException(entityClass, id);
        }
        return res;
    }

    private <E> void validateExists(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }

    private <E> UUID validateExistsNew(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (exists) {
            return id;
        }
        return null;
    }
}