package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArticleRepositoryCustom {
    Page<Article> findByFilter(ArticleFilter filter, Pageable pageable);
}