package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface ArticleRepository extends CrudRepository<Article, UUID>, ArticleRepositoryCustom {

    List<Article> findByDateArticleBetween(Instant from, Instant before);
}