package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.MarkActorRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MarkActorRoleRepository extends CrudRepository<MarkActorRole, UUID> {

    @Query("select avg(a.mark) from MarkActorRole a where a.actorRole.id = :actorRoleId")
    Double calcAverageRatingOfActorRole(UUID actorRoleId);

    @Query("select count(a.mark) from MarkActorRole a where a.actorRole.id = :actorRoleId")
    Integer amountMark(UUID actorRoleId);

    @Query("select avg(m.mark) from MarkActorRole m join m.actorRole a join a.moviePerson "
            + "where a.moviePerson.id =:moviePersonId")
    Double calcAverageRatingMoviePersonForActorRole(UUID moviePersonId);
}