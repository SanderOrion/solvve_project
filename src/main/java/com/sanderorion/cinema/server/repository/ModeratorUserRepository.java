package com.sanderorion.cinema.server.repository;

import com.sanderorion.cinema.server.domain.ModeratorUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ModeratorUserRepository extends CrudRepository<ModeratorUser, UUID> {
}