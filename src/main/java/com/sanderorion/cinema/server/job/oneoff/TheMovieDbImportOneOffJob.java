package com.sanderorion.cinema.server.job.oneoff;

import com.sanderorion.cinema.server.client.TheMovieDbClient;
import com.sanderorion.cinema.server.client.themoviedb.dto.MovieReadShortDTO;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import com.sanderorion.cinema.server.exception.ImportedEntityAlreadyExistException;
import com.sanderorion.cinema.server.service.AsyncService;
import com.sanderorion.cinema.server.service.importer.MovieImporterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Component
public class TheMovieDbImportOneOffJob {

    @Value("${themoviedb.import.job.enabled}")
    private boolean enable;

    @Autowired
    private AsyncService asyncService;

    @PostConstruct
    void executeJob() {
        if (!enable) {
            log.info("Import is disabled");
            return;
        }

        asyncService.executeAsync(this::doImport);
    }

    @Autowired
    private TheMovieDbClient client;

    @Autowired
    private MovieImporterService movieImporterService;

    public void doImport() {
        log.info("Starting import");

        try {
            List<MovieReadShortDTO> moviesToImport = client.getTopRatedMovies().getResults();
            int successfullyImported = 0;
            int skipped = 0;
            int failed = 0;

            for (MovieReadShortDTO m : moviesToImport) {
                try {
                    successfullyImported++;
                    movieImporterService.importMovie(m.getId());
                } catch (ImportedEntityAlreadyExistException | ImportAlreadyPerformedException e) {
                    log.info("Can't import movie id = {}, title = {} : {}", m.getId(), m.getTitle(), e.getMessage());
                    skipped++;
                } catch (Exception e) {
                    log.warn("Failed to import movie id = {}, title = {}", m.getId(), m.getTitle(), e);
                    failed++;
                }
            }
            log.info("Total movies to import: {}, successfully imported: {}, skipped: {}, failed {}",
                    moviesToImport.size(), successfullyImported, skipped, failed);
        } catch (Exception e) {
            log.warn("Failed to perform import", e);
        }

        log.info("Import finished");
    }
}