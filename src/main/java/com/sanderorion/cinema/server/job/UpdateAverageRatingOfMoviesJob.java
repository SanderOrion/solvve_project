package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.service.MovieService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingOfMoviesJob {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "${update.average.rating.of.movies.job.cron}")
    public void updateAverageRatingOfMovies() {
        log.info("Job update average rating of movies started");

        movieRepository.getIdsOfMovies().forEach(movieId -> {
            try {
                movieService.updateAverageRatingOfMovie(movieId);
            } catch (Exception e) {
                log.error("Failed to update average mark for movie: {}", movieId, e);
            }
        });

        log.info("Job update average rating of movies finished");
    }
}