package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.repository.ActorRoleRepository;
import com.sanderorion.cinema.server.service.ActorRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingOfActorRolesJob {

    @Autowired
    private ActorRoleService actorRoleService;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "${update.average.rating.of.actorroles.job.cron}")
    public void updateAverageRatingOfActorRoles() {
        log.info("Job update average rating of actor roles started");

        actorRoleRepository.getIdsOfActorRoles().forEach(actorRoleId -> {
            try {
                actorRoleService.updateAverageRatingOfActorRole(actorRoleId);
            } catch (Exception e) {
                log.error("Failed to update average mark for actorRole: {}", actorRoleId, e);
            }
        });

        log.info("Job update average rating of actor roles finished");
    }
}