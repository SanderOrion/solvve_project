package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.repository.MoviePersonRepository;
import com.sanderorion.cinema.server.service.MoviePersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingMoviePersonOfMoviesJob {

    @Autowired
    private MoviePersonService moviePersonService;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "${update.average.rating.movie.person.of.movies.job.cron}")
    public void updateAverageRatingMoviePersonOfMovies() {
        log.info("Job update average rating movie person of movies started");

        moviePersonRepository.getIdsMoviePersons().forEach(moviePersonId -> {
            try {
                moviePersonService.updateCalcAverageRatingMoviePersonForMovie(moviePersonId);
            } catch (Exception e) {
                log.error("Failed to update average mark movie person for movies: {}", moviePersonId, e);
            }
        });

        log.info("Job update average rating movie person of movies finished");
    }
}