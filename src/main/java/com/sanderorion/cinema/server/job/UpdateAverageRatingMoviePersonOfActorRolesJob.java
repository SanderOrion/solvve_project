package com.sanderorion.cinema.server.job;

import com.sanderorion.cinema.server.repository.MoviePersonRepository;
import com.sanderorion.cinema.server.service.MoviePersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingMoviePersonOfActorRolesJob {

    @Autowired
    private MoviePersonService moviePersonService;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Scheduled(cron = "${update.average.rating.movie.person.of.actor.roles.job.cron}")
    public void updateAverageRatingMoviePersonOfActorRoles() {
        log.info("Job update average rating movie person of actor roles started");

        moviePersonRepository.getIdsMoviePersons().forEach(moviePersonId -> {
            try {
                moviePersonService.updateCalcAverageRatingMoviePersonForActorRole(moviePersonId);
            } catch (Exception e) {
                log.error("Failed to update average mark movie person for actor roles: {}", moviePersonId, e);
            }
        });

        log.info("Job update average rating movie person of actor roles finished");
    }
}