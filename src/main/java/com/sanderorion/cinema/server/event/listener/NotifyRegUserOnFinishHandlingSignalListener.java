package com.sanderorion.cinema.server.event.listener;

import com.sanderorion.cinema.server.event.SignalToContentManagerStatusChangedEvent;
import com.sanderorion.cinema.server.service.RegUserNotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotifyRegUserOnFinishHandlingSignalListener {

    @Autowired
    private RegUserNotificationService regUserNotificationService;

    @Async
    @EventListener(condition = "#event.newStatus == T(com.sanderorion.cinema.server.domain.enums.SignalStatus).FIXED")
    public void onEvent(SignalToContentManagerStatusChangedEvent event) {
        log.info("handling {}", event);
        regUserNotificationService.notifyOnSignalStatusChangedToFinished(event.getSignalToContentManagerId());
    }
}