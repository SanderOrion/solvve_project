package com.sanderorion.cinema.server.event;

import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class SignalToContentManagerStatusChangedEvent {
    private UUID signalToContentManagerId;
    private SignalStatus newStatus;
}