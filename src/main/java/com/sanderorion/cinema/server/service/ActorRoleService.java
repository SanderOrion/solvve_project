package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePutDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.ActorRoleRepository;
import com.sanderorion.cinema.server.repository.MarkActorRoleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ActorRoleService {

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Transactional
    public ActorRoleReadDTO getActorRole(UUID id) {
        ActorRole actorRole = repositoryHelper.getRequired(ActorRole.class, id);
        Integer amountMark = markActorRoleRepository.amountMark(id);
        actorRole.setRatingCount(amountMark);
        actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    @Transactional
    public List<ActorRoleReadDTO> getMovieActorRoles(UUID movieId) {
        return repositoryHelper.getReferenceIfExist(Movie.class, movieId)
                .getActorRoles()
                .stream()
                .map(a -> translationService.translate(a, ActorRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    public ActorRoleReadDTO createActorRole(UUID movieId, ActorRoleCreateDTO create) {
        ActorRole actorRole = translationService.translate(create, ActorRole.class);
        actorRole.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        actorRole = actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public ActorRoleReadDTO patchActorRole(UUID id, ActorRolePatchDTO patch) {
        ActorRole actorRole = repositoryHelper.getRequired(ActorRole.class, id);
        translationService.map(patch, actorRole);
        actorRole = actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public ActorRoleReadDTO updateActorRole(UUID id, ActorRolePutDTO put) {
        ActorRole actorRole = repositoryHelper.getRequired(ActorRole.class, id);
        translationService.map(put, actorRole);
        actorRole = actorRoleRepository.save(actorRole);
        return translationService.translate(actorRole, ActorRoleReadDTO.class);
    }

    public void deleteActorRole(UUID id) {
        actorRoleRepository.delete(repositoryHelper.getRequired(ActorRole.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingOfActorRole(UUID actorRoleId) {
        Double avgRating = markActorRoleRepository.calcAverageRatingOfActorRole(actorRoleId);
        ActorRole actorRole = actorRoleRepository.findById(actorRoleId).orElseThrow(
                () -> new EntityNotFoundException(ActorRole.class, actorRoleId));

        log.info("Setting new average rating of actor role: {}. Old value: {}, new value: {}", actorRoleId,
                actorRole.getAvgRating(), avgRating);

        if (avgRating != null) {
            avgRating = Precision.round(avgRating, 2);
            actorRole.setAvgRating(avgRating);
        }
        actorRoleRepository.save(actorRole);
    }
}