package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.LikeReviewActorRole;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewActorRole;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.repository.LikeReviewActorRoleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LikeReviewActorRoleService {

    @Autowired
    private LikeReviewActorRoleRepository likeReviewActorRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public LikeReviewActorRoleReadDTO getLikeReviewActorRole(UUID id) {
        LikeReviewActorRole likeReviewActorRole = repositoryHelper.getRequired(LikeReviewActorRole.class, id);
        return translationService.translate(likeReviewActorRole, LikeReviewActorRoleReadDTO.class);
    }

    @Transactional
    public List<LikeReviewActorRoleReadDTO> getRegLikeReviewActorRoles(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getLikeReviewActorRoles()
                .stream()
                .map(l -> translationService.translate(l, LikeReviewActorRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    public LikeReviewActorRoleReadDTO createLikeReviewActorRole(UUID regUserId, UUID reviewActorRoleId,
                                                                LikeReviewActorRoleCreateDTO create) {
        LikeReviewActorRole likeReviewActorRole = translationService.translate(create, LikeReviewActorRole.class);
        likeReviewActorRole.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        likeReviewActorRole.setReviewActorRole(repositoryHelper
                .getReferenceIfExist(ReviewActorRole.class, reviewActorRoleId));
        likeReviewActorRole = likeReviewActorRoleRepository.save(likeReviewActorRole);
        return translationService.translate(likeReviewActorRole, LikeReviewActorRoleReadDTO.class);
    }

    public LikeReviewActorRoleReadDTO patchLikeReviewActorRole(UUID id, LikeReviewActorRolePatchDTO patch) {
        LikeReviewActorRole likeReviewActorRole = repositoryHelper.getRequired(LikeReviewActorRole.class, id);
        translationService.map(patch, likeReviewActorRole);
        likeReviewActorRole = likeReviewActorRoleRepository.save(likeReviewActorRole);
        return translationService.translate(likeReviewActorRole, LikeReviewActorRoleReadDTO.class);
    }

    public LikeReviewActorRoleReadDTO updateLikeReviewActorRole(UUID id, LikeReviewActorRolePutDTO put) {
        LikeReviewActorRole likeReviewActorRole = repositoryHelper.getRequired(LikeReviewActorRole.class, id);
        translationService.map(put, likeReviewActorRole);
        likeReviewActorRole = likeReviewActorRoleRepository.save(likeReviewActorRole);
        return translationService.translate(likeReviewActorRole, LikeReviewActorRoleReadDTO.class);
    }

    public void deleteLikeReviewActorRole(UUID id) {
        likeReviewActorRoleRepository.delete(repositoryHelper.getRequired(LikeReviewActorRole.class, id));
    }
}