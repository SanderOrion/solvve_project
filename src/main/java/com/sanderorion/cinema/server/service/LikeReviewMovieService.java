package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.LikeReviewMovie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.repository.LikeReviewMovieRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LikeReviewMovieService {

    @Autowired
    private LikeReviewMovieRepository likeReviewMovieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public LikeReviewMovieReadDTO getLikeReviewMovie(UUID id) {
        LikeReviewMovie likeReviewMovie = repositoryHelper.getRequired(LikeReviewMovie.class, id);
        return translationService.translate(likeReviewMovie, LikeReviewMovieReadDTO.class);
    }

    @Transactional
    public List<LikeReviewMovieReadDTO> getRegLikeReviewMovies(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getLikeReviewMovies()
                .stream()
                .map(l -> translationService.translate(l, LikeReviewMovieReadDTO.class))
                .collect(Collectors.toList());
    }

    public LikeReviewMovieReadDTO createLikeReviewMovie(UUID regUserId, UUID reviewMovieId,
                                                        LikeReviewMovieCreateDTO create) {
        LikeReviewMovie likeReviewMovie = translationService.translate(create, LikeReviewMovie.class);
        likeReviewMovie.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        likeReviewMovie.setReviewMovie(repositoryHelper.getReferenceIfExist(ReviewMovie.class, reviewMovieId));
        likeReviewMovie = likeReviewMovieRepository.save(likeReviewMovie);
        return translationService.translate(likeReviewMovie, LikeReviewMovieReadDTO.class);
    }

    public LikeReviewMovieReadDTO patchLikeReviewMovie(UUID id, LikeReviewMoviePatchDTO patch) {
        LikeReviewMovie likeReviewMovie = repositoryHelper.getRequired(LikeReviewMovie.class, id);
        translationService.map(patch, likeReviewMovie);
        likeReviewMovie = likeReviewMovieRepository.save(likeReviewMovie);
        return translationService.translate(likeReviewMovie, LikeReviewMovieReadDTO.class);
    }

    public LikeReviewMovieReadDTO updateLikeReviewMovie(UUID id, LikeReviewMoviePutDTO put) {
        LikeReviewMovie likeReviewMovie = repositoryHelper.getRequired(LikeReviewMovie.class, id);
        translationService.map(put, likeReviewMovie);
        likeReviewMovie = likeReviewMovieRepository.save(likeReviewMovie);
        return translationService.translate(likeReviewMovie, LikeReviewMovieReadDTO.class);
    }

    public void deleteLikeReviewMovie(UUID id) {
        likeReviewMovieRepository.delete(repositoryHelper.getRequired(LikeReviewMovie.class, id));
    }
}