package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.MarkActorRole;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.repository.MarkActorRoleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MarkActorRoleService {

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MarkActorRoleReadDTO getMarkActorRole(UUID id) {
        MarkActorRole markActorRole = repositoryHelper.getRequired(MarkActorRole.class, id);
        return translationService.translate(markActorRole, MarkActorRoleReadDTO.class);
    }

    @Transactional
    public List<MarkActorRoleReadDTO> getUserMarkActorRoles(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getMarkActorRoles()
                .stream()
                .map(m -> translationService.translate(m, MarkActorRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    public MarkActorRoleReadDTO createMarkActorRole(UUID regUserId, UUID actorRoleId, MarkActorRoleCreateDTO create) {
        MarkActorRole markActorRole = translationService.translate(create, MarkActorRole.class);
        markActorRole.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        markActorRole.setActorRole(repositoryHelper.getReferenceIfExist(ActorRole.class, actorRoleId));
        markActorRole = markActorRoleRepository.save(markActorRole);
        return translationService.translate(markActorRole, MarkActorRoleReadDTO.class);
    }

    public void deleteMarkActorRole(UUID id) {
        markActorRoleRepository.delete(repositoryHelper.getRequired(MarkActorRole.class, id));
    }
}