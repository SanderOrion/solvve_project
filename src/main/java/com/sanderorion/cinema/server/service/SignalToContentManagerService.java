package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.exception.SignalToContentManagerWrongStatusException;
import com.sanderorion.cinema.server.repository.ArticleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import com.sanderorion.cinema.server.repository.SignalToContentManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SignalToContentManagerService {

    @Autowired
    private SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public SignalToContentManagerReadDTO getSignalToCM(UUID id) {
        SignalToContentManager signal = repositoryHelper.getRequired(SignalToContentManager.class, id);
        return translationService.translate(signal, SignalToContentManagerReadDTO.class);
    }

    @Transactional
    public List<SignalToContentManagerReadDTO> getRegUserSignalsToCM(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getSignalToContentManagers()
                .stream()
                .map(s -> translationService.translate(s, SignalToContentManagerReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public PageResult<SignalToContentManagerReadDTO> getSignalsForFix(Pageable pageable) {
        Page<SignalToContentManager> signals = signalToContentManagerRepository.getAllSignalsNeedToFix(pageable);
        return translationService.toPageResult(signals, SignalToContentManagerReadDTO.class);
    }

    public SignalToContentManagerReadDTO createSignalToCM(UUID articleId, UUID regUserId,
                                                          SignalToContentManagerCreateDTO create) {
        SignalToContentManager signal = translationService.translate(create, SignalToContentManager.class);
        signal.setArticle(repositoryHelper.getReferenceIfExist(Article.class, articleId));
        signal.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        signal.setSignalStatus(SignalStatus.NEED_TO_FIX);
        signal = signalToContentManagerRepository.save(signal);
        return translationService.translate(signal, SignalToContentManagerReadDTO.class);
    }

    @Transactional
    public SignalToContentManagerReadDTO updateSignalToCM(UUID id, UUID cmId, SignalToContentManagerPutDTO put) {
        SignalToContentManager signal = repositoryHelper.getRequired(SignalToContentManager.class, id);
        translationService.map(put, signal);

        if (signal.getSignalStatus() != SignalStatus.NEED_TO_FIX) {
            throw new SignalToContentManagerWrongStatusException(SignalToContentManager.class, id);
        }

        Article article = articleRepository.findById(signal.getArticle().getId()).get();

        if (article.getBody().contains(signal.getTypo()) & put.getCorrectText() != null) {
            fixArticle(article, signal, put.getCorrectText());
        }

        if (article.getBody().contains(signal.getTypo()) & put.getCorrectText() == null) {
            fixArticle(article, signal, signal.getUserSuggestion());
        }

        signal.setSignalStatus(SignalStatus.FIXED);
        signal.setCorrectionTime(Instant.now().truncatedTo(ChronoUnit.MICROS));
        signal.setContentManagerUser(repositoryHelper.getReferenceIfExist(RegUser.class, cmId));

        List<SignalToContentManager> similarSignals = signalToContentManagerRepository
                .getAllSimilarSignals(signal.getTypo())
                .collect(Collectors.toList());

        for (SignalToContentManager s : similarSignals) {
            s.setSignalStatus(SignalStatus.CANCELED);
            s.setCorrectionTime(Instant.now().truncatedTo(ChronoUnit.MICROS));
            s.setContentManagerUser(repositoryHelper.getReferenceIfExist(RegUser.class, cmId));
            signalToContentManagerRepository.save(s);
        }

        signal = signalToContentManagerRepository.save(signal);
        return translationService.translate(signal, SignalToContentManagerReadDTO.class);
    }

    public void deleteSignalToCM(UUID id) {
        signalToContentManagerRepository.delete(repositoryHelper.getRequired(SignalToContentManager.class, id));
    }

    private void fixArticle(Article article, SignalToContentManager signal, String fixText) {
        StringBuffer fix = new StringBuffer(article.getBody());
        int i = article.getBody().indexOf(signal.getTypo());
        fix.replace(i, i + signal.getTypo().length(), fixText);
        signal.setCorrectText(fixText);
        article.setBody(fix.toString());
        articleRepository.save(article);
    }
}