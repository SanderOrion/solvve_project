package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ContentManagerUser;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserCreateDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPatchDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPutDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserReadDTO;
import com.sanderorion.cinema.server.repository.ContentManagerUserRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ContentManagerUserService {

    @Autowired
    private ContentManagerUserRepository contentManagerUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ContentManagerUserReadDTO getContentManagerUser(UUID id) {
        ContentManagerUser contentManagerUser = repositoryHelper.getRequired(ContentManagerUser.class, id);
        return translationService.translate(contentManagerUser, ContentManagerUserReadDTO.class);
    }

    public ContentManagerUserReadDTO createContentManagerUser(ContentManagerUserCreateDTO create) {
        ContentManagerUser contentManagerUser = translationService.translate(create, ContentManagerUser.class);
        contentManagerUser = contentManagerUserRepository.save(contentManagerUser);
        return translationService.translate(contentManagerUser, ContentManagerUserReadDTO.class);
    }

    public ContentManagerUserReadDTO patchContentManagerUser(UUID id, ContentManagerUserPatchDTO patch) {
        ContentManagerUser contentManagerUser = repositoryHelper.getRequired(ContentManagerUser.class, id);
        translationService.map(patch, contentManagerUser);
        contentManagerUser = contentManagerUserRepository.save(contentManagerUser);
        return translationService.translate(contentManagerUser, ContentManagerUserReadDTO.class);
    }

    public ContentManagerUserReadDTO updateContentManagerUser(UUID id, ContentManagerUserPutDTO put) {
        ContentManagerUser contentManagerUser = repositoryHelper.getRequired(ContentManagerUser.class, id);
        translationService.map(put, contentManagerUser);
        contentManagerUser = contentManagerUserRepository.save(contentManagerUser);
        return translationService.translate(contentManagerUser, ContentManagerUserReadDTO.class);
    }

    public void deleteContentManagerUser(UUID id) {
        contentManagerUserRepository.delete(repositoryHelper.getRequired(ContentManagerUser.class, id));
    }
}
