package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.LikeArticle;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleCreateDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePatchDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePutDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.repository.LikeArticleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LikeArticleService {

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public LikeArticleReadDTO getLikeArticle(UUID id) {
        LikeArticle likeArticle = repositoryHelper.getRequired(LikeArticle.class, id);
        return translationService.translate(likeArticle, LikeArticleReadDTO.class);
    }

    @Transactional
    public List<LikeArticleReadDTO> getRegUserLikeArticles(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getLikeArticles()
                .stream()
                .map(l -> translationService.translate(l, LikeArticleReadDTO.class))
                .collect(Collectors.toList());
    }

    public LikeArticleReadDTO createLikeArticle(UUID regUserId, UUID articleId, LikeArticleCreateDTO create) {
        LikeArticle likeArticle = translationService.translate(create, LikeArticle.class);
        likeArticle.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        likeArticle.setArticle(repositoryHelper.getReferenceIfExist(Article.class, articleId));
        likeArticle = likeArticleRepository.save(likeArticle);
        return translationService.translate(likeArticle, LikeArticleReadDTO.class);
    }

    public LikeArticleReadDTO patchLikeArticle(UUID id, LikeArticlePatchDTO patch) {
        LikeArticle likeArticle = repositoryHelper.getRequired(LikeArticle.class, id);
        translationService.map(patch, likeArticle);
        likeArticle = likeArticleRepository.save(likeArticle);
        return translationService.translate(likeArticle, LikeArticleReadDTO.class);
    }

    public LikeArticleReadDTO updateLikeArticle(UUID id, LikeArticlePutDTO put) {
        LikeArticle likeArticle = repositoryHelper.getRequired(LikeArticle.class, id);
        translationService.map(put, likeArticle);
        likeArticle = likeArticleRepository.save(likeArticle);
        return translationService.translate(likeArticle, LikeArticleReadDTO.class);
    }

    public void deleteLikeArticle(UUID id) {
        likeArticleRepository.delete(repositoryHelper.getRequired(LikeArticle.class, id));
    }
}