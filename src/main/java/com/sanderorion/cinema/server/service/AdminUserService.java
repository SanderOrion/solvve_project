package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.AdminUser;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserCreateDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPatchDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPutDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserReadDTO;
import com.sanderorion.cinema.server.repository.AdminUserRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AdminUserService {

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public AdminUserReadDTO getAdminUser(UUID id) {
        AdminUser adminUser = repositoryHelper.getRequired(AdminUser.class, id);
        return translationService.translate(adminUser, AdminUserReadDTO.class);
    }

    public AdminUserReadDTO createAdminUser(AdminUserCreateDTO create) {
        AdminUser adminUser = translationService.translate(create, AdminUser.class);
        adminUser = adminUserRepository.save(adminUser);
        return translationService.translate(adminUser, AdminUserReadDTO.class);
    }

    public AdminUserReadDTO patchAdminUser(UUID id, AdminUserPatchDTO patch) {
        AdminUser adminUser = repositoryHelper.getRequired(AdminUser.class, id);
        translationService.map(patch, adminUser);
        adminUser = adminUserRepository.save(adminUser);
        return translationService.translate(adminUser, AdminUserReadDTO.class);
    }

    public AdminUserReadDTO updateAdminUser(UUID id, AdminUserPutDTO put) {
        AdminUser adminUser = repositoryHelper.getRequired(AdminUser.class, id);
        translationService.map(put, adminUser);
        adminUser = adminUserRepository.save(adminUser);
        return translationService.translate(adminUser, AdminUserReadDTO.class);
    }

    public void deleteAdminUser(UUID id) {
        adminUserRepository.delete(repositoryHelper.getRequired(AdminUser.class, id));
    }
}