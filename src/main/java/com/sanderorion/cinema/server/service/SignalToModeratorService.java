package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutTrustDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorCreateDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.exception.SignalToContentManagerWrongStatusException;
import com.sanderorion.cinema.server.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SignalToModeratorService {

    @Autowired
    private SignalToModeratorRepository signalToModeratorRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    public SignalToModeratorReadDTO getSignalToModerator(UUID id) {
        SignalToModerator signal = repositoryHelper.getRequired(SignalToModerator.class, id);
        return translationService.translate(signal, SignalToModeratorReadDTO.class);
    }

    @Transactional
    public List<SignalToModeratorReadDTO> getRegUserSignalsToModerator(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getSignalToModerators()
                .stream()
                .map(s -> translationService.translate(s, SignalToModeratorReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public PageResult<SignalToModeratorReadDTO> getSignalsForCheck(Pageable pageable) {
        Page<SignalToModerator> signals = signalToModeratorRepository.getAllSignalsNeedToCheck(pageable);
        return translationService.toPageResult(signals, SignalToModeratorReadDTO.class);
    }

    @Transactional
    public SignalToModeratorReadDTO createSignalToModerator(UUID regUserId, UUID reviewId,
                                                            SignalToModeratorCreateDTO create) {
        SignalToModerator signal = translationService.translate(create, SignalToModerator.class);
        signal.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));

        if (repositoryHelper.getReferenceIfExistNew(ReviewMovie.class, reviewId) != null) {
            signal.setReviewMovie(repositoryHelper.getReferenceIfExist(ReviewMovie.class, reviewId));
        }

        if (repositoryHelper.getReferenceIfExistNew(ReviewActorRole.class, reviewId) != null) {
            signal.setReviewActorRole(repositoryHelper.getReferenceIfExist(ReviewActorRole.class, reviewId));
        }

        signal.setSignalStatus(SignalStatus.NEED_TO_FIX);
        signal = signalToModeratorRepository.save(signal);
        return translationService.translate(signal, SignalToModeratorReadDTO.class);
    }

    @Transactional
    public SignalToModeratorReadDTO updateSignalToModerator(UUID id, UUID moderatorId, SignalToModeratorPutDTO put) {
        SignalToModerator signal = repositoryHelper.getRequired(SignalToModerator.class, id);
        translationService.map(put, signal);

        if (signal.getSignalStatus() != SignalStatus.NEED_TO_FIX) {
            throw new SignalToContentManagerWrongStatusException(SignalToModerator.class, id);
        }

        if (signal.getReviewMovie() != null) {
            ReviewMovie reviewMovie = reviewMovieRepository.findById(signal.getReviewMovie().getId()).get();
            if (reviewMovie.getBody().contains(signal.getTypo()) & put.getCorrectText() != null) {
                fixReview(reviewMovie, signal, put.getCorrectText());
            }
        }

        if (signal.getReviewActorRole() != null) {
            ReviewActorRole reviewActorRole = reviewActorRoleRepository
                    .findById(signal.getReviewActorRole().getId()).get();
            if (reviewActorRole.getBody().contains(signal.getTypo()) & put.getCorrectText() != null) {
                fixReview(reviewActorRole, signal, put.getCorrectText());
            }
        }

        signal.setSignalStatus(SignalStatus.FIXED);
        signal.setCorrectionTime(Instant.now().truncatedTo(ChronoUnit.MICROS));
        signal.setModeratorUser(repositoryHelper.getReferenceIfExist(RegUser.class, moderatorId));

        List<SignalToModerator> similarSignals = signalToModeratorRepository
                .getAllSimilarSignals(signal.getTypo()).collect(Collectors.toList());

        for (SignalToModerator s : similarSignals) {
            s.setSignalStatus(SignalStatus.CANCELED);
            s.setCorrectionTime(Instant.now().truncatedTo(ChronoUnit.MICROS));
            s.setModeratorUser(repositoryHelper.getReferenceIfExist(RegUser.class, moderatorId));
            signalToModeratorRepository.save(s);
        }

        signal = signalToModeratorRepository.save(signal);
        return translationService.translate(signal, SignalToModeratorReadDTO.class);
    }

    @Transactional
    public RegUserReadDTO updateUserTrustForReview(UUID id, RegUserPutTrustDTO put) {
        RegUser regUser = repositoryHelper.getReferenceIfExist(RegUser.class, id);
        regUser.setTrustForReview(put.getTrustForReview());
        regUserRepository.save(regUser);
        return translationService.translate(regUser, RegUserReadDTO.class);
    }

    public void deleteSignalToModerator(UUID id) {
        signalToModeratorRepository.delete(repositoryHelper.getRequired(SignalToModerator.class, id));
    }

    private void fixReview(ReviewMovie reviewMovie, SignalToModerator signal, String fixText) {
        StringBuffer fix = new StringBuffer(reviewMovie.getBody());
        int i = reviewMovie.getBody().indexOf(signal.getTypo());
        fix.replace(i, i + signal.getTypo().length(), fixText);
        signal.setCorrectText(fixText);
        reviewMovie.setBody(fix.toString());
        reviewMovieRepository.save(reviewMovie);
    }

    private void fixReview(ReviewActorRole reviewActorRole, SignalToModerator signal, String fixText) {
        StringBuffer fix = new StringBuffer(reviewActorRole.getBody());
        int i = reviewActorRole.getBody().indexOf(signal.getTypo());
        fix.replace(i, i + signal.getTypo().length(), fixText);
        signal.setCorrectText(fixText);
        reviewActorRole.setBody(fix.toString());
        reviewActorRoleRepository.save(reviewActorRole);
    }
}