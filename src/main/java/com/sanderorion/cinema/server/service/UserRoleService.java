package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.UserRole;
import com.sanderorion.cinema.server.dto.userrole.UserRoleReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RegUserRepository regUserRepository;

    @Transactional
    public List<UserRoleReadDTO> getAllUserRoles() {
        return userRoleRepository.getAllUserRoles()
                .stream()
                .map(e -> translationService.translate(e, UserRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<UserRoleReadDTO> getUserRoles(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getUserRoles()
                .stream()
                .map(e -> translationService.translate(e, UserRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<UserRoleReadDTO> addUserRoleToRegUser(UUID userId, UUID userRoleId) {
        RegUser regUser = repositoryHelper.getByIdRequired(RegUser.class, userId);
        UserRole userRole = repositoryHelper.getByIdRequired(UserRole.class, userRoleId);

        if (regUser.getUserRoles().stream().anyMatch(r -> r.getId().equals(userRoleId))) {
            throw new LinkDuplicatedException(String.format("RegUser %s already has userRole %s", userId, userRoleId));
        }

        regUser.getUserRoles().add(userRole);
        regUser = regUserRepository.save(regUser);

        return translationService.translateList(regUser.getUserRoles(), UserRoleReadDTO.class);
    }

    @Transactional
    public List<UserRoleReadDTO> removeUserRoleToRegUser(UUID regUserId, UUID userRoleId) {
        RegUser regUser = repositoryHelper.getByIdRequired(RegUser.class, regUserId);

        boolean removed = regUser.getUserRoles().removeIf(g -> g.getId().equals(userRoleId));
        if (!removed) {
            throw new EntityNotFoundException("RegUser " + regUserId + " has no UserRole" + userRoleId);
        }

        regUser = regUserRepository.save(regUser);

        return translationService.translateList(regUser.getUserRoles(), UserRoleReadDTO.class);
    }
}