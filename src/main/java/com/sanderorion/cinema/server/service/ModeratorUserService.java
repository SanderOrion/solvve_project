package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ModeratorUser;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserCreateDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPatchDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPutDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserReadDTO;
import com.sanderorion.cinema.server.repository.ModeratorUserRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ModeratorUserService {

    @Autowired
    private ModeratorUserRepository moderatorUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ModeratorUserReadDTO getModeratorUser(UUID id) {
        ModeratorUser moderatorUser = repositoryHelper.getRequired(ModeratorUser.class, id);
        return translationService.translate(moderatorUser, ModeratorUserReadDTO.class);
    }

    public ModeratorUserReadDTO createModeratorUser(ModeratorUserCreateDTO create) {
        ModeratorUser moderatorUser = translationService.translate(create, ModeratorUser.class);
        moderatorUser = moderatorUserRepository.save(moderatorUser);
        return translationService.translate(moderatorUser, ModeratorUserReadDTO.class);
    }

    public ModeratorUserReadDTO patchModeratorUser(UUID id, ModeratorUserPatchDTO patch) {
        ModeratorUser moderatorUser = repositoryHelper.getRequired(ModeratorUser.class, id);
        translationService.map(patch, moderatorUser);
        moderatorUser = moderatorUserRepository.save(moderatorUser);
        return translationService.translate(moderatorUser, ModeratorUserReadDTO.class);
    }

    public ModeratorUserReadDTO updateModeratorUser(UUID id, ModeratorUserPutDTO put) {
        ModeratorUser moderatorUser = repositoryHelper.getRequired(ModeratorUser.class, id);
        translationService.map(put, moderatorUser);
        moderatorUser = moderatorUserRepository.save(moderatorUser);
        return translationService.translate(moderatorUser, ModeratorUserReadDTO.class);
    }

    public void deleteModeratorUser(UUID id) {
        moderatorUserRepository.delete(repositoryHelper.getRequired(ModeratorUser.class, id));
    }
}
