package com.sanderorion.cinema.server.service.importer;

import com.sanderorion.cinema.server.client.themoviedb.ExternalSystemImport;
import com.sanderorion.cinema.server.client.themoviedb.ExternalSystemImportRepository;
import com.sanderorion.cinema.server.client.themoviedb.dto.ImportedEntityType;
import com.sanderorion.cinema.server.domain.AbstractEntity;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ExternalSystemImportService {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    public void validateNotImported(Class<? extends AbstractEntity> entityToImport,
                                    Integer idInExternalSystem) throws ImportAlreadyPerformedException {
        ImportedEntityType importedEntityType = getImportedEntityType(entityToImport);
        ExternalSystemImport esi = externalSystemImportRepository.findByIdInExternalSystemAndEntityType(
                idInExternalSystem, importedEntityType);

        if (esi != null) {
            throw new ImportAlreadyPerformedException(esi);
        }
    }

    public <T extends AbstractEntity> UUID createExternalSystemImport(T entity, Integer idInExternalSystem) {
        ImportedEntityType importedEntityType = getImportedEntityType(entity.getClass());
        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setEntityType(importedEntityType);
        esi.setEntityId(entity.getId());
        esi.setIdInExternalSystem(idInExternalSystem);
        esi = externalSystemImportRepository.save(esi);
        return esi.getId();
    }

    private ImportedEntityType getImportedEntityType(Class<? extends AbstractEntity> entityToImport) {

        if (Movie.class.equals(entityToImport)) {
            return ImportedEntityType.MOVIE;
        } else if (MoviePerson.class.equals(entityToImport)) {
            return ImportedEntityType.PERSON;
        }

        throw new IllegalArgumentException("Importing of entity " + entityToImport + " is not supported");
    }
}