package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.MarkMovie;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieCreateDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MarkMovieService {

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MarkMovieReadDTO getMarkMovie(UUID id) {
        MarkMovie markMovie = repositoryHelper.getRequired(MarkMovie.class, id);
        return translationService.translate(markMovie, MarkMovieReadDTO.class);
    }

    @Transactional
    public List<MarkMovieReadDTO> getUserMarksMovies(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getMarkMovies()
                .stream()
                .map(m -> translationService.translate(m, MarkMovieReadDTO.class))
                .collect(Collectors.toList());
    }

    public MarkMovieReadDTO createMarkMovie(UUID regUserId, UUID movieId, MarkMovieCreateDTO create) {
        MarkMovie markMovie = translationService.translate(create, MarkMovie.class);
        markMovie.setRegUser(repositoryHelper.getReferenceIfExist(RegUser.class, regUserId));
        markMovie.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        markMovie = markMovieRepository.save(markMovie);
        return translationService.translate(markMovie, MarkMovieReadDTO.class);
    }

    public void deleteMarkMovie(UUID id) {
        markMovieRepository.delete(repositoryHelper.getRequired(MarkMovie.class, id));
    }
}