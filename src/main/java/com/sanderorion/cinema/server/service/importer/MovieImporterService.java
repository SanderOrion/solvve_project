package com.sanderorion.cinema.server.service.importer;

import com.sanderorion.cinema.server.client.TheMovieDbClient;
import com.sanderorion.cinema.server.client.themoviedb.dto.*;
import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import com.sanderorion.cinema.server.domain.enums.Profession;
import com.sanderorion.cinema.server.exception.ImportAlreadyPerformedException;
import com.sanderorion.cinema.server.exception.ImportedEntityAlreadyExistException;
import com.sanderorion.cinema.server.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ActorRoleRepository actorRoleRepository;

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Transactional
    public UUID importMovie(Integer movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        log.info("Importing movie with external id = {}", movieExternalId);

        externalSystemImportService.validateNotImported(Movie.class, movieExternalId);
        MovieReadDTO read = theMovieDbClient.getMovie(movieExternalId, null);
        Movie existingMovie = movieRepository.findByTitle(read.getTitle());
        if (existingMovie != null) {
            throw new ImportedEntityAlreadyExistException(Movie.class, existingMovie.getId(),
                    "Movie with name = " + read.getTitle() + " already exist");
        }

        Movie movie = new Movie();
        movie.setOriginalTitle(read.getOriginalTitle());
        movie.setTitle(read.getTitle());
        if (read.getImdbId() != null) {
            movie.setImdbId(read.getImdbId());
        }
        if (read.getTagline() != null) {
            movie.setTagline(read.getTagline());
        }
        if (read.getOverview() != null) {
            movie.setOverview(read.getOverview());
        }
        movie.setReleaseDate(read.getReleaseDate());
        movie.setStatus(MovieRelease.valueOf(read.getStatus().toUpperCase()));
        if (read.getRuntime() != null) {
            movie.setDuration(read.getRuntime());
        }
        movie.setLanguage(read.getOriginalLanguage());
        movie.setAvgRating(read.getVoteAverage());
        movie.setRatingCount(read.getVoteCount());
        movie.setBudget(read.getBudget());
        movie.setRevenue(read.getRevenue());

        List<GenreReadDTO> genresReadDTO = read.getGenres();
        List<Genre> genres = new ArrayList<>();

        for (GenreReadDTO genreReadDTO : genresReadDTO) {
            String name = genreReadDTO.getName().replace(" ", "_");
            Genre genre = genreRepository.findByGenreName(MovieGenre.valueOf(name.toUpperCase()));
            if (genre == null) {
                genre = new Genre(MovieGenre.valueOf(name.toUpperCase()), null);
                genreRepository.save(genre);
            }
            genres.add(genre);
        }

        movie.setGenres(genres);
        movieRepository.save(movie);

        CreditsReadDTO creditsReadDTO = theMovieDbClient.getCredits(movieExternalId);

        List<CastReadDTO> castsReadDTO = creditsReadDTO.getCast();

        for (CastReadDTO casts : castsReadDTO) {
            ActorRole actorRole = new ActorRole();
            Crew crew = new Crew();
            actorRole.setInternalId(casts.getId());
            crew.setInternalId(casts.getId());
            if (casts.getGender() != null) {
                actorRole.setGender(Gender.getGender(casts.getGender()));
                crew.setGender(Gender.getGender(casts.getGender()));
            }

            crew.setDescription(casts.getCharacter());
            crew.setCrewName(casts.getName());
            crew.setProfession(Profession.CAST);
            crew.setMovie(movie);

            actorRole.setRoleName(casts.getName());
            actorRole.setRoleInfo(casts.getCharacter());
            actorRole.setMovie(movie);

            MoviePerson moviePerson = moviePersonRepository.findByInternalId(casts.getId());
            if (moviePerson == null) {
                moviePerson = getPerson(casts.getId());
                actorRole.setMoviePerson(moviePerson);
                crew.setMoviePerson(moviePerson);
                moviePersonRepository.save(moviePerson);
            } else {
                actorRole.setMoviePerson(moviePerson);
                crew.setMoviePerson(moviePerson);
            }
            crewRepository.save(crew);
            actorRoleRepository.save(actorRole);
        }

        List<CrewReadDTO> crewsReadDTO = creditsReadDTO.getCrew();

        for (CrewReadDTO crews : crewsReadDTO) {
            Crew crew = new Crew();
            crew.setDescription(crews.getJob());
            if (crews.getGender() != null) {
                crew.setGender(Gender.getGender(crews.getGender()));
            }
            crew.setInternalId(crews.getId());
            crew.setCrewName(crews.getName());
            crew.setProfession(Profession.valueOf(crews.getDepartment().toUpperCase()
                    .replace(" ", "_")
                    .replace("&", "")
                    .replace("-", "_")));

            crew.setMovie(movie);

            MoviePerson moviePerson = moviePersonRepository.findByInternalId(crews.getId());
            if (moviePerson == null) {
                moviePerson = getPerson(crews.getId());
                crew.setMoviePerson(moviePerson);
                moviePersonRepository.save(moviePerson);
            } else {
                crew.setMoviePerson(moviePerson);
            }
            crewRepository.save(crew);
        }
        externalSystemImportService.createExternalSystemImport(movie, movieExternalId);

        log.info("Movie with external id = {} imported", movieExternalId);

        return movie.getId();
    }

    private MoviePerson getPerson(Integer personId) {
        PersonReadDTO personReadDTO = theMovieDbClient.getPerson(personId);
        MoviePerson moviePerson = new MoviePerson();
        moviePerson.setInternalId(personReadDTO.getId());
        moviePerson.setName(personReadDTO.getName());
        moviePerson.setProfession(personReadDTO.getKnownForDepartment());
        moviePerson.setBiography(personReadDTO.getBiography());
        moviePerson.setGender(Gender.getGender(personReadDTO.getGender()));
        if (personReadDTO.getBirthday() != null) {
            moviePerson.setBirthday(personReadDTO.getBirthday());
        }
        if (personReadDTO.getDeathday() != null) {
            moviePerson.setDeathDay(personReadDTO.getDeathday());
        }
        if (personReadDTO.getPlaceOfBirth() != null) {
            moviePerson.setPlaceOfBirth(personReadDTO.getPlaceOfBirth());
        }
        return moviePerson;
    }
}