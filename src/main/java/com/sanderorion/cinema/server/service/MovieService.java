package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieCreateDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePatchDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePutDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Transactional
    public MovieReadDTO getMovie(UUID id) {
        Movie movie = repositoryHelper.getRequired(Movie.class, id);
        Integer amountMark = markMovieRepository.amountMark(id);
        movie.setRatingCount(amountMark);
        movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieRepository.getMoviesLeaderBoard();
    }

    public MovieReadDTO createMovie(MovieCreateDTO create) {
        Movie movie = translationService.translate(create, Movie.class);
        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO patchMovie(UUID id, MoviePatchDTO patch) {
        Movie movie = repositoryHelper.getRequired(Movie.class, id);
        translationService.map(patch, movie);
        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public MovieReadDTO updateMovie(UUID id, MoviePutDTO put) {
        Movie movie = repositoryHelper.getRequired(Movie.class, id);
        translationService.map(put, movie);
        movie = movieRepository.save(movie);
        return translationService.translate(movie, MovieReadDTO.class);
    }

    public void deleteMovie(UUID id) {
        movieRepository.delete(repositoryHelper.getRequired(Movie.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingOfMovie(UUID movieId) {
        Double avgRating = markMovieRepository.calcAverageRatingOfMovie(movieId);
        Movie movie = movieRepository.findById(movieId).orElseThrow(
                () -> new EntityNotFoundException(Movie.class, movieId));

        log.info("Setting new average rating of movie: {}. Old value: {}, new value: {}", movieId,
                movie.getAvgRating(), avgRating);

        if (avgRating != null) {
            avgRating = Precision.round(avgRating, 2);
            movie.setAvgRating(avgRating);
        }
        movieRepository.save(movie);
    }
}