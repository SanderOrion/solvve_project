package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewMovie;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.repository.LikeReviewMovieRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import com.sanderorion.cinema.server.repository.ReviewMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReviewMovieService {

    @Autowired
    private ReviewMovieRepository reviewMovieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private LikeReviewMovieRepository likeReviewMovieRepository;

    public ReviewMovieReadDTO getReviewMovie(UUID id) {
        ReviewMovie reviewMovie = repositoryHelper.getRequired(ReviewMovie.class, id);
        updateLikeReviewMovie(id);
        return translationService.translate(reviewMovie, ReviewMovieReadDTO.class);
    }

    @Transactional
    public List<ReviewMovieReadDTO> getRegUserReviewMovies(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getReviewMovies()
                .stream()
                .map(r -> translationService.translate(r, ReviewMovieReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public PageResult<ReviewMovieReadDTO> getAllConfirmedReviewsMovie(UUID id, Pageable pageable) {
        Page<ReviewMovie> reviews = reviewMovieRepository.getAllConfirmedReviewsMovie(id, pageable);
        return translationService.toPageResult(reviews, ReviewMovieReadDTO.class);
    }

    @Transactional
    public PageResult<ReviewMovieReadDTO> getReviewsMovieToConfirm(Pageable pageable) {
        Page<ReviewMovie> reviews = reviewMovieRepository.getReviewsMovieToConfirm(pageable);
        return translationService.toPageResult(reviews, ReviewMovieReadDTO.class);
    }

    @Transactional
    public ReviewMovieReadDTO createReviewMovie(UUID regUserId, UUID movieId, ReviewMovieCreateDTO create) {
        ReviewMovie reviewMovie = translationService.translate(create, ReviewMovie.class);
        RegUser regUser = repositoryHelper.getReferenceIfExist(RegUser.class, regUserId);
        reviewMovie.setRegUser(regUser);
        reviewMovie.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));

        if (regUser.getTrustForReview() == TrustForReview.CREDIBLE) {
            reviewMovie.setConfirmStatus(ReviewConfirmStatus.CONFIRMED);
        } else {
            reviewMovie.setConfirmStatus(ReviewConfirmStatus.NEED_TO_CONFIRM);
        }

        reviewMovie = reviewMovieRepository.save(reviewMovie);
        return translationService.translate(reviewMovie, ReviewMovieReadDTO.class);
    }

    public ReviewMovieReadDTO patchReviewMovie(UUID id, ReviewMoviePatchDTO patch) {
        ReviewMovie reviewMovie = repositoryHelper.getRequired(ReviewMovie.class, id);
        translationService.map(patch, reviewMovie);
        reviewMovie = reviewMovieRepository.save(reviewMovie);
        return translationService.translate(reviewMovie, ReviewMovieReadDTO.class);
    }

    public ReviewMovieReadDTO updateReviewMovie(UUID id, UUID moderatorId, ReviewMoviePutDTO put) {
        ReviewMovie reviewMovie = repositoryHelper.getRequired(ReviewMovie.class, id);
        translationService.map(put, reviewMovie);
        reviewMovie.setModerator(repositoryHelper.getReferenceIfExist(RegUser.class, moderatorId));
        reviewMovie.setConfirmStatus(put.getConfirmStatus());
        reviewMovie = reviewMovieRepository.save(reviewMovie);
        return translationService.translate(reviewMovie, ReviewMovieReadDTO.class);
    }

    public void deleteReviewMovie(UUID id) {
        reviewMovieRepository.delete(repositoryHelper.getRequired(ReviewMovie.class, id));
    }

    @Transactional
    public void updateLikeReviewMovie(UUID reviewId) {
        ReviewMovie reviewMovie = repositoryHelper.getByIdRequired(ReviewMovie.class, reviewId);
        Integer amountLike = likeReviewMovieRepository.amountLike(reviewId, true);
        Integer amountDislike = likeReviewMovieRepository.amountLike(reviewId, false);

        reviewMovie.setAmountLike(amountLike);
        reviewMovie.setAmountDislike(amountDislike);
        reviewMovieRepository.save(reviewMovie);
    }
}