package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.MoviePerson;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonCreateDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPatchDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPutDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.repository.MarkActorRoleRepository;
import com.sanderorion.cinema.server.repository.MarkMovieRepository;
import com.sanderorion.cinema.server.repository.MoviePersonRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class MoviePersonService {

    @Autowired
    private MoviePersonRepository moviePersonRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private MarkMovieRepository markMovieRepository;

    @Autowired
    private MarkActorRoleRepository markActorRoleRepository;

    public MoviePersonReadDTO getMoviePerson(UUID id) {
        MoviePerson moviePerson = repositoryHelper.getRequired(MoviePerson.class, id);
        return translationService.translate(moviePerson, MoviePersonReadDTO.class);
    }

    public MoviePersonReadDTO createMoviePerson(MoviePersonCreateDTO create) {
        MoviePerson moviePerson = translationService.translate(create, MoviePerson.class);
        moviePerson = moviePersonRepository.save(moviePerson);
        return translationService.translate(moviePerson, MoviePersonReadDTO.class);
    }

    public MoviePersonReadDTO patchMoviePerson(UUID id, MoviePersonPatchDTO patch) {
        MoviePerson moviePerson = repositoryHelper.getRequired(MoviePerson.class, id);
        translationService.map(patch, moviePerson);
        moviePerson = moviePersonRepository.save(moviePerson);
        return translationService.translate(moviePerson, MoviePersonReadDTO.class);
    }

    public MoviePersonReadDTO updateMoviePerson(UUID id, MoviePersonPutDTO put) {
        MoviePerson moviePerson = repositoryHelper.getRequired(MoviePerson.class, id);
        translationService.map(put, moviePerson);
        moviePerson = moviePersonRepository.save(moviePerson);
        return translationService.translate(moviePerson, MoviePersonReadDTO.class);
    }

    public void deleteMoviePerson(UUID id) {
        moviePersonRepository.delete(repositoryHelper.getRequired(MoviePerson.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateCalcAverageRatingMoviePersonForMovie(UUID moviePersonId) {
        Double avgMovieRating = markMovieRepository.calcAverageRatingMoviePersonForMovie(moviePersonId);
        MoviePerson moviePerson = moviePersonRepository.findById(moviePersonId).orElseThrow(
                () -> new EntityNotFoundException(MoviePerson.class, moviePersonId));

        log.info("Setting new average rating movie person of movies: {}. Old value: {}, new value: {}", moviePersonId,
                moviePerson.getAvgMovieRating(), avgMovieRating);

        if (avgMovieRating != null) {
            avgMovieRating = Precision.round(avgMovieRating, 2);
            moviePerson.setAvgMovieRating(avgMovieRating);
        }
        moviePersonRepository.save(moviePerson);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateCalcAverageRatingMoviePersonForActorRole(UUID moviePersonId) {
        Double avgRoleRating = markActorRoleRepository.calcAverageRatingMoviePersonForActorRole(moviePersonId);
        MoviePerson moviePerson = moviePersonRepository.findById(moviePersonId).orElseThrow(
                () -> new EntityNotFoundException(MoviePerson.class, moviePersonId));

        log.info("Setting new average rating movie person of actor roles: {}. Old value: {}, new value: {}",
                moviePersonId, moviePerson.getAvgRoleRating(), avgRoleRating);

        if (avgRoleRating != null) {
            avgRoleRating = Precision.round(avgRoleRating, 2);
            moviePerson.setAvgRoleRating(avgRoleRating);
        }
        moviePersonRepository.save(moviePerson);
    }
}