package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Article;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.article.ArticleCreateDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePatchDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePutDTO;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.ArticleRepository;
import com.sanderorion.cinema.server.repository.LikeArticleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private LikeArticleRepository likeArticleRepository;

    @Transactional
    public ArticleReadDTO getArticle(UUID id) {
        Article article = repositoryHelper.getRequired(Article.class, id);
        Integer amountLike = likeArticleRepository.amountLike(id, true);
        Integer amountDislike = likeArticleRepository.amountLike(id, false);
        article.setAmountLike(amountLike);
        article.setAmountDislike(amountDislike);
        articleRepository.save(article);
        return translationService.translate(article, ArticleReadDTO.class);
    }

    @Transactional
    public PageResult<ArticleReadDTO> getArticles(ArticleFilter filter, Pageable pageable) {
        Page<Article> articles = articleRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(articles, ArticleReadDTO.class);
    }

    public ArticleReadDTO createArticle(ArticleCreateDTO create) {
        Article article = translationService.translate(create, Article.class);
        article = articleRepository.save(article);
        return translationService.translate(article, ArticleReadDTO.class);
    }

    public ArticleReadDTO patchArticle(UUID id, ArticlePatchDTO patch) {
        Article article = repositoryHelper.getRequired(Article.class, id);
        translationService.map(patch, article);
        article = articleRepository.save(article);
        return translationService.translate(article, ArticleReadDTO.class);
    }

    public ArticleReadDTO updateArticle(UUID id, ArticlePutDTO put) {
        Article article = repositoryHelper.getRequired(Article.class, id);
        translationService.map(put, article);
        article = articleRepository.save(article);
        return translationService.translate(article, ArticleReadDTO.class);
    }

    public void deleteArticle(UUID id) {
        articleRepository.delete(repositoryHelper.getRequired(Article.class, id));
    }

    @Transactional
    public List<MovieReadDTO> addMovieToArticle(UUID articleId, UUID movieId) {
        Article article = repositoryHelper.getByIdRequired(Article.class, articleId);
        Movie movie = repositoryHelper.getByIdRequired(Movie.class, movieId);

        if (article.getMovies().stream().anyMatch(m -> m.getId().equals(movieId))) {
            throw new LinkDuplicatedException(String.format("Article %s already has genre %s", articleId, movieId));
        }

        article.getMovies().add(movie);
        article = articleRepository.save(article);

        return translationService.translateList(article.getMovies(), MovieReadDTO.class);
    }

    @Transactional
    public List<MovieReadDTO> removeMovieToArticle(UUID articleId, UUID movieId) {
        Article article = repositoryHelper.getByIdRequired(Article.class, articleId);

        boolean removed = article.getMovies().removeIf(m -> m.getId().equals(movieId));
        if (!removed) {
            throw new EntityNotFoundException("Article " + articleId + " has no movie" + movieId);
        }

        article = articleRepository.save(article);

        return translationService.translateList(article.getMovies(), MovieReadDTO.class);
    }
}