package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import com.sanderorion.cinema.server.dto.reguser.RegUserCreateDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import com.sanderorion.cinema.server.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class RegUserService {

    @Autowired
    private RegUserRepository regUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public RegUserReadDTO getRegUser(UUID id) {
        RegUser regUser = repositoryHelper.getRequired(RegUser.class, id);
        return translationService.translate(regUser, RegUserReadDTO.class);
    }

    @Transactional
    public RegUserReadDTO createRegUser(RegUserCreateDTO create) {
        RegUser regUser = translationService.translate(create, RegUser.class);
        regUser.setEncodedPassword(passwordEncoder.encode(create.getPassword()));
        regUser.getUserRoles().add(userRoleRepository.findUserRoleByUserRoleType(UserRoleType.REG_USER));
        regUser = regUserRepository.save(regUser);
        return translationService.translate(regUser, RegUserReadDTO.class);
    }

    public RegUserReadDTO patchRegUser(UUID id, RegUserPatchDTO patch) {
        RegUser regUser = repositoryHelper.getRequired(RegUser.class, id);
        translationService.map(patch, regUser);
        regUser = regUserRepository.save(regUser);
        return translationService.translate(regUser, RegUserReadDTO.class);
    }

    public RegUserReadDTO updateRegUser(UUID id, RegUserPutDTO put) {
        RegUser regUser = repositoryHelper.getRequired(RegUser.class, id);
        translationService.map(put, regUser);
        regUser = regUserRepository.save(regUser);
        return translationService.translate(regUser, RegUserReadDTO.class);
    }

    public void deleteRegUser(UUID id) {
        regUserRepository.delete(repositoryHelper.getRequired(RegUser.class, id));
    }
}