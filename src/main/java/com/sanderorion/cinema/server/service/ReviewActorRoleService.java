package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.ActorRole;
import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.ReviewActorRole;
import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.repository.LikeReviewActorRoleRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import com.sanderorion.cinema.server.repository.ReviewActorRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReviewActorRoleService {

    @Autowired
    private ReviewActorRoleRepository reviewActorRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private LikeReviewActorRoleRepository likeReviewActorRoleRepository;

    public ReviewActorRoleReadDTO getReviewActorRole(UUID id) {
        ReviewActorRole reviewActorRole = repositoryHelper.getRequired(ReviewActorRole.class, id);
        updateLikeReviewActorRole(id);
        return translationService.translate(reviewActorRole, ReviewActorRoleReadDTO.class);
    }

    @Transactional
    public List<ReviewActorRoleReadDTO> getRegUserReviewActorRoles(UUID regUserId) {
        return repositoryHelper.getReferenceIfExist(RegUser.class, regUserId)
                .getReviewActorRoles()
                .stream()
                .map(r -> translationService.translate(r, ReviewActorRoleReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public PageResult<ReviewActorRoleReadDTO> getAllConformedReviewsActorRole(UUID id, Pageable pageable) {
        Page<ReviewActorRole> reviews = reviewActorRoleRepository.getAllConformedReviewsActorRole(id, pageable);
        return translationService.toPageResult(reviews, ReviewActorRoleReadDTO.class);
    }

    @Transactional
    public PageResult<ReviewActorRoleReadDTO> getReviewsActorRoleToConfirm(Pageable pageable) {
        Page<ReviewActorRole> reviews = reviewActorRoleRepository.getReviewsActorRoleToConfirm(pageable);
        return translationService.toPageResult(reviews, ReviewActorRoleReadDTO.class);
    }

    @Transactional
    public ReviewActorRoleReadDTO createReviewActorRole(UUID regUserId, UUID actorRoleId,
                                                        ReviewActorRoleCreateDTO create) {
        ReviewActorRole reviewActorRole = translationService.translate(create, ReviewActorRole.class);
        RegUser regUser = repositoryHelper.getReferenceIfExist(RegUser.class, regUserId);
        reviewActorRole.setRegUser(regUser);
        reviewActorRole.setActorRole(repositoryHelper.getReferenceIfExist(ActorRole.class, actorRoleId));

        if (regUser.getTrustForReview() == TrustForReview.CREDIBLE) {
            reviewActorRole.setConfirmStatus(ReviewConfirmStatus.CONFIRMED);
        } else {
            reviewActorRole.setConfirmStatus(ReviewConfirmStatus.NEED_TO_CONFIRM);
        }

        reviewActorRole = reviewActorRoleRepository.save(reviewActorRole);
        return translationService.translate(reviewActorRole, ReviewActorRoleReadDTO.class);
    }

    public ReviewActorRoleReadDTO patchReviewActorRole(UUID id, ReviewActorRolePatchDTO patch) {
        ReviewActorRole reviewActorRole = repositoryHelper.getRequired(ReviewActorRole.class, id);
        translationService.map(patch, reviewActorRole);
        reviewActorRole = reviewActorRoleRepository.save(reviewActorRole);
        return translationService.translate(reviewActorRole, ReviewActorRoleReadDTO.class);
    }

    public ReviewActorRoleReadDTO updateReviewActorRole(UUID id, UUID moderatorId, ReviewActorRolePutDTO put) {
        ReviewActorRole reviewActorRole = repositoryHelper.getRequired(ReviewActorRole.class, id);
        translationService.map(put, reviewActorRole);
        reviewActorRole.setModerator(repositoryHelper.getReferenceIfExist(RegUser.class, moderatorId));
        reviewActorRole.setConfirmStatus(put.getConfirmStatus());
        reviewActorRole = reviewActorRoleRepository.save(reviewActorRole);
        return translationService.translate(reviewActorRole, ReviewActorRoleReadDTO.class);
    }

    public void deleteReviewActorRole(UUID id) {
        reviewActorRoleRepository.delete(repositoryHelper.getRequired(ReviewActorRole.class, id));
    }

    @Transactional
    public void updateLikeReviewActorRole(UUID reviewActorRoleId) {
        ReviewActorRole reviewActorRole = repositoryHelper.getByIdRequired(ReviewActorRole.class, reviewActorRoleId);
        Integer amountLike = likeReviewActorRoleRepository.amountLike(reviewActorRoleId, true);
        Integer amountDislike = likeReviewActorRoleRepository.amountLike(reviewActorRoleId, false);

        reviewActorRole.setAmountLike(amountLike);
        reviewActorRole.setAmountDislike(amountDislike);
        reviewActorRoleRepository.save(reviewActorRole);
    }
}