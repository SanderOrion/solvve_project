package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Crew;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPatchDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.repository.CrewRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CrewService {

    @Autowired
    private CrewRepository crewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public CrewReadDTO getCrew(UUID id) {
        Crew crew = repositoryHelper.getRequired(Crew.class, id);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    @Transactional
    public List<CrewReadDTO> getMovieCrews(UUID movieId) {
        return repositoryHelper.getReferenceIfExist(Movie.class, movieId)
                .getCrews()
                .stream()
                .map(c -> translationService.translate(c, CrewReadDTO.class))
                .collect(Collectors.toList());
    }

    public CrewReadDTO createCrew(UUID movieId, CrewCreateDTO create) {
        Crew crew = translationService.translate(create, Crew.class);
        crew.setMovie(repositoryHelper.getReferenceIfExist(Movie.class, movieId));
        crew = crewRepository.save(crew);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public CrewReadDTO patchCrew(UUID id, CrewPatchDTO patch) {
        Crew crew = repositoryHelper.getRequired(Crew.class, id);
        translationService.map(patch, crew);
        crew = crewRepository.save(crew);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public CrewReadDTO updateCrew(UUID id, CrewPutDTO put) {
        Crew crew = repositoryHelper.getRequired(Crew.class, id);
        translationService.map(put, crew);
        crew = crewRepository.save(crew);
        return translationService.translate(crew, CrewReadDTO.class);
    }

    public void deleteCrew(UUID id) {
        crewRepository.delete(repositoryHelper.getRequired(Crew.class, id));
    }
}