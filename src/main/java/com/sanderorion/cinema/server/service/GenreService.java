package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.Genre;
import com.sanderorion.cinema.server.domain.Movie;
import com.sanderorion.cinema.server.dto.genre.GenreReadDTO;
import com.sanderorion.cinema.server.exception.EntityNotFoundException;
import com.sanderorion.cinema.server.exception.LinkDuplicatedException;
import com.sanderorion.cinema.server.repository.GenreRepository;
import com.sanderorion.cinema.server.repository.MovieRepository;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional
    public List<GenreReadDTO> getAllGenres() {
        return genreRepository.getAllGenres()
                .stream()
                .map(e -> translationService.translate(e, GenreReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<GenreReadDTO> getGenres(UUID movieId) {
        return repositoryHelper.getReferenceIfExist(Movie.class, movieId)
                .getGenres()
                .stream()
                .map(e -> translationService.translate(e, GenreReadDTO.class))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<GenreReadDTO> addGenreToMovie(UUID movieId, UUID genreId) {
        Movie movie = repositoryHelper.getByIdRequired(Movie.class, movieId);
        Genre genre = repositoryHelper.getByIdRequired(Genre.class, genreId);

        if (movie.getGenres().stream().anyMatch(g -> g.getId().equals(genreId))) {
            throw new LinkDuplicatedException(String.format("Movie %s already has genre %s", movieId, genreId));
        }

        movie.getGenres().add(genre);
        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getGenres(), GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> removeGenreFromMovie(UUID movieId, UUID genreId) {
        Movie movie = repositoryHelper.getByIdRequired(Movie.class, movieId);

        boolean removed = movie.getGenres().removeIf(g -> g.getId().equals(genreId));
        if (!removed) {
            throw new EntityNotFoundException("Movie " + movieId + " has no genre" + genreId);
        }

        movie = movieRepository.save(movie);

        return translationService.translateList(movie.getGenres(), GenreReadDTO.class);
    }
}