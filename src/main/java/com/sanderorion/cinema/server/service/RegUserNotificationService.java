package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.RegUser;
import com.sanderorion.cinema.server.domain.SignalToContentManager;
import com.sanderorion.cinema.server.repository.RegUserRepository;
import com.sanderorion.cinema.server.repository.SignalToContentManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegUserNotificationService {

    @Autowired
    SignalToContentManagerRepository signalToContentManagerRepository;

    @Autowired
    private RegUserRepository regUserRepository;

    public String notifyOnSignalStatusChangedToFinished(UUID signalToContentManagerId) {
        SignalToContentManager signal = signalToContentManagerRepository.findById(signalToContentManagerId).get();

        RegUser regUser = regUserRepository.findById(signal.getRegUser().getId()).get();
        String subject = "response on typo";
        String text = "Dear User. Your request typo handling finished";

        return "Email Sent!!!" + " " + regUser.getEmail() + " " + " " + subject + " " + text;
    }
}