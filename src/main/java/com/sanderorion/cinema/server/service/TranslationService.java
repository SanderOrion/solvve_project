package com.sanderorion.cinema.server.service;

import com.sanderorion.cinema.server.domain.*;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePutDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPatchDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPutDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePatchDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePutDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPatchDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPatchDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePatchDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePutDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPatchDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPutDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePatchDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePutDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPatchDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPutDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    private ObjectTranslator objectTranslator;

    public TranslationService() {
        objectTranslator = new ObjectTranslator(createConfiguration());
    }

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> res = new PageResult<>();
        res.setPage(page.getNumber());
        res.setPageSize(page.getSize());
        res.setTotalPages(page.getTotalPages());
        res.setTotalElements(page.getTotalElements());
        res.setData(page.getContent().stream().map(e -> translate(e, dtoType)).collect(Collectors.toList()));
        return res;
    }

    public <T> List<T> translateList(List<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }

    private Configuration createConfiguration() {
        Configuration c = new Configuration();
        configureForAbstractEntity(c);
        configureForActorRole(c);
        configureForAdminUser(c);
        configureForArticle(c);
        configureForContentManagerUser(c);
        configureForCrew(c);
        configureForLikeArticle(c);
        configureForLikeReviewActorRole(c);
        configureForLikeReviewMovie(c);
        configureForMarkActorRole(c);
        configureForMarkMovie(c);
        configureForModeratorUser(c);
        configureForMoviePerson(c);
        configureForMovie(c);
        configureForRegUser(c);
        configureForReviewActorRole(c);
        configureForReviewMovie(c);
        configureForSignalToContentManager(c);
        configureForSignalToModerator(c);
        return c;
    }

    private void configureForAbstractEntity(Configuration c) {
        c.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        c.beanOfClass(AbstractEntity.class).setBeanFinder(
                (beanClass, id) -> repositoryHelper.getReferenceIfExist(beanClass, (UUID) id));
    }

    private void configureForMovie(Configuration c) {
        c.beanOfClass(MoviePatchDTO.class).translationTo(Movie.class).mapOnlyNotNullProperties();
        c.beanOfClass(MoviePutDTO.class).translationTo(Movie.class);
    }

    private void configureForAdminUser(Configuration c) {
        c.beanOfClass(AdminUserPatchDTO.class).translationTo(AdminUser.class).mapOnlyNotNullProperties();
        c.beanOfClass(AdminUserPutDTO.class).translationTo(AdminUser.class);
    }

    private void configureForArticle(Configuration c) {
        c.beanOfClass(ArticlePatchDTO.class).translationTo(Article.class).mapOnlyNotNullProperties();
        c.beanOfClass(ArticlePutDTO.class).translationTo(Article.class);
    }

    private void configureForModeratorUser(Configuration c) {
        c.beanOfClass(ModeratorUserPatchDTO.class).translationTo(ModeratorUser.class).mapOnlyNotNullProperties();
        c.beanOfClass(ModeratorUserPutDTO.class).translationTo(ModeratorUser.class);
    }

    private void configureForMoviePerson(Configuration c) {
        c.beanOfClass(MoviePersonPatchDTO.class).translationTo(MoviePerson.class).mapOnlyNotNullProperties();
        c.beanOfClass(MoviePersonPutDTO.class).translationTo(MoviePerson.class);
    }

    private void configureForContentManagerUser(Configuration c) {
        c.beanOfClass(ContentManagerUserPatchDTO.class).translationTo(ContentManagerUser.class)
                .mapOnlyNotNullProperties();
        c.beanOfClass(ContentManagerUserPutDTO.class).translationTo(ContentManagerUser.class);
    }

    private void configureForRegUser(Configuration c) {
        c.beanOfClass(RegUserPatchDTO.class).translationTo(RegUser.class).mapOnlyNotNullProperties();
        c.beanOfClass(RegUserPutDTO.class).translationTo(RegUser.class);
    }

    private void configureForActorRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ActorRole.class).translationTo(ActorRoleReadDTO.class);
        t.srcProperty("movie.id").translatesTo("movieId");
        t.srcProperty("moviePerson.id").translatesTo("moviePersonId");

        Configuration.Translation fromCreateToEntity = c.beanOfClass(ActorRoleCreateDTO.class)
                .translationTo(ActorRole.class);
        fromCreateToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");

        Configuration.Translation fromUpdateToEntity = c.beanOfClass(ActorRolePutDTO.class)
                .translationTo(ActorRole.class);
        fromUpdateToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");

        c.beanOfClass(ActorRolePatchDTO.class).translationTo(ActorRole.class).mapOnlyNotNullProperties();
        Configuration.Translation fromPatchToEntity = c.beanOfClass(ActorRolePatchDTO.class)
                .translationTo(ActorRole.class);
        fromPatchToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");
        fromPatchToEntity.mapOnlyNotNullProperties();
    }

    private void configureForCrew(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Crew.class).translationTo(CrewReadDTO.class);
        t.srcProperty("movie.id").translatesTo("movieId");
        t.srcProperty("moviePerson.id").translatesTo("moviePersonId");

        Configuration.Translation fromCreateToEntity = c.beanOfClass(CrewCreateDTO.class)
                .translationTo(Crew.class);
        fromCreateToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");

        Configuration.Translation fromUpdateToEntity = c.beanOfClass(CrewPutDTO.class).translationTo(Crew.class);
        fromUpdateToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");

        Configuration.Translation fromPatchToEntity = c.beanOfClass(CrewPatchDTO.class).translationTo(Crew.class);
        fromPatchToEntity.srcProperty("moviePersonId").translatesTo("moviePerson.id");
        fromPatchToEntity.mapOnlyNotNullProperties();
    }

    private void configureForReviewActorRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewActorRole.class).translationTo(ReviewActorRoleReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("actorRole.id").translatesTo("actorRoleId");
        t.srcProperty("moderator.id").translatesTo("moderatorId");

        c.beanOfClass(ReviewActorRolePutDTO.class).translationTo(ReviewActorRole.class);
        c.beanOfClass(ReviewActorRolePatchDTO.class).translationTo(ReviewActorRole.class).mapOnlyNotNullProperties();
    }

    private void configureForReviewMovie(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewMovie.class).translationTo(ReviewMovieReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("movie.id").translatesTo("movieId");
        t.srcProperty("moderator.id").translatesTo("moderatorId");

        c.beanOfClass(ReviewMoviePatchDTO.class).translationTo(ReviewMovie.class).mapOnlyNotNullProperties();
        c.beanOfClass(ReviewMoviePutDTO.class).translationTo(ReviewMovie.class);
    }

    private void configureForLikeArticle(Configuration c) {
        Configuration.Translation t = c.beanOfClass(LikeArticle.class).translationTo(LikeArticleReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("article.id").translatesTo("articleId");

        c.beanOfClass(LikeArticlePatchDTO.class).translationTo(LikeArticle.class).mapOnlyNotNullProperties();
        c.beanOfClass(LikeArticlePutDTO.class).translationTo(LikeArticle.class);
    }

    private void configureForLikeReviewActorRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(LikeReviewActorRole.class)
                .translationTo(LikeReviewActorRoleReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("reviewActorRole.id").translatesTo("reviewActorRoleId");

        c.beanOfClass(LikeReviewActorRolePatchDTO.class).translationTo(LikeReviewActorRole.class)
                .mapOnlyNotNullProperties();
        c.beanOfClass(LikeReviewActorRolePutDTO.class).translationTo(LikeReviewActorRole.class);
    }

    private void configureForLikeReviewMovie(Configuration c) {
        Configuration.Translation t = c.beanOfClass(LikeReviewMovie.class)
                .translationTo(LikeReviewMovieReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("reviewMovie.id").translatesTo("reviewMovieId");

        c.beanOfClass(LikeReviewMoviePatchDTO.class).translationTo(LikeReviewMovie.class).mapOnlyNotNullProperties();
        c.beanOfClass(LikeReviewMoviePutDTO.class).translationTo(LikeReviewMovie.class);
    }

    private void configureForMarkActorRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MarkActorRole.class)
                .translationTo(MarkActorRoleReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("actorRole.id").translatesTo("actorRoleId");
    }

    private void configureForMarkMovie(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MarkMovie.class)
                .translationTo(MarkMovieReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("movie.id").translatesTo("movieId");
    }

    private void configureForSignalToContentManager(Configuration c) {
        Configuration.Translation t = c.beanOfClass(SignalToContentManager.class)
                .translationTo(SignalToContentManagerReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("article.id").translatesTo("articleId");
        t.srcProperty("contentManagerUser.id").translatesTo("contentManagerUserId");

        c.beanOfClass(SignalToContentManagerPutDTO.class).translationTo(SignalToContentManager.class);
    }

    private void configureForSignalToModerator(Configuration c) {
        Configuration.Translation t = c.beanOfClass(SignalToModerator.class)
                .translationTo(SignalToModeratorReadDTO.class);
        t.srcProperty("regUser.id").translatesTo("regUserId");
        t.srcProperty("moderatorUser.id").translatesTo("moderatorUserId");
        t.srcProperty("reviewActorRole.id").translatesTo("reviewActorRoleId");
        t.srcProperty("reviewMovie.id").translatesTo("reviewMovieId");

        c.beanOfClass(SignalToModeratorPutDTO.class).translationTo(SignalToModerator.class);
    }
}