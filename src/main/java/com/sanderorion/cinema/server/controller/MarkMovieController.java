package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.Admin;
import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieCreateDTO;
import com.sanderorion.cinema.server.dto.markmovie.MarkMovieReadDTO;
import com.sanderorion.cinema.server.service.MarkMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users")
public class MarkMovieController {

    @Autowired
    private MarkMovieService markMovieService;

    @AdminOrCurrentUser
    @GetMapping("/{id}/mark-movies/{movieId}")
    public MarkMovieReadDTO getMarkMovie(@PathVariable UUID id,
                                         @PathVariable UUID movieId) {
        return markMovieService.getMarkMovie(movieId);
    }

    @AdminOrCurrentUser
    @GetMapping("/{id}/mark-movies")
    public List<MarkMovieReadDTO> getRegMarkMovie(@PathVariable UUID id) {
        return markMovieService.getUserMarksMovies(id);
    }

    @CurrentUser
    @PostMapping("/{id}/mark-movies/{movieId}")
    public MarkMovieReadDTO createMarkMovie(@PathVariable UUID id,
                                            @PathVariable UUID movieId,
                                            @RequestBody @Valid MarkMovieCreateDTO createDTO) {
        return markMovieService.createMarkMovie(id, movieId, createDTO);
    }

    @Admin
    @DeleteMapping("/mark-movies/{movieId}")
    public void deleteMarkMovie(@PathVariable UUID movieId) {
        markMovieService.deleteMarkMovie(movieId);
    }
}