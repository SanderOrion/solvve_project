package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.Admin;
import com.sanderorion.cinema.server.controller.secutity.AdminOrModerator;
import com.sanderorion.cinema.server.controller.secutity.AllRoleTypeAndCurrentUser;
import com.sanderorion.cinema.server.dto.userrole.UserRoleReadDTO;
import com.sanderorion.cinema.server.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @Admin
    @GetMapping("/user-roles")
    public List<UserRoleReadDTO> getAllUserRoles() {
        return userRoleService.getAllUserRoles();
    }

    @AllRoleTypeAndCurrentUser
    @GetMapping("/reg-users/{id}/user-roles")
    public List<UserRoleReadDTO> getUserRoles(@PathVariable UUID id) {
        return userRoleService.getUserRoles(id);
    }

    @AdminOrModerator
    @PostMapping("/reg-users/{id}/user-roles/{userRoleId}")
    public List<UserRoleReadDTO> addUserRoleToRegUser(@PathVariable UUID id, @PathVariable UUID userRoleId) {
        return userRoleService.addUserRoleToRegUser(id, userRoleId);
    }

    @AdminOrModerator
    @DeleteMapping("/reg-users/{id}/user-roles/{userRoleId}")
    public List<UserRoleReadDTO> removeUserRoleToRegUser(@PathVariable UUID id, @PathVariable UUID userRoleId) {
        return userRoleService.removeUserRoleToRegUser(id, userRoleId);
    }
}