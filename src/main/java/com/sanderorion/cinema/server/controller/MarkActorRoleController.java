package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.Admin;
import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.markactorrole.MarkActorRoleReadDTO;
import com.sanderorion.cinema.server.service.MarkActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users")
public class MarkActorRoleController {

    @Autowired
    private MarkActorRoleService markActorRoleService;

    @AdminOrCurrentUser
    @GetMapping("/{id}/mark-actor-roles/{actorRoleId}")
    public MarkActorRoleReadDTO getMarkActorRole(@PathVariable UUID id,
                                                 @PathVariable UUID actorRoleId) {
        return markActorRoleService.getMarkActorRole(actorRoleId);
    }

    @AdminOrCurrentUser
    @GetMapping("/{id}/mark-actor-roles")
    public List<MarkActorRoleReadDTO> getRegMarkActorRole(@PathVariable UUID id) {
        return markActorRoleService.getUserMarkActorRoles(id);
    }

    @CurrentUser
    @PostMapping("/{id}/mark-actor-roles/{actorRoleId}")
    public MarkActorRoleReadDTO createMarkActorRole(@PathVariable UUID id,
                                                    @PathVariable UUID actorRoleId,
                                                    @RequestBody @Valid MarkActorRoleCreateDTO createDTO) {
        return markActorRoleService.createMarkActorRole(id, actorRoleId, createDTO);
    }

    @Admin
    @DeleteMapping("/mark-actor-roles/{actorRoleId}")
    public void deleteMarkActorRole(@PathVariable UUID actorRoleId) {
        markActorRoleService.deleteMarkActorRole(actorRoleId);
    }
}