package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.dto.genre.GenreReadDTO;
import com.sanderorion.cinema.server.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @AllRoleType
    @GetMapping("/genres")
    public List<GenreReadDTO> getAllGenres() {
        return genreService.getAllGenres();
    }

    @AllRoleType
    @GetMapping("/movies/{movieId}/genres")
    public List<GenreReadDTO> getGenres(@PathVariable UUID movieId) {
        return genreService.getGenres(movieId);
    }

    @AdminOrContentManager
    @PostMapping("/movies/{movieId}/genres{genreId}")
    public List<GenreReadDTO> addGenreToMovie(@PathVariable UUID movieId, @PathVariable UUID genreId) {
        return genreService.addGenreToMovie(movieId, genreId);
    }

    @AdminOrContentManager
    @DeleteMapping("/movies/{movieId}/genres{genreId}")
    public List<GenreReadDTO> removeGenre(@PathVariable UUID movieId, @PathVariable UUID genreId) {
        return genreService.removeGenreFromMovie(movieId, genreId);
    }
}