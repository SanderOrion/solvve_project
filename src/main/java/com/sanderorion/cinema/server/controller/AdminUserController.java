package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.dto.adminuser.AdminUserCreateDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPatchDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserPutDTO;
import com.sanderorion.cinema.server.dto.adminuser.AdminUserReadDTO;
import com.sanderorion.cinema.server.service.AdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admin-users")
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;

    @GetMapping("/{id}")
    public AdminUserReadDTO getAdminUser(@PathVariable UUID id) {
        return adminUserService.getAdminUser(id);
    }

    @PostMapping
    public AdminUserReadDTO createAdminUser(@RequestBody @Valid AdminUserCreateDTO createDTO) {
        return adminUserService.createAdminUser(createDTO);
    }

    @PatchMapping("/{id}")
    public AdminUserReadDTO patchAdminUser(@PathVariable UUID id, @RequestBody AdminUserPatchDTO patch) {
        return adminUserService.patchAdminUser(id, patch);
    }

    @PutMapping("/{id}")
    public AdminUserReadDTO updateAdminUser(@PathVariable UUID id, @RequestBody AdminUserPutDTO put) {
        return adminUserService.updateAdminUser(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteAdminUser(@PathVariable UUID id) {
        adminUserService.deleteAdminUser(id);
    }
}