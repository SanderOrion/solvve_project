package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.likereviewactorrole.LikeReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.service.LikeReviewActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users/{id}/like-review-actor-roles")
public class LikeReviewActorRoleController {

    @Autowired
    private LikeReviewActorRoleService likeReviewActorRoleService;

    @AdminOrCurrentUser
    @GetMapping("/{likeReviewActorRoleId}")
    public LikeReviewActorRoleReadDTO getLikeReviewActorRole(@PathVariable UUID id,
                                                             @PathVariable UUID likeReviewActorRoleId) {
        return likeReviewActorRoleService.getLikeReviewActorRole(likeReviewActorRoleId);
    }

    @AdminOrCurrentUser
    @GetMapping()
    public List<LikeReviewActorRoleReadDTO> getRegLikeReviewActorRole(@PathVariable UUID id) {
        return likeReviewActorRoleService.getRegLikeReviewActorRoles(id);
    }

    @CurrentUser
    @PostMapping("/{reviewActorRoleId}")
    public LikeReviewActorRoleReadDTO createActorRole(@PathVariable UUID id,
                                                      @PathVariable UUID reviewActorRoleId,
                                                      @RequestBody @Valid LikeReviewActorRoleCreateDTO createDTO) {
        return likeReviewActorRoleService.createLikeReviewActorRole(id, reviewActorRoleId, createDTO);
    }

    @CurrentUser
    @PatchMapping("/{likeReviewActorRoleId}")
    public LikeReviewActorRoleReadDTO patchLikeReviewActorRole(@PathVariable UUID likeReviewActorRoleId,
                                                               @PathVariable UUID id,
                                                               @RequestBody LikeReviewActorRolePatchDTO patch) {
        return likeReviewActorRoleService.patchLikeReviewActorRole(likeReviewActorRoleId, patch);
    }

    @CurrentUser
    @PutMapping("/{likeReviewActorRoleId}")
    public LikeReviewActorRoleReadDTO updateLikeReviewActorRole(@PathVariable UUID likeReviewActorRoleId,
                                                                @PathVariable UUID id,
                                                                @RequestBody LikeReviewActorRolePutDTO put) {
        return likeReviewActorRoleService.updateLikeReviewActorRole(likeReviewActorRoleId, put);
    }

    @CurrentUser
    @DeleteMapping("/{likeReviewActorRoleId}")
    public void deleteLikeReviewActorRole(@PathVariable UUID likeReviewActorRoleId,
                                          @PathVariable UUID id) {
        likeReviewActorRoleService.deleteLikeReviewActorRole(likeReviewActorRoleId);
    }
}