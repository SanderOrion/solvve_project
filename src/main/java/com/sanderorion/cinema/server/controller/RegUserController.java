package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.Admin;
import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.dto.reguser.RegUserCreateDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPatchDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.service.RegUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users")
public class RegUserController {

    @Autowired
    private RegUserService regUserService;

    @Admin
    @GetMapping("/{id}")
    public RegUserReadDTO getRegUser(@PathVariable UUID id) {
        return regUserService.getRegUser(id);
    }

    @PostMapping
    public RegUserReadDTO createRegUser(@RequestBody @Valid RegUserCreateDTO createDTO) {
        return regUserService.createRegUser(createDTO);
    }

    @AdminOrCurrentUser
    @PatchMapping("/{id}")
    public RegUserReadDTO patchRegUser(@PathVariable UUID id, @RequestBody RegUserPatchDTO patch) {
        return regUserService.patchRegUser(id, patch);
    }

    @AdminOrCurrentUser
    @PutMapping("/{id}")
    public RegUserReadDTO updateRegUser(@PathVariable UUID id, @RequestBody RegUserPutDTO put) {
        return regUserService.updateRegUser(id, put);
    }

    @Admin
    @DeleteMapping("/{id}")
    public void deleteRegUser(@PathVariable UUID id) {
        regUserService.deleteRegUser(id);
    }
}