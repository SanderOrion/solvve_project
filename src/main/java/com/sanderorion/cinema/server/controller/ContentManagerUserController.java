package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserCreateDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPatchDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserPutDTO;
import com.sanderorion.cinema.server.dto.contentmanageruser.ContentManagerUserReadDTO;
import com.sanderorion.cinema.server.service.ContentManagerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/content-manager-users")
public class ContentManagerUserController {

    @Autowired
    private ContentManagerUserService contentManagerUserService;

    @GetMapping("/{id}")
    public ContentManagerUserReadDTO getContentManagerUser(@PathVariable UUID id) {
        return contentManagerUserService.getContentManagerUser(id);
    }

    @PostMapping
    public ContentManagerUserReadDTO createContentManagerUser(@RequestBody @Valid ContentManagerUserCreateDTO create) {
        return contentManagerUserService.createContentManagerUser(create);
    }

    @PatchMapping("/{id}")
    public ContentManagerUserReadDTO patchContentManagerUser(@PathVariable UUID id,
                                                             @RequestBody ContentManagerUserPatchDTO patch) {
        return contentManagerUserService.patchContentManagerUser(id, patch);
    }

    @PutMapping("/{id}")
    public ContentManagerUserReadDTO updateContentManagerUser(@PathVariable UUID id,
                                                              @RequestBody ContentManagerUserPutDTO put) {
        return contentManagerUserService.updateContentManagerUser(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteContentManagerUser(@PathVariable UUID id) {
        contentManagerUserService.deleteContentManagerUser(id);
    }
}