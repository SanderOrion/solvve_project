package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.documentation.ApiPageable;
import com.sanderorion.cinema.server.controller.secutity.AdminOrModerator;
import com.sanderorion.cinema.server.controller.secutity.AllRoleTypeAndCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reguser.RegUserPutTrustDTO;
import com.sanderorion.cinema.server.dto.reguser.RegUserReadDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorCreateDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorPutDTO;
import com.sanderorion.cinema.server.dto.signaltomoderator.SignalToModeratorReadDTO;
import com.sanderorion.cinema.server.service.SignalToModeratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class SignalToModeratorController {

    @Autowired
    private SignalToModeratorService signalToModeratorService;

    @AllRoleTypeAndCurrentUser
    @GetMapping("/reg-users/{id}/signals-to-moderator/{signalId}")
    public SignalToModeratorReadDTO getSignalToModerator(@PathVariable UUID id,
                                                         @PathVariable UUID signalId) {
        return signalToModeratorService.getSignalToModerator(signalId);
    }

    @AllRoleTypeAndCurrentUser
    @GetMapping("/reg-users/{id}/signals-to-moderator")
    public List<SignalToModeratorReadDTO> getRegUserSignalsToModerator(@PathVariable UUID id) {
        return signalToModeratorService.getRegUserSignalsToModerator(id);
    }

    @ApiPageable
    @AdminOrModerator
    @GetMapping("/moderator-users/signals-for-check")
    public PageResult<SignalToModeratorReadDTO> getSignalsForCheck(@ApiIgnore Pageable pageable) {
        return signalToModeratorService.getSignalsForCheck(pageable);
    }

    @CurrentUser
    @PostMapping("/reviews/{reviewId}/reg-users/{id}")
    public SignalToModeratorReadDTO createSignalToModerator(@PathVariable UUID id,
                                                            @PathVariable UUID reviewId,
                                                            @RequestBody @Valid SignalToModeratorCreateDTO create) {
        return signalToModeratorService.createSignalToModerator(id, reviewId, create);
    }

    @AdminOrModerator
    @PutMapping("/moderator-users/{moderatorId}/signals-for-check/{id}")
    public SignalToModeratorReadDTO updateSignalToModerator(@PathVariable UUID id,
                                                            @PathVariable UUID moderatorId,
                                                            @RequestBody SignalToModeratorPutDTO put) {
        return signalToModeratorService.updateSignalToModerator(id, moderatorId, put);
    }

    @AdminOrModerator
    @PutMapping("/moderator-users/update-users-for-review/reg-users/{id}")
    public RegUserReadDTO updateUserTrustForReview(@PathVariable UUID id,
                                                   @RequestBody RegUserPutTrustDTO put) {
        return signalToModeratorService.updateUserTrustForReview(id, put);
    }

    @AdminOrModerator
    @DeleteMapping("/moderator-users/signals/{id}")
    public void deleteSignalToModerator(@PathVariable UUID id) {
        signalToModeratorService.deleteSignalToModerator(id);
    }
}