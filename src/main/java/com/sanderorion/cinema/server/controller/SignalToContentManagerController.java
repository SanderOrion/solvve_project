package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.documentation.ApiPageable;
import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.controller.secutity.AllRoleTypeAndCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerCreateDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerPutDTO;
import com.sanderorion.cinema.server.dto.signaltocontentmanager.SignalToContentManagerReadDTO;
import com.sanderorion.cinema.server.service.SignalToContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class SignalToContentManagerController {

    @Autowired
    private SignalToContentManagerService signalToContentManagerService;

    @AllRoleTypeAndCurrentUser
    @GetMapping("/reg-users/{id}/signals/{signalId}")
    public SignalToContentManagerReadDTO getSignalToCM(@PathVariable UUID id,
                                                       @PathVariable UUID signalId) {
        return signalToContentManagerService.getSignalToCM(signalId);
    }

    @AllRoleTypeAndCurrentUser
    @GetMapping("/reg-users/{id}/signals")
    public List<SignalToContentManagerReadDTO> getRegSignalToCM(@PathVariable UUID id) {
        return signalToContentManagerService.getRegUserSignalsToCM(id);
    }

    @ApiPageable
    @AdminOrContentManager
    @GetMapping("/content-manager-users/signals-for-fix")
    public PageResult<SignalToContentManagerReadDTO> getSignalsForFix(@ApiIgnore Pageable pageable) {
        return signalToContentManagerService.getSignalsForFix(pageable);
    }

    @CurrentUser
    @PostMapping("/articles/{articleId}/reg-users/{id}")
    public SignalToContentManagerReadDTO createSignalToCM(@PathVariable UUID id,
                                                          @PathVariable UUID articleId,
                                                          @RequestBody @Valid SignalToContentManagerCreateDTO create) {
        return signalToContentManagerService.createSignalToCM(articleId, id, create);
    }

    @AdminOrContentManager
    @PutMapping("/content-manager-users/{cmId}/signals-for-fix/{id}")
    public SignalToContentManagerReadDTO updateSignalToCM(@PathVariable UUID id,
                                                          @PathVariable UUID cmId,
                                                          @RequestBody SignalToContentManagerPutDTO put) {
        return signalToContentManagerService.updateSignalToCM(id, cmId, put);
    }

    @AdminOrContentManager
    @DeleteMapping("/content-manager-users/signals/{id}")
    public void deleteSignalToCM(@PathVariable UUID id) {
        signalToContentManagerService.deleteSignalToCM(id);
    }
}