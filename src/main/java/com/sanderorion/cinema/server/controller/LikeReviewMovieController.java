package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.likereviewmovie.LikeReviewMovieReadDTO;
import com.sanderorion.cinema.server.service.LikeReviewMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users/{id}/like-review-movies")
public class LikeReviewMovieController {

    @Autowired
    private LikeReviewMovieService likeReviewMovieService;

    @AdminOrCurrentUser
    @GetMapping("/{likeReviewMovieId}")
    public LikeReviewMovieReadDTO getLikeReviewMovie(@PathVariable UUID id,
                                                     @PathVariable UUID likeReviewMovieId) {
        return likeReviewMovieService.getLikeReviewMovie(likeReviewMovieId);
    }

    @AdminOrCurrentUser
    @GetMapping
    public List<LikeReviewMovieReadDTO> getRegLikeReviewMovie(@PathVariable UUID id) {
        return likeReviewMovieService.getRegLikeReviewMovies(id);
    }

    @CurrentUser
    @PostMapping("/{reviewMovieId}")
    public LikeReviewMovieReadDTO createActorRole(@PathVariable UUID id,
                                                  @PathVariable UUID reviewMovieId,
                                                  @RequestBody @Valid LikeReviewMovieCreateDTO createDTO) {
        return likeReviewMovieService.createLikeReviewMovie(id, reviewMovieId, createDTO);
    }

    @CurrentUser
    @PatchMapping("/{likeReviewMovieId}")
    public LikeReviewMovieReadDTO patchLikeReviewMovie(@PathVariable UUID likeReviewMovieId,
                                                       @PathVariable UUID id,
                                                       @RequestBody LikeReviewMoviePatchDTO patch) {
        return likeReviewMovieService.patchLikeReviewMovie(likeReviewMovieId, patch);
    }

    @CurrentUser
    @PutMapping("/{likeReviewMovieId}")
    public LikeReviewMovieReadDTO updateLikeReviewMovie(@PathVariable UUID likeReviewMovieId,
                                                        @PathVariable UUID id,
                                                        @RequestBody LikeReviewMoviePutDTO put) {
        return likeReviewMovieService.updateLikeReviewMovie(likeReviewMovieId, put);
    }

    @CurrentUser
    @DeleteMapping("/{likeReviewMovieId}")
    public void deleteLikeReviewMovie(@PathVariable UUID likeReviewMovieId,
                                      @PathVariable UUID id) {
        likeReviewMovieService.deleteLikeReviewMovie(likeReviewMovieId);
    }
}