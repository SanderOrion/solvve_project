package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonCreateDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPatchDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonPutDTO;
import com.sanderorion.cinema.server.dto.movieperson.MoviePersonReadDTO;
import com.sanderorion.cinema.server.service.MoviePersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movie-persons")
public class MoviePersonController {

    @Autowired
    private MoviePersonService moviePersonService;

    @GetMapping("/{id}")
    public MoviePersonReadDTO getMoviePerson(@PathVariable UUID id) {
        return moviePersonService.getMoviePerson(id);
    }

    @AdminOrContentManager
    @PostMapping
    public MoviePersonReadDTO createMoviePerson(@RequestBody @Valid MoviePersonCreateDTO createDTO) {
        return moviePersonService.createMoviePerson(createDTO);
    }

    @AdminOrContentManager
    @PatchMapping("/{id}")
    public MoviePersonReadDTO patchMoviePerson(@PathVariable UUID id, @RequestBody MoviePersonPatchDTO patch) {
        return moviePersonService.patchMoviePerson(id, patch);
    }

    @AdminOrContentManager
    @PutMapping("/{id}")
    public MoviePersonReadDTO updateMoviePerson(@PathVariable UUID id, @RequestBody MoviePersonPutDTO put) {
        return moviePersonService.updateMoviePerson(id, put);
    }

    @AdminOrContentManager
    @DeleteMapping("/{id}")
    public void deleteMoviePerson(@PathVariable UUID id) {
        moviePersonService.deleteMoviePerson(id);
    }
}