package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.*;
import com.sanderorion.cinema.server.dto.MovieInLeaderBoardReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieCreateDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePatchDTO;
import com.sanderorion.cinema.server.dto.movie.MoviePutDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.service.MovieService;
import com.sanderorion.cinema.server.service.importer.MovieImporterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieImporterService movieImporterService;

    @AdminOrContentManager
    @GetMapping("/content-manager-users/import-movie/{id}")
    public UUID importMovie(@PathVariable Integer id) {
        try {
            return movieImporterService.importMovie(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/movies/{id}")
    public MovieReadDTO getMovie(@PathVariable UUID id) {
        return movieService.getMovie(id);
    }

    @AllRoleType
    @GetMapping("/movies/leader-board")
    public List<MovieInLeaderBoardReadDTO> getMoviesLeaderBoard() {
        return movieService.getMoviesLeaderBoard();
    }

    @AdminOrContentManager
    @PostMapping("/movies")
    public MovieReadDTO createMovie(@RequestBody @Valid MovieCreateDTO createDTO) {
        return movieService.createMovie(createDTO);
    }

    @AdminOrContentManager
    @PatchMapping("/movies/{id}")
    public MovieReadDTO patchMovie(@PathVariable UUID id, @RequestBody MoviePatchDTO patch) {
        return movieService.patchMovie(id, patch);
    }

    @AdminOrContentManager
    @PutMapping("/movies/{id}")
    public MovieReadDTO updateMovie(@PathVariable UUID id, @RequestBody MoviePutDTO put) {
        return movieService.updateMovie(id, put);
    }

    @AdminOrContentManager
    @DeleteMapping("/movies/{id}")
    public void deleteMovie(@PathVariable UUID id) {
        movieService.deleteMovie(id);
    }
}