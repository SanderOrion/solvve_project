package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserCreateDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPatchDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserPutDTO;
import com.sanderorion.cinema.server.dto.moderatoruser.ModeratorUserReadDTO;
import com.sanderorion.cinema.server.service.ModeratorUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/moderator-users")
public class ModeratorUserController {

    @Autowired
    private ModeratorUserService moderatorUserService;

    @GetMapping("/{id}")
    public ModeratorUserReadDTO getModeratorUser(@PathVariable UUID id) {
        return moderatorUserService.getModeratorUser(id);
    }

    @PostMapping
    public ModeratorUserReadDTO createModeratorUser(@RequestBody @Valid ModeratorUserCreateDTO createDTO) {
        return moderatorUserService.createModeratorUser(createDTO);
    }

    @PatchMapping("/{id}")
    public ModeratorUserReadDTO patchModeratorUser(@PathVariable UUID id, @RequestBody ModeratorUserPatchDTO patch) {
        return moderatorUserService.patchModeratorUser(id, patch);
    }

    @PutMapping("/{id}")
    public ModeratorUserReadDTO updateModeratorUser(@PathVariable UUID id, @RequestBody ModeratorUserPutDTO put) {
        return moderatorUserService.updateModeratorUser(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteModeratorUser(@PathVariable UUID id) {
        moderatorUserService.deleteModeratorUser(id);
    }
}