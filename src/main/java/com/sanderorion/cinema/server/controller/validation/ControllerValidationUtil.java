package com.sanderorion.cinema.server.controller.validation;

import com.sanderorion.cinema.server.controller.ControllerValidationException;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ControllerValidationUtil {
    public static <T extends Comparable<T>> void validateNotEqual(T value1, T value2,
                                                                  String field1Name, String field2Name) {
        if (value1.equals(value2)) {
            throw new ControllerValidationException(String.format("Field %s=%s should be no equal %s=%s",
                    field1Name, value1, field2Name, value2));
        }
    }
}