package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.documentation.ApiPageable;
import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.controller.validation.ControllerValidationUtil;
import com.sanderorion.cinema.server.dto.ArticleFilter;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.article.ArticleCreateDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePatchDTO;
import com.sanderorion.cinema.server.dto.article.ArticlePutDTO;
import com.sanderorion.cinema.server.dto.article.ArticleReadDTO;
import com.sanderorion.cinema.server.dto.movie.MovieReadDTO;
import com.sanderorion.cinema.server.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping("/{id}")
    public ArticleReadDTO getArticle(@PathVariable UUID id) {
        return articleService.getArticle(id);
    }

    @ApiPageable
    @AllRoleType
    @GetMapping
    public PageResult<ArticleReadDTO> getArticles(ArticleFilter filter, @ApiIgnore Pageable pageable) {
        return articleService.getArticles(filter, pageable);
    }

    @AdminOrContentManager
    @PostMapping
    public ArticleReadDTO createArticle(@RequestBody @Valid ArticleCreateDTO createDTO) {
        ControllerValidationUtil.validateNotEqual(createDTO.getTitle(), createDTO.getBody(),
                "title", "body");
        return articleService.createArticle(createDTO);
    }

    @AdminOrContentManager
    @PatchMapping("/{id}")
    public ArticleReadDTO patchArticle(@PathVariable UUID id, @RequestBody ArticlePatchDTO patch) {
        return articleService.patchArticle(id, patch);
    }

    @AdminOrContentManager
    @DeleteMapping("/{id}")
    public void deleteArticle(@PathVariable UUID id) {
        articleService.deleteArticle(id);
    }

    @AdminOrContentManager
    @PutMapping("/{id}")
    public ArticleReadDTO updateArticle(@PathVariable UUID id, @RequestBody ArticlePutDTO put) {
        return articleService.updateArticle(id, put);
    }

    @AdminOrContentManager
    @PostMapping("/{articleId}/movies/{movieId}")
    public List<MovieReadDTO> addMovieToArticle(@PathVariable UUID articleId, @PathVariable UUID movieId) {
        return articleService.addMovieToArticle(articleId, movieId);
    }

    @AdminOrContentManager
    @DeleteMapping("/{articleId}/movies/{movieId}")
    public List<MovieReadDTO> removeMovieToArticle(@PathVariable UUID articleId, @PathVariable UUID movieId) {
        return articleService.removeMovieToArticle(articleId, movieId);
    }
}