package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrCurrentUser;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleCreateDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePatchDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticlePutDTO;
import com.sanderorion.cinema.server.dto.likearticle.LikeArticleReadDTO;
import com.sanderorion.cinema.server.service.LikeArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reg-users/{id}/like-articles")
public class LikeArticleController {

    @Autowired
    private LikeArticleService likeArticleService;

    @AdminOrCurrentUser
    @GetMapping("/{articleLikeId}")
    public LikeArticleReadDTO getLikeArticle(@PathVariable UUID id,
                                             @PathVariable UUID articleLikeId) {
        return likeArticleService.getLikeArticle(articleLikeId);
    }

    @AdminOrCurrentUser
    @GetMapping
    public List<LikeArticleReadDTO> getRegLikeArticle(@PathVariable UUID id) {
        return likeArticleService.getRegUserLikeArticles(id);
    }

    @CurrentUser
    @PostMapping("/{articleId}")
    public LikeArticleReadDTO createLikeArticle(@PathVariable UUID id,
                                                @PathVariable UUID articleId,
                                                @RequestBody @Valid LikeArticleCreateDTO createDTO) {
        return likeArticleService.createLikeArticle(id, articleId, createDTO);
    }

    @CurrentUser
    @PatchMapping("/{articleLikeId}")
    public LikeArticleReadDTO patchLikeArticle(@PathVariable UUID articleLikeId,
                                               @PathVariable UUID id,
                                               @RequestBody LikeArticlePatchDTO patch) {
        return likeArticleService.patchLikeArticle(articleLikeId, patch);
    }

    @CurrentUser
    @PutMapping("/{articleLikeId}")
    public LikeArticleReadDTO updateLikeArticle(@PathVariable UUID articleLikeId,
                                                @PathVariable UUID id,
                                                @RequestBody LikeArticlePutDTO put) {
        return likeArticleService.updateLikeArticle(articleLikeId, put);
    }

    @CurrentUser
    @DeleteMapping("/{articleLikeId}")
    public void deleteLikeArticle(@PathVariable UUID articleLikeId,
                                  @PathVariable UUID id) {
        likeArticleService.deleteLikeArticle(articleLikeId);
    }
}