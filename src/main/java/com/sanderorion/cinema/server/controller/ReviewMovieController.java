package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.documentation.ApiPageable;
import com.sanderorion.cinema.server.controller.secutity.AdminOrModerator;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieCreateDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePatchDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMoviePutDTO;
import com.sanderorion.cinema.server.dto.reviewmovie.ReviewMovieReadDTO;
import com.sanderorion.cinema.server.service.ReviewMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ReviewMovieController {

    @Autowired
    private ReviewMovieService reviewMovieService;

    @AllRoleType
    @GetMapping("/reg-users/review-movies/{id}")
    public ReviewMovieReadDTO getReviewMovie(@PathVariable UUID id) {
        return reviewMovieService.getReviewMovie(id);
    }

    @AllRoleType
    @GetMapping("/reg-users/{id}/review-movies")
    public List<ReviewMovieReadDTO> getRegUserReviewMovie(@PathVariable UUID id) {
        return reviewMovieService.getRegUserReviewMovies(id);
    }

    @ApiPageable
    @AllRoleType
    @GetMapping("/movies/{id}/review-movies")
    public PageResult<ReviewMovieReadDTO> getAllConfirmedReviewsMovie(@PathVariable UUID id,
                                                                      @ApiIgnore Pageable pageable) {
        return reviewMovieService.getAllConfirmedReviewsMovie(id, pageable);
    }

    @ApiPageable
    @AdminOrModerator
    @GetMapping("/movies/review-movies-to-confirm")
    public PageResult<ReviewMovieReadDTO> getReviewsMovieToConfirm(@ApiIgnore Pageable pageable) {
        return reviewMovieService.getReviewsMovieToConfirm(pageable);
    }

    @CurrentUser
    @PostMapping("/reg-users/{id}/review-movies/{movieId}")
    public ReviewMovieReadDTO createReviewMovie(@PathVariable UUID id,
                                                @PathVariable UUID movieId,
                                                @RequestBody @Valid ReviewMovieCreateDTO createDTO) {
        return reviewMovieService.createReviewMovie(id, movieId, createDTO);
    }

    @AdminOrModerator
    @PatchMapping("/reg-users/review-movies/{id}")
    public ReviewMovieReadDTO patchReviewMovie(@PathVariable UUID id,
                                               @RequestBody ReviewMoviePatchDTO patch) {
        return reviewMovieService.patchReviewMovie(id, patch);
    }

    @AdminOrModerator
    @PutMapping("/moderator-users/{moderatorId}/review-movies-to-confirm/{id}")
    public ReviewMovieReadDTO updateReviewMovie(@PathVariable UUID id,
                                                @PathVariable UUID moderatorId,
                                                @RequestBody ReviewMoviePutDTO put) {
        return reviewMovieService.updateReviewMovie(id, moderatorId, put);
    }

    @AdminOrModerator
    @DeleteMapping("/reg-users/review-movies/{id}")
    public void deleteReviewMovie(@PathVariable UUID id) {
        reviewMovieService.deleteReviewMovie(id);
    }
}