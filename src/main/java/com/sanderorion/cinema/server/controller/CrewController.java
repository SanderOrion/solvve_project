package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.dto.crew.CrewCreateDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPatchDTO;
import com.sanderorion.cinema.server.dto.crew.CrewPutDTO;
import com.sanderorion.cinema.server.dto.crew.CrewReadDTO;
import com.sanderorion.cinema.server.service.CrewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies/{movieId}/crews")
public class CrewController {

    @Autowired
    private CrewService crewService;

    @AllRoleType
    @GetMapping("/{id}")
    public CrewReadDTO getCrew(@PathVariable UUID id) {
        return crewService.getCrew(id);
    }

    @AllRoleType
    @GetMapping
    public List<CrewReadDTO> getMovieCrew(@PathVariable UUID movieId) {
        return crewService.getMovieCrews(movieId);
    }

    @AdminOrContentManager
    @PostMapping
    public CrewReadDTO createCrew(@PathVariable UUID movieId, @RequestBody @Valid CrewCreateDTO createDTO) {
        return crewService.createCrew(movieId, createDTO);
    }

    @AdminOrContentManager
    @PatchMapping("/{id}")
    public CrewReadDTO patchCrew(@PathVariable UUID id, @RequestBody CrewPatchDTO patch) {
        return crewService.patchCrew(id, patch);
    }

    @AdminOrContentManager
    @PutMapping("/{id}")
    public CrewReadDTO updateCrew(@PathVariable UUID id, @RequestBody CrewPutDTO put) {
        return crewService.updateCrew(id, put);
    }

    @AdminOrContentManager
    @DeleteMapping("/{id}")
    public void deleteCrew(@PathVariable UUID id) {
        crewService.deleteCrew(id);
    }
}