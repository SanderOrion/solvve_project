package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.secutity.AdminOrContentManager;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRolePutDTO;
import com.sanderorion.cinema.server.dto.actorrole.ActorRoleReadDTO;
import com.sanderorion.cinema.server.service.ActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies/{movieId}/actor-roles")
public class ActorRoleController {

    @Autowired
    private ActorRoleService actorRoleService;

    @AllRoleType
    @GetMapping("/{id}")
    public ActorRoleReadDTO getActorRole(@PathVariable UUID id) {
        return actorRoleService.getActorRole(id);
    }

    @AllRoleType
    @GetMapping
    public List<ActorRoleReadDTO> getMovieActorRoles(@PathVariable UUID movieId) {
        return actorRoleService.getMovieActorRoles(movieId);
    }

    @AdminOrContentManager
    @PostMapping
    public ActorRoleReadDTO createActorRole(@PathVariable UUID movieId,
                                            @RequestBody @Valid ActorRoleCreateDTO createDTO) {
        return actorRoleService.createActorRole(movieId, createDTO);
    }

    @AdminOrContentManager
    @PatchMapping("/{id}")
    public ActorRoleReadDTO patchActorRole(@PathVariable UUID id, @RequestBody ActorRolePatchDTO patch) {
        return actorRoleService.patchActorRole(id, patch);
    }

    @AdminOrContentManager
    @PutMapping("/{id}")
    public ActorRoleReadDTO updateActorRole(@PathVariable UUID id, @RequestBody ActorRolePutDTO put) {
        return actorRoleService.updateActorRole(id, put);
    }

    @AdminOrContentManager
    @DeleteMapping("/{id}")
    public void deleteActorRole(@PathVariable UUID id) {
        actorRoleService.deleteActorRole(id);
    }
}