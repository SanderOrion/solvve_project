package com.sanderorion.cinema.server.controller;

import com.sanderorion.cinema.server.controller.documentation.ApiPageable;
import com.sanderorion.cinema.server.controller.secutity.AdminOrModerator;
import com.sanderorion.cinema.server.controller.secutity.AllRoleType;
import com.sanderorion.cinema.server.controller.secutity.CurrentUser;
import com.sanderorion.cinema.server.dto.PageResult;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleCreateDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePatchDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRolePutDTO;
import com.sanderorion.cinema.server.dto.reviewactorrole.ReviewActorRoleReadDTO;
import com.sanderorion.cinema.server.service.ReviewActorRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ReviewActorRoleController {

    @Autowired
    private ReviewActorRoleService reviewActorRoleService;

    @AllRoleType
    @GetMapping("/reg-users/review-actor-roles/{id}")
    public ReviewActorRoleReadDTO getReviewActorRole(@PathVariable UUID id) {
        return reviewActorRoleService.getReviewActorRole(id);
    }

    @AllRoleType
    @GetMapping("/reg-users/{id}/review-actor-roles")
    public List<ReviewActorRoleReadDTO> getRegUserReviewActorRoles(@PathVariable UUID id) {
        return reviewActorRoleService.getRegUserReviewActorRoles(id);
    }

    @ApiPageable
    @AllRoleType
    @GetMapping("/movies/{id}/actor-roles/review-actor-roles")
    public PageResult<ReviewActorRoleReadDTO> getAllConfirmedReviewsActorRole(@PathVariable UUID id,
                                                                              @ApiIgnore Pageable pageable) {
        return reviewActorRoleService.getAllConformedReviewsActorRole(id, pageable);
    }

    @ApiPageable
    @AdminOrModerator
    @GetMapping("/movies/actor-roles/review-actor-roles-to-confirm")
    public PageResult<ReviewActorRoleReadDTO> getReviewsActorRoleToConfirm(@ApiIgnore Pageable pageable) {
        return reviewActorRoleService.getReviewsActorRoleToConfirm(pageable);
    }

    @CurrentUser
    @PostMapping("/reg-users/{id}/review-actor-roles/{actorRoleId}")
    public ReviewActorRoleReadDTO createActorRole(@PathVariable UUID id,
                                                  @PathVariable UUID actorRoleId,
                                                  @RequestBody @Valid ReviewActorRoleCreateDTO createDTO) {
        return reviewActorRoleService.createReviewActorRole(id, actorRoleId, createDTO);
    }

    @AdminOrModerator
    @PatchMapping("/reg-users/review-actor-roles/{id}")
    public ReviewActorRoleReadDTO patchReviewActorRole(@PathVariable UUID id,
                                                       @RequestBody ReviewActorRolePatchDTO patch) {
        return reviewActorRoleService.patchReviewActorRole(id, patch);
    }

    @AdminOrModerator
    @PutMapping("/moderator-users/{moderatorId}/review-actor-roles-to-confirm/{id}")
    public ReviewActorRoleReadDTO updateReviewActorRole(@PathVariable UUID id,
                                                        @PathVariable UUID moderatorId,
                                                        @RequestBody ReviewActorRolePutDTO put) {
        return reviewActorRoleService.updateReviewActorRole(id, moderatorId, put);
    }

    @AdminOrModerator
    @DeleteMapping("/reg-users/review-actor-roles/{id}")
    public void deleteReviewActorRole(@PathVariable UUID id) {
        reviewActorRoleService.deleteReviewActorRole(id);
    }
}