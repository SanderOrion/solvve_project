package com.sanderorion.cinema.server.client.themoviedb.dto;

public enum ImportedEntityType {
    MOVIE,
    PERSON
}