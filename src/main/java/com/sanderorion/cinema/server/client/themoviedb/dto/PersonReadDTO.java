package com.sanderorion.cinema.server.client.themoviedb.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonReadDTO {
    private Integer id;
    private String name;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthday;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate deathday;
    private String knownForDepartment;
    private String biography;
    private Integer gender;
    private String placeOfBirth;
}