package com.sanderorion.cinema.server.client;

import com.sanderorion.cinema.server.client.themoviedb.TheMovieDbClientConfig;
import com.sanderorion.cinema.server.client.themoviedb.dto.CreditsReadDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.MovieReadDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.MoviesPageDTO;
import com.sanderorion.cinema.server.client.themoviedb.dto.PersonReadDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "api.themoviedb.org", url = "${themoviedb.api.url}", configuration = TheMovieDbClientConfig.class)
public interface TheMovieDbClient {

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") Integer movieId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/credits")
    CreditsReadDTO getCredits(@PathVariable("movieId") Integer movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}")
    PersonReadDTO getPerson(@PathVariable("personId") Integer personId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/top_rated")
    MoviesPageDTO getTopRatedMovies();
}