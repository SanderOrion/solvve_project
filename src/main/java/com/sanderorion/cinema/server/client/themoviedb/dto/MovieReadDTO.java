package com.sanderorion.cinema.server.client.themoviedb.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MovieReadDTO {
    private Integer id;
    private String originalTitle;
    private String title;
    private String imdbId;
    private String tagline;
    private String overview;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate releaseDate;
    private String status;
    private Integer runtime;
    private String originalLanguage;
    private Double voteAverage;
    private Integer voteCount;
    private Long budget;
    private Long revenue;
    private List<GenreReadDTO> genres;
}