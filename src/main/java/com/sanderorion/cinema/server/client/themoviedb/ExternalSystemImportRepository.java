package com.sanderorion.cinema.server.client.themoviedb;

import com.sanderorion.cinema.server.client.themoviedb.dto.ImportedEntityType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExternalSystemImportRepository extends CrudRepository<ExternalSystemImport, UUID> {
    ExternalSystemImport findByIdInExternalSystemAndEntityType(Integer idInExternalSystem,
                                                               ImportedEntityType entityType);
}