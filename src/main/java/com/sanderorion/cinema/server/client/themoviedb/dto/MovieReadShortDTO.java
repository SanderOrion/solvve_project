package com.sanderorion.cinema.server.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieReadShortDTO {
    private Integer id;
    private String title;
}