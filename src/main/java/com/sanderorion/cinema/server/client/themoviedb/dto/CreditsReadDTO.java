package com.sanderorion.cinema.server.client.themoviedb.dto;

import lombok.Data;

import java.util.List;

@Data
public class CreditsReadDTO {
    private Integer id;
    private List<CastReadDTO> cast;
    private List<CrewReadDTO> crew;
}