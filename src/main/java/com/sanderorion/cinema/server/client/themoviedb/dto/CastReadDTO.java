package com.sanderorion.cinema.server.client.themoviedb.dto;

import lombok.Data;

@Data
public class CastReadDTO {
    private String character;
    private Integer gender;
    private Integer id;
    private String name;
}