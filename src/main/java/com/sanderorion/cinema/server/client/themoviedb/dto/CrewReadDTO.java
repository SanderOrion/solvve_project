package com.sanderorion.cinema.server.client.themoviedb.dto;

import lombok.Data;

@Data
public class CrewReadDTO {
    private String job;
    private Integer gender;
    private Integer id;
    private String name;
    private String department;
}