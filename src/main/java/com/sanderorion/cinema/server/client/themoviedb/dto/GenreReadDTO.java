package com.sanderorion.cinema.server.client.themoviedb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenreReadDTO {
    private String name;
}