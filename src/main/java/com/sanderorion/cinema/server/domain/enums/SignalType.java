package com.sanderorion.cinema.server.domain.enums;

public enum SignalType {
    INSULT,
    SPOILER_ALERT,
    SPAM
}