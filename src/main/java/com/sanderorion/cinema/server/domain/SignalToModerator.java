package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.SignalType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("SM")
public class SignalToModerator extends UserSignal {

    @NotNull
    @Enumerated(EnumType.STRING)
    private SignalType signalType;

    @ManyToOne
    private RegUser moderatorUser;

    @ManyToOne
    private ReviewActorRole reviewActorRole;

    @ManyToOne
    private ReviewMovie reviewMovie;
}