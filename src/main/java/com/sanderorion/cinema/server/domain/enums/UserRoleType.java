package com.sanderorion.cinema.server.domain.enums;

public enum UserRoleType {
    ADMIN,
    CONTENT_MANAGER,
    MODERATOR,
    REG_USER,
    BLOCKED
}