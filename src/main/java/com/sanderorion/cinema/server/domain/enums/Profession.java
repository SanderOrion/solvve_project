package com.sanderorion.cinema.server.domain.enums;

public enum Profession {
    ART,
    CAMERA,
    CAST,
    COSTUME__MAKE_UP,
    CREW,
    DIRECTING,
    EDITING,
    LIGHTING,
    PRODUCTION,
    SOUND,
    VISUAL_EFFECTS,
    WRITING
}