package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class RegUser extends AbstractUser {

    private Double userRatingActivity;
    private Double userRatingReviews;

    @Enumerated(EnumType.STRING)
    private TrustForReview trustForReview;

    @OneToMany(mappedBy = "regUser")
    private List<ReviewMovie> reviewMovies = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<ReviewActorRole> reviewActorRoles = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<LikeArticle> likeArticles = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<LikeReviewActorRole> likeReviewActorRoles = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<LikeReviewMovie> likeReviewMovies = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<MarkActorRole> markActorRoles = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<MarkMovie> markMovies = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<SignalToContentManager> signalToContentManagers = new ArrayList<>();

    @OneToMany(mappedBy = "regUser")
    private List<SignalToModerator> signalToModerators = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "reg_user_user_roles",
            joinColumns = @JoinColumn(name = "reg_user_id"),
            inverseJoinColumns = @JoinColumn(name = "user_role_id"))
    private List<UserRole> userRoles = new ArrayList<>();

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}