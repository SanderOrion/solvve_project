package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BD_TYPE")
public abstract class UserSignal extends AbstractEntity {

    @NotNull
    private String typo;
    private Instant correctionTime;
    private String correctText;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SignalStatus signalStatus;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegUser regUser;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}