package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BD_TYPE")
public abstract class Review extends AbstractEntity {

    @NotNull
    private String body;
    private Integer amountLike;
    private Integer amountDislike;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegUser regUser;

    @ManyToOne
    private RegUser moderator;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReviewConfirmStatus confirmStatus;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}