package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.Profession;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Crew extends AbstractEntity {

    @NotNull
    private Integer internalId;

    @NotNull
    private String crewName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Profession profession;

    @NotNull
    private String description;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Movie movie;

    @ManyToOne
    private MoviePerson moviePerson;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}