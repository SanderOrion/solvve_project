package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;

@Entity
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class MoviePerson extends AbstractEntity {

    @NotNull
    private String name;
    private LocalDate birthday;
    private LocalDate deathDay;

    @NotNull
    private Integer internalId;

    @NotNull
    private String profession;

    @NotNull
    private String biography;

    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String placeOfBirth;
    private Double avgMovieRating;
    private Double avgRoleRating;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}