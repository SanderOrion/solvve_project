package com.sanderorion.cinema.server.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("MM")
public class MarkMovie extends Mark {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegUser regUser;

    @NotNull
    @ManyToOne
    private Movie movie;

    @CreatedDate
    private Instant createdAt;
}