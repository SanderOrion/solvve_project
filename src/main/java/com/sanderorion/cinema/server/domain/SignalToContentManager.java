package com.sanderorion.cinema.server.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("SCM")
public class SignalToContentManager extends UserSignal {

    @NotNull
    @ManyToOne
    private Article article;
    private String userSuggestion;

    @ManyToOne
    private RegUser contentManagerUser;
}