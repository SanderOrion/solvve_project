package com.sanderorion.cinema.server.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("RAR")
public class ReviewActorRole extends Review {

    @NotNull
    @ManyToOne
    private ActorRole actorRole;
}