package com.sanderorion.cinema.server.domain.enums;

public enum SignalStatus {
    FIXED,
    NEED_TO_FIX,
    CANCELED
}