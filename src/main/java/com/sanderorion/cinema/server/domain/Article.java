package com.sanderorion.cinema.server.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Article extends AbstractEntity {

    @NotNull
    private String title;

    @NotNull
    private String body;

    @NotNull
    private Instant dateArticle;

    private Integer amountLike;
    private Integer amountDislike;

    @ManyToMany
    @JoinColumn(name = "article_id")
    private List<Movie> movies = new ArrayList<>();

    @ManyToMany
    @JoinColumn(name = "article_id")
    private List<MoviePerson> moviePersons = new ArrayList<>();

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}