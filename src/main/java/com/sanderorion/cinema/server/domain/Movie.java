package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "movie")
@EntityListeners(AuditingEntityListener.class)
public class Movie extends AbstractEntity {

    @NotNull
    private String originalTitle;

    @NotNull
    private String title;
    private String imdbId;
    private String tagline;
    private String overview;

    @NotNull
    private LocalDate releaseDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MovieRelease status;
    private String country;
    private Integer duration;

    @NotNull
    private String language;
    private Double avgRating;
    private Integer ratingCount;

    @NotNull
    private Long budget;

    @NotNull
    private Long revenue;

    @OneToMany(mappedBy = "movie")
    private List<ActorRole> actorRoles = new ArrayList<>();

    @OneToMany(mappedBy = "movie")
    private List<Crew> crews = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "movie_genres",
            joinColumns = @JoinColumn(name = "movies_id"),
            inverseJoinColumns = @JoinColumn(name = "genres_id"))
    private List<Genre> genres = new ArrayList<>();

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}