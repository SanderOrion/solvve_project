package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Genre extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private MovieGenre movieGenre;

    @ManyToMany(mappedBy = "genres")
    private List<Movie> movies = new ArrayList<>();
}