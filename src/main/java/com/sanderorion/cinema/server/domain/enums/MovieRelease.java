package com.sanderorion.cinema.server.domain.enums;

public enum MovieRelease {
    RELEASED,
    RUMORED,
    IN_PRODUCTION,
    POST_PRODUCTION,
    CANCELED,
    PLANNED
}