package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUser extends AbstractEntity {

    @NotNull
    private String name;

    @NotNull
    private String login;

    @NotNull
    private String email;

    @NotNull
    private String encodedPassword;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;
}