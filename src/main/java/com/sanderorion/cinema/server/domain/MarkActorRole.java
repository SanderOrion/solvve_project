package com.sanderorion.cinema.server.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("MAR")
public class MarkActorRole extends Mark {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegUser regUser;

    @NotNull
    @ManyToOne
    private ActorRole actorRole;

    @CreatedDate
    private Instant createdAt;
}