package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class ActorRole extends AbstractEntity {

    @NotNull
    private Integer internalId;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    private String roleName;

    @NotNull
    private String roleInfo;
    private Double avgRating;
    private Integer ratingCount;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Movie movie;

    @ManyToOne
    private MoviePerson moviePerson;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}