package com.sanderorion.cinema.server.domain.enums;

public enum ReviewConfirmStatus {
    CONFIRMED,
    NEED_TO_CONFIRM,
    UNCONFIRMED
}