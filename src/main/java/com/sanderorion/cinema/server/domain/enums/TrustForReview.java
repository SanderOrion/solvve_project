package com.sanderorion.cinema.server.domain.enums;

public enum TrustForReview {
    CREDIBLE,
    UNTRUSTWORTHY
}