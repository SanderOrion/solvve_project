package com.sanderorion.cinema.server.domain;

import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class UserRole extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private UserRoleType userRoleType;

    @ManyToMany(mappedBy = "userRoles")
    private List<RegUser> regUsers = new ArrayList<>();
}