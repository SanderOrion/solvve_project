package com.sanderorion.cinema.server.domain.enums;

public enum Gender {
    UNDEFINED(0),
    FEMALE(1),
    MALE(2),
    NON_BINARY(3);

    private final Integer position;

    Gender(int position) {
        this.position = position;
    }

    public static Gender getGender(Integer position) {
        for (Gender gender : Gender.values()) {
            if (position.equals(gender.position)) {
                return gender;
            }
        }
        return null;
    }
}