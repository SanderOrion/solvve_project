package com.sanderorion.cinema.server.dto.actorrole;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ActorRoleCreateDTO {

    @NotNull
    private Integer internalId;
    private Gender gender;

    @NotNull
    private String roleName;

    @NotNull
    private String roleInfo;
    private UUID moviePersonId;
}