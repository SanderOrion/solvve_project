package com.sanderorion.cinema.server.dto.movie;

import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class MovieCreateDTO {

    @NotNull
    private String originalTitle;

    @NotNull
    private String title;
    private String imdbId;
    private String tagline;
    private String overview;

    @NotNull
    private LocalDate releaseDate;

    @NotNull
    private MovieRelease status;
    private String country;
    private Integer duration;

    @NotNull
    private String language;

    @NotNull
    private Long budget;

    @NotNull
    private Long revenue;
}