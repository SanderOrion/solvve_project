package com.sanderorion.cinema.server.dto.genre;

import com.sanderorion.cinema.server.domain.enums.MovieGenre;
import lombok.Data;

import java.util.UUID;

@Data
public class GenreReadDTO {
    private UUID id;
    private MovieGenre movieGenre;
}