package com.sanderorion.cinema.server.dto.adminuser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class AdminUserReadDTO {
    private UUID id;
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
    private Instant createdAt;
    private Instant updatedAt;
}