package com.sanderorion.cinema.server.dto.likereviewmovie;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class LikeReviewMovieReadDTO {
    private UUID id;
    private Boolean userLike;
    private UUID regUserId;
    private UUID reviewMovieId;
    private Instant createdAt;
    private Instant updatedAt;
}