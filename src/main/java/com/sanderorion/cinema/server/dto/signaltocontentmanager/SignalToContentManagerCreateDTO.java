package com.sanderorion.cinema.server.dto.signaltocontentmanager;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SignalToContentManagerCreateDTO {

    @NotNull
    private String typo;

    private String userSuggestion;
}