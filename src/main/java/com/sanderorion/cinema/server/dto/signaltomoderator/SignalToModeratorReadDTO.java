package com.sanderorion.cinema.server.dto.signaltomoderator;

import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import com.sanderorion.cinema.server.domain.enums.SignalType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class SignalToModeratorReadDTO {
    private UUID id;
    private String typo;
    private Instant correctionTime;
    private SignalStatus signalStatus;
    private SignalType signalType;
    private String correctText;
    private UUID regUserId;
    private UUID reviewActorRoleId;
    private UUID reviewMovieId;
    private UUID moderatorUserId;
    private Instant createdAt;
    private Instant updatedAt;
}