package com.sanderorion.cinema.server.dto.userrole;

import com.sanderorion.cinema.server.domain.enums.UserRoleType;
import lombok.Data;

import java.util.UUID;

@Data
public class UserRoleReadDTO {
    private UUID id;
    private UserRoleType userRoleType;
}