package com.sanderorion.cinema.server.dto.contentmanageruser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

@Data
public class ContentManagerUserPutDTO {
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
}