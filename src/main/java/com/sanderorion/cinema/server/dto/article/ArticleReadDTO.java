package com.sanderorion.cinema.server.dto.article;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ArticleReadDTO {
    private UUID id;
    private String title;
    private String body;
    private Instant dateArticle;
    private Integer amountLike;
    private Integer amountDislike;
    private Instant createdAt;
    private Instant updatedAt;
}