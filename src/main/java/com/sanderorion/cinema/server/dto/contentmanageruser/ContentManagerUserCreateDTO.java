package com.sanderorion.cinema.server.dto.contentmanageruser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ContentManagerUserCreateDTO {

    @NotNull
    private String name;

    @NotNull
    private String login;

    @NotNull
    private String email;

    @NotNull
    private String encodedPassword;

    @NotNull
    private Gender gender;
}