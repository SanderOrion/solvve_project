package com.sanderorion.cinema.server.dto.markactorrole;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MarkActorRoleReadDTO {
    private UUID id;
    private Integer mark;
    private UUID regUserId;
    private UUID actorRoleId;
    private Instant createdAt;
}