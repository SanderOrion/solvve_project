package com.sanderorion.cinema.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieInLeaderBoardReadDTO {
    private UUID id;
    private String title;
    private Double avgRating;
    private long marksCount;
}
