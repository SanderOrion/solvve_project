package com.sanderorion.cinema.server.dto.signaltocontentmanager;

import lombok.Data;

@Data
public class SignalToContentManagerPutDTO {
    private String correctText;
}