package com.sanderorion.cinema.server.dto.signaltomoderator;

import com.sanderorion.cinema.server.domain.enums.SignalType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SignalToModeratorCreateDTO {

    @NotNull
    private String typo;

    @NotNull
    private SignalType signalType;
}