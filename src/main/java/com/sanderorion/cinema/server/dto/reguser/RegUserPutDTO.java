package com.sanderorion.cinema.server.dto.reguser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

@Data
public class RegUserPutDTO {
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
}