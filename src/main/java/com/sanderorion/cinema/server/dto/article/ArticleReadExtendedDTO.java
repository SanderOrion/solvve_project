package com.sanderorion.cinema.server.dto.article;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ArticleReadExtendedDTO {
    private UUID id;
    private String title;
    private String body;
    private Instant createdAt;
    private Instant updatedAt;
}