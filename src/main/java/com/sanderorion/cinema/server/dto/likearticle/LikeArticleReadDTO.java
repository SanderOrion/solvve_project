package com.sanderorion.cinema.server.dto.likearticle;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class LikeArticleReadDTO {
    private UUID id;
    private Boolean userLike;
    private UUID regUserId;
    private UUID articleId;
    private Instant createdAt;
    private Instant updatedAt;
}