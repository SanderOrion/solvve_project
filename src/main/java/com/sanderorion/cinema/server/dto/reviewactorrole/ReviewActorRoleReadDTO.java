package com.sanderorion.cinema.server.dto.reviewactorrole;

import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ReviewActorRoleReadDTO {
    private UUID id;
    private String body;
    private Integer amountLike;
    private Integer amountDislike;
    private ReviewConfirmStatus confirmStatus;
    private UUID regUserId;
    private UUID actorRoleId;
    private UUID moderatorId;
    private Instant createdAt;
    private Instant updatedAt;
}