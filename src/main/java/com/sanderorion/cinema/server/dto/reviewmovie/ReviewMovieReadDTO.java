package com.sanderorion.cinema.server.dto.reviewmovie;

import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ReviewMovieReadDTO {
    private UUID id;
    private String body;
    private Integer amountLike;
    private Integer amountDislike;
    private ReviewConfirmStatus confirmStatus;
    private UUID regUserId;
    private UUID movieId;
    private UUID moderatorId;
    private Instant createdAt;
    private Instant updatedAt;
}