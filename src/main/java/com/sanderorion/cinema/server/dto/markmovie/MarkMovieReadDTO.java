package com.sanderorion.cinema.server.dto.markmovie;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MarkMovieReadDTO {
    private UUID id;
    private Integer mark;
    private UUID regUserId;
    private UUID movieId;
    private Instant createdAt;
}