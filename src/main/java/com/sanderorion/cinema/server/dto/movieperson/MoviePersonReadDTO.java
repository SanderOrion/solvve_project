package com.sanderorion.cinema.server.dto.movieperson;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class MoviePersonReadDTO {
    private UUID id;
    private String name;
    private LocalDate birthday;
    private LocalDate deathDay;
    private String profession;
    private Integer internalId;
    private String biography;
    private Gender gender;
    private String placeOfBirth;
    private Double avgMovieRating;
    private Double avgRoleRating;
    private Instant createdAt;
    private Instant updatedAt;
}