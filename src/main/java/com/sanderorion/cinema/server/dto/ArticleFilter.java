package com.sanderorion.cinema.server.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
public class ArticleFilter {
    private UUID movieId;
    private UUID moviePersonId;
    private Set<String> title;
    private Set<String> body;
    private Instant dateArticle;
}