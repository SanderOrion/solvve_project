package com.sanderorion.cinema.server.dto.signaltomoderator;

import lombok.Data;

@Data
public class SignalToModeratorPutDTO {
    private String correctText;
}