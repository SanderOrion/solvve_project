package com.sanderorion.cinema.server.dto.reguser;

import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import lombok.Data;

@Data
public class RegUserPutTrustDTO {
    private TrustForReview trustForReview;
}