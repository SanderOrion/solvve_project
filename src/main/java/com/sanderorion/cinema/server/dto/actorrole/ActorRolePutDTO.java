package com.sanderorion.cinema.server.dto.actorrole;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import java.util.UUID;

@Data
public class ActorRolePutDTO {
    private Integer internalId;
    private Gender gender;
    private String roleName;
    private String roleInfo;
    private UUID moviePersonId;
}