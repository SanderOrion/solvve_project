package com.sanderorion.cinema.server.dto.markmovie;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MarkMovieCreateDTO {

    @NotNull
    private Integer mark;
}