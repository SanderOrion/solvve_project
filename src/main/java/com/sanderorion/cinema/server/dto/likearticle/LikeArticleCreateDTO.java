package com.sanderorion.cinema.server.dto.likearticle;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LikeArticleCreateDTO {

    @NotNull
    private Boolean userLike;
}