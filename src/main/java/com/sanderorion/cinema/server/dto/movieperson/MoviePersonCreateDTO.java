package com.sanderorion.cinema.server.dto.movieperson;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class MoviePersonCreateDTO {

    @NotNull
    private String name;
    private LocalDate birthday;
    private LocalDate deathDay;

    @NotNull
    private String profession;

    @NotNull
    private Integer internalId;

    @NotNull
    private String biography;
    private Gender gender;
    private String placeOfBirth;
}