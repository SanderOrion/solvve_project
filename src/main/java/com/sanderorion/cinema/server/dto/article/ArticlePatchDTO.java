package com.sanderorion.cinema.server.dto.article;

import lombok.Data;

@Data
public class ArticlePatchDTO {
    private String title;
    private String body;
}