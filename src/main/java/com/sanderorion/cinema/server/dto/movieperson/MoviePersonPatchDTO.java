package com.sanderorion.cinema.server.dto.movieperson;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MoviePersonPatchDTO {
    private String name;
    private LocalDate birthday;
    private LocalDate deathDay;
    private String profession;
    private Integer internalId;
    private String biography;
    private Gender gender;
    private String placeOfBirth;
}