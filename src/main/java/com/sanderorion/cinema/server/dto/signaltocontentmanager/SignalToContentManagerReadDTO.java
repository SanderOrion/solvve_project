package com.sanderorion.cinema.server.dto.signaltocontentmanager;

import com.sanderorion.cinema.server.domain.enums.SignalStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class SignalToContentManagerReadDTO {
    private UUID id;
    private SignalStatus signalStatus;
    private String typo;
    private String userSuggestion;
    private String correctText;
    private Instant correctionTime;
    private UUID regUserId;
    private UUID articleId;
    private UUID contentManagerUserId;
    private Instant createdAt;
    private Instant updatedAt;
}