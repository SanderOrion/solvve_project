package com.sanderorion.cinema.server.dto.reviewactorrole;

import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import lombok.Data;

@Data
public class ReviewActorRolePutDTO {
    private ReviewConfirmStatus confirmStatus;
}