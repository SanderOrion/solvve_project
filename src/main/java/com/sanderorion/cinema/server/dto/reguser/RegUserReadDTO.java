package com.sanderorion.cinema.server.dto.reguser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.TrustForReview;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RegUserReadDTO {
    private UUID id;
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
    private TrustForReview trustForReview;
    private Double userRatingReviews;
    private Double userRatingActivity;
    private Instant createdAt;
    private Instant updatedAt;
}