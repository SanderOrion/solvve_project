package com.sanderorion.cinema.server.dto.likereviewactorrole;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class LikeReviewActorRoleReadDTO {
    private UUID id;
    private Boolean userLike;
    private UUID regUserId;
    private UUID reviewActorRoleId;
    private Instant createdAt;
    private Instant updatedAt;
}