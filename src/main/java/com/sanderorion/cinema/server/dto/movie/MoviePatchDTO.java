package com.sanderorion.cinema.server.dto.movie;

import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MoviePatchDTO {
    private String originalTitle;
    private String title;
    private String imdbId;
    private String tagline;
    private String overview;
    private LocalDate releaseDate;
    private MovieRelease status;
    private String country;
    private Integer duration;
    private String language;
    private Long budget;
    private Long revenue;
}