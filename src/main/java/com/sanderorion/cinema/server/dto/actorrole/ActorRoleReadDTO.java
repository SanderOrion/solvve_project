package com.sanderorion.cinema.server.dto.actorrole;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorRoleReadDTO {
    private UUID id;
    private Integer internalId;
    private Gender gender;
    private String roleName;
    private String roleInfo;
    private Double avgRating;
    private Integer ratingCount;
    private UUID movieId;
    private UUID moviePersonId;
    private Instant createdAt;
    private Instant updatedAt;
}