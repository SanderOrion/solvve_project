package com.sanderorion.cinema.server.dto.markactorrole;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MarkActorRoleCreateDTO {

    @NotNull
    private Integer mark;
}