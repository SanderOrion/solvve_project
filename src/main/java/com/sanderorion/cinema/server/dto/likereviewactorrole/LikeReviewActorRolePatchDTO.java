package com.sanderorion.cinema.server.dto.likereviewactorrole;

import lombok.Data;

@Data
public class LikeReviewActorRolePatchDTO {
    private Boolean userLike;
}