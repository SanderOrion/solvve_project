package com.sanderorion.cinema.server.dto.article;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class ArticleCreateDTO {

    @NotNull
    private String title;

    @NotNull
    private String body;

    @NotNull
    private Instant dateArticle;
}