package com.sanderorion.cinema.server.dto.likereviewactorrole;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LikeReviewActorRoleCreateDTO {

    @NotNull
    private Boolean userLike;
}