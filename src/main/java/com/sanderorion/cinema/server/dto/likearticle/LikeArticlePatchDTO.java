package com.sanderorion.cinema.server.dto.likearticle;

import lombok.Data;

@Data
public class LikeArticlePatchDTO {
    private Boolean userLike;
}