package com.sanderorion.cinema.server.dto.likereviewmovie;

import lombok.Data;

@Data
public class LikeReviewMoviePatchDTO {
    private Boolean userLike;
}