package com.sanderorion.cinema.server.dto.likereviewactorrole;

import lombok.Data;

@Data
public class LikeReviewActorRolePutDTO {
    private Boolean userLike;
}