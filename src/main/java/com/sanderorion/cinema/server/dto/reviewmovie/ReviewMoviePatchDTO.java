package com.sanderorion.cinema.server.dto.reviewmovie;

import lombok.Data;

@Data
public class ReviewMoviePatchDTO {
    private String body;
}