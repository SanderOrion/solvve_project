package com.sanderorion.cinema.server.dto.adminuser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

@Data
public class AdminUserPutDTO {
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
}