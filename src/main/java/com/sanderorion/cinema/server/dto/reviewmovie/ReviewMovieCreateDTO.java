package com.sanderorion.cinema.server.dto.reviewmovie;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReviewMovieCreateDTO {

    @NotNull
    private String body;
}