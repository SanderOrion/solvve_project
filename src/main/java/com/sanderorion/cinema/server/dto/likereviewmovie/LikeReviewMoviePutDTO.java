package com.sanderorion.cinema.server.dto.likereviewmovie;

import lombok.Data;

@Data
public class LikeReviewMoviePutDTO {
    private Boolean userLike;
}