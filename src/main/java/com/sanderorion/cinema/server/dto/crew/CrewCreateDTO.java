package com.sanderorion.cinema.server.dto.crew;

import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.Profession;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class CrewCreateDTO {

    @NotNull
    private Integer internalId;

    @NotNull
    private String crewName;
    private Gender gender;

    @NotNull
    private Profession profession;

    @NotNull
    private String description;
    private UUID moviePersonId;
}