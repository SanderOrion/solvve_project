package com.sanderorion.cinema.server.dto.reviewactorrole;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReviewActorRoleCreateDTO {

    @NotNull
    private String body;
}