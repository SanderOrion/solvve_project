package com.sanderorion.cinema.server.dto.movie;

import com.sanderorion.cinema.server.domain.enums.MovieRelease;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class MovieReadDTO {
    private UUID id;
    private String originalTitle;
    private String title;
    private String imdbId;
    private String tagline;
    private String overview;
    private LocalDate releaseDate;
    private MovieRelease status;
    private String country;
    private Integer duration;
    private String language;
    private Double avgRating;
    private Integer ratingCount;
    private Long budget;
    private Long revenue;
    private Instant createdAt;
    private Instant updatedAt;
}