package com.sanderorion.cinema.server.dto.likereviewmovie;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LikeReviewMovieCreateDTO {

    @NotNull
    private Boolean userLike;
}