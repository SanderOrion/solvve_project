package com.sanderorion.cinema.server.dto.reviewactorrole;

import lombok.Data;

@Data
public class ReviewActorRolePatchDTO {
    private String body;
}