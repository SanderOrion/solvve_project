package com.sanderorion.cinema.server.dto.crew;

import com.sanderorion.cinema.server.domain.enums.Gender;
import com.sanderorion.cinema.server.domain.enums.Profession;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class CrewReadDTO {
    private UUID id;
    private Integer internalId;
    private String crewName;
    private Gender gender;
    private Profession profession;
    private String description;
    private UUID movieId;
    private UUID moviePersonId;
    private Instant createdAt;
    private Instant updatedAt;
}