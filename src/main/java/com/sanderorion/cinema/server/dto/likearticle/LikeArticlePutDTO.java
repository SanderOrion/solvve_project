package com.sanderorion.cinema.server.dto.likearticle;

import lombok.Data;

@Data
public class LikeArticlePutDTO {
    private Boolean userLike;
}