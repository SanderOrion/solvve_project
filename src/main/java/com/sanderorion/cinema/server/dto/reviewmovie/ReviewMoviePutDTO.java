package com.sanderorion.cinema.server.dto.reviewmovie;

import com.sanderorion.cinema.server.domain.enums.ReviewConfirmStatus;
import lombok.Data;

@Data
public class ReviewMoviePutDTO {
    private ReviewConfirmStatus confirmStatus;
}