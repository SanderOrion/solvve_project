package com.sanderorion.cinema.server.dto.reguser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegUserCreateDTO {

    @NotNull
    private String name;

    @NotNull
    private String login;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @NotNull
    private Gender gender;
}