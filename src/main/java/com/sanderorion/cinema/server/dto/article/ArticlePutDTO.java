package com.sanderorion.cinema.server.dto.article;

import lombok.Data;

@Data
public class ArticlePutDTO {
    private String title;
    private String body;
}