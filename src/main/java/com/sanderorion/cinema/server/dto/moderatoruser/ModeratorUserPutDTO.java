package com.sanderorion.cinema.server.dto.moderatoruser;

import com.sanderorion.cinema.server.domain.enums.Gender;
import lombok.Data;

@Data
public class ModeratorUserPutDTO {
    private String name;
    private String login;
    private String email;
    private String encodedPassword;
    private Gender gender;
}